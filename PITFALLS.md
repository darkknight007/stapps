# Pitfalls

This file is used to document problems, that occurred during development and how to fix them.

## Ionic Framework

### Build platform Android

#### Problem

After calling `ionic cordova build android` the gradle version is not set correctly (4.1 instead of 4.6)

#### Solution

* Go to folder `APP_FOLDER/platforms/android/cordova/lib/builders/`
* Open file `GradleBuilder.js` and change gradle version to `gradle-4.6-all.zip` 
in line `var distributionUrl = process.env['CORDOVA_ANDROID_GRADLE_DISTRIBUTION_URL'] || 
'https\\://services.gradle.org/distributions/gradle-4.1-all.zip';`
* Repeat this for file `StudioBuilder.js`

### Run platform iOS
#### Problem

Currently the iOS project build with cordova uses the old build system. 
The command `ionic cordova run ios` runs into the error `/platforms/ios/build/emulator/StApps.app/Info.plist 
file not found.`

#### Solution 

* Either use the command: `ionic cordova emulate ios -- --buildFlag="-UseModernBuildSystem=0"`
* Or open the iOS project in Xcode and change build system in workspace settings to `Lagacy Build System`. Then the normal run command works also.

### Set base image
FROM ubuntu:20.04

LABEL version="2.0.0" \
  description="Build environment for the StApps app." \
  maintainer="Jovan Krunić <krunic@uni-frankfurt.de>"

### Configure versions to install
ENV	ANDROID_APIS="android-30" \
  ANDROID_BUILD_TOOLS_VERSION="30.0.2" \
  NPM_VERSION="latest" \
  IONIC_VERSION="^6.0.0" \
  CORDOVA_VERSION="^9.0.0" \
  ### Configure download URLs
  ANDROID_SDK_TOOLS_DOWNLOAD_URL="https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip" \
  GOOGLE_SIGNING_KEY_URL="https://dl-ssl.google.com/linux/linux_signing_key.pub" \
  GOOGLE_CHROME_REPOSITORY_URL="http://dl.google.com/linux/chrome/deb/" \
  ### Android SDK path
  ANDROID_SDK_ROOT="/opt/android-sdk" \
  ### Installation files
  SCRIPTS_DIRECTORY="scripts" \
  NODE_SETUP_SCRIPT="node_setup.sh"

### Set $PATH
#ENV PATH=$ANDROID_SDK_ROOT/cmdline-tools/:$ANDROID_SDK_ROOT/cmdline-tools/bin:$ANDROID_SDK_ROOT/platform-tools:$ANDROID_SDK_ROOT/build-tools/$ANDROID_BUILD_TOOLS_VERSION:$PATH
ENV PATH=$ANDROID_SDK_ROOT/cmdline-tools/latest/bin:$ANDROID_SDK_ROOT/cmdline-tools/tools/bin:$ANDROID_SDK_ROOT/platform-tools:$ANDROID_SDK_ROOT/build-tools/$ANDROID_BUILD_TOOLS_VERSION:$PATH

### Replace shell with bash
RUN	rm /bin/sh && ln -s /bin/bash /bin/sh && \
  ### Set debconf to run non-interactively
  echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

### Install JAVA 8
RUN apt-get update && \
  apt-get install --no-install-recommends -y openjdk-8-jdk

### Install locales and base dependencies
RUN	apt-get update && \
  apt-get install -y --no-install-recommends \
  locales \
  apt-transport-https \
  build-essential \
  ca-certificates \
  curl \
  libssl-dev \
  git \
  gradle \
  ca-certificates-java \
  python \
  software-properties-common \
  ssh \
  unzip \
  wget \
  gpg-agent

### Setup the locale
RUN sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
  locale-gen en_US.UTF-8
ENV LANG=en_US.UTF-8 \
  LANGUAGE=en_US \
  LC_ALL=en_US.UTF-8
RUN dpkg-reconfigure --frontend noninteractive locales

### add chrome repository
RUN wget -q -O - $GOOGLE_SIGNING_KEY_URL | apt-key add -
RUN echo "deb $GOOGLE_CHROME_REPOSITORY_URL stable main" >> /etc/apt/sources.list.d/google.list

### Install Chrome
RUN apt-get update -y && \
  apt-get install -y --no-install-recommends \
  ### Install chrome and virtual frame buffer
  google-chrome-stable xvfb && \
  ### Clear apt cache
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

### Workaround to fix cacerts problem (Ubuntu):
### https://stackoverflow.com/questions/6784463/error-trustanchors-parameter-must-be-non-empty
RUN rm /etc/ssl/certs/java/cacerts && \
  update-ca-certificates -f

### Install android
RUN	curl $ANDROID_SDK_TOOLS_DOWNLOAD_URL > /tmp/android-sdk.zip && \
  unzip /tmp/android-sdk.zip && \
  mkdir -p $ANDROID_SDK_ROOT/cmdline-tools && \
  mv cmdline-tools $ANDROID_SDK_ROOT/cmdline-tools/tools && \
  ### Add licences (for "auto-accept licenses")
  yes | sdkmanager --licenses && \
  ### Install platform tools
  sdkmanager "platforms;$ANDROID_APIS" "build-tools;$ANDROID_BUILD_TOOLS_VERSION"

### Set working directory
WORKDIR /app

### Add contents to working directory
ADD . /app

### Copy scripts directory into the tmp folder, so it's available to the following commands
COPY $SCRIPTS_DIRECTORY/$NODE_SETUP_SCRIPT /tmp/

RUN	bash /tmp/$NODE_SETUP_SCRIPT && apt-get install -y nodejs && \
  ### Update npm to latest version
  npm install -g npm@$NPM_VERSION && \
  ### Install needed npm packages
  npm install -g @ionic/cli@$IONIC_VERSION cordova@$CORDOVA_VERSION

### Create, build, delete an empty cordova project to download necessary maven files and keep them in image
RUN	cordova create tmp-project && \
  cd tmp-project && \
  cordova platform add android && \
  cordova build && \
  cd .. && \
  rm -rf  tmp-project

CMD [""]

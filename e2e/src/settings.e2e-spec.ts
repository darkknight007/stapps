/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StAppsApp} from './app.po';

describe('Settings', () => {
  let page: StAppsApp;

  beforeEach(() => {
    page = new StAppsApp();
  });

  describe('settings-page', () => {
    beforeEach(() => {
      page.navigateTo('#/settings');
    });

    it('should have a title saying Settings', () => {
      expect(page.getPageTitle()).toBe('Settings');
    });
  });
});

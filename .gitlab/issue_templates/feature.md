## Description

(Describe the feature that you're requesting concisely)


## Explanation

(Explain why the feature is necessary)


## Mockups/Screenshots

(If possible, provide mockups or screenshots, which demonstrate the feature)


## Dependencies, issues to be resolved beforehand

(List issues or dependencies that need to be resolved before this feature can be implemented)

/label ~feature ~meeting

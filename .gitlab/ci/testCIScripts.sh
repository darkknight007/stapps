#!/usr/bin/env sh

# test all CI scripts

SCRIPT_DIR=$(dirname "$0")

TAG_VERSION=$(sh $SCRIPT_DIR/getRegistryTag.sh "v1.0.0")
TAG_TEST=$(sh $SCRIPT_DIR/getRegistryTag.sh "TEST")
BRANCH_NAME=$(sh $SCRIPT_DIR/getRegistryBranch.sh "very:first:test")

# Leaving out pushAsLatestVersion.sh as its controll flow
# is based on the same condition as getRegistryTag.sh

if [ $TAG_VERSION != "1.0.0" ]; then
	echo "ERROR in CI SCRIPT: $SCRIPT_DIR/getRegistryTag.sh"
	return 1
fi

if [ $TAG_TEST != "TEST" ]; then
	echo "ERROR in CI SCRIPT: $SCRIPT_DIR/getRegistryTag.sh"
	return 2
fi

if [ $BRANCH_NAME != "test" ]; then
	echo "ERROR in CI SCRIPT: $SCRIPT_DIR/getRegistryBranch.sh"
	return 3
fi

return 0

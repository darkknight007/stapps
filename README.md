# @openstapps/app

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/app.svg?style=flat-square)](https://gitlab.com/openstapps/app/commits/master)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://openstapps.gitlab.io/app)

This is a hybrid mobile app which is built using [Ionic](https://github.com/ionic-team/ionic) and Apache Cordova Framework.

### Why not refactoring legacy app?

The StApps 1.x.x (legacy app, but current app in stores) is written using Ionic 1 framework (AngularJS). For StApps 2.x.x project (this repository) we want to use latest version of Ionic (Ionic 2+ which uses Angular 2+; at the time of writing of the documentation current versions are: Ionic 4, Angular 6), which introduces significant changes. That said, simple refactoring of the app 1.x.x was not viable solution and this project was created with goal of coding of the existing and new features, defined by in new requirements (details available in internal documents).

## How to quickly start running the app?

There are (`npm`) scripts defined to get the app running as quickly as possible. Those scripts (shortcuts for docker commands) are called using the syntax `npm run + <script-name>`. So we have the following commands available:

```
npm run docker:fresh
```
which pulls the fresh image ([Dockerfile](Dockerfile)) which contains all the tools needed for building, serving and deploying the app.

```
npm run docker:enter
```
which enters the container on docker builder image, where we can run `npm install` (to install the required npm packages) and `npm build` (to build the app: convert into executable files), but also any other arbitrary commands with the tools available in the docker image.

```
npm run docker:build
```
which runs `npm install` (to install the required npm packages) and `npm build` (to build the app: convert into executable files) in the docker container which runs on the docker builder image.

```
npm run docker:serve
```
which serves the app for running it in the browser. It basically runs `ionic serve` in the docker container (on the docker builder image).

## How to build and start the app using the default backend?

```
npm run build
npm run start
```
will build and serve the app using the configuration for a default backend.

## How to use the fake backend?

```
npm run start:fake
```

will build and serve the app using the configuration for the fake-backend (interesting in development with fake backend using HTTP interceptors).

## Further explanation of npm scripts

All the npm scripts are defined in `package.json` [file](package.json). It is recommended to open the file and check what these scripts exactly do.

## Most useful commands

### Running the app

Install the npm packages needed for running the app (as for any other node project which uses npm):

```
npm install
```

Build the app (transpile etc.):

```
npm run build
```

Open the app in the browser:
```
ionic serve
```

### Executing tests
Execute unit tests:

```
npm test
```
Execute e2e tests:
```
npm run e2e
```
As mentioned, we can always check the [package.json](package.json) for details on each npm script/command.

## Using Gitlab CI as a reference

As we use GitLab CI for building the app, running tests and deployment of the app, we can always refer to [.gitlab-ci.yml](.gitlab-ci.yml), file which shows us which commands (`script` part) should be run for each phase of the development process. We can use these commands to reproduce the same thing on our local computers in a docker container (as we can use the same docker image GitLab CI is using).

/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, TestBed} from '@angular/core/testing';

import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Platform} from '@ionic/angular';

import {TranslateService} from '@ngx-translate/core';
import {AppComponent} from './app.component';
import {ConfigProvider} from './modules/config/config.provider';
import {SettingsProvider} from './modules/settings/settings.provider';
import {NGXLogger} from "ngx-logger";

describe('AppComponent', () => {

  let statusBarSpy: jasmine.SpyObj<StatusBar>;
  let splashScreenSpy: jasmine.SpyObj<SplashScreen>;
  let platformReadySpy: any;
  let platformSpy: jasmine.SpyObj<Platform>;
  let translateServiceSpy: jasmine.SpyObj<TranslateService>;
  let settingsProvider: jasmine.SpyObj<SettingsProvider>;
  let configProvider: jasmine.SpyObj<ConfigProvider>;
  let ngxLogger: jasmine.SpyObj<NGXLogger>;

  beforeEach(async(() => {
    statusBarSpy = jasmine.createSpyObj('StatusBar', ['styleDefault']);
    splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });
    translateServiceSpy = jasmine.createSpyObj('TranslateService', ['setDefaultLang', 'use']);
    settingsProvider = jasmine.createSpyObj('SettingsProvider',
      ['getSettingValue', 'provideSetting', 'setCategoriesOrder']);
    configProvider = jasmine.createSpyObj('ConfigProvider',
      ['init']);
    ngxLogger = jasmine.createSpyObj('NGXLogger',
      ['log', 'error', 'warn']);

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      providers: [
        {provide: StatusBar, useValue: statusBarSpy},
        {provide: SplashScreen, useValue: splashScreenSpy},
        {provide: Platform, useValue: platformSpy},
        {provide: TranslateService, useValue: translateServiceSpy},
        {provide: SettingsProvider, useValue: settingsProvider},
        {provide: ConfigProvider, useValue: configProvider},
        {provide: NGXLogger, useValue: ngxLogger},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {
    TestBed.createComponent(AppComponent);
    expect(platformSpy.ready).toHaveBeenCalled();
    await platformReadySpy;
    expect(statusBarSpy.styleDefault).toHaveBeenCalled();
    expect(splashScreenSpy.hide).toHaveBeenCalled();
  });

  // TODO: add more tests!

});

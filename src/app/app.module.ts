/*
 * Copyright (C) 2018-2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule, HashLocationStrategy, LocationStrategy, registerLocaleData} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import localeDe from '@angular/common/locales/de';
import {NgModule, Provider} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CatalogComponent} from './catalog/catalog.component';
import {FilterListComponent} from './catalog/CatalogContent/filter-list/filter-list.component';
import {DisplayDetailComponent} from './catalog/CatalogContent/display-detail/display-detail.component';
import {ConfigModule} from './modules/config/config.module';
import {DataModule} from './modules/data/data.module';
import {MenuModule} from './modules/menu/menu.module';
import {NewsModule} from './modules/news/news.module';
import {SettingsModule} from './modules/settings/settings.module';
import {StorageModule} from './modules/storage/storage.module';
import {fakeBackendProvider} from './_helpers/fake-backend.interceptor';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';




registerLocaleData(localeDe);

/**
 * TODO
 *
 * @param http TODO
 */
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const providers : Provider[] = [
  StatusBar,
  SplashScreen,
  {
    provide: RouteReuseStrategy,
    useClass: IonicRouteStrategy,
  },
  {
    provide: LocationStrategy,
    useClass: HashLocationStrategy,
  },
];

/**
 * TODO
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [AppComponent,
  CatalogComponent, FilterListComponent, DisplayDetailComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    ConfigModule,
    DataModule,
    IonicModule.forRoot(),
    MenuModule,
    NewsModule,
    SettingsModule,
    StorageModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    TranslateModule.forRoot({
      loader: {
        deps: [HttpClient],
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
      },
    }),
    // use maximal logging level when not in production, minimal (log only fatal errors) in production
    LoggerModule.forRoot({level: environment.production ? NgxLoggerLevel.FATAL : NgxLoggerLevel.TRACE}),
  ],
  providers:
    environment.use_fake_backend ? [providers, fakeBackendProvider] : providers,
})
export class AppModule {
}

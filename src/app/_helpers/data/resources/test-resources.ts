/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/* tslint:disable */

import moment from 'moment'

export const sampleResources = [{
  'errorNames': [],
  "instance": {
    'type': 'academic event',
    'description': 'Fortsetzung der Algebra I: Galoistheorie mit Anwendungen, ausgewählte Spezialthemen.',
    'uid': '681a59a1-23c2-5d78-861a-8c86a3abf2b9',
    'name': 'Algebra II',
    'categories': [
      'lecture'
    ],
    'academicTerms': [
      {
        'uid': 'aacd5611-b5be-54ce-b39f-c52f7e9a631d',
        'type': 'semester',
        'name': 'Sommersemester 2018',
        'acronym': 'SS 2018',
        'alternateNames': [
          'SoSe 2018'
        ],
        'startDate': '2018-04-01',
        'endDate': '2018-09-30',
        'eventsStartDate': '2018-04-09',
        'eventsEndDate': '2018-07-13',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'performers': [
      {
        'type': 'person',
        'uid': '7f8ce700-2679-51a5-86b5-3dfba85a33ff',
        'givenName': 'Peter',
        'familyName': 'Bürgisser',
        'gender': 'male',
        'honorificPrefix': 'Prof. Dr.',
        'name': 'Peter Bürgisser',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote',
        }
      }
    ],
    'majors': [
      'Mathematik D',
      'Mathematik L2',
      'Mathematik StRGym',
      'Mathematik StRBeruf',
      'Mathematik BSc',
      'Mathematik MSc'
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote',
      'maintainer': {
        'type': 'organization',
        'name': 'tubIT',
        'uid': '25f76840-db89-5da2-a8a2-75992f637613',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      },
      'modified': '2018-09-01T10:00:00Z',
      'originalId': 'foo bar',
      'responsibleEntity': {
        'type': 'person',
        'uid': '7f8ce700-2679-51a5-86b5-3dfba85a33ff',
        'givenName': 'Peter',
        'familyName': 'Bürgisser',
        'gender': 'male',
        'honorificPrefix': 'Prof. Dr.',
        'name': 'Peter Bürgisser',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    }
  },
  'schema': 'SCAcademicEvent'
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'academic event',
    'description': 'Grundlagen, algebraische Grundbegriffe, Vektorräume, lineare Abbildungen und Gleichungen, Determinanten',
    'uid': 'b17eb963-42b5-5861-adce-2b7b2607ef0a',
    'name': 'Lineare Algebra I für Mathematiker',
    'categories': [
      'lecture'
    ],
    'academicTerms': [
      {
        'uid': 'aacd5611-b5be-54ce-b39f-c52f7e9a631d',
        'type': 'semester',
        'name': 'Sommersemester 2018',
        'acronym': 'SS 2018',
        'alternateNames': [
          'SoSe 2018'
        ],
        'startDate': '2018-04-01',
        'endDate': '2018-09-30',
        'eventsStartDate': '2018-04-09',
        'eventsEndDate': '2018-07-13',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'performers': [
      {
        'type': 'person',
        'uid': 'fc8b10cf-10c0-5b92-b16e-92ff734676da',
        'givenName': 'Jörg',
        'familyName': 'Liesen',
        'gender': 'male',
        'honorificPrefix': 'Prof. Dr.',
        'name': 'Jörg Liesen',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'catalogs': [
      {
        'uid': '5a1f4f51-2498-5af1-91cb-c939673cc69c',
        'type': 'catalog',
        'level': 3,
        'description': 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
        'categories': [
          'university events'
        ],
        'name': 'Mathematik: Lehrveranstaltungen für andere Fachrichtungen (Service)',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'majors': [
      'Wirtschaftsmathematik BSc',
      'Technomathematik BSc',
      'Mathematik BSc'
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCAcademicEvent',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'academic event',
    'description': 'Die Übung hat 2 SWS und wird auf 2 Gruppen verteilt.',
    'uid': '7e2b64b0-925d-5f63-b464-a6e3e9492411',
    'name': 'Algebra II',
    'categories': [
      'tutorial'
    ],
    'academicTerms': [
      {
        'uid': 'aacd5611-b5be-54ce-b39f-c52f7e9a631d',
        'type': 'semester',
        'name': 'Sommersemester 2018',
        'acronym': 'SS 2018',
        'alternateNames': [
          'SoSe 2018'
        ],
        'startDate': '2018-04-01',
        'endDate': '2018-09-30',
        'eventsStartDate': '2018-04-09',
        'eventsEndDate': '2018-07-13',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'performers': [
      {
        'type': 'person',
        'uid': '97be73c0-5319-579a-a393-c4eeeacae58b',
        'givenName': 'Paul',
        'familyName': 'Breiding',
        'gender': 'male',
        'name': 'Paul Breiding',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      },
      {
        'type': 'person',
        'uid': '0db9b55a-4c27-5faf-9bf0-4b564be45a08',
        'givenName': 'Pierre',
        'familyName': 'Lairez',
        'gender': 'male',
        'name': 'Pierre Lairez',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'catalogs': [
      {
        'uid': '6c259ad8-99af-5ea2-8aae-a3c9027d26e2',
        'type': 'catalog',
        'level': 3,
        'categories': [
          'university events'
        ],
        'name': 'Mathematik: Grundstudiums-Veranstaltungen (Diplom, Bachelor)',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      },
      {
        'uid': '5a1f4f51-2498-5af1-91cb-c939673cc69c',
        'type': 'catalog',
        'level': 3,
        'description': 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
        'categories': [
          'university events'
        ],
        'name': 'Mathematik: Lehrveranstaltungen für andere Fachrichtungen (Service)',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'majors': [
      'Mathematik D',
      'Mathematik L2',
      'Mathematik StRGym',
      'Mathematik StRBeruf',
      'Mathematik BSc',
      'Mathematik MSc'
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCAcademicEvent',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '8d8bd89c-8429-5f81-b754-15a5be55e593',
    'type': 'article',
    'categories': [
      'unipedia'
    ],
    'url': 'https://www.mydesk.tu-berlin.de/wiki/abk%C3%BCrzungen',
    'name': 'Abkürzungen',
    'keywords': [
      'Abkürzungen',
      'Studium'
    ],
    'articleBody': 'Siehe [c.t.](#/b-tu/data/detail/Article/tub-unipedia-384edcfd026dab697ff9f8adda0d19a6959d4e29) (lat. cum tempore)\n\n### S\n\n**SWS** ist eine Semesterwochenstunde. Ein SWS beträgt 45 MInuten.',
    'translations': {
      'en': {
        'name': 'Abbreviations',
        'keywords': [
          'Abbreviations',
          'Studies'
        ]
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCArticle',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '4f772b29-0b28-53a4-a8b9-206d9b425962',
    'type': 'article',
    'categories': [
      'unipedia'
    ],
    'url': 'https://www.mydesk.tu-berlin.de/wiki/ag_ziethen',
    'name': 'AG Ziethen',
    'keywords': [
      'Bologna',
      'Change Agent',
      'Hochschulpolitik',
      'Lehre',
      'Manifest',
      'Mitarbeiten',
      'Mitmachen',
      'Mitwirken',
      'Prüfungen',
      'Reform',
      'Regelungen',
      'Studium',
      'Verändern'
    ],
    'articleBody': 'Die AG Ziethen ist seit Ende 2012 eine Arbeitsgruppe für alle Angehörigen (Studierende und Mitarbeiter_innen) der TUB. Alle Interessierten dürfen, können und sollen sehr gerne mitarbeiten um das Thema Lehre an der TU Berlin zu stärken. Ziel ist ein Kulturwandel und Perspektivwechsel und der Überschrift: "Shift from teaching to learning!". Die AG Ziethen gehört zur Programmlinie TU inspire, gefördert im Rahmen des Qualitätspakts Lehre des Bundesministeriums für Bildung und Forschung.\n\nDie Arbeitsgruppe ist auf einem ersten Treffen Anfang Dezember 2012 im Schloß Ziethen in Groß Ziethen (daher der Name) entstanden und hat einzelne Gruppen zu speziellen Themen gebildet (siehe [Maßnahmen und Initiaven](http://www.tu-berlin.de/qualitaet/ag_ziethen/massnahmen_und_initiativen/)). Wichtigstes Ergebnis ist aber das [Ziethener Manifest](http://www.tu-berlin.de/qualitaet/ag_ziethen/ziethener_manifest/), das einen Kulturwandel in der Lehre unterstützen soll.\n\nDas Mitwirken, Mitarbeiten, Mitmachen bei den einzelnen Themen ist unbedingt erwünscht!  \nEine Übersicht zu den aktuellen Gruppen gibt es unter anderem auf der zentralen [ISIS-Seite](https://www.isis.tu-berlin.de/2.0/course/view.php?id=1065) zur AG Ziethen.\n\n##### Links\n\n*   [AG Ziehten zur allgemeinen Information](http://www.tu-berlin.de/qualitaet/ag_ziethen/)\n*   [ISIS-Seite zum Mitmachen](https://www.isis.tu-berlin.de/2.0/course/view.php?id=1065)',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCArticle',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'book',
    'uid': 'a0520263-29ae-5357-a3ce-ba1902d121e0',
    'name': 'Kundenorientierung durch Quality Function Deployment: Systematisches Entwickeln von Produkten und Dienstleistungen',
    'authors': [
      {
        'type': 'person',
        'uid': '10dfe386-71b4-554a-beb1-2d38561e42f8',
        'name': 'Jutta Saatweber',
        'givenName': 'Jutta',
        'familyName': 'Saatweber',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'datePublished': '2007-08-01',
    'publishers': [
      {
        'type': 'organization',
        'uid': '28df2bb9-c854-5898-b9d5-1abbd3524804',
        'name': 'Symposion Publishing',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'inLanguages': [
      {
        'name': 'german',
        'code': 'de'
      }
    ],
    'bookEdition': '2., überarb. u. erw. Aufl.',
    'isbn': '3936608776',
    'numberOfPages': 537,
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCBook',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'book',
    'uid': 'db47f7f4-7699-5a37-afcc-24beaa998d36',
    'name': 'Minimal Book',
    'authors': [
      {
        'type': 'person',
        'uid': '10dfe386-71b4-554a-beb1-2d38561e42f8',
        'name': 'Jutta Saatweber',
        'givenName': 'Jutta',
        'familyName': 'Saatweber',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'datePublished': '2007-08-01',
    'isbn': '3936608776',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCBook',
}
,
{
  'errorNames': [],
  'instance': {
    'geo': {
      'point': {
        'type': 'Point',
        'coordinates': [
          13.32577,
          52.51398
        ]
      },
      'polygon': {
        'type': 'Polygon',
        'coordinates': [
          [
            [
              13.3259988,
              52.5141108
            ],
            [
              13.3259718,
              52.5143107
            ],
            [
              13.3262958,
              52.5143236
            ],
            [
              13.3263291,
              52.5143052
            ],
            [
              13.3263688,
              52.5140098
            ],
            [
              13.3264324,
              52.5139643
            ],
            [
              13.3264849,
              52.5139415
            ],
            [
              13.3265148,
              52.5139004
            ],
            [
              13.3265336,
              52.5138571
            ],
            [
              13.3265411,
              52.5137933
            ],
            [
              13.3265336,
              52.5137546
            ],
            [
              13.3264961,
              52.5137044
            ],
            [
              13.3264399,
              52.5136725
            ],
            [
              13.3263875,
              52.5136497
            ],
            [
              13.3263351,
              52.5136429
            ],
            [
              13.3263613,
              52.5134286
            ],
            [
              13.3262564,
              52.5133603
            ],
            [
              13.3260767,
              52.5133671
            ],
            [
              13.3259418,
              52.5134286
            ],
            [
              13.3258744,
              52.5135061
            ],
            [
              13.3258444,
              52.5135677
            ],
            [
              13.3261366,
              52.5135836
            ],
            [
              13.3261066,
              52.513807
            ],
            [
              13.3260579,
              52.5138047
            ],
            [
              13.3260317,
              52.5139096
            ],
            [
              13.3254137,
              52.5138708
            ],
            [
              13.3254287,
              52.5137819
            ],
            [
              13.3250879,
              52.513766
            ],
            [
              13.3250018,
              52.5142697
            ],
            [
              13.3253613,
              52.5142902
            ],
            [
              13.3253838,
              52.5140747
            ],
            [
              13.3259988,
              52.5141108
            ]
          ]
        ]
      }
    },
    'type': 'building',
    'name': 'Mathematikgebäude',
    'alternateNames': [
      'MA'
    ],
    'uid': 'edfaba58-254f-5da0-82d6-3b46a76c48ce',
    'categories': [
      'education'
    ],
    'address': {
      'addressCountry': 'Germany',
      'addressLocality': 'Berlin',
      'addressRegion': 'Berlin',
      'postalCode': '10623',
      'streetAddress': 'Straße des 17. Juni 136'
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCBuilding',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': 'c8dc1f7f-9e3e-5b1f-8c38-084f46413b87',
    'type': 'catalog',
    'level': 1,
    'categories': [
      'university events'
    ],
    'name': 'Lehrveranstaltungen des Fachbereichs 3 - Gesellschaftswissenschaften',
    'academicTerm': {
      'uid': 'b621f5b5-dd5d-5730-9e2e-e4ba52011388',
      'type': 'semester',
      'acronym': 'WS 2017/18',
      'name': 'Wintersemester 2017/2018',
      'alternateNames': [
        'WiSe 2017/18'
      ],
      'startDate': '2017-10-01',
      'endDate': '2018-03-31',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'superCatalog': {
      'type': 'catalog',
      'level': 0,
      'categories': [
        'university events'
      ],
      'uid': 'a7404d36-282d-546e-bfa5-6c7b25ba7838',
      'name': 'Vorlesungsverzeichnis WS 2017/18',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'superCatalogs': [
      {
        'type': 'catalog',
        'level': 0,
        'categories': [
          'university events'
        ],
        'uid': 'a7404d36-282d-546e-bfa5-6c7b25ba7838',
        'name': 'Vorlesungsverzeichnis WS 2017/18',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCCatalog',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '5a8bc725-8658-528f-b515-5f7cd6987169',
    'type': 'catalog',
    'level': 3,
    'description': 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
    'categories': [
      'university events'
    ],
    'name': 'Mathematik: Lehrveranstaltungen für andere Fachrichtungen (Service)',
    'academicTerm': {
      'uid': 'b621f5b5-dd5d-5730-9e2e-e4ba52011388',
      'type': 'semester',
      'acronym': 'WS 2017/18',
      'name': 'Wintersemester 2017/2018',
      'alternateNames': [
        'WiSe 2017/18'
      ],
      'startDate': '2017-10-01',
      'endDate': '2018-03-31',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'superCatalog': {
      'uid': '2fdcccce-1948-5f5a-8938-3711b7e65e8a',
      'type': 'catalog',
      'level': 2,
      'categories': [
        'university events'
      ],
      'name': 'Mathematik',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'superCatalogs': [
      {
        'type': 'catalog',
        'level': 0,
        'categories': [
          'university events'
        ],
        'uid': 'a7404d36-282d-546e-bfa5-6c7b25ba7838',
        'name': 'Vorlesungsverzeichnis WS 2017',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      },
      {
        'uid': '0718211b-d0c2-50fb-bc78-b968f20fd95b',
        'type': 'catalog',
        'level': 1,
        'categories': [
          'university events'
        ],
        'name': 'Fakultät II Mathematik und Naturwissenschaften',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      },
      {
        'uid': '2fdcccce-1948-5f5a-8938-3711b7e65e8a',
        'type': 'catalog',
        'level': 2,
        'categories': [
          'university events'
        ],
        'name': 'Mathematik',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCCatalog',
}
,
{
  'errorNames': [],
  'instance': {
    'academicDegree': 'bachelor',
    'academicDegreewithField': 'Bachelor of Arts',
    'academicDegreewithFieldShort': 'B.A.',
    'department': {
      'name': 'Technische Universität Berlin',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      },
      'type': 'organization',
      'uid': 'b0f878fd-8fda-53b8-b065-a8d854c3d0d2'
    },
    'mainLanguage': {
      'code': 'de',
      'name': 'german'
    },
    'major': 'Astroturfing',
    'mode': 'dual',
    'name': 'Astroturfing Bachelor',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    },
    'secretary': {
      'name': 'Technische Universität Berlin',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      },
      'type': 'organization',
      'uid': 'b0f878fd-8fda-53b8-b065-a8d854c3d0d2'
    },
    'timeMode': 'parttime',
    'type': 'course of studies',
    'uid': '4c6f0a18-343d-5175-9fb1-62d28545c2aa'
  },
  'schema': 'SCCourseOfStudies',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'date series',
    'uid': '1b421872-1b4c-579b-ba03-688f943d59ad',
    'name': 'Einführung in die Wirtschaftspolitik',
    'duration': 'PT2H',
    'inPlace': {
      'type': 'room',
      'categories': [
        'education'
      ],
      'uid': '5a4bbced-8e1f-5f29-a1d1-336e455ce7f9',
      'name': 'H 0105',
      'geo': {
        'point': {
          'type': 'Point',
          'coordinates': [
            13.32687,
            52.51211
          ]
        }
      },
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'frequency': 'once',
    'dates': [
      '2016-04-15T17:00:00+00:00'
    ],
    'event': {
      'type': 'academic event',
      'uid': 'dbb4e5e1-0789-59c1-9970-877430af56b3',
      'name': 'Einführung in die Wirtschaftspolitik',
      'categories': [
        'written exam'
      ],
      'majors': [
        'Economics BSc',
        'Wirtschaftsingenieurwesen BSc'
      ],
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCDateSeries',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'date series',
    'uid': '4ce41895-4b54-52db-b86f-7e6920b975c8',
    'name': 'Distributed Algorithms',
    'duration': 'PT4H',
    'inPlace': {
      'geo': {
        'point': {
          'type': 'Point',
          'coordinates': [
            13.3266207,
            52.5144409
          ]
        }
      },
      'type': 'room',
      'categories': [
        'education'
      ],
      'uid': 'b535c86a-777b-54c3-b89a-cad528d0580f',
      'name': 'EMH 225',
      'floor': '2',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'frequency': 'once',
    'dates': [
      '2016-04-12T11:00:00+00:00'
    ],
    'event': {
      'type': 'academic event',
      'uid': 'e6fb74d4-c6d9-59bb-930f-e47eb6e39432',
      'name': 'Distributed Algorithms',
      'categories': [
        'written exam'
      ],
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCDateSeries',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'date series',
    'uid': 'e6462830-187a-50b1-bdb4-8f39e49a88b8',
    'name': 'Dance course for beginners',
    'duration': 'PT8H',
    'inPlace': {
      'geo': {
        'point': {
          'type': 'Point',
          'coordinates': [
            13.3266207,
            52.5144409
          ]
        }
      },
      'type': 'room',
      'categories': [
        'student union'
      ],
      'uid': 'b535c86a-777b-54c3-b89a-cad528d0580f',
      'name': 'EMH 225',
      'floor': '2',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'frequency': 'once',
    'dates': [
      '2016-04-12T11:00:00+00:00'
    ],
    'event': {
      'type': 'academic event',
      'uid': '4f86e8bb-ce73-520b-bfd9-e1ba9f754391',
      'name': 'Dance course for beginners',
      'categories': [
        'special'
      ],
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    },
    'offers': [
      {
        'availability': 'in stock',
        'availabilityStarts': moment().startOf('day').add(2,'days').toISOString(),
        'availabilityEnds': moment().endOf('day').add(2,'days').toISOString(),
        'prices': {
          'default': 6.5,
          'student': 5,
          'alumni': 5
        },
        'provider': {
          'name': 'Studentenwerk',
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          },
          'type': 'organization',
          'uid': '3b9b3df6-3a7a-58cc-922f-c7335c002634'
        }
      }
    ]
  },
  'schema': 'SCDateSeries',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': 'a5acde0d-18c4-5511-9f86-aabf2a530f91',
    'dateCreated': '2017-02-07T09:26:35.957Z',
    'name': 'changed_testuid',
    'changes': [
      {
        'op': 'replace',
        'path': '/name',
        'value': 'Name Two'
      }
    ],
    'action': 'changed',
    'type': 'diff',
    'object': {
      'uid': '072db1e5-e479-5040-88e0-4a98d731e443',
      'name': 'Name One',
      'type': 'message',
      'message': 'Message',
      'audiences': [
        'students'
      ],
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCDiff',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': 'f71cc2c8-fef2-59ee-af0a-511cc75e7471',
    'dateCreated': '2017-03-07T09:26:35.957Z',
    'name': 'changed_testuid',
    'changes': [
      {
        'op': 'replace',
        'path': '/name',
        'value': 'bar'
      }
    ],
    'action': 'changed',
    'type': 'diff',
    'object': {
      'uid': '072db1e5-e479-5040-88e0-4a98d731e443',
      'name': 'Name One',
      'type': 'message',
      'message': 'Message',
      'audiences': [
        'students'
      ],
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCDiff',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'dish',
    'name': 'Pizza mit Geflügelsalami und Champignons',
    'categories': [
      'main dish'
    ],
    'characteristics': [],
    'additives': [
      'konserviert',
      'Antioxidationsmittel',
      'Farbstoff',
      'Weizen',
      'Milch(Laktose; Milcheiweiß)',
      'Nitritpökelsalz',
      'Hefe'
    ],
    'offers': [
      {
        'availability': 'in stock',
        'availabilityStarts': moment().startOf('day').toISOString(),
        'availabilityEnds': moment().endOf('day').add(2,'days').toISOString(),
        'prices': {
          'default': 4.85,
          'student': 2.85,
          'employee': 3.85,
          'guest': 4.85
        },
        'provider': {
          'name': 'Studentenwerk',
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          },
          'type': 'organization',
          'uid': 'b7206fb5-bd77-5572-928f-16aa70910f64'
        },
        'inPlace': {
          'geo': {
            'point': {
              'type': 'Point',
              'coordinates': [
                13.32612,
                52.50978
              ]
            }
          },
          'type': 'building',
          'categories': [
            'restaurant'
          ],
          'openingHours': 'Mo-Fr 11:00-14:30',
          'name': 'TU-Mensa',
          'alternateNames': [
            'MensaHardenberg'
          ],
          'uid': 'b7206fb5-bd77-5572-928f-16aa70910f64',
          'address': {
            'addressCountry': 'Germany',
            'addressLocality': 'Berlin',
            'addressRegion': 'Berlin',
            'postalCode': '10623',
            'streetAddress': 'Hardenbergstraße 34'
          },
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          }
        }
      }
    ],
    'uid': 'c9f32915-8ed5-5960-b850-3f7375a89922',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCDish',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'dish',
    'name': 'Sahne-Bärlauchsauce',
    'description': 'Nudelauswahl',
    'categories': [
      'main dish'
    ],
    'offers': [
      {
        'prices': {
          'default': 3.45,
          'student': 2.45,
          'employee': 3.45
        },
        'provider': {
          'name': 'Studentenwerk',
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          },
          'type': 'organization',
          'uid': '3b9b3df6-3a7a-58cc-922f-c7335c002634'
        },
        'availability': 'in stock',
        'availabilityStarts': moment().startOf('day').add(2,'days').toISOString(),
        'availabilityEnds': moment().endOf('day').add(2,'days').toISOString(),
        'inPlace': {
          'geo': {
            'point': {
              'type': 'Point',
              'coordinates': [
                13.32612,
                52.50978
              ]
            }
          },
          'type': 'building',
          'categories': [
            'restaurant'
          ],
          'openingHours': 'Mo-Fr 11:00-14:30',
          'name': 'TU-Mensa',
          'alternateNames': [
            'MensaHardenberg'
          ],
          'uid': '072db1e5-e479-5040-88e0-4a98d731e443',
          'address': {
            'addressCountry': 'Germany',
            'addressLocality': 'Berlin',
            'addressRegion': 'Berlin',
            'postalCode': '10623',
            'streetAddress': 'Hardenbergstraße 34'
          },
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          }
        }
      }
    ],
    'characteristics': [
      {
        'name': 'bad'
      },
      {
        'name': 'vegetarian',
        'image': 'https://backend/res/img/characteristic_small_vegetarian.png'
      }
    ],
    'additives': [
      'Weizen',
      'Milch(Laktose; Milcheiweiß)'
    ],
    'uid': '3222631f-82b3-5faf-a8e8-9c10719cc95b',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCDish',
}
,
{
  'errorNames': [],
  'instance': {
    'additives': [
      '1 = mit Farbstoff',
      '2 = konserviert',
      '3 = mit Antioxidationsmittel',
      '9 = mit Süßungsmittel',
      'A = Glutenhaltige Getreide',
      'G = Milch u. Milcherzeugnisse'
    ],
    'offers': [
      {
        'availabilityEnds': moment().endOf('day').toISOString(),
        'availabilityStarts': moment().startOf('day').toISOString(),
        'availability': 'in stock',
        'inPlace': {
          'type': 'room',
          'name': 'Cafeteria LEVEL',
          'categories': [
            'cafe'
          ],
          'uid': 'b7206fb5-bd77-5572-928f-16aa70910f64',
          'alternateNames': [
            'Cafeteria LEVEL'
          ],
          'openingHours': 'Mo-Fr 08:30-17:00',
          'geo': {
            'point': {
              'type': 'Point',
              'coordinates': [
                8.6285375,
                50.1743717
              ]
            }
          },
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          }
        },
        'prices': {
          'default': 6.5,
          'student': 4.9,
          'employee': 6.5
        },
        'provider': {
          'name': 'Studentenwerk',
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          },
          'type': 'organization',
          'uid': 'b7206fb5-bd77-5572-928f-16aa70910f64'
        }
      }
    ],
    'categories': [
      'main dish'
    ],
    'characteristics': [
      {
        'name': 'Rind',
        'image': 'https://backend/res/img/characteristic_small_rind.png'
      }
    ],
    'description': 'Salsa Burger (1,2,3,9,A,G)',
    'name': 'Salsa Burger',
    'dishAddOns': [
      {
        'characteristics': [
          {
            'name': 'Vegan',
            'image': 'https://backend/res/img/characteristic_small_vegan.png'
          }
        ],
        'description': 'Pommes frites',
        'type': 'dish',
        'uid': 'db0caac1-062c-5333-9fcb-cfaf0ff7d799',
        'nutrition': {
          'calories': 106,
          'fatContent': 5.4,
          'saturatedFatContent': 1.8,
          'carbohydrateContent': 6.8,
          'sugarContent': 6.1,
          'proteinContent': 6.9,
          'saltContent': 3.7
        },
        'additives': [
          '3 = mit Antioxidationsmittel',
          '5 = geschwefelt'
        ],
        'name': 'Pommes frites',
        'categories': [
          'side dish'
        ],
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      },
      {
        'characteristics': [
          {
            'name': 'Vegan',
            'image': 'https://backend/res/img/characteristic_small_vegan.png'
          }
        ],
        'description': 'Glasierte Karotten',
        'type': 'dish',
        'uid': 'f702fd43-1551-53b2-b35a-b5916e1cf9a1',
        'nutrition': {
          'calories': 106,
          'fatContent': 5.4,
          'saturatedFatContent': 1.8,
          'carbohydrateContent': 6.8,
          'sugarContent': 6.1,
          'proteinContent': 6.9,
          'saltContent': 3.7
        },
        'additives': [
          'F = Soja u. Sojaerzeugnisse'
        ],
        'name': 'Glasierte Karotten',
        'categories': [
          'side dish',
          'salad'
        ],
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'type': 'dish',
    'uid': '1c99689c-c6ec-551f-8ad8-f13c5fa812c2',
    'nutrition': {
      'calories': 600,
      'fatContent': 30.5,
      'saturatedFatContent': 9.9,
      'carbohydrateContent': 42.2,
      'sugarContent': 5.7,
      'proteinContent': 38.6,
      'saltContent': 3.5
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCDish',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'favorite',
    'name': 'Favorite #1',
    'uid': '3af3ccaa-f066-5eff-9a3d-a70567f3d70d',
    'data': {
      'type': 'academic event',
      'description': 'Grundlagen, algebraische Grundbegriffe, Vektorräume, lineare Abbildungen und Gleichungen, Determinanten',
      'uid': 'b17eb963-42b5-5861-adce-2b7b2607ef0a',
      'name': 'Lineare Algebra I für Mathematiker',
      'categories': [
        'lecture'
      ],
      'majors': [
        'Wirtschaftsmathematik BSc',
        'Technomathematik BSc',
        'Mathematik BSc'
      ],
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'created': '2018-09-11T12:30:00Z',
      'deleted': false,
      'type': 'user',
      'updated': '2018-12-11T12:30:00Z',
      'maintainer': {
        'type': 'organization',
        'name': 'tubIT',
        'uid': '25f76840-db89-5da2-a8a2-75992f637613',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      },
    }
  },
  'schema': 'SCFavorite',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '0effdc13-d4af-5a63-a538-c4b2c080e253',
    'name': 'MA E:0',
    'type': 'floor',
    'inPlace': {
      'geo': {
        'point': {
          'type': 'Point',
          'coordinates': [
            13.32577,
            52.51398
          ]
        },
        'polygon': {
          'type': 'Polygon',
          'coordinates': [
            [
              [
                13.3259988,
                52.5141108
              ],
              [
                13.3259718,
                52.5143107
              ],
              [
                13.3262958,
                52.5143236
              ],
              [
                13.3263291,
                52.5143052
              ],
              [
                13.3263688,
                52.5140098
              ],
              [
                13.3264324,
                52.5139643
              ],
              [
                13.3264849,
                52.5139415
              ],
              [
                13.3265148,
                52.5139004
              ],
              [
                13.3265336,
                52.5138571
              ],
              [
                13.3265411,
                52.5137933
              ],
              [
                13.3265336,
                52.5137546
              ],
              [
                13.3264961,
                52.5137044
              ],
              [
                13.3264399,
                52.5136725
              ],
              [
                13.3263875,
                52.5136497
              ],
              [
                13.3263351,
                52.5136429
              ],
              [
                13.3263613,
                52.5134286
              ],
              [
                13.3262564,
                52.5133603
              ],
              [
                13.3260767,
                52.5133671
              ],
              [
                13.3259418,
                52.5134286
              ],
              [
                13.3258744,
                52.5135061
              ],
              [
                13.3258444,
                52.5135677
              ],
              [
                13.3261366,
                52.5135836
              ],
              [
                13.3261066,
                52.513807
              ],
              [
                13.3260579,
                52.5138047
              ],
              [
                13.3260317,
                52.5139096
              ],
              [
                13.3254137,
                52.5138708
              ],
              [
                13.3254287,
                52.5137819
              ],
              [
                13.3250879,
                52.513766
              ],
              [
                13.3250018,
                52.5142697
              ],
              [
                13.3253613,
                52.5142902
              ],
              [
                13.3253838,
                52.5140747
              ],
              [
                13.3259988,
                52.5141108
              ]
            ]
          ]
        }
      },
      'type': 'building',
      'categories': [
        'education'
      ],
      'name': 'Mathematikgebäude',
      'alternateNames': [
        'MA'
      ],
      'uid': 'edfaba58-254f-5da0-82d6-3b46a76c48ce',
      'address': {
        'addressCountry': 'Germany',
        'addressLocality': 'Berlin',
        'addressRegion': 'Berlin',
        'postalCode': '10623',
        'streetAddress': 'Straße des 17. Juni 136'
      },
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'floorName': '0',
    'plan': {
      'type': 'FeatureCollection',
      'crs': {
        'type': 'name',
        'properties': {
          'name': 'urn:ogc:def:crs:EPSG::3857'
        }
      },
      'features': [
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E436',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325186160430565,
                52.514261822595024
              ],
              [
                13.325133104724124,
                52.51425842050136
              ],
              [
                13.325134305945877,
                52.5142514480264
              ],
              [
                13.325134603712506,
                52.51425146712009
              ],
              [
                13.32514187695557,
                52.5142092496341
              ],
              [
                13.325168378185788,
                52.51421094897377
              ],
              [
                13.325167645734922,
                52.514215200482944
              ],
              [
                13.325168985684849,
                52.51421528640462
              ],
              [
                13.32516935191029,
                52.51421316065005
              ],
              [
                13.325175605009935,
                52.51421356161783
              ],
              [
                13.325175238784485,
                52.51421568737242
              ],
              [
                13.325176578734414,
                52.514215773294104
              ],
              [
                13.325177311185291,
                52.514211521784915
              ],
              [
                13.325211789168993,
                52.51421373261825
              ],
              [
                13.325211056718071,
                52.51421798412746
              ],
              [
                13.325246441895343,
                52.5142202531327
              ],
              [
                13.325243973534553,
                52.514234580718586
              ],
              [
                13.325245313484618,
                52.51423466664027
              ],
              [
                13.325186160430565,
                52.514261822595024
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E438',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325133263762659,
                52.514251381198406
              ],
              [
                13.32513356152929,
                52.51425140029212
              ],
              [
                13.325132360307542,
                52.5142583727671
              ],
              [
                13.325079506732449,
                52.51425498363452
              ],
              [
                13.325080707954097,
                52.51424801115953
              ],
              [
                13.32508100572071,
                52.51424803025323
              ],
              [
                13.325088278963108,
                52.5142058127672
              ],
              [
                13.325104805010342,
                52.51420687246782
              ],
              [
                13.325104072559554,
                52.51421112397701
              ],
              [
                13.32510541250934,
                52.514211209898676
              ],
              [
                13.325105778734747,
                52.5142090841441
              ],
              [
                13.325112031833816,
                52.51420948511189
              ],
              [
                13.325111665608404,
                52.514211610866475
              ],
              [
                13.32511300555821,
                52.51421169678813
              ],
              [
                13.325113738009003,
                52.51420744527896
              ],
              [
                13.325140537005714,
                52.51420916371241
              ],
              [
                13.325133263762659,
                52.514251381198406
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E45C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325336015281833,
                52.514211682924824
              ],
              [
                13.325327082280262,
                52.51421111011364
              ],
              [
                13.32532695043914,
                52.51421187538531
              ],
              [
                13.325335883440706,
                52.51421244819646
              ],
              [
                13.325334257399824,
                52.514221886546856
              ],
              [
                13.325325324398278,
                52.51422131373568
              ],
              [
                13.325325192557095,
                52.51422207900732
              ],
              [
                13.325334125558637,
                52.514222651818486
              ],
              [
                13.32533249951699,
                52.514232090168875
              ],
              [
                13.325323566515467,
                52.5142315173577
              ],
              [
                13.325323434674237,
                52.51423228262932
              ],
              [
                13.325332367675744,
                52.5142328554405
              ],
              [
                13.325331107859158,
                52.51424016803625
              ],
              [
                13.325298825047389,
                52.51423809796434
              ],
              [
                13.325263030602374,
                52.51423580271576
              ],
              [
                13.325266231414234,
                52.51421722362067
              ],
              [
                13.325267637719726,
                52.51420906072307
              ],
              [
                13.325269747176975,
                52.5141968163766
              ],
              [
                13.325269940667578,
                52.514196828783795
              ],
              [
                13.325271887789025,
                52.51419593003067
              ],
              [
                13.32527264713314,
                52.51419646503919
              ],
              [
                13.325289940713589,
                52.514188482661034
              ],
              [
                13.32532435346189,
                52.51419068931098
              ],
              [
                13.325339465122893,
                52.51419165831656
              ],
              [
                13.325337773163021,
                52.5142014793028
              ],
              [
                13.32532884016142,
                52.514200906491624
              ],
              [
                13.325328708320367,
                52.51420167176328
              ],
              [
                13.325337641321962,
                52.514202244574435
              ],
              [
                13.325336015281833,
                52.514211682924824
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E6CA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326324306053099,
                52.51423310196347
              ],
              [
                13.326324159563,
                52.51423395226529
              ],
              [
                13.326323866582777,
                52.514235652868955
              ],
              [
                13.326315380217894,
                52.51423510869814
              ],
              [
                13.3263145012771,
                52.51424021050915
              ],
              [
                13.326322987641975,
                52.51424075467997
              ],
              [
                13.326319146832077,
                52.51426304864311
              ],
              [
                13.326319095076926,
                52.51426307196875
              ],
              [
                13.326319095809989,
                52.51426307219388
              ],
              [
                13.326284018199502,
                52.51427926330608
              ],
              [
                13.326282529363844,
                52.51427916783753
              ],
              [
                13.326214880004777,
                52.51427482996017
              ],
              [
                13.326214838507221,
                52.51427480003597
              ],
              [
                13.326214838123667,
                52.51427480045219
              ],
              [
                13.326202863362136,
                52.51426636344555
              ],
              [
                13.32619621909262,
                52.514261682121024
              ],
              [
                13.32618604492978,
                52.51425451375475
              ],
              [
                13.326187615141297,
                52.51424539947001
              ],
              [
                13.326188640572601,
                52.514239447357184
              ],
              [
                13.326189519513495,
                52.514234345546164
              ],
              [
                13.32619054494428,
                52.514228393433314
              ],
              [
                13.326191423884731,
                52.51422329162231
              ],
              [
                13.326192449314997,
                52.51421733950945
              ],
              [
                13.32619264736304,
                52.51421618993884
              ],
              [
                13.326196751921135,
                52.51421595604901
              ],
              [
                13.326196765994409,
                52.514215874360616
              ],
              [
                13.326196765994409,
                52.514215874360616
              ],
              [
                13.32619737436437,
                52.51421592058035
              ],
              [
                13.32621499440062,
                52.514214916538755
              ],
              [
                13.326216459300317,
                52.51420641352039
              ],
              [
                13.326199566483515,
                52.51420319641229
              ],
              [
                13.326199566483515,
                52.51420319641229
              ],
              [
                13.326198955656672,
                52.51420316445351
              ],
              [
                13.326198969729935,
                52.51420308276509
              ],
              [
                13.32619503457641,
                52.51420233334511
              ],
              [
                13.326195232624316,
                52.514201183774496
              ],
              [
                13.326196258053542,
                52.514195231661645
              ],
              [
                13.326197136992661,
                52.5141901298506
              ],
              [
                13.326198162421376,
                52.5141841777377
              ],
              [
                13.32619904136004,
                52.514179075926634
              ],
              [
                13.326200066788235,
                52.51417312381373
              ],
              [
                13.326201636993904,
                52.514164009528876
              ],
              [
                13.326214031767377,
                52.514158288349975
              ],
              [
                13.326222126213821,
                52.51415455211578
              ],
              [
                13.326236765606586,
                52.51414779486552
              ],
              [
                13.326305903803894,
                52.5141522282115
              ],
              [
                13.326305945301474,
                52.51415225813572
              ],
              [
                13.32630594568502,
                52.51415225771949
              ],
              [
                13.326320283474184,
                52.514162359627
              ],
              [
                13.326320172070284,
                52.51416241104885
              ],
              [
                13.326320209948111,
                52.51416243773622
              ],
              [
                13.326320321351993,
                52.51416238631443
              ],
              [
                13.32633473889974,
                52.51417254441732
              ],
              [
                13.326334413856427,
                52.5141744311365
              ],
              [
                13.326325927491386,
                52.51417388696568
              ],
              [
                13.32632504855305,
                52.51417898877675
              ],
              [
                13.326333534918074,
                52.514179532947566
              ],
              [
                13.326329579693002,
                52.51420249109728
              ],
              [
                13.326324306053099,
                52.51423310196347
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E6FF',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32507966577097,
                52.51424794433156
              ],
              [
                13.325079963537574,
                52.51424796342527
              ],
              [
                13.32507876231593,
                52.51425493590026
              ],
              [
                13.32498892422526,
                52.514249175207105
              ],
              [
                13.32495978369376,
                52.51422864374659
              ],
              [
                13.32496862309692,
                52.514177335442504
              ],
              [
                13.324980831526055,
                52.51417811828438
              ],
              [
                13.324980802228056,
                52.51417828834473
              ],
              [
                13.325054837731741,
                52.51418303572778
              ],
              [
                13.325056986298932,
                52.51418454953696
              ],
              [
                13.32506609842527,
                52.51419096963908
              ],
              [
                13.325060316685132,
                52.51419363837785
              ],
              [
                13.325061170946949,
                52.514194240262405
              ],
              [
                13.32506695268711,
                52.51419157152365
              ],
              [
                13.325076634321176,
                52.51419839288214
              ],
              [
                13.325070852580787,
                52.51420106162091
              ],
              [
                13.325071706842587,
                52.51420166350549
              ],
              [
                13.325077488582997,
                52.51419899476671
              ],
              [
                13.325086600709017,
                52.5142054148688
              ],
              [
                13.325086950323534,
                52.514205661195604
              ],
              [
                13.32507966577097,
                52.51424794433156
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E825',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325339596963891,
                52.5141908930449
              ],
              [
                13.325325825253104,
                52.51419000996099
              ],
              [
                13.325341675436745,
                52.51409800730132
              ],
              [
                13.3253554471479,
                52.51409889038522
              ],
              [
                13.325339596963891,
                52.5141908930449
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E827',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325125721312837,
                52.514072440162344
              ],
              [
                13.32512674937755,
                52.514072506084986
              ],
              [
                13.325124846671855,
                52.51408355040293
              ],
              [
                13.325106690603613,
                52.51409193089402
              ],
              [
                13.325107449947902,
                52.51409246590251
              ],
              [
                13.325106836156015,
                52.51409274921701
              ],
              [
                13.325089434805124,
                52.51409163338926
              ],
              [
                13.32507359739348,
                52.51409061784603
              ],
              [
                13.325047821921933,
                52.514088965043996
              ],
              [
                13.325047792624058,
                52.51408913510436
              ],
              [
                13.32503811520973,
                52.51408851455896
              ],
              [
                13.32504152841039,
                52.51406870252575
              ],
              [
                13.325091374769023,
                52.51407189882662
              ],
              [
                13.325099966353417,
                52.514022028622634
              ],
              [
                13.325196323409688,
                52.51402820733099
              ],
              [
                13.325206894125337,
                52.51402888515748
              ],
              [
                13.325203707989617,
                52.514047379222895
              ],
              [
                13.325202219156296,
                52.51404728375439
              ],
              [
                13.325169688148945,
                52.51404519776706
              ],
              [
                13.325168199315684,
                52.514045102298546
              ],
              [
                13.325168433698169,
                52.51404374181556
              ],
              [
                13.325155034199057,
                52.51404288259883
              ],
              [
                13.325154917007813,
                52.51404356284031
              ],
              [
                13.32513198897754,
                52.51404209262507
              ],
              [
                13.325129532672124,
                52.51405635034972
              ],
              [
                13.32512850460741,
                52.51405628442707
              ],
              [
                13.325127186204968,
                52.514063937143774
              ],
              [
                13.325128214269684,
                52.51406400306643
              ],
              [
                13.325128067780497,
                52.51406485336829
              ],
              [
                13.325127039715783,
                52.51406478744563
              ],
              [
                13.325125721312837,
                52.514072440162344
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E82E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32532471968686,
                52.51418856355639
              ],
              [
                13.325308193634307,
                52.514187503855716
              ],
              [
                13.32530834012428,
                52.514186653553864
              ],
              [
                13.325307000174087,
                52.5141865676322
              ],
              [
                13.325306853684118,
                52.51418741793403
              ],
              [
                13.325288765200963,
                52.51418625804554
              ],
              [
                13.325270274182788,
                52.51419479313759
              ],
              [
                13.3252710335269,
                52.514195328146094
              ],
              [
                13.325269517483104,
                52.51419602792223
              ],
              [
                13.325267050234451,
                52.514195869714754
              ],
              [
                13.32525037529995,
                52.51419480046724
              ],
              [
                13.325248886466527,
                52.514194704998715
              ],
              [
                13.325249003658552,
                52.51419402475723
              ],
              [
                13.325112216103278,
                52.51418525352237
              ],
              [
                13.32511209891127,
                52.51418593376384
              ],
              [
                13.325111038880568,
                52.51418586579143
              ],
              [
                13.325110620470955,
                52.51418557099391
              ],
              [
                13.325102599900582,
                52.51417991996654
              ],
              [
                13.325094477488689,
                52.51417419718508
              ],
              [
                13.325093807073575,
                52.51417372483287
              ],
              [
                13.32509385631111,
                52.514173439033215
              ],
              [
                13.325095047377573,
                52.51417351540805
              ],
              [
                13.325106873017848,
                52.514104873283024
              ],
              [
                13.32510568195135,
                52.514104796908214
              ],
              [
                13.325105828440812,
                52.51410394660635
              ],
              [
                13.325107469122365,
                52.51409442322561
              ],
              [
                13.325107650665974,
                52.51409336945051
              ],
              [
                13.325108304210223,
                52.51409306778709
              ],
              [
                13.325109063554509,
                52.514093602795604
              ],
              [
                13.32512841706009,
                52.514084669590645
              ],
              [
                13.32513047146045,
                52.5140727447563
              ],
              [
                13.325131825528791,
                52.51407283158329
              ],
              [
                13.325133143931755,
                52.5140651788666
              ],
              [
                13.32513178986341,
                52.51406509203961
              ],
              [
                13.325131936352602,
                52.514064241737735
              ],
              [
                13.325133290420947,
                52.51406432856472
              ],
              [
                13.325134608823399,
                52.514056675848025
              ],
              [
                13.325133254755045,
                52.51405658902104
              ],
              [
                13.325135344837854,
                52.51404445705103
              ],
              [
                13.32515455078518,
                52.51404568859495
              ],
              [
                13.32515443359394,
                52.514046368836446
              ],
              [
                13.32516783309304,
                52.51404722805317
              ],
              [
                13.325168067475541,
                52.514045867570225
              ],
              [
                13.325169556308785,
                52.514045963038754
              ],
              [
                13.325202087316136,
                52.514048049026094
              ],
              [
                13.325203576149455,
                52.514048144494595
              ],
              [
                13.325203158655594,
                52.51405056785487
              ],
              [
                13.325203158655594,
                52.51405056785487
              ],
              [
                13.325203029694759,
                52.51405170494505
              ],
              [
                13.325203037028936,
                52.514052845485054
              ],
              [
                13.32520318062385,
                52.51405398414249
              ],
              [
                13.325203459808145,
                52.51405511559377
              ],
              [
                13.325203873276552,
                52.51405623454908
              ],
              [
                13.32520441909598,
                52.51405733577691
              ],
              [
                13.32520509471455,
                52.51405841412872
              ],
              [
                13.325205896973545,
                52.514059464562834
              ],
              [
                13.325206822122166,
                52.51406048216817
              ],
              [
                13.325207865835061,
                52.51406146218717
              ],
              [
                13.325209023232546,
                52.514062400037844
              ],
              [
                13.325210288903445,
                52.51406329133552
              ],
              [
                13.325211656930348,
                52.514064131913074
              ],
              [
                13.325213120917327,
                52.51406491784063
              ],
              [
                13.325214674019792,
                52.51406564544363
              ],
              [
                13.325216308976508,
                52.5140663113204
              ],
              [
                13.32521801814357,
                52.51406691235767
              ],
              [
                13.32521979353008,
                52.51406744574547
              ],
              [
                13.325221626835592,
                52.51406790899003
              ],
              [
                13.325223509488815,
                52.51406829992557
              ],
              [
                13.32522543268778,
                52.51406861672427
              ],
              [
                13.325227387440954,
                52.51406885790509
              ],
              [
                13.325229364609273,
                52.51406902234041
              ],
              [
                13.325231354948851,
                52.5140691092614
              ],
              [
                13.325233349154248,
                52.514069118261695
              ],
              [
                13.325235337901955,
                52.51406904929925
              ],
              [
                13.32523731189395,
                52.514068902696444
              ],
              [
                13.325239261901231,
                52.51406867913875
              ],
              [
                13.325241178806918,
                52.51406837967131
              ],
              [
                13.325243053648881,
                52.51406800569424
              ],
              [
                13.325244877661659,
                52.51406755895598
              ],
              [
                13.325246642317444,
                52.51406704154523
              ],
              [
                13.325248339365919,
                52.51406645588096
              ],
              [
                13.325249960872865,
                52.5140658047014
              ],
              [
                13.325251499257247,
                52.514065091050995
              ],
              [
                13.325252947326627,
                52.514064318266264
              ],
              [
                13.325254298310858,
                52.51406348996021
              ],
              [
                13.325255545893665,
                52.51406261000541
              ],
              [
                13.325262720676346,
                52.514063073648735
              ],
              [
                13.325327559876815,
                52.51406722839818
              ],
              [
                13.325325340673809,
                52.51408010984094
              ],
              [
                13.325325194184543,
                52.514080960142806
              ],
              [
                13.325326534134778,
                52.51408104606447
              ],
              [
                13.32532668062404,
                52.51408019576262
              ],
              [
                13.325328899837395,
                52.51406731425983
              ],
              [
                13.325341629463027,
                52.51406812994542
              ],
              [
                13.325352051298482,
                52.514068798225125
              ],
              [
                13.325360537650335,
                52.51406934239576
              ],
              [
                13.325355578988342,
                52.51409812511356
              ],
              [
                13.325340467326921,
                52.51409715610796
              ],
              [
                13.32532471968686,
                52.51418856355639
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E82F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32509132529488,
                52.51417327673673
              ],
              [
                13.325092516361346,
                52.51417335311156
              ],
              [
                13.325092412513502,
                52.51417395589709
              ],
              [
                13.325093538616889,
                52.514174749312126
              ],
              [
                13.325101559187353,
                52.51418040033952
              ],
              [
                13.325109579757706,
                52.5141860513669
              ],
              [
                13.32511035201418,
                52.514186595473156
              ],
              [
                13.32511196707027,
                52.514186699035484
              ],
              [
                13.325111849878265,
                52.51418737927697
              ],
              [
                13.325248637433457,
                52.51419615051182
              ],
              [
                13.325248754625504,
                52.51419547027035
              ],
              [
                13.325250243458909,
                52.51419556573888
              ],
              [
                13.325266918393412,
                52.5141966349864
              ],
              [
                13.325268407226865,
                52.514196730454906
              ],
              [
                13.325266297769613,
                52.5142089748014
              ],
              [
                13.325264891464128,
                52.514217137699
              ],
              [
                13.325261690652274,
                52.514235716794055
              ],
              [
                13.325245313484618,
                52.51423466664027
              ],
              [
                13.325248646137533,
                52.51421532227356
              ],
              [
                13.325247306187459,
                52.51421523635187
              ],
              [
                13.325246573736525,
                52.514219487861055
              ],
              [
                13.32522981652095,
                52.5142184133374
              ],
              [
                13.32523054897187,
                52.51421416182823
              ],
              [
                13.325229209021831,
                52.51421407590656
              ],
              [
                13.325228476570919,
                52.51421832741572
              ],
              [
                13.325212528509253,
                52.514217304777475
              ],
              [
                13.325213260960146,
                52.51421305326829
              ],
              [
                13.325211921010146,
                52.51421296734663
              ],
              [
                13.325211789168993,
                52.51421373261825
              ],
              [
                13.325197559317917,
                52.51421282015688
              ],
              [
                13.325197691159063,
                52.51421205488524
              ],
              [
                13.325191735825873,
                52.514211673011125
              ],
              [
                13.325191603984727,
                52.51421243828277
              ],
              [
                13.325177311185291,
                52.514211521784915
              ],
              [
                13.325177443026428,
                52.51421075651329
              ],
              [
                13.325176103076496,
                52.51421067059161
              ],
              [
                13.325175736851085,
                52.51421279634621
              ],
              [
                13.325169483751438,
                52.51421239537838
              ],
              [
                13.325169849976847,
                52.514210269623796
              ],
              [
                13.325112529900322,
                52.51420659408565
              ],
              [
                13.32511216367496,
                52.51420871984024
              ],
              [
                13.325105910575891,
                52.51420831887245
              ],
              [
                13.325106276801252,
                52.51420619311785
              ],
              [
                13.325104936851465,
                52.51420610719618
              ],
              [
                13.325104805010342,
                52.51420687246782
              ],
              [
                13.325090512212707,
                52.51420595596998
              ],
              [
                13.325090644053834,
                52.51420519069835
              ],
              [
                13.325087740829352,
                52.51420500453472
              ],
              [
                13.32507852929627,
                52.51419851439374
              ],
              [
                13.325067139138497,
                52.51419048926609
              ],
              [
                13.32505802701211,
                52.51418406916398
              ],
              [
                13.32505612865242,
                52.51418273164269
              ],
              [
                13.325056194572925,
                52.51418234900689
              ],
              [
                13.325056341062911,
                52.51418149870503
              ],
              [
                13.325057747366472,
                52.51417333580736
              ],
              [
                13.325057879207401,
                52.51417257053572
              ],
              [
                13.325054157124908,
                52.51417233186442
              ],
              [
                13.32505477238253,
                52.51416876059668
              ],
              [
                13.325058494465026,
                52.51416899926798
              ],
              [
                13.325064310109498,
                52.51413524228479
              ],
              [
                13.325060588026957,
                52.51413500361349
              ],
              [
                13.32506120328353,
                52.51413143234572
              ],
              [
                13.325064925366066,
                52.514131671017026
              ],
              [
                13.325065057206748,
                52.514130905745354
              ],
              [
                13.325066463507065,
                52.51412274284762
              ],
              [
                13.325066888326846,
                52.514120276972285
              ],
              [
                13.32506829462647,
                52.51411211407453
              ],
              [
                13.32506842646704,
                52.51411134880285
              ],
              [
                13.325064704384479,
                52.514111110131545
              ],
              [
                13.325065319640364,
                52.51410753886378
              ],
              [
                13.325069041722932,
                52.514107777535095
              ],
              [
                13.325071883617927,
                52.514091281679185
              ],
              [
                13.325073465553048,
                52.51409138311769
              ],
              [
                13.325089302964685,
                52.51409239866091
              ],
              [
                13.325106275661954,
                52.5140934870021
              ],
              [
                13.325106129172559,
                52.51409433730392
              ],
              [
                13.325104488491018,
                52.51410386068468
              ],
              [
                13.325104342001556,
                52.51410471098654
              ],
              [
                13.325103150935076,
                52.514104634611705
              ],
              [
                13.32509132529488,
                52.51417327673673
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E831',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325060257608113,
                52.514107214270794
              ],
              [
                13.325063979690649,
                52.51410745294211
              ],
              [
                13.325063364434756,
                52.51411102420987
              ],
              [
                13.325059642352224,
                52.51411078553856
              ],
              [
                13.32505951051166,
                52.51411155081022
              ],
              [
                13.325066954676746,
                52.514112028152844
              ],
              [
                13.325065548377115,
                52.5141201910506
              ],
              [
                13.325065401887551,
                52.51412104135244
              ],
              [
                13.324991481332031,
                52.51411630134027
              ],
              [
                13.324991452034118,
                52.514116471400634
              ],
              [
                13.32497924360477,
                52.51411568855875
              ],
              [
                13.324980947515735,
                52.51410579816789
              ],
              [
                13.325017608787164,
                52.51408887603005
              ],
              [
                13.32503782955545,
                52.51409017264758
              ],
              [
                13.325038041965042,
                52.514088939709886
              ],
              [
                13.32504771937937,
                52.514089560255286
              ],
              [
                13.325047690081501,
                52.51408973031566
              ],
              [
                13.325070543668193,
                52.51409119575751
              ],
              [
                13.325067833613746,
                52.51410692634175
              ],
              [
                13.325060389448645,
                52.51410644899914
              ],
              [
                13.325060257608113,
                52.514107214270794
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E832',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325060128259075,
                52.51415165221891
              ],
              [
                13.324986207704221,
                52.51414691220676
              ],
              [
                13.324986178406267,
                52.51414708226711
              ],
              [
                13.32497396997703,
                52.51414629942525
              ],
              [
                13.32497917035999,
                52.5141161137097
              ],
              [
                13.32499137878934,
                52.51411689655158
              ],
              [
                13.324991349491427,
                52.514117066611945
              ],
              [
                13.325065270046933,
                52.51412180662412
              ],
              [
                13.325065123557344,
                52.51412265692596
              ],
              [
                13.32506371725704,
                52.51413081982371
              ],
              [
                13.325056273091983,
                52.51413034248107
              ],
              [
                13.325056141251308,
                52.514131107752746
              ],
              [
                13.325059863333824,
                52.51413134642406
              ],
              [
                13.325059248077253,
                52.514134917691806
              ],
              [
                13.325055525994745,
                52.51413467902051
              ],
              [
                13.325055394154038,
                52.51413544429216
              ],
              [
                13.325062838319079,
                52.51413592163479
              ],
              [
                13.325060128259075,
                52.51415165221891
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E834',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325057286356225,
                52.51416814807466
              ],
              [
                13.325049842191266,
                52.514167670732036
              ],
              [
                13.325049710350363,
                52.5141684360037
              ],
              [
                13.325053432432837,
                52.51416867467501
              ],
              [
                13.325052817175221,
                52.51417224594274
              ],
              [
                13.32504909509275,
                52.51417200727144
              ],
              [
                13.325048963251822,
                52.51417277254308
              ],
              [
                13.325056407416769,
                52.514173249885694
              ],
              [
                13.325055001113212,
                52.51418141278337
              ],
              [
                13.325054854623222,
                52.51418226308522
              ],
              [
                13.32498093406903,
                52.51417752307308
              ],
              [
                13.32498090477104,
                52.51417769313344
              ],
              [
                13.324968696341909,
                52.51417691029156
              ],
              [
                13.324973896732152,
                52.51414672457616
              ],
              [
                13.324986105161388,
                52.514147507418045
              ],
              [
                13.324986075863423,
                52.51414767747841
              ],
              [
                13.325059996418275,
                52.51415241749059
              ],
              [
                13.325057286356225,
                52.51416814807466
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E863',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325360262565786,
                52.514056085450704
              ],
              [
                13.32533644122753,
                52.514054557954196
              ],
              [
                13.32533966398558,
                52.514035851315846
              ],
              [
                13.325345901668538,
                52.514036251294165
              ],
              [
                13.325345901668841,
                52.514036251292424
              ],
              [
                13.325357216798976,
                52.51403697685288
              ],
              [
                13.325363485324408,
                52.51403737880975
              ],
              [
                13.325360262565786,
                52.514056085450704
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E866',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325103972367536,
                52.514012055127836
              ],
              [
                13.325102639318821,
                52.51401979287483
              ],
              [
                13.325102492829934,
                52.51402064317668
              ],
              [
                13.325017331582375,
                52.51401518237713
              ],
              [
                13.325015161816708,
                52.51401504324511
              ],
              [
                13.325005573935478,
                52.51400828794707
              ],
              [
                13.325007164417064,
                52.51399905591421
              ],
              [
                13.325008754897974,
                52.51398982388132
              ],
              [
                13.32502043541266,
                52.513984432377974
              ],
              [
                13.32507932971464,
                52.51398820886072
              ],
              [
                13.325107766426665,
                52.51399003230956
              ],
              [
                13.325103972367536,
                52.514012055127836
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E883',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325366612294573,
                52.51398427822003
              ],
              [
                13.325277282279295,
                52.51397855010818
              ],
              [
                13.325278747164898,
                52.513970047089494
              ],
              [
                13.325357655344657,
                52.51397510692161
              ],
              [
                13.325368077180405,
                52.51397577520135
              ],
              [
                13.325366612294573,
                52.51398427822003
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E884',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32536875892349,
                52.51400676794269
              ],
              [
                13.325362490397994,
                52.51400636598584
              ],
              [
                13.32534493758489,
                52.514005240446195
              ],
              [
                13.32534816033611,
                52.51398653380513
              ],
              [
                13.32537198167485,
                52.51398806130165
              ],
              [
                13.32536875892349,
                52.51400676794269
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E885',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325338982250372,
                52.51400485857207
              ],
              [
                13.325315160912803,
                52.514003331075585
              ],
              [
                13.325318383663868,
                52.513984624434514
              ],
              [
                13.325342205001567,
                52.513986151930986
              ],
              [
                13.325338982250372,
                52.51400485857207
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E886',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325313672079234,
                52.51400323560706
              ],
              [
                13.325276897891072,
                52.514000877534365
              ],
              [
                13.325273473574063,
                52.51400065795676
              ],
              [
                13.325276696324895,
                52.51398195131568
              ],
              [
                13.325316894830292,
                52.51398452896596
              ],
              [
                13.325313672079234,
                52.51400323560706
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E887',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325352256383328,
                52.514067607802566
              ],
              [
                13.325341834547858,
                52.51406693952284
              ],
              [
                13.325262926370225,
                52.51406187969076
              ],
              [
                13.325264391261411,
                52.5140533766722
              ],
              [
                13.325353721274737,
                52.51405910478398
              ],
              [
                13.325352256383328,
                52.514067607802566
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E888',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325206136322247,
                52.51405075879194
              ],
              [
                13.325210135472096,
                52.51402754555115
              ],
              [
                13.325262244641985,
                52.514030886949705
              ],
              [
                13.325258245491789,
                52.51405410019049
              ],
              [
                13.325258245491789,
                52.51405410019049
              ],
              [
                13.325258003199274,
                52.514055134116795
              ],
              [
                13.325257635151846,
                52.51405615486646
              ],
              [
                13.3252571431426,
                52.51405715746645
              ],
              [
                13.325256529568572,
                52.51405813703223
              ],
              [
                13.32525579741901,
                52.51405908879144
              ],
              [
                13.32525495026088,
                52.51406000810723
              ],
              [
                13.325253992221459,
                52.51406089050072
              ],
              [
                13.32525292796823,
                52.51406173167306
              ],
              [
                13.3252517626861,
                52.51406252752608
              ],
              [
                13.325250502052242,
                52.51406327418252
              ],
              [
                13.32524915220831,
                52.514063968004685
              ],
              [
                13.325247719730621,
                52.51406460561236
              ],
              [
                13.325246211598058,
                52.514065183899234
              ],
              [
                13.325244635158105,
                52.5140657000479
              ],
              [
                13.325242998091015,
                52.51406615154375
              ],
              [
                13.325241308372398,
                52.51406653618713
              ],
              [
                13.325239574234422,
                52.5140668521041
              ],
              [
                13.325237804125624,
                52.514067097755564
              ],
              [
                13.325236006669785,
                52.51406727194474
              ],
              [
                13.325234190623943,
                52.51406737382293
              ],
              [
                13.325232364835696,
                52.51406740289386
              ],
              [
                13.325230538200078,
                52.51406735901588
              ],
              [
                13.3252287196163,
                52.51406724240274
              ],
              [
                13.325226917944299,
                52.514067053622604
              ],
              [
                13.325225141961656,
                52.51406679359515
              ],
              [
                13.325223400320764,
                52.51406646358724
              ],
              [
                13.32522170150674,
                52.514066065206634
              ],
              [
                13.325220053796022,
                52.51406560039419
              ],
              [
                13.325218465216082,
                52.514065071414414
              ],
              [
                13.325216943506335,
                52.51406448084447
              ],
              [
                13.325215496080398,
                52.51406383156155
              ],
              [
                13.325214129989975,
                52.514063126728885
              ],
              [
                13.325212851890523,
                52.514062369780355
              ],
              [
                13.32521166800882,
                52.51406156440372
              ],
              [
                13.325210584112595,
                52.51406071452273
              ],
              [
                13.3252096054825,
                52.51405982427788
              ],
              [
                13.32520873688632,
                52.514058898006375
              ],
              [
                13.325207982555758,
                52.51405794022094
              ],
              [
                13.325207346165842,
                52.514056955587776
              ],
              [
                13.325206830816992,
                52.51405594890389
              ],
              [
                13.325206439019954,
                52.51405492507379
              ],
              [
                13.325206172683496,
                52.5140538890855
              ],
              [
                13.325206033105195,
                52.51405284598618
              ],
              [
                13.325206020965071,
                52.51405180085773
              ],
              [
                13.325206136322247,
                52.51405075879194
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E891',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325330485892788,
                52.514054176081785
              ],
              [
                13.32526497721743,
                52.51404997546649
              ],
              [
                13.325268197397955,
                52.51403128378725
              ],
              [
                13.325275939331092,
                52.51403178022353
              ],
              [
                13.325325966722424,
                52.514034973003135
              ],
              [
                13.325327753317143,
                52.51403508756501
              ],
              [
                13.32532802132026,
                52.514035104754115
              ],
              [
                13.325333708650705,
                52.51403546944409
              ],
              [
                13.325330485892788,
                52.514054176081785
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E8EB',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325352031060916,
                52.51392256277974
              ],
              [
                13.32530059186722,
                52.51391926434244
              ],
              [
                13.325300738355363,
                52.513918414040546
              ],
              [
                13.325300738355013,
                52.513918414040546
              ],
              [
                13.325302034775227,
                52.513910888868885
              ],
              [
                13.325303902497946,
                52.51390004751993
              ],
              [
                13.325321768497972,
                52.51390119314211
              ],
              [
                13.32533494467533,
                52.51390203803862
              ],
              [
                13.325355341691939,
                52.51390334595723
              ],
              [
                13.325352031060916,
                52.51392256277974
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E8ED',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325209033770859,
                52.51386181165049
              ],
              [
                13.325167941969381,
                52.513859176718945
              ],
              [
                13.325172131518483,
                52.51383485808496
              ],
              [
                13.325213223319134,
                52.51383749301644
              ],
              [
                13.325209033770859,
                52.51386181165049
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E8EE',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325204639134792,
                52.513887320707056
              ],
              [
                13.325163547332458,
                52.51388468577546
              ],
              [
                13.325167736886447,
                52.51386036714159
              ],
              [
                13.325208828687956,
                52.513863002073116
              ],
              [
                13.325204639134792,
                52.513887320707056
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E8EF',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.3252002444936,
                52.51391282976352
              ],
              [
                13.325159152690407,
                52.513910194831865
              ],
              [
                13.325163342249281,
                52.5138858761981
              ],
              [
                13.325204434051653,
                52.51388851112969
              ],
              [
                13.3252002444936,
                52.51391282976352
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E8F0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325348221195629,
                52.51394467742639
              ],
              [
                13.32537838302928,
                52.51396488423241
              ],
              [
                13.32537661974579,
                52.51397511932325
              ],
              [
                13.325368282278372,
                52.51397458469747
              ],
              [
                13.325357860442622,
                52.51397391641776
              ],
              [
                13.325357860431142,
                52.51397391648439
              ],
              [
                13.325278952251352,
                52.51396885665227
              ],
              [
                13.325278952248837,
                52.51396885666688
              ],
              [
                13.3252202922107,
                52.513965095206785
              ],
              [
                13.32522557311775,
                52.513934441824226
              ],
              [
                13.325228187933066,
                52.513919263935755
              ],
              [
                13.325228773885826,
                52.513915862728254
              ],
              [
                13.32535182597748,
                52.51392375320238
              ],
              [
                13.325348221195629,
                52.51394467742639
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E8F2',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325223488751018,
                52.51393430816832
              ],
              [
                13.32522334226271,
                52.513935158470176
              ],
              [
                13.325205178495768,
                52.51393399375413
              ],
              [
                13.325172126396481,
                52.51393187435279
              ],
              [
                13.325155600350993,
                52.51393081465237
              ],
              [
                13.325158947606978,
                52.513911385254495
              ],
              [
                13.325209567943576,
                52.513914631184775
              ],
              [
                13.325222744118781,
                52.51391547608123
              ],
              [
                13.325226689519086,
                52.51391572907232
              ],
              [
                13.325226103566326,
                52.51391913027983
              ],
              [
                13.325223488751018,
                52.51393430816832
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E8F4',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325182848052515,
                52.51396269417328
              ],
              [
                13.32518262473108,
                52.51396267985322
              ],
              [
                13.325177409736877,
                52.513992950599736
              ],
              [
                13.325175920903604,
                52.51399285513122
              ],
              [
                13.325145250938938,
                52.513990888479576
              ],
              [
                13.32515546851153,
                52.51393157992407
              ],
              [
                13.325171994557007,
                52.51393263962448
              ],
              [
                13.325187850631469,
                52.5139336563643
              ],
              [
                13.325182848052515,
                52.51396269417328
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E93B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325366093905528,
                52.5138409337992
              ],
              [
                13.325345696884236,
                52.5138396258803
              ],
              [
                13.325332520706631,
                52.513838780983804
              ],
              [
                13.325314654703373,
                52.51383763536141
              ],
              [
                13.32531781883427,
                52.51381926884054
              ],
              [
                13.325401937937604,
                52.51382466281266
              ],
              [
                13.325401595339535,
                52.51382665145859
              ],
              [
                13.325366093905528,
                52.5138409337992
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E93C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325355548932178,
                52.513902143014164
              ],
              [
                13.325335151915613,
                52.51390083509555
              ],
              [
                13.325321975738241,
                52.51389999019905
              ],
              [
                13.325321973581227,
                52.513900002719666
              ],
              [
                13.325304107581148,
                52.513898857097466
              ],
              [
                13.325314449620723,
                52.51383882578406
              ],
              [
                13.325332315623925,
                52.513839971406426
              ],
              [
                13.325345491801517,
                52.51384081630295
              ],
              [
                13.325365888815634,
                52.5138421242214
              ],
              [
                13.325355548932178,
                52.513902143014164
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E93D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325299950408603,
                52.51391075521295
              ],
              [
                13.325298653988401,
                52.51391828038464
              ],
              [
                13.325298507500253,
                52.513919130686496
              ],
              [
                13.32522294920222,
                52.51391428565861
              ],
              [
                13.325209773027016,
                52.51391344076211
              ],
              [
                13.325202328860268,
                52.513912963419486
              ],
              [
                13.325219555824443,
                52.513812967917495
              ],
              [
                13.325229382124796,
                52.5138135980098
              ],
              [
                13.325300846131514,
                52.51381818049927
              ],
              [
                13.325315734467255,
                52.51381913518459
              ],
              [
                13.325299950408253,
                52.51391075521296
              ],
              [
                13.325299950408603,
                52.51391075521295
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E93E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325213428401788,
                52.5138363025938
              ],
              [
                13.32517233660118,
                52.51383366766235
              ],
              [
                13.325176379657908,
                52.51381019933015
              ],
              [
                13.325217471457718,
                52.51381283426156
              ],
              [
                13.325213428401788,
                52.5138363025938
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E961',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325849813686785,
                52.51396458352464
              ],
              [
                13.325834850902497,
                52.513963624065696
              ],
              [
                13.325846137822024,
                52.513898108306456
              ],
              [
                13.32585013014701,
                52.51389836430638
              ],
              [
                13.325849837170917,
                52.51390006491028
              ],
              [
                13.325860807629912,
                52.5139007683692
              ],
              [
                13.325859694321212,
                52.51390723066348
              ],
              [
                13.325849813686785,
                52.51396458352464
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E962',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325582642363564,
                52.51394745169595
              ],
              [
                13.32556767958037,
                52.51394649223754
              ],
              [
                13.325577560199534,
                52.513889139375564
              ],
              [
                13.325577560197608,
                52.51388913937547
              ],
              [
                13.32557867350579,
                52.513882677081114
              ],
              [
                13.32558964396245,
                52.51388338053956
              ],
              [
                13.325589936167733,
                52.513881684407366
              ],
              [
                13.32559392849586,
                52.5138819404074
              ],
              [
                13.325582642363564,
                52.51394745169595
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E963',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32556383086092,
                52.5139469886235
              ],
              [
                13.325546111269798,
                52.513955167646245
              ],
              [
                13.32553670959293,
                52.51395456478263
              ],
              [
                13.325412839579931,
                52.51394662186273
              ],
              [
                13.325412839579094,
                52.51394662186643
              ],
              [
                13.325403437904061,
                52.5139460190028
              ],
              [
                13.325388892873654,
                52.513935771075936
              ],
              [
                13.325402930655414,
                52.51385428755637
              ],
              [
                13.325420650240485,
                52.513846108531446
              ],
              [
                13.32542335167509,
                52.51384628175555
              ],
              [
                13.325560622166567,
                52.51385508395451
              ],
              [
                13.325560622167464,
                52.513855083950446
              ],
              [
                13.325563323602625,
                52.51385525717451
              ],
              [
                13.325577868646853,
                52.51386550510405
              ],
              [
                13.325577434389006,
                52.5138680257952
              ],
              [
                13.325577434390924,
                52.51386802579531
              ],
              [
                13.325576321083513,
                52.51387448808951
              ],
              [
                13.325576321082918,
                52.51387448808948
              ],
              [
                13.325574951421721,
                52.513882438409965
              ],
              [
                13.325574951422308,
                52.51388243841
              ],
              [
                13.325573838114165,
                52.51388890070425
              ],
              [
                13.32556383086092,
                52.5139469886235
              ]
            ]
          },
          'place': {
            'geo': {
              'point': {
                'type': 'Point',
                'coordinates': [
                  13.3254773,
                  52.5138698
                ]
              }
            },
            'type': 'room',
            'categories': [
              'education'
            ],
            'uid': '593655b5-0fc9-59de-a82f-b8f5f908deef',
            'name': 'MA 043',
            'floor': '0',
            'origin': {
              'indexed': '2018-09-11T12:30:00Z',
              'name': 'Dummy',
              'type': 'remote'
            }
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E987',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32559422224182,
                52.513880235332024
              ],
              [
                13.325589309084862,
                52.51387992028572
              ],
              [
                13.325589309085537,
                52.51387992028182
              ],
              [
                13.32558335374886,
                52.51387953840765
              ],
              [
                13.325583353748147,
                52.51387953841176
              ],
              [
                13.325579259457907,
                52.513879275873634
              ],
              [
                13.32558004316757,
                52.513874726760776
              ],
              [
                13.325580043166973,
                52.513874726760726
              ],
              [
                13.325581156474414,
                52.51386826446642
              ],
              [
                13.325581449449835,
                52.513866563862635
              ],
              [
                13.325596412233462,
                52.51386752332099
              ],
              [
                13.32559422224182,
                52.513880235332024
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5E988',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325861393581732,
                52.51389736716166
              ],
              [
                13.32585729928891,
                52.51389710462321
              ],
              [
                13.325857299289614,
                52.513897104619105
              ],
              [
                13.32585134395061,
                52.513896722744946
              ],
              [
                13.32585134394993,
                52.513896722748875
              ],
              [
                13.325846430798059,
                52.5138964077027
              ],
              [
                13.32584862079283,
                52.51388369569183
              ],
              [
                13.325863583577691,
                52.51388465515086
              ],
              [
                13.325863290601964,
                52.513886355754636
              ],
              [
                13.325863290599738,
                52.51388635575449
              ],
              [
                13.325861393581732,
                52.51389736716166
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5F2F6',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326182714561684,
                52.51425200134395
              ],
              [
                13.326182598501816,
                52.51425212728901
              ],
              [
                13.326159160496987,
                52.514250624374554
              ],
              [
                13.326160310445244,
                52.51424394950516
              ],
              [
                13.326155612237041,
                52.514240639308795
              ],
              [
                13.326121369024005,
                52.51423844353221
              ],
              [
                13.326115645384807,
                52.51424108544873
              ],
              [
                13.32611449543664,
                52.514247760318135
              ],
              [
                13.326102145839975,
                52.51424696842526
              ],
              [
                13.326098261080844,
                52.51426951747891
              ],
              [
                13.326098209325707,
                52.51426954080457
              ],
              [
                13.326098254663748,
                52.51426955472686
              ],
              [
                13.326098245188332,
                52.51426960972683
              ],
              [
                13.326065945671864,
                52.51428591253369
              ],
              [
                13.325991342491946,
                52.514281128755435
              ],
              [
                13.325992177487587,
                52.51427628203503
              ],
              [
                13.325983691129123,
                52.5142757378644
              ],
              [
                13.325985492961165,
                52.51426527915191
              ],
              [
                13.325935071660462,
                52.51426204598754
              ],
              [
                13.325934690785491,
                52.5142642567723
              ],
              [
                13.325934045047097,
                52.514264215365614
              ],
              [
                13.325934075291178,
                52.514264039814186
              ],
              [
                13.325926663491446,
                52.51426283935823
              ],
              [
                13.325931058199954,
                52.51423733030327
              ],
              [
                13.32593871714123,
                52.514237096228236
              ],
              [
                13.325939129205759,
                52.51423470440097
              ],
              [
                13.32593171740579,
                52.51423350394502
              ],
              [
                13.325936287896402,
                52.51420697452769
              ],
              [
                13.325943946837574,
                52.5142067404527
              ],
              [
                13.325944358901525,
                52.51420434862542
              ],
              [
                13.325936947101317,
                52.514203148169436
              ],
              [
                13.325941561532526,
                52.51417636366147
              ],
              [
                13.32594922047359,
                52.514176129586446
              ],
              [
                13.325949632536965,
                52.51417373775915
              ],
              [
                13.325942220736513,
                52.51417253730318
              ],
              [
                13.325943106999487,
                52.514167392977015
              ],
              [
                13.326121858410275,
                52.514178855050226
              ],
              [
                13.326130385084866,
                52.51412936179788
              ],
              [
                13.32611433455024,
                52.51411767811725
              ],
              [
                13.326119549568102,
                52.51408740737132
              ],
              [
                13.326259993801711,
                52.51409641307245
              ],
              [
                13.326256849521766,
                52.51411466415142
              ],
              [
                13.326238723383943,
                52.51412303081425
              ],
              [
                13.32623879877478,
                52.514123052081686
              ],
              [
                13.32623488213106,
                52.51414578634158
              ],
              [
                13.326198087316966,
                52.51416277009025
              ],
              [
                13.326182714561684,
                52.51425200134395
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5F35A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326118502546404,
                52.51417649062429
              ],
              [
                13.326074574424405,
                52.5141736738224
              ],
              [
                13.326068626565352,
                52.51417329242795
              ],
              [
                13.326067137730153,
                52.51417319695939
              ],
              [
                13.325991951556789,
                52.514168375797865
              ],
              [
                13.325995335467965,
                52.51414873382524
              ],
              [
                13.325991613380369,
                52.51414849515386
              ],
              [
                13.32598822946921,
                52.51416813712651
              ],
              [
                13.325947286507205,
                52.514165511741545
              ],
              [
                13.325950960009793,
                52.51414418882796
              ],
              [
                13.325954743134494,
                52.514144073206886
              ],
              [
                13.325954949165894,
                52.51414287729326
              ],
              [
                13.325955155197274,
                52.514141681379584
              ],
              [
                13.325951494147201,
                52.51414108841579
              ],
              [
                13.325956216272319,
                52.51411367871887
              ],
              [
                13.32595818349714,
                52.51411361859589
              ],
              [
                13.325958318993049,
                52.51411283210588
              ],
              [
                13.325965986494102,
                52.51411332376893
              ],
              [
                13.325992934407791,
                52.514115051749606
              ],
              [
                13.325997326470237,
                52.514115333381746
              ],
              [
                13.325996447533907,
                52.514120435192865
              ],
              [
                13.326000169621524,
                52.51412067386422
              ],
              [
                13.326001280597032,
                52.514114225174986
              ],
              [
                13.326009553280997,
                52.514111543822956
              ],
              [
                13.326018949484656,
                52.514112146335556
              ],
              [
                13.326019049097413,
                52.51411156813029
              ],
              [
                13.326029470943173,
                52.514112236410085
              ],
              [
                13.326108384594196,
                52.51411730016309
              ],
              [
                13.326114337220409,
                52.5141216332638
              ],
              [
                13.326124167781105,
                52.51412827520236
              ],
              [
                13.32612651474904,
                52.51412998363297
              ],
              [
                13.326118502546404,
                52.51417649062429
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6038F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325974586619791,
                52.51387947935864
              ],
              [
                13.3259032714338,
                52.51387490641363
              ],
              [
                13.325903271433164,
                52.513874906417286
              ],
              [
                13.32588823420125,
                52.513873942185
              ],
              [
                13.32588823420196,
                52.513873942180886
              ],
              [
                13.32588399102662,
                52.513873670095656
              ],
              [
                13.325863876553404,
                52.513882954547086
              ],
              [
                13.325863730065546,
                52.513883804849
              ],
              [
                13.325846682912132,
                52.51388271173393
              ],
              [
                13.325832488206403,
                52.51396510598326
              ],
              [
                13.325799213269127,
                52.51396297228799
              ],
              [
                13.32581370095797,
                52.51387887763983
              ],
              [
                13.32581452720548,
                52.51387676283733
              ],
              [
                13.32581494155177,
                52.51387435782143
              ],
              [
                13.32580734812673,
                52.51386900771954
              ],
              [
                13.325803137068025,
                52.513868737684476
              ],
              [
                13.325800824365588,
                52.51386980517487
              ],
              [
                13.325798532506132,
                52.5138681903987
              ],
              [
                13.325795554837988,
                52.51386799945516
              ],
              [
                13.32578051698193,
                52.513867035182706
              ],
              [
                13.325780517008807,
                52.51386703502668
              ],
              [
                13.325754387965613,
                52.513865359553996
              ],
              [
                13.325754497879085,
                52.51386472155094
              ],
              [
                13.325748542539845,
                52.513864339676694
              ],
              [
                13.325748432626373,
                52.51386497767978
              ],
              [
                13.325700789924463,
                52.51386192268654
              ],
              [
                13.325700899837914,
                52.513861284683465
              ],
              [
                13.32569494449909,
                52.51386090280924
              ],
              [
                13.32569483458563,
                52.51386154081231
              ],
              [
                13.325668705546885,
                52.51385986533972
              ],
              [
                13.325668705520007,
                52.51385986549571
              ],
              [
                13.325653668289563,
                52.51385890126324
              ],
              [
                13.32565366831606,
                52.51385890110942
              ],
              [
                13.325650690647429,
                52.513858710172364
              ],
              [
                13.325647898563075,
                52.513859998945165
              ],
              [
                13.325646000198219,
                52.51385866142391
              ],
              [
                13.32564178913889,
                52.51385839139822
              ],
              [
                13.32563253836098,
                52.513862661380735
              ],
              [
                13.325632124031241,
                52.51386506639766
              ],
              [
                13.325632211504857,
                52.51386723979173
              ],
              [
                13.325617723835762,
                52.51395133464542
              ],
              [
                13.32558444840371,
                52.51394920092539
              ],
              [
                13.325598643089139,
                52.513866806674976
              ],
              [
                13.325581595937555,
                52.51386571356075
              ],
              [
                13.325581742425259,
                52.51386486325884
              ],
              [
                13.325565231559924,
                52.51385323028066
              ],
              [
                13.325560988386707,
                52.513852958194754
              ],
              [
                13.325545951162297,
                52.51385199396251
              ],
              [
                13.325545951162221,
                52.513851993962966
              ],
              [
                13.32547977248824,
                52.51384775038938
              ],
              [
                13.325482885351423,
                52.51382968146688
              ],
              [
                13.325977699485623,
                52.51386141043607
              ],
              [
                13.325974586619791,
                52.51387947935864
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6171F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.3253850190958,
                52.51393641292114
              ],
              [
                13.325401529945516,
                52.51394804589665
              ],
              [
                13.325381486383705,
                52.513964345611136
              ],
              [
                13.325351324549795,
                52.513944138805115
              ],
              [
                13.325368866482401,
                52.51384231516206
              ],
              [
                13.325405176613252,
                52.51382770748026
              ],
              [
                13.325405660022048,
                52.513824901484
              ],
              [
                13.32540568931955,
                52.51382473142361
              ],
              [
                13.32548154540253,
                52.5138295955453
              ],
              [
                13.325478432539347,
                52.51384766446786
              ],
              [
                13.325438755115773,
                52.51384512023319
              ],
              [
                13.325438755116672,
                52.51384512022794
              ],
              [
                13.325423717895148,
                52.51384415599576
              ],
              [
                13.325423717894072,
                52.51384415600081
              ],
              [
                13.32541947472157,
                52.51384388391588
              ],
              [
                13.325399360263907,
                52.51385316836871
              ],
              [
                13.3253850190958,
                52.51393641292114
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '634FA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326083841987582,
                52.51397138671343
              ],
              [
                13.326045504481051,
                52.51396892839831
              ],
              [
                13.326054293774044,
                52.51391791028577
              ],
              [
                13.32609263128115,
                52.51392036860091
              ],
              [
                13.326083841987582,
                52.51397138671343
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6378D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326408631936944,
                52.51387742654512
              ],
              [
                13.326406835565345,
                52.51387731135648
              ],
              [
                13.32638807623257,
                52.51387610845268
              ],
              [
                13.326388074413599,
                52.51387611901103
              ],
              [
                13.32634310180492,
                52.513873235234456
              ],
              [
                13.326337146461677,
                52.51387285336025
              ],
              [
                13.326333783423618,
                52.51387440567281
              ],
              [
                13.32631234618491,
                52.5138843006827
              ],
              [
                13.326303095398057,
                52.513888570664825
              ],
              [
                13.326281658158928,
                52.51389846567465
              ],
              [
                13.326278274814626,
                52.51390002736014
              ],
              [
                13.32627768886304,
                52.51390342856768
              ],
              [
                13.326273265783712,
                52.51392910270339
              ],
              [
                13.32627142003344,
                52.51393981650704
              ],
              [
                13.326264211951289,
                52.51398165634033
              ],
              [
                13.326246345923865,
                52.51398051071772
              ],
              [
                13.326244588062313,
                52.51399071434013
              ],
              [
                13.326186567300871,
                52.51398699387679
              ],
              [
                13.326142230333565,
                52.51395575561197
              ],
              [
                13.326181702250457,
                52.51372663679828
              ],
              [
                13.326235691952514,
                52.513701716187455
              ],
              [
                13.326260980247369,
                52.5137033377479
              ],
              [
                13.326259222408032,
                52.51371354137079
              ],
              [
                13.326309842821793,
                52.51371678730166
              ],
              [
                13.326302708056657,
                52.51375820198554
              ],
              [
                13.326300862320815,
                52.51376891578953
              ],
              [
                13.32629643926871,
                52.51379458992577
              ],
              [
                13.326295853319944,
                52.513797991133366
              ],
              [
                13.326298630523981,
                52.51379994785139
              ],
              [
                13.326316227192017,
                52.513812345830594
              ],
              [
                13.32632382066159,
                52.51381769591617
              ],
              [
                13.326341417328825,
                52.51383009389534
              ],
              [
                13.32634417786444,
                52.513832038869566
              ],
              [
                13.326350133207763,
                52.513832420743775
              ],
              [
                13.326395105816978,
                52.51383530452037
              ],
              [
                13.326395103998015,
                52.513835315078715
              ],
              [
                13.32641386333102,
                52.513836517982526
              ],
              [
                13.326415659702642,
                52.51383663317119
              ],
              [
                13.326408631936944,
                52.51387742654512
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '638BF',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32633629105234,
                52.514043463411525
              ],
              [
                13.32633627669514,
                52.51404346571231
              ],
              [
                13.326336133165121,
                52.5140434887135
              ],
              [
                13.326336295496537,
                52.51404380581122
              ],
              [
                13.326332474462768,
                52.514065985184
              ],
              [
                13.32633229867598,
                52.514067005546224
              ],
              [
                13.326271033086318,
                52.51406307701527
              ],
              [
                13.326277683679915,
                52.514024473310734
              ],
              [
                13.326312088559542,
                52.514026679453465
              ],
              [
                13.326312419946749,
                52.51402678121485
              ],
              [
                13.32631248999981,
                52.514026705195015
              ],
              [
                13.326312507436134,
                52.51402670631309
              ],
              [
                13.32633629105234,
                52.514043463411525
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '638F3',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32635856366631,
                52.513830038366365
              ],
              [
                13.326346772688016,
                52.51382928229397
              ],
              [
                13.326345348912508,
                52.51382827915293
              ],
              [
                13.32633680625935,
                52.51382226030664
              ],
              [
                13.326337846972638,
                52.51382177993367
              ],
              [
                13.326328792958234,
                52.51381540080076
              ],
              [
                13.326321199488497,
                52.51381005071518
              ],
              [
                13.32631214547383,
                52.51380367158225
              ],
              [
                13.32631110476065,
                52.51380415195524
              ],
              [
                13.326302562106974,
                52.51379813310894
              ],
              [
                13.326301138331344,
                52.51379712996791
              ],
              [
                13.326302298450573,
                52.51379039592045
              ],
              [
                13.32635856366631,
                52.513830038366365
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '638F4',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326281990734211,
                52.51390827413
              ],
              [
                13.32628315085934,
                52.51390154008265
              ],
              [
                13.326284885381922,
                52.51390073946102
              ],
              [
                13.326295292517415,
                52.513895935731114
              ],
              [
                13.326296146782353,
                52.51389653761578
              ],
              [
                13.326307176886312,
                52.51389144633582
              ],
              [
                13.32631642767332,
                52.51388717635367
              ],
              [
                13.326327457777147,
                52.513882085073696
              ],
              [
                13.32632660351209,
                52.51388148318908
              ],
              [
                13.326337010647297,
                52.51387667945918
              ],
              [
                13.326338745169828,
                52.51387587883751
              ],
              [
                13.326350536147961,
                52.513876634909906
              ],
              [
                13.326281990734211,
                52.51390827413
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '64092',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326144091874617,
                52.51358820869837
              ],
              [
                13.3261353594497,
                52.513587648749336
              ],
              [
                13.326135162544684,
                52.51358879171796
              ],
              [
                13.326132837501618,
                52.51358864262928
              ],
              [
                13.326132837608377,
                52.51358864200883
              ],
              [
                13.326123159964977,
                52.51358802270414
              ],
              [
                13.326123160071736,
                52.513588022083674
              ],
              [
                13.326120838290084,
                52.51358787366785
              ],
              [
                13.326197641271802,
                52.51355242053072
              ],
              [
                13.326196230286307,
                52.513560602947194
              ],
              [
                13.326194229091556,
                52.51356047462465
              ],
              [
                13.32619337030816,
                52.513565461209225
              ],
              [
                13.326144091874617,
                52.51358820869837
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6410F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325220866609474,
                52.51394952857186
              ],
              [
                13.32521349688433,
                52.51394905600263
              ],
              [
                13.32521336504477,
                52.513949821274316
              ],
              [
                13.325220734769907,
                52.51395029384353
              ],
              [
                13.325218207843989,
                52.51396496155082
              ],
              [
                13.325200416285517,
                52.5139638207019
              ],
              [
                13.325202943211368,
                52.51394915299461
              ],
              [
                13.325204432044705,
                52.51394924846315
              ],
              [
                13.325204563884267,
                52.51394848319148
              ],
              [
                13.325200237953032,
                52.513948205799565
              ],
              [
                13.325200106113467,
                52.513948971071244
              ],
              [
                13.32520160326137,
                52.51394906707294
              ],
              [
                13.325199076335524,
                52.51396373478022
              ],
              [
                13.325184188002472,
                52.51396278009496
              ],
              [
                13.325186714928252,
                52.513948112387645
              ],
              [
                13.325191181428155,
                52.51394839879325
              ],
              [
                13.325191313267721,
                52.51394763352156
              ],
              [
                13.325186846767817,
                52.51394734711597
              ],
              [
                13.32518919058143,
                52.51393374228598
              ],
              [
                13.325191498273059,
                52.5139338902622
              ],
              [
                13.325205046656286,
                52.513934759025815
              ],
              [
                13.325223210423237,
                52.513935923741855
              ],
              [
                13.325220866609474,
                52.51394952857186
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '64110',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325267650080018,
                52.51399951081095
              ],
              [
                13.325252761745492,
                52.51399855612564
              ],
              [
                13.325252017328772,
                52.51399850839138
              ],
              [
                13.325250528495337,
                52.51399841292286
              ],
              [
                13.325222240663344,
                52.51399659902099
              ],
              [
                13.325223954788605,
                52.51398664928391
              ],
              [
                13.325224028032927,
                52.51398622413297
              ],
              [
                13.32522738241388,
                52.513966753425386
              ],
              [
                13.32525715907957,
                52.51396866279581
              ],
              [
                13.325257903496292,
                52.513968710530065
              ],
              [
                13.325272791830947,
                52.51396966521535
              ],
              [
                13.325270448013725,
                52.51398327004528
              ],
              [
                13.325270374769415,
                52.51398369519623
              ],
              [
                13.325267650080018,
                52.51399951081095
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '64111',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32522244151994,
                52.51398320065591
              ],
              [
                13.325222368275629,
                52.51398362580682
              ],
              [
                13.325220156296627,
                52.51399646536503
              ],
              [
                13.325196632729858,
                52.5139949569623
              ],
              [
                13.325183084346824,
                52.51399408819868
              ],
              [
                13.32517936226362,
                52.513993849527374
              ],
              [
                13.325181574242475,
                52.51398100996917
              ],
              [
                13.32518164748678,
                52.51398058481825
              ],
              [
                13.325184504013778,
                52.51396400393178
              ],
              [
                13.325203114430165,
                52.51396519728839
              ],
              [
                13.325203858846821,
                52.51396524502265
              ],
              [
                13.32522529804714,
                52.51396661976945
              ],
              [
                13.325223906405952,
                52.513974697637224
              ],
              [
                13.325223833161669,
                52.51397512278816
              ],
              [
                13.32522244151994,
                52.51398320065591
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '646AB',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326111631609145,
                52.51360561910057
              ],
              [
                13.326111631609145,
                52.51360561910057
              ],
              [
                13.326111518291533,
                52.51360627687137
              ],
              [
                13.326061493426302,
                52.513603069128045
              ],
              [
                13.3260622073362,
                52.51359892511939
              ],
              [
                13.3260622073362,
                52.51359892511939
              ],
              [
                13.32606352570922,
                52.51359127240206
              ],
              [
                13.32606352570922,
                52.51359127240206
              ],
              [
                13.326064241568309,
                52.513587117076774
              ],
              [
                13.32606769532924,
                52.51358733854221
              ],
              [
                13.32606769532924,
                52.51358733854221
              ],
              [
                13.32609598119997,
                52.51358916375808
              ],
              [
                13.32609598119997,
                52.51358916375808
              ],
              [
                13.326114264484207,
                52.51359033613668
              ],
              [
                13.326114151166678,
                52.51359099390747
              ],
              [
                13.326111631609145,
                52.51360561910057
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '648F8',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32619350820641,
                52.51408974267243
              ],
              [
                13.32619336171724,
                52.514090592974256
              ],
              [
                13.32612576858989,
                52.51408625870214
              ],
              [
                13.326119813248592,
                52.51408587682794
              ],
              [
                13.326120032982319,
                52.51408460137517
              ],
              [
                13.326106335698464,
                52.51408372306457
              ],
              [
                13.32602506071963,
                52.51407851147089
              ],
              [
                13.326018792189084,
                52.51407810951403
              ],
              [
                13.32602362630272,
                52.51405004969336
              ],
              [
                13.326124867121024,
                52.51405654141386
              ],
              [
                13.326125350534555,
                52.51405373541769
              ],
              [
                13.326198899003888,
                52.51405845156404
              ],
              [
                13.326198490312699,
                52.51406082383068
              ],
              [
                13.32619350820641,
                52.51408974267243
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '64CE8',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326321931376542,
                52.51371214977673
              ],
              [
                13.326310794984964,
                52.51371126033925
              ],
              [
                13.326310794984964,
                52.51371126033925
              ],
              [
                13.326309842821793,
                52.51371678730166
              ],
              [
                13.326259222408032,
                52.51371354137079
              ],
              [
                13.326260980247369,
                52.5137033377479
              ],
              [
                13.326235691952514,
                52.513701716187455
              ],
              [
                13.326181702250457,
                52.51372663679828
              ],
              [
                13.326142230333565,
                52.51395575561197
              ],
              [
                13.326186567300871,
                52.51398699387679
              ],
              [
                13.326244588062313,
                52.51399071434013
              ],
              [
                13.326246345923865,
                52.51398051071772
              ],
              [
                13.326264211951289,
                52.51398165634033
              ],
              [
                13.326257063308969,
                52.51402315107145
              ],
              [
                13.326275152659294,
                52.514024311014204
              ],
              [
                13.326268502065721,
                52.51406291471873
              ],
              [
                13.326125350534555,
                52.51405373541769
              ],
              [
                13.326130565537166,
                52.51402346467141
              ],
              [
                13.326124723490192,
                52.51402309006198
              ],
              [
                13.32612461063245,
                52.51402307925111
              ],
              [
                13.326045701927743,
                52.51401802296419
              ],
              [
                13.326045684349097,
                52.51401812500041
              ],
              [
                13.326029307163472,
                52.51401707484646
              ],
              [
                13.326031213728342,
                52.51400600808189
              ],
              [
                13.326011803156215,
                52.51398717844973
              ],
              [
                13.326031917645752,
                52.513977893996575
              ],
              [
                13.326032459653886,
                52.51397474787434
              ],
              [
                13.326044370335513,
                52.51397551162276
              ],
              [
                13.326045504481051,
                52.51396892839831
              ],
              [
                13.326086224123982,
                52.513971539463114
              ],
              [
                13.326105136868247,
                52.51386175887537
              ],
              [
                13.326094587300508,
                52.51386108240569
              ],
              [
                13.326087990719193,
                52.51385643468888
              ],
              [
                13.326094426763065,
                52.513819076005596
              ],
              [
                13.32609457564483,
                52.51381908555233
              ],
              [
                13.326094648888514,
                52.5138186604014
              ],
              [
                13.326094500006754,
                52.513818650854645
              ],
              [
                13.326095269065396,
                52.51381418676971
              ],
              [
                13.32609541794716,
                52.51381419631645
              ],
              [
                13.326095491190827,
                52.5138137711655
              ],
              [
                13.326095342309074,
                52.51381376161876
              ],
              [
                13.326095898960942,
                52.51381053047157
              ],
              [
                13.326110787313922,
                52.51381148515706
              ],
              [
                13.326111358614451,
                52.51380816897967
              ],
              [
                13.326103914437914,
                52.51380769163691
              ],
              [
                13.326109656735746,
                52.51377435980255
              ],
              [
                13.326117100912361,
                52.5137748371453
              ],
              [
                13.326119063838728,
                52.51376344309982
              ],
              [
                13.326120403790526,
                52.51376352902153
              ],
              [
                13.326121282712446,
                52.51375842721013
              ],
              [
                13.326119942760647,
                52.51375834128843
              ],
              [
                13.326121905685524,
                52.5137469472429
              ],
              [
                13.326114461508846,
                52.513746469900155
              ],
              [
                13.326120203790607,
                52.513713138065434
              ],
              [
                13.326127647967358,
                52.51371361540821
              ],
              [
                13.326129610888211,
                52.513702221362614
              ],
              [
                13.326130950840039,
                52.513702307284305
              ],
              [
                13.326131829759502,
                52.51369720547283
              ],
              [
                13.326130489807676,
                52.51369711955116
              ],
              [
                13.32613245272706,
                52.513685725505525
              ],
              [
                13.32612500855024,
                52.513685248162794
              ],
              [
                13.32613075081592,
                52.5136519163277
              ],
              [
                13.326138194992799,
                52.51365239367045
              ],
              [
                13.326140157908162,
                52.513640999624776
              ],
              [
                13.32614149786001,
                52.51364108554647
              ],
              [
                13.32614237677701,
                52.51363598373498
              ],
              [
                13.326141036825163,
                52.51363589781327
              ],
              [
                13.326144625734107,
                52.51361506541617
              ],
              [
                13.326136571637692,
                52.51360939079335
              ],
              [
                13.326114239107305,
                52.51360795876507
              ],
              [
                13.326117242139087,
                52.51359052716549
              ],
              [
                13.32614166996955,
                52.51359209396527
              ],
              [
                13.326153758961077,
                52.513586513548525
              ],
              [
                13.326155182753704,
                52.51358751668226
              ],
              [
                13.326160386256898,
                52.5135851146857
              ],
              [
                13.326161810049538,
                52.51358611781945
              ],
              [
                13.32616701347451,
                52.51358371585897
              ],
              [
                13.326168437267183,
                52.51358471899274
              ],
              [
                13.3261823130671,
                52.51357831376479
              ],
              [
                13.32618088927435,
                52.513577310631014
              ],
              [
                13.32618609269925,
                52.51357490867056
              ],
              [
                13.326184669356024,
                52.51357390585355
              ],
              [
                13.326189872895311,
                52.51357150406492
              ],
              [
                13.32618844846026,
                52.513570500478664
              ],
              [
                13.326200538379549,
                52.51356491963348
              ],
              [
                13.326201226618497,
                52.513560923326864
              ],
              [
                13.326205032324568,
                52.513538826760715
              ],
              [
                13.326205032324568,
                52.513538826760715
              ],
              [
                13.326206414026622,
                52.51353074827125
              ],
              [
                13.326207795728173,
                52.513522669781786
              ],
              [
                13.326207795728173,
                52.513522669781786
              ],
              [
                13.326209660929006,
                52.51351184287024
              ],
              [
                13.326212984915932,
                52.51349264428455
              ],
              [
                13.326213204629537,
                52.51349136883072
              ],
              [
                13.326220697116307,
                52.51344787717687
              ],
              [
                13.326258020984948,
                52.51345027049401
              ],
              [
                13.32625797989302,
                52.51345050902068
              ],
              [
                13.326258724310849,
                52.51345055675496
              ],
              [
                13.326258765402779,
                52.513450318228294
              ],
              [
                13.326275887013075,
                52.513451416116695
              ],
              [
                13.32627584592114,
                52.51345165464336
              ],
              [
                13.32627659033899,
                52.51345170237765
              ],
              [
                13.32627663143092,
                52.51345146385098
              ],
              [
                13.326308841017452,
                52.51345352922506
              ],
              [
                13.32630950074123,
                52.51345373602404
              ],
              [
                13.32631274585357,
                52.51345028861131
              ],
              [
                13.326366950058368,
                52.513488478879275
              ],
              [
                13.326360273740605,
                52.51348958507575
              ],
              [
                13.326321931376542,
                52.51371214977673
              ]
            ]
          },
          'place': {
            'geo': {
              'point': {
                'type': 'Point',
                'coordinates': [
                  13.3262843,
                  52.5135435
                ]
              }
            },
            'description': 'loses Mobiliar im Foyer',
            'type': 'room',
            'categories': [
              'learn'
            ],
            'uid': '31b1ab4c-1803-51ed-8c66-59bfc97cd350',
            'name': 'MA Foyer',
            'floor': '0',
            'origin': {
              'indexed': '2018-09-11T12:30:00Z',
              'name': 'Dummy',
              'type': 'remote'
            }
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '65A6C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32610647020726,
                52.513824748529174
              ],
              [
                13.326107422375342,
                52.513819221566855
              ],
              [
                13.32609774494591,
                52.51381860102125
              ],
              [
                13.326096792777848,
                52.5138241279836
              ],
              [
                13.32610647020726,
                52.513824748529174
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '65D15',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325368539189405,
                52.514008043395435
              ],
              [
                13.32526804292379,
                52.51400159926969
              ],
              [
                13.325263208792125,
                52.5140296592312
              ],
              [
                13.325363705056917,
                52.51403610335692
              ],
              [
                13.325368539189405,
                52.514008043395435
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '65D18',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325267518240157,
                52.51400027608261
              ],
              [
                13.325160770759446,
                52.513993431109114
              ],
              [
                13.325155497162237,
                52.5140240419762
              ],
              [
                13.325262244641985,
                52.514030886949705
              ],
              [
                13.325267518240157,
                52.51400027608261
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '65D19',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325159875591423,
                52.51399337370821
              ],
              [
                13.325110446326295,
                52.51399020415289
              ],
              [
                13.32510517272953,
                52.51402081502002
              ],
              [
                13.325154601994218,
                52.51402398457531
              ],
              [
                13.325159875591423,
                52.51399337370821
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '65F53',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326157380640069,
                52.514260955541815
              ],
              [
                13.326151657000095,
                52.5142635974583
              ],
              [
                13.326117413787296,
                52.51426140168171
              ],
              [
                13.326112715579862,
                52.51425809148539
              ],
              [
                13.326114180482621,
                52.514249588467045
              ],
              [
                13.326115645384807,
                52.51424108544873
              ],
              [
                13.326121369024005,
                52.51423844353221
              ],
              [
                13.326155612237041,
                52.514240639308795
              ],
              [
                13.326160310445244,
                52.51424394950516
              ],
              [
                13.326159160496987,
                52.514250624374554
              ],
              [
                13.326158845542944,
                52.51425245252347
              ],
              [
                13.326157380640069,
                52.514260955541815
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6D573',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32614004630669,
                52.51362766719533
              ],
              [
                13.326138691310078,
                52.51363553248808
              ],
              [
                13.326129386086059,
                52.51363493580945
              ],
              [
                13.326129041843602,
                52.513636934018976
              ],
              [
                13.326090555453401,
                52.5136344661571
              ],
              [
                13.326094217605332,
                52.513613208609065
              ],
              [
                13.326097344159445,
                52.51361340909303
              ],
              [
                13.32609822307539,
                52.513608307281466
              ],
              [
                13.326114004729746,
                52.513609319248125
              ],
              [
                13.326135350546839,
                52.51361068800542
              ],
              [
                13.326142146515078,
                52.51361547619703
              ],
              [
                13.32614004630669,
                52.51362766719533
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6DD8B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326047667591752,
                52.51405030060897
              ],
              [
                13.326041427824325,
                52.514049900795385
              ],
              [
                13.326041427731896,
                52.514049901331894
              ],
              [
                13.32603011240009,
                52.514049175759084
              ],
              [
                13.326025930421306,
                52.51404890779798
              ],
              [
                13.326029153183526,
                52.514030201114835
              ],
              [
                13.326050890176681,
                52.51403159495562
              ],
              [
                13.326047667591752,
                52.51405030060897
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6DD8C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326122940223872,
                52.51403277623846
              ],
              [
                13.326033610110015,
                52.5140270481256
              ],
              [
                13.32603507510662,
                52.51401854447352
              ],
              [
                13.326045496952666,
                52.51401921275336
              ],
              [
                13.32612440585099,
                52.514024268927834
              ],
              [
                13.326122940223872,
                52.51403277623846
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6DD8D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326119129295837,
                52.51405488394468
              ],
              [
                13.326113176171907,
                52.514054502212666
              ],
              [
                13.326113174657047,
                52.51405449798946
              ],
              [
                13.326111388041085,
                52.514054383511684
              ],
              [
                13.326111387379344,
                52.514054387352786
              ],
              [
                13.32609530795739,
                52.5140533562924
              ],
              [
                13.326095308488211,
                52.51405335321124
              ],
              [
                13.326086375403973,
                52.51405278082213
              ],
              [
                13.326077442504653,
                52.51405220844485
              ],
              [
                13.326077442119288,
                52.51405221068168
              ],
              [
                13.326061362508888,
                52.51405117960917
              ],
              [
                13.32606136276312,
                52.51405117813228
              ],
              [
                13.326053620762742,
                52.51405068206208
              ],
              [
                13.3260568408074,
                52.51403197524737
              ],
              [
                13.326122350091778,
                52.51403617276752
              ],
              [
                13.326119129295837,
                52.51405488394468
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6DD8E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326408949262904,
                52.514098276592016
              ],
              [
                13.32639726872303,
                52.514103668095046
              ],
              [
                13.326196041621102,
                52.51409076481766
              ],
              [
                13.326196188110279,
                52.514089914515814
              ],
              [
                13.32620117021658,
                52.51406099567408
              ],
              [
                13.326201315227648,
                52.514060153950794
              ],
              [
                13.326402542331381,
                52.51407305722822
              ],
              [
                13.326412130232999,
                52.51407981252646
              ],
              [
                13.32641053974828,
                52.51408904455923
              ],
              [
                13.326408949262904,
                52.514098276592016
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6DD8F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326110635144607,
                52.514104201594556
              ],
              [
                13.326045126394682,
                52.514100000978495
              ],
              [
                13.326048349156514,
                52.51408129434086
              ],
              [
                13.326054304497736,
                52.51408167621185
              ],
              [
                13.32610611596475,
                52.51408499851737
              ],
              [
                13.326113857907348,
                52.51408549495374
              ],
              [
                13.326110635144607,
                52.514104201594556
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6DD90',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32603917105409,
                52.51409961910431
              ],
              [
                13.326017434061368,
                52.51409822526354
              ],
              [
                13.326020656823603,
                52.51407951862271
              ],
              [
                13.326024840985939,
                52.51407978692369
              ],
              [
                13.326036156132588,
                52.51408051248636
              ],
              [
                13.326042393816008,
                52.51408091246598
              ],
              [
                13.32603917105409,
                52.51409961910431
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6DD91',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326108584294154,
                52.51411610582048
              ],
              [
                13.326029676028247,
                52.51411104598752
              ],
              [
                13.326019254182485,
                52.5141103777077
              ],
              [
                13.32602071907556,
                52.51410187468916
              ],
              [
                13.326110049187449,
                52.514107602801985
              ],
              [
                13.326108584294154,
                52.51411610582048
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '6DDDD',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325796823333762,
                52.51396024063275
              ],
              [
                13.325796662213856,
                52.51396117586482
              ],
              [
                13.32579502459447,
                52.51396107027695
              ],
              [
                13.32562768159503,
                52.51395033974076
              ],
              [
                13.325622468670028,
                52.51395000547755
              ],
              [
                13.325620830853712,
                52.513949901034735
              ],
              [
                13.325620830952353,
                52.51394990046222
              ],
              [
                13.325620992011572,
                52.513948965582145
              ],
              [
                13.325634600826746,
                52.51386997208753
              ],
              [
                13.325634800704314,
                52.51386881187945
              ],
              [
                13.32563812987282,
                52.513867275198876
              ],
              [
                13.325634644880559,
                52.513864819795415
              ],
              [
                13.325634816501555,
                52.513863823604794
              ],
              [
                13.325642729554307,
                52.5138601710907
              ],
              [
                13.325645526597025,
                52.51386035044542
              ],
              [
                13.32564853699824,
                52.51386247146855
              ],
              [
                13.32565186616672,
                52.51386093478798
              ],
              [
                13.32565330209694,
                52.513861026864085
              ],
              [
                13.325694468358225,
                52.51386366661377
              ],
              [
                13.325694358500463,
                52.51386430429345
              ],
              [
                13.325700313839286,
                52.513864686167665
              ],
              [
                13.325700423695944,
                52.51386404849447
              ],
              [
                13.325748066388925,
                52.51386710353934
              ],
              [
                13.325747956541168,
                52.513867741160915
              ],
              [
                13.325753911880403,
                52.51386812303513
              ],
              [
                13.325754021727045,
                52.513867485420036
              ],
              [
                13.325795188604262,
                52.51387012520888
              ],
              [
                13.325796624534235,
                52.51387021728822
              ],
              [
                13.325799357254718,
                52.513872142682004
              ],
              [
                13.325803024706898,
                52.51387044987018
              ],
              [
                13.325805821749189,
                52.51387062923114
              ],
              [
                13.325812317113423,
                52.513875205672015
              ],
              [
                13.325812145485562,
                52.51387620186223
              ],
              [
                13.325807899857624,
                52.51387816154663
              ],
              [
                13.32581063257805,
                52.51388008694039
              ],
              [
                13.325810432692595,
                52.51388124714795
              ],
              [
                13.32581043214997,
                52.51388124711316
              ],
              [
                13.325796823371588,
                52.51396024041222
              ],
              [
                13.325796823333762,
                52.51396024063275
              ]
            ]
          },
          'place': {
            'geo': {
              'point': {
                'type': 'Point',
                'coordinates': [
                  13.325708,
                  52.5138796
                ]
              }
            },
            'type': 'room',
            'categories': [
              'education'
            ],
            'uid': '56ce0744-ae39-5209-b18f-d3a6edcb8f0a',
            'name': 'MA 042',
            'floor': '0',
            'origin': {
              'indexed': '2018-09-11T12:30:00Z',
              'name': 'Dummy',
              'type': 'remote'
            }
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '70BB7',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325867954177527,
                52.513975805181126
              ],
              [
                13.325877355819038,
                52.51397640815371
              ],
              [
                13.325896114661633,
                52.513977611019186
              ],
              [
                13.325982467079223,
                52.513983148243746
              ],
              [
                13.32600122595533,
                52.513984350969864
              ],
              [
                13.32601062763847,
                52.513984953834125
              ],
              [
                13.326028347243309,
                52.51397677481258
              ],
              [
                13.32604244152589,
                52.51389496346191
              ],
              [
                13.326027839997334,
                52.513885043359736
              ],
              [
                13.326025138567106,
                52.51388487013599
              ],
              [
                13.326010101331969,
                52.513883905906866
              ],
              [
                13.325902905214122,
                52.51387703216831
              ],
              [
                13.325887867982413,
                52.51387606793604
              ],
              [
                13.325867446944743,
                52.51388407373608
              ],
              [
                13.325867012686677,
                52.51388659442586
              ],
              [
                13.325864529715838,
                52.51390100704058
              ],
              [
                13.32586341640811,
                52.513907469334946
              ],
              [
                13.325853409139294,
                52.513965557253314
              ],
              [
                13.325867954177527,
                52.513975805181126
              ]
            ]
          },
          'place': {
            'geo': {
              'point': {
                'type': 'Point',
                'coordinates': [
                  13.325944,
                  52.5138959
                ]
              }
            },
            'type': 'room',
            'categories': [
              'education'
            ],
            'uid': '96b7e555-1214-5ca8-8294-e8209ac04f5a',
            'name': 'MA 041',
            'floor': '0',
            'origin': {
              'indexed': '2018-09-11T12:30:00Z',
              'name': 'Dummy',
              'type': 'remote'
            }
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '70C60',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325931437520264,
                52.51357685363762
              ],
              [
                13.32600931657659,
                52.513582832024646
              ],
              [
                13.326009875049278,
                52.513579589366316
              ],
              [
                13.326037865149914,
                52.513581384175055
              ],
              [
                13.326037865149914,
                52.513581384175055
              ],
              [
                13.326055596714355,
                52.513548403076
              ],
              [
                13.326059891402805,
                52.51354977698632
              ],
              [
                13.326058877163806,
                52.513550870981284
              ],
              [
                13.326069200203813,
                52.51355405955309
              ],
              [
                13.326068124738436,
                52.51355527432339
              ],
              [
                13.326057780039655,
                52.513552054949514
              ],
              [
                13.326042629422473,
                52.513581689674425
              ],
              [
                13.326056426417571,
                52.51358257437887
              ],
              [
                13.326083054534417,
                52.51357028310861
              ],
              [
                13.326089209640044,
                52.513561786908376
              ],
              [
                13.32607999566598,
                52.51355900326153
              ],
              [
                13.3260811554484,
                52.513557752278246
              ],
              [
                13.326090299328195,
                52.513560580669505
              ],
              [
                13.326157189563386,
                52.51352968857315
              ],
              [
                13.326157189238572,
                52.513529688625844
              ],
              [
                13.326154655388706,
                52.51352477592061
              ],
              [
                13.326156555714455,
                52.51352363982022
              ],
              [
                13.326159494772781,
                52.513529314607545
              ],
              [
                13.326174955004484,
                52.51352688955799
              ],
              [
                13.326201993184222,
                52.51351538222424
              ],
              [
                13.326203350253751,
                52.51350748755359
              ],
              [
                13.326170280928089,
                52.51350750820152
              ],
              [
                13.326149399495819,
                52.513509765127715
              ],
              [
                13.32615273221812,
                52.51351622670991
              ],
              [
                13.326150423381883,
                52.51351657077765
              ],
              [
                13.32614806843563,
                52.51350990899259
              ],
              [
                13.326147070533505,
                52.51351007017434
              ],
              [
                13.326143622227832,
                52.51350784325961
              ],
              [
                13.326203850219908,
                52.51350476858697
              ],
              [
                13.326203821836899,
                52.51350476676848
              ],
              [
                13.3262081845243,
                52.51347944104427
              ],
              [
                13.326212800039466,
                52.51347973627347
              ],
              [
                13.326218104961601,
                52.51344894277329
              ],
              [
                13.326213518234862,
                52.513448648658766
              ],
              [
                13.326214723201032,
                52.51344383861279
              ],
              [
                13.32619630337871,
                52.51344338014589
              ],
              [
                13.32616205075095,
                52.5134452114212
              ],
              [
                13.326158710218527,
                52.51343581848163
              ],
              [
                13.326087336297213,
                52.513445306633635
              ],
              [
                13.326015954538285,
                52.513468841765665
              ],
              [
                13.325964968794622,
                52.513499963249686
              ],
              [
                13.32592878911265,
                52.51354155300742
              ],
              [
                13.325942245173344,
                52.51354406415593
              ],
              [
                13.325931437520264,
                52.51357685363762
              ]
            ]
          },
          'place': {
            'geo': {
              'point': {
                'type': 'Point',
                'coordinates': [
                  13.3260909,
                  52.5134807
                ]
              }
            },
            'type': 'room',
            'categories': [
              'education'
            ],
            'uid': 'ed50a94d-eb06-5d07-8058-dfbdeb6c3a0b',
            'name': 'MA 001',
            'floor': '0',
            'origin': {
              'indexed': '2018-09-11T12:30:00Z',
              'name': 'Dummy',
              'type': 'remote'
            }
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '77A92',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325090166659564,
                52.514071047633294
              ],
              [
                13.3250416602507,
                52.51406793725408
              ],
              [
                13.325050119994058,
                52.51401883232175
              ],
              [
                13.325098626403626,
                52.514021942700964
              ],
              [
                13.325090166659564,
                52.514071047633294
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '77D44',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326029307162523,
                52.514017074846386
              ],
              [
                13.32602362630272,
                52.51405004969336
              ],
              [
                13.326018792189071,
                52.51407810951403
              ],
              [
                13.32601328712302,
                52.51411006385766
              ],
              [
                13.32600761179871,
                52.51410969993892
              ],
              [
                13.325995831638554,
                52.51411351813832
              ],
              [
                13.32599322738652,
                52.5141133511459
              ],
              [
                13.325979753429538,
                52.51411248715557
              ],
              [
                13.325966279472818,
                52.51411162316526
              ],
              [
                13.325958611971757,
                52.51411113150217
              ],
              [
                13.325895903639912,
                52.51410028899733
              ],
              [
                13.325895903639912,
                52.51410028899733
              ],
              [
                13.325894750120987,
                52.514072906590705
              ],
              [
                13.325894750120987,
                52.514072906590705
              ],
              [
                13.325841915436873,
                52.51406433149142
              ],
              [
                13.325788328355806,
                52.51405755455173
              ],
              [
                13.325633984807904,
                52.51404643300916
              ],
              [
                13.325633984807904,
                52.51404643300916
              ],
              [
                13.325584903889402,
                52.51404504481958
              ],
              [
                13.325535799165458,
                52.51404514998604
              ],
              [
                13.325486809499273,
                52.51404674821115
              ],
              [
                13.325486809499273,
                52.51404674821115
              ],
              [
                13.325476591556317,
                52.51407268496999
              ],
              [
                13.325411284763998,
                52.51407534747914
              ],
              [
                13.325403766153464,
                52.51407486536306
              ],
              [
                13.325403473175033,
                52.51407656596675
              ],
              [
                13.32537652528451,
                52.51407483798632
              ],
              [
                13.325376818262932,
                52.51407313738263
              ],
              [
                13.325363046552267,
                52.514072254298775
              ],
              [
                13.325363837593855,
                52.514067662668765
              ],
              [
                13.325361306575674,
                52.514067500372185
              ],
              [
                13.325367774066851,
                52.51402995954512
              ],
              [
                13.325364796399484,
                52.51402976860805
              ],
              [
                13.325367447847906,
                52.51401437814435
              ],
              [
                13.32537042551529,
                52.514014569081404
              ],
              [
                13.32537689299081,
                52.51397702825397
              ],
              [
                13.325379275124746,
                52.51397718100362
              ],
              [
                13.325381486383705,
                52.513964345611136
              ],
              [
                13.325401529945516,
                52.51394804589665
              ],
              [
                13.3254124733584,
                52.51394874762111
              ],
              [
                13.325412839579737,
                52.51394662186273
              ],
              [
                13.325431598401149,
                52.51394782473517
              ],
              [
                13.325431232179897,
                52.51394995049301
              ],
              [
                13.325517584547786,
                52.51395548766541
              ],
              [
                13.32551795076865,
                52.51395336191017
              ],
              [
                13.32553670959293,
                52.51395456478263
              ],
              [
                13.32553634337216,
                52.51395669053732
              ],
              [
                13.32554728678719,
                52.513957392261815
              ],
              [
                13.325567401252775,
                52.51394810781111
              ],
              [
                13.32558444840371,
                52.51394920092539
              ],
              [
                13.325617723839631,
                52.513951334645675
              ],
              [
                13.325622190342251,
                52.513951621051156
              ],
              [
                13.325622468670028,
                52.51395000547755
              ],
              [
                13.32562768159503,
                52.51395033974076
              ],
              [
                13.325641227982652,
                52.513951208375936
              ],
              [
                13.325640949169488,
                52.51395282392306
              ],
              [
                13.325646904991258,
                52.513953205828116
              ],
              [
                13.325646904991572,
                52.51395320582626
              ],
              [
                13.325655613596972,
                52.51395376424827
              ],
              [
                13.32580538487233,
                52.513963368030915
              ],
              [
                13.325832488206403,
                52.51396510598326
              ],
              [
                13.325849535358822,
                52.51396619909823
              ],
              [
                13.325866046217264,
                52.51397783207483
              ],
              [
                13.32587698963745,
                52.51397853380003
              ],
              [
                13.325877355839939,
                52.51397640815505
              ],
              [
                13.325896114661633,
                52.513977611019186
              ],
              [
                13.325895748436743,
                52.51397973677362
              ],
              [
                13.32598210085428,
                52.513985273998195
              ],
              [
                13.325982467079223,
                52.513983148243746
              ],
              [
                13.32600122595533,
                52.513984350969864
              ],
              [
                13.326000859734073,
                52.51398647672452
              ],
              [
                13.326011803156215,
                52.51398717844973
              ],
              [
                13.326031213728342,
                52.51400600808189
              ],
              [
                13.326029307162523,
                52.514017074846386
              ]
            ]
          }
        }
      ]
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCFloor',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '3c56f5c4-006f-580e-8aff-9a9315f3fd81',
    'name': 'H_4',
    'type': 'floor',
    'inPlace': {
      'geo': {
        'point': {
          'type': 'Point',
          'coordinates': [
            13.32687,
            52.51211
          ]
        },
        'polygon': {
          'type': 'Polygon',
          'coordinates': [
            [
              [
                13.3254469,
                52.5123792
              ],
              [
                13.3256455,
                52.5123886
              ],
              [
                13.3256706,
                52.5122521
              ],
              [
                13.3274413,
                52.5123518
              ],
              [
                13.3274581,
                52.5126071
              ],
              [
                13.327813,
                52.5126284
              ],
              [
                13.3279179,
                52.5123973
              ],
              [
                13.3282613,
                52.5124033
              ],
              [
                13.3282347,
                52.5125532
              ],
              [
                13.328432,
                52.5125591
              ],
              [
                13.3285734,
                52.5118008
              ],
              [
                13.3283154,
                52.5117772
              ],
              [
                13.3282977,
                52.5118288
              ],
              [
                13.3258416,
                52.5116718
              ],
              [
                13.3258416,
                52.5116266
              ],
              [
                13.325554,
                52.5116065
              ],
              [
                13.3254469,
                52.5123792
              ]
            ]
          ]
        }
      },
      'type': 'building',
      'categories': [
        'education'
      ],
      'name': 'Hauptgebäude',
      'alternateNames': [
        'H'
      ],
      'uid': 'ebe95e80-f826-5a70-a844-436b561d5181',
      'address': {
        'addressCountry': 'Germany',
        'addressLocality': 'Berlin',
        'addressRegion': 'Berlin',
        'postalCode': '10623',
        'streetAddress': 'Straße des 17. Juni 135'
      },
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'floorName': '4',
    'plan': {
      'type': 'FeatureCollection',
      'crs': {
        'type': 'name',
        'properties': {
          'name': 'urn:ogc:def:crs:EPSG::3857'
        }
      },
      'features': [
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': 'FA30',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325708952557708,
                52.51213751938585
              ],
              [
                13.325687489715303,
                52.51213614314453
              ],
              [
                13.325665360785402,
                52.51213472419226
              ],
              [
                13.32567629749104,
                52.51207123499326
              ],
              [
                13.325678073726221,
                52.512071348889094
              ],
              [
                13.32569198756865,
                52.51207224107313
              ],
              [
                13.325695836078308,
                52.51207248784744
              ],
              [
                13.325692630899715,
                52.512091094403324
              ],
              [
                13.325716684085435,
                52.51209263674278
              ],
              [
                13.325708952557708,
                52.51213751938585
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '168C0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325796457385175,
                52.51205393359172
              ],
              [
                13.32577973116781,
                52.51205286107259
              ],
              [
                13.325763597029319,
                52.51205182651873
              ],
              [
                13.3256755253626,
                52.51204617918351
              ],
              [
                13.32567710449652,
                52.51203701205105
              ],
              [
                13.325679903157976,
                52.5120207653509
              ],
              [
                13.325681169590682,
                52.51201341349216
              ],
              [
                13.32570218837419,
                52.51201476125953
              ],
              [
                13.325716990334783,
                52.51201571039149
              ],
              [
                13.325728831903488,
                52.51201646969707
              ],
              [
                13.325743633864668,
                52.51201741882905
              ],
              [
                13.325755475433837,
                52.51201817813464
              ],
              [
                13.325770277395579,
                52.51201912726662
              ],
              [
                13.325782118965208,
                52.5120198865722
              ],
              [
                13.325796920927528,
                52.512020835704206
              ],
              [
                13.32580210161442,
                52.51202116790038
              ],
              [
                13.325796457385175,
                52.51205393359172
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '168CD',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32587638798708,
                52.51205905890447
              ],
              [
                13.325833166253032,
                52.512056287439044
              ],
              [
                13.325816588054401,
                52.51205522441122
              ],
              [
                13.325797937581422,
                52.51205402850491
              ],
              [
                13.325803581810685,
                52.51202126281356
              ],
              [
                13.325808762497617,
                52.51202159500977
              ],
              [
                13.325823564460519,
                52.51202254414174
              ],
              [
                13.32583525801143,
                52.51202329395604
              ],
              [
                13.325850059974902,
                52.51202424308804
              ],
              [
                13.325861753526269,
                52.51202499290231
              ],
              [
                13.325876703509957,
                52.51202595152564
              ],
              [
                13.325882032217095,
                52.51202629321315
              ],
              [
                13.32587638798708,
                52.51205905890447
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '168DA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325956318598298,
                52.512064184217316
              ],
              [
                13.325938852278755,
                52.51206306424155
              ],
              [
                13.325922422097214,
                52.51206201070502
              ],
              [
                13.325877868183495,
                52.51205915381768
              ],
              [
                13.325883512413522,
                52.51202638812637
              ],
              [
                13.325888545081424,
                52.51202671083123
              ],
              [
                13.32590334704604,
                52.51202765996325
              ],
              [
                13.325915188617966,
                52.51202841926885
              ],
              [
                13.325929990583168,
                52.51202936840087
              ],
              [
                13.32594183215555,
                52.512030127706495
              ],
              [
                13.32595663412133,
                52.512031076838504
              ],
              [
                13.325961962829078,
                52.512031418526014
              ],
              [
                13.325956318598298,
                52.512064184217316
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '168E7',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326036249218841,
                52.51206930953027
              ],
              [
                13.325992287376387,
                52.512066490608156
              ],
              [
                13.325975857193573,
                52.512065437071605
              ],
              [
                13.32595779879489,
                52.512064279130534
              ],
              [
                13.325963443025685,
                52.51203151343923
              ],
              [
                13.32596847569417,
                52.51203183614412
              ],
              [
                13.325983277660526,
                52.512032785276155
              ],
              [
                13.325995119233827,
                52.51203354458176
              ],
              [
                13.326009921200752,
                52.5120344937138
              ],
              [
                13.326021762774515,
                52.51203525301942
              ],
              [
                13.326036564742013,
                52.512036202151464
              ],
              [
                13.326041893450396,
                52.512036543839
              ],
              [
                13.326036249218841,
                52.51206930953027
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '168F4',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326116179848707,
                52.51207443484333
              ],
              [
                13.326098565505399,
                52.512073305376184
              ],
              [
                13.326082135320028,
                52.51207225183961
              ],
              [
                13.326037729415608,
                52.51206940444348
              ],
              [
                13.326043373647172,
                52.5120366387522
              ],
              [
                13.32604840631625,
                52.51203696145711
              ],
              [
                13.326063208284321,
                52.512037910589136
              ],
              [
                13.326075049859007,
                52.51203866989479
              ],
              [
                13.32608985182765,
                52.51203961902682
              ],
              [
                13.326101693402798,
                52.512040378332465
              ],
              [
                13.326116495372027,
                52.51204132746451
              ],
              [
                13.326121824081019,
                52.512041669152055
              ],
              [
                13.326116179848707,
                52.51207443484333
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16901',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326196110487883,
                52.51207956015645
              ],
              [
                13.326140899137977,
                52.51207601989386
              ],
              [
                13.326124468951594,
                52.51207496635727
              ],
              [
                13.326117660045645,
                52.51207452975654
              ],
              [
                13.32612330427797,
                52.512041764065266
              ],
              [
                13.326128336947631,
                52.51204208677017
              ],
              [
                13.326143138917438,
                52.51204303590222
              ],
              [
                13.326154980493504,
                52.51204379520787
              ],
              [
                13.326169782463873,
                52.512044744339946
              ],
              [
                13.326181624040402,
                52.51204550364562
              ],
              [
                13.326196426011359,
                52.51204645277768
              ],
              [
                13.326201754720977,
                52.51204679446521
              ],
              [
                13.326196110487883,
                52.51207956015645
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1690E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326276041136387,
                52.51208468546969
              ],
              [
                13.326256650552288,
                52.51208344210666
              ],
              [
                13.326240220363134,
                52.51208238857004
              ],
              [
                13.326197590685,
                52.51207965506968
              ],
              [
                13.326203234918097,
                52.51204688937844
              ],
              [
                13.326208267588344,
                52.512047212083345
              ],
              [
                13.32622306955987,
                52.5120481612154
              ],
              [
                13.326234911137322,
                52.51204892052108
              ],
              [
                13.326249713109418,
                52.512049869653175
              ],
              [
                13.326261554687333,
                52.512050628958825
              ],
              [
                13.326276356660006,
                52.51205157809093
              ],
              [
                13.32628168537024,
                52.51205191977847
              ],
              [
                13.326276041136387,
                52.51208468546969
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1691B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32635597179421,
                52.51208981078302
              ],
              [
                13.32630875349708,
                52.51208678305163
              ],
              [
                13.326292323306676,
                52.512085729514986
              ],
              [
                13.32627752133367,
                52.51208478038291
              ],
              [
                13.32628316556754,
                52.51205201469168
              ],
              [
                13.32628819823837,
                52.51205233739659
              ],
              [
                13.326303000211622,
                52.51205328652868
              ],
              [
                13.32631484179045,
                52.51205404583438
              ],
              [
                13.326329643764282,
                52.51205499496647
              ],
              [
                13.326341485343567,
                52.51205575427216
              ],
              [
                13.326356287317976,
                52.51205670340427
              ],
              [
                13.326361616028832,
                52.51205704509182
              ],
              [
                13.32635597179421,
                52.51208981078302
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16928',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326435902461355,
                52.512094936096446
              ],
              [
                13.326406002469968,
                52.51209301884956
              ],
              [
                13.326389720296984,
                52.51209197480424
              ],
              [
                13.326357451991669,
                52.512089905696236
              ],
              [
                13.3263630962263,
                52.51205714000504
              ],
              [
                13.326368128897721,
                52.51205746270996
              ],
              [
                13.326382930872699,
                52.51205841184206
              ],
              [
                13.326394772452906,
                52.512059171147776
              ],
              [
                13.326409574428462,
                52.51206012027986
              ],
              [
                13.32642141600913,
                52.51206087958557
              ],
              [
                13.326436217985256,
                52.51206182871772
              ],
              [
                13.326441546696739,
                52.512062170405265
              ],
              [
                13.326435902461355,
                52.512094936096446
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16933',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326487857399995,
                52.51209826755023
              ],
              [
                13.326473203442541,
                52.51209732790942
              ],
              [
                13.326458401465642,
                52.51209637877729
              ],
              [
                13.326437382658984,
                52.512095031009665
              ],
              [
                13.326443026894388,
                52.512062265318484
              ],
              [
                13.326448059566387,
                52.512062588023404
              ],
              [
                13.326462565503551,
                52.51206351817289
              ],
              [
                13.326471594709501,
                52.51206409714353
              ],
              [
                13.326486248666935,
                52.51206503678431
              ],
              [
                13.326493501635884,
                52.51206550185906
              ],
              [
                13.326487857399995,
                52.51209826755023
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16949',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326617982816137,
                52.5120926170984
              ],
              [
                13.326616888365434,
                52.51209897055649
              ],
              [
                13.32661618478982,
                52.51210305492243
              ],
              [
                13.326611152116584,
                52.51210273221749
              ],
              [
                13.326610565803469,
                52.512106135855745
              ],
              [
                13.326519385612546,
                52.51210028920168
              ],
              [
                13.326504583634641,
                52.51209934006955
              ],
              [
                13.326489337597735,
                52.51209836246344
              ],
              [
                13.326494981833642,
                52.51206559677227
              ],
              [
                13.3264970541105,
                52.512065729650786
              ],
              [
                13.326511856088258,
                52.512066678782915
              ],
              [
                13.326523697670702,
                52.51206743808864
              ],
              [
                13.326538499649034,
                52.512068387220786
              ],
              [
                13.326550341231926,
                52.512069146526514
              ],
              [
                13.326565143210832,
                52.51207009565865
              ],
              [
                13.3265769847942,
                52.5120708549644
              ],
              [
                13.326591786773681,
                52.512071804096564
              ],
              [
                13.326616210040534,
                52.51207337016463
              ],
              [
                13.326615608093222,
                52.512076864566595
              ],
              [
                13.326620640766487,
                52.51207718727154
              ],
              [
                13.326619546316566,
                52.512083540729655
              ],
              [
                13.326617982816137,
                52.5120926170984
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16950',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326597615856604,
                52.51212875175282
              ],
              [
                13.326561499026992,
                52.51212643587035
              ],
              [
                13.32656168664739,
                52.5121253467061
              ],
              [
                13.32656326578529,
                52.51211617957373
              ],
              [
                13.326564360236919,
                52.51210982611565
              ],
              [
                13.3266004770667,
                52.512112141998095
              ],
              [
                13.326597615856604,
                52.51212875175282
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '169EE',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326585639401694,
                52.512198276736456
              ],
              [
                13.326585639401694,
                52.512198276736456
              ],
              [
                13.326585639401694,
                52.512198276736456
              ],
              [
                13.326521102774391,
                52.51219413852031
              ],
              [
                13.326521102774391,
                52.51219413852031
              ],
              [
                13.326521102774391,
                52.51219413852031
              ],
              [
                13.326532836884983,
                52.51212602037378
              ],
              [
                13.326533576983897,
                52.512126067830366
              ],
              [
                13.326548378962414,
                52.51212701696253
              ],
              [
                13.326558000248616,
                52.51212763389842
              ],
              [
                13.32655797679607,
                52.51212777004395
              ],
              [
                13.326597350061023,
                52.51213029473548
              ],
              [
                13.326594066702265,
                52.51214935510966
              ],
              [
                13.326611496033522,
                52.51215047271279
              ],
              [
                13.32661040939746,
                52.51215678078899
              ],
              [
                13.32660884589179,
                52.51216585715761
              ],
              [
                13.32660871299378,
                52.512166628648956
              ],
              [
                13.326591283662609,
                52.51216551104584
              ],
              [
                13.326585639401694,
                52.512198276736456
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '169FC',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326637593880807,
                52.51214948493734
              ],
              [
                13.326635077544111,
                52.51214932358488
              ],
              [
                13.326623235959785,
                52.51214856427915
              ],
              [
                13.326603993385687,
                52.51214733040733
              ],
              [
                13.326605650700298,
                52.51213770945656
              ],
              [
                13.326603430403315,
                52.51213756708673
              ],
              [
                13.326604759381478,
                52.512129852173366
              ],
              [
                13.326640580173752,
                52.51213214907321
              ],
              [
                13.326637593880807,
                52.51214948493734
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A1E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326672156506358,
                52.51215170116096
              ],
              [
                13.326653209970507,
                52.51215048627178
              ],
              [
                13.326641072346256,
                52.51214970798341
              ],
              [
                13.326639296108576,
                52.512149594087546
              ],
              [
                13.326640738441464,
                52.51214122113747
              ],
              [
                13.326673598839315,
                52.5121433282109
              ],
              [
                13.326672156506358,
                52.51215170116096
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A28',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326690732993177,
                52.51215289232184
              ],
              [
                13.326689400814825,
                52.51215280689994
              ],
              [
                13.326675634971975,
                52.51215192420703
              ],
              [
                13.326673858734216,
                52.512151810311146
              ],
              [
                13.326675480870167,
                52.51214239357868
              ],
              [
                13.32664091824444,
                52.51214017735506
              ],
              [
                13.326642282401528,
                52.5121322582234
              ],
              [
                13.326657824481444,
                52.51213325481218
              ],
              [
                13.326693719286398,
                52.51213555645772
              ],
              [
                13.326690732993177,
                52.51215289232184
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A2F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325716863888225,
                52.512091592960324
              ],
              [
                13.325694512927916,
                52.512090159771084
              ],
              [
                13.325697538303741,
                52.51207259699762
              ],
              [
                13.325699314538982,
                52.512072710893456
              ],
              [
                13.325710416009326,
                52.512073422742425
              ],
              [
                13.325719889264162,
                52.5120740301869
              ],
              [
                13.325716863888225,
                52.512091592960324
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A39',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32664156806156,
                52.51221415966838
              ],
              [
                13.326590797269859,
                52.51221090414508
              ],
              [
                13.326597833054858,
                52.5121700604865
              ],
              [
                13.326603013747777,
                52.512170392682734
              ],
              [
                13.326603498434713,
                52.512167579008455
              ],
              [
                13.326617856355313,
                52.51216849966665
              ],
              [
                13.32663428655347,
                52.51216955320334
              ],
              [
                13.326649088534136,
                52.512170502335515
              ],
              [
                13.32664156806156,
                52.51221415966838
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A44',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326695949650418,
                52.512211223086204
              ],
              [
                13.326692989254068,
                52.51221103325977
              ],
              [
                13.326691894797573,
                52.512217386717744
              ],
              [
                13.32664304825963,
                52.51221425458161
              ],
              [
                13.326650568732218,
                52.51217059724871
              ],
              [
                13.326663002396252,
                52.51217139451974
              ],
              [
                13.32667950660541,
                52.51217245280212
              ],
              [
                13.326699119231149,
                52.51217371040225
              ],
              [
                13.326697758980346,
                52.51218160684294
              ],
              [
                13.326701015416369,
                52.51218181565201
              ],
              [
                13.326695949650418,
                52.512211223086204
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A53',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326751910386815,
                52.512199119371815
              ],
              [
                13.326748142330421,
                52.51222099342003
              ],
              [
                13.326697815590286,
                52.512217766370625
              ],
              [
                13.326698910046794,
                52.51221141291261
              ],
              [
                13.326697429848606,
                52.51221131799942
              ],
              [
                13.326702495614569,
                52.51218191056524
              ],
              [
                13.326703087693838,
                52.51218194853052
              ],
              [
                13.326703267497146,
                52.51218090474814
              ],
              [
                13.326699641011567,
                52.51218067221076
              ],
              [
                13.326700821459074,
                52.51217381955245
              ],
              [
                13.326716215520468,
                52.512174806649924
              ],
              [
                13.326732645720988,
                52.51217586018665
              ],
              [
                13.326755662804358,
                52.5121773360872
              ],
              [
                13.326753473894577,
                52.51219004300324
              ],
              [
                13.326751910386815,
                52.512199119371815
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A60',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326802523927771,
                52.51221805683795
              ],
              [
                13.32679956353095,
                52.51221786701153
              ],
              [
                13.326798469074266,
                52.512224220469484
              ],
              [
                13.32674962252873,
                52.51222108833326
              ],
              [
                13.326753390585118,
                52.51219921428503
              ],
              [
                13.326754954092893,
                52.51219013791646
              ],
              [
                13.326757143002679,
                52.51217743100041
              ],
              [
                13.326768910579387,
                52.51217818556052
              ],
              [
                13.326785340781177,
                52.51217923909725
              ],
              [
                13.326805693509149,
                52.51218054415401
              ],
              [
                13.326804333258108,
                52.51218844059473
              ],
              [
                13.326807589694633,
                52.51218864940379
              ],
              [
                13.326802523927771,
                52.51221805683795
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A6F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326909098221684,
                52.51222489058987
              ],
              [
                13.326906877923728,
                52.51222474822002
              ],
              [
                13.326905783466835,
                52.51223110167802
              ],
              [
                13.326856196814377,
                52.512227922085074
              ],
              [
                13.326858471720664,
                52.51221471596885
              ],
              [
                13.326861528379514,
                52.51219697166832
              ],
              [
                13.326862357038523,
                52.51219216119295
              ],
              [
                13.326862357038523,
                52.51219216119295
              ],
              [
                13.326862622834767,
                52.512190618210305
              ],
              [
                13.326863717289704,
                52.51218426475229
              ],
              [
                13.326863717289704,
                52.51218426475229
              ],
              [
                13.326863717289704,
                52.51218426475229
              ],
              [
                13.326863717289704,
                52.51218426475229
              ],
              [
                13.326915524240762,
                52.512187586715065
              ],
              [
                13.326909098221684,
                52.51222489058987
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A94',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326914474006504,
                52.51218499580046
              ],
              [
                13.326914395831164,
                52.5121854496189
              ],
              [
                13.326899815874556,
                52.51218451472367
              ],
              [
                13.326899643888813,
                52.512185513124194
              ],
              [
                13.326838511688385,
                52.51218159320816
              ],
              [
                13.326822081485325,
                52.51218053967139
              ],
              [
                13.32678552058443,
                52.51217819531487
              ],
              [
                13.326769090382642,
                52.512177141778125
              ],
              [
                13.326732825524227,
                52.51217481640427
              ],
              [
                13.3267163953237,
                52.51217376286753
              ],
              [
                13.326679686408637,
                52.512171409019736
              ],
              [
                13.326663182199471,
                52.51217035073738
              ],
              [
                13.326634466356678,
                52.51216850942097
              ],
              [
                13.326618036158518,
                52.51216745588427
              ],
              [
                13.326610043089275,
                52.51216694335289
              ],
              [
                13.326610293250235,
                52.51216549113389
              ],
              [
                13.326611778580617,
                52.512156868583716
              ],
              [
                13.32661313883003,
                52.512148972142995
              ],
              [
                13.326623056156741,
                52.512149608061534
              ],
              [
                13.326634897741059,
                52.51215036736727
              ],
              [
                13.326640892543207,
                52.512150751765795
              ],
              [
                13.326653030167451,
                52.51215153005417
              ],
              [
                13.326675455168907,
                52.51215296798944
              ],
              [
                13.326689221011756,
                52.51215385068234
              ],
              [
                13.326710831905384,
                52.51215523641534
              ],
              [
                13.326722969530868,
                52.5121560147037
              ],
              [
                13.326734219037615,
                52.51215673604419
              ],
              [
                13.326746060623854,
                52.51215749534994
              ],
              [
                13.326838128963873,
                52.512163398952225
              ],
              [
                13.326849970551901,
                52.51216415825799
              ],
              [
                13.326885051257664,
                52.51216640770134
              ],
              [
                13.326897188886237,
                52.51216718598975
              ],
              [
                13.326902739631086,
                52.512167541914344
              ],
              [
                13.326902552010413,
                52.51216863107861
              ],
              [
                13.326917131967077,
                52.512169565973835
              ],
              [
                13.32691603751295,
                52.512175919431876
              ],
              [
                13.326914474006504,
                52.51218499580046
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16AA1',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326730920374784,
                52.51215546921573
              ],
              [
                13.326723149333953,
                52.51215497092134
              ],
              [
                13.326711011708467,
                52.51215419263293
              ],
              [
                13.326709531510245,
                52.51215409771971
              ],
              [
                13.326710063101896,
                52.512151011754376
              ],
              [
                13.32671115755506,
                52.51214465829633
              ],
              [
                13.326703756564006,
                52.51214418373022
              ],
              [
                13.326702662110861,
                52.512150537188276
              ],
              [
                13.326702130519212,
                52.51215362315362
              ],
              [
                13.326692435221085,
                52.51215300147205
              ],
              [
                13.326695734215026,
                52.51213385033417
              ],
              [
                13.326734219368937,
                52.51213631807786
              ],
              [
                13.326730920374784,
                52.51215546921573
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16AA8',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326762818648033,
                52.51215751459559
              ],
              [
                13.326746240426942,
                52.512156451567535
              ],
              [
                13.3267343988407,
                52.51215569226178
              ],
              [
                13.326732622602782,
                52.51215557836593
              ],
              [
                13.326735608896204,
                52.5121382425018
              ],
              [
                13.326765804941617,
                52.51214017873147
              ],
              [
                13.326762818648033,
                52.51215751459559
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16B03',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326901111865517,
                52.51216005035682
              ],
              [
                13.326911621275864,
                52.5121607242407
              ],
              [
                13.326910542457217,
                52.51216698693506
              ],
              [
                13.32689736868938,
                52.512166142207384
              ],
              [
                13.326885231060794,
                52.512165363918946
              ],
              [
                13.32687205729344,
                52.51216451919127
              ],
              [
                13.326873136112011,
                52.512158256496924
              ],
              [
                13.326882757402771,
                52.512158873432874
              ],
              [
                13.326884711783487,
                52.51214752797207
              ],
              [
                13.326879309058643,
                52.51214718153881
              ],
              [
                13.326873388264335,
                52.51214680188591
              ],
              [
                13.326869761777855,
                52.512146569348516
              ],
              [
                13.326871575442205,
                52.512136040760865
              ],
              [
                13.32691982991715,
                52.51213913493191
              ],
              [
                13.32691801625265,
                52.51214966351958
              ],
              [
                13.326914463775866,
                52.512149435727835
              ],
              [
                13.326908542981261,
                52.512149056074946
              ],
              [
                13.326903066246294,
                52.51214870489603
              ],
              [
                13.326901111865517,
                52.51216005035682
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16B22',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.3269656948796,
                52.51221778302792
              ],
              [
                13.326963862019197,
                52.51222840215119
              ],
              [
                13.326962771116607,
                52.51223475583706
              ],
              [
                13.3269117042614,
                52.51223148133091
              ],
              [
                13.326912798718293,
                52.51222512787292
              ],
              [
                13.326910578420327,
                52.51222498550308
              ],
              [
                13.326916113240355,
                52.51219285515837
              ],
              [
                13.326938316220595,
                52.51219427885671
              ],
              [
                13.326954598406562,
                52.512195322902166
              ],
              [
                13.326969400394143,
                52.512196272034416
              ],
              [
                13.326968219945812,
                52.5122031246927
              ],
              [
                13.326968219945812,
                52.5122031246927
              ],
              [
                13.326968040142392,
                52.51220416847505
              ],
              [
                13.326968040142392,
                52.51220416847505
              ],
              [
                13.3269656948796,
                52.51221778302792
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16B54',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327076555778465,
                52.51219538879854
              ],
              [
                13.327075820929997,
                52.51219965469176
              ],
              [
                13.327072416472301,
                52.512199436391356
              ],
              [
                13.327072056865559,
                52.512201523956115
              ],
              [
                13.327060955373186,
                52.51220081210694
              ],
              [
                13.327044377144903,
                52.51219974907879
              ],
              [
                13.32695482511513,
                52.5121940068287
              ],
              [
                13.326938542929161,
                52.51219296278325
              ],
              [
                13.32691974440584,
                52.51219175738533
              ],
              [
                13.326920104012482,
                52.512189669820565
              ],
              [
                13.326916699555554,
                52.51218945152018
              ],
              [
                13.326917356228472,
                52.51218563944535
              ],
              [
                13.326915876029817,
                52.51218554453212
              ],
              [
                13.326915954205157,
                52.51218509071369
              ],
              [
                13.326917517711616,
                52.5121760143451
              ],
              [
                13.326918612165745,
                52.512169660887054
              ],
              [
                13.32692157256308,
                52.512169850713505
              ],
              [
                13.326921760183762,
                52.512168761549276
              ],
              [
                13.326923388402303,
                52.51216886595381
              ],
              [
                13.326924811192141,
                52.51216060645836
              ],
              [
                13.326937170851226,
                52.51216139898377
              ],
              [
                13.326950492639718,
                52.51216225320278
              ],
              [
                13.326969365173847,
                52.5121634633464
              ],
              [
                13.326982686962959,
                52.51216431756542
              ],
              [
                13.327001559497972,
                52.51216552770904
              ],
              [
                13.327014881287713,
                52.51216638192805
              ],
              [
                13.327021172132957,
                52.512166785309255
              ],
              [
                13.327019929146047,
                52.512174001022295
              ],
              [
                13.327031474697453,
                52.512174741345454
              ],
              [
                13.32703271768437,
                52.5121675256324
              ],
              [
                13.327046631554285,
                52.512168417816724
              ],
              [
                13.327064245921935,
                52.5121695472841
              ],
              [
                13.327075051374582,
                52.51217024015068
              ],
              [
                13.327073628584378,
                52.512178499646105
              ],
              [
                13.327079401360507,
                52.51217886980769
              ],
              [
                13.32707811928534,
                52.51218631242995
              ],
              [
                13.327076555778465,
                52.51219538879854
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16B62',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32712298695877,
                52.51223860555081
              ],
              [
                13.327122246859217,
                52.512238558094204
              ],
              [
                13.327121152401922,
                52.51224491155215
              ],
              [
                13.327070085534901,
                52.512241637045854
              ],
              [
                13.327077606012969,
                52.51219797971312
              ],
              [
                13.327099586968577,
                52.51219938917455
              ],
              [
                13.327116017178291,
                52.51220044271136
              ],
              [
                13.327131115209182,
                52.51220141082628
              ],
              [
                13.327129762774922,
                52.5122092618851
              ],
              [
                13.327125544207453,
                52.512208991382415
              ],
              [
                13.327125364404017,
                52.51221003516479
              ],
              [
                13.327127880742507,
                52.51221019651728
              ],
              [
                13.32712631723402,
                52.51221927288583
              ],
              [
                13.32712298695877,
                52.51223860555081
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16B72',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327192942061993,
                52.51224951484371
              ],
              [
                13.327127073198348,
                52.51224529120506
              ],
              [
                13.327128167655653,
                52.512238937747114
              ],
              [
                13.327124467157875,
                52.51223870046404
              ],
              [
                13.327127797433125,
                52.512219367799055
              ],
              [
                13.327129360941615,
                52.51221029143052
              ],
              [
                13.327131285200467,
                52.51221041481769
              ],
              [
                13.327132035684325,
                52.512206058160785
              ],
              [
                13.327137216381281,
                52.512206390357086
              ],
              [
                13.327137998135145,
                52.51220185217279
              ],
              [
                13.327153910276062,
                52.51220287249
              ],
              [
                13.327170488506994,
                52.51220393551814
              ],
              [
                13.327200462541624,
                52.512205857511034
              ],
              [
                13.327198273630183,
                52.512218564427016
              ],
              [
                13.32719671012123,
                52.51222764079556
              ],
              [
                13.327192942061993,
                52.51224951484371
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16B87',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327240883697124,
                52.51217862536643
              ],
              [
                13.327239588522689,
                52.512178542317365
              ],
              [
                13.327238494068547,
                52.5121848957754
              ],
              [
                13.327237962476413,
                52.5121879817407
              ],
              [
                13.327235002077709,
                52.51218779191426
              ],
              [
                13.327223160483022,
                52.51218703260842
              ],
              [
                13.3271893749343,
                52.51218486621397
              ],
              [
                13.327192673931032,
                52.51216571507616
              ],
              [
                13.327242556647859,
                52.512168913652
              ],
              [
                13.327240883697124,
                52.51217862536643
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16B91',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327262052721345,
                52.512189526453554
              ],
              [
                13.32726053551696,
                52.512189429167485
              ],
              [
                13.327248323871986,
                52.512188646133325
              ],
              [
                13.327245363473235,
                52.512188456306866
              ],
              [
                13.327245895065362,
                52.51218537034155
              ],
              [
                13.327246989519526,
                52.51217901688352
              ],
              [
                13.327242585926394,
                52.51217873451666
              ],
              [
                13.327244258877133,
                52.51216902280221
              ],
              [
                13.327265351718482,
                52.51217037531575
              ],
              [
                13.327262052721345,
                52.512189526453554
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16BA0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327341853101439,
                52.51218454912103
              ],
              [
                13.327338152602499,
                52.51218431183794
              ],
              [
                13.327336432745595,
                52.5121942958434
              ],
              [
                13.32731345264769,
                52.51219282231548
              ],
              [
                13.327301833081572,
                52.512192077246624
              ],
              [
                13.327298317607784,
                52.51219185182769
              ],
              [
                13.327299939745458,
                52.512182435095255
              ],
              [
                13.327265377088253,
                52.512180218871315
              ],
              [
                13.327267053947818,
                52.51217048446597
              ],
              [
                13.32726883018712,
                52.51217059836185
              ],
              [
                13.327280819802555,
                52.512171367159006
              ],
              [
                13.327282596041893,
                52.512171481054885
              ],
              [
                13.327282345881063,
                52.51217293327389
              ],
              [
                13.327343182081245,
                52.51217683420769
              ],
              [
                13.327341853101439,
                52.51218454912103
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16BA7',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32729661537838,
                52.512191742677466
              ],
              [
                13.327295172183886,
                52.512191650137076
              ],
              [
                13.327283034548264,
                52.51219087184859
              ],
              [
                13.327263754950666,
                52.51218963560376
              ],
              [
                13.327265197285078,
                52.512181262653726
              ],
              [
                13.327298057712872,
                52.51218336972745
              ],
              [
                13.32729661537838,
                52.512191742677466
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16BC6',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327330928024788,
                52.51221169963694
              ],
              [
                13.327330677863511,
                52.512213151855924
              ],
              [
                13.327280203060573,
                52.51220991531475
              ],
              [
                13.327263624826982,
                52.51220885228656
              ],
              [
                13.32722706390254,
                52.51220650792978
              ],
              [
                13.327210559680202,
                52.512205449647276
              ],
              [
                13.327170668310371,
                52.51220289173576
              ],
              [
                13.327154090079432,
                52.51220182870761
              ],
              [
                13.327116196981649,
                52.512199398928985
              ],
              [
                13.327099766771937,
                52.51219834539217
              ],
              [
                13.327092365776693,
                52.512197870826014
              ],
              [
                13.327092537762502,
                52.512196872425484
              ],
              [
                13.32707795780211,
                52.51219593753021
              ],
              [
                13.327078035977474,
                52.512195483711764
              ],
              [
                13.327079599484353,
                52.51218640734318
              ],
              [
                13.327080693938798,
                52.51218005388515
              ],
              [
                13.327095273899252,
                52.51218098878044
              ],
              [
                13.327095461519988,
                52.5121798996162
              ],
              [
                13.327158147952515,
                52.51218391919139
              ],
              [
                13.32716998954607,
                52.512184678497206
              ],
              [
                13.327222980679792,
                52.51218807639081
              ],
              [
                13.327234822274475,
                52.51218883569664
              ],
              [
                13.327248144068744,
                52.51218968991573
              ],
              [
                13.327260355713712,
                52.51219047294986
              ],
              [
                13.32728285474501,
                52.512191915630986
              ],
              [
                13.327294992380624,
                52.51219269391948
              ],
              [
                13.327301653278317,
                52.51219312102899
              ],
              [
                13.327313272844425,
                52.512193866097896
              ],
              [
                13.327333773608057,
                52.51219518064613
              ],
              [
                13.327332413356986,
                52.5122030770868
              ],
              [
                13.327330928024788,
                52.51221169963694
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16BDB',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327249997296791,
                52.5122310576718
              ],
              [
                13.327246229237216,
                52.51225293171994
              ],
              [
                13.327233647542691,
                52.512252124957506
              ],
              [
                13.327234742000194,
                52.51224577149955
              ],
              [
                13.327228821202837,
                52.51224539184663
              ],
              [
                13.327227726745349,
                52.512251745304575
              ],
              [
                13.32719442226125,
                52.512249609756935
              ],
              [
                13.327198190320498,
                52.51222773570879
              ],
              [
                13.327199753829449,
                52.51221865934024
              ],
              [
                13.327201942740897,
                52.512205952424246
              ],
              [
                13.32721037987682,
                52.51220649342965
              ],
              [
                13.327226884099156,
                52.512207551712166
              ],
              [
                13.327239465793571,
                52.51220835847462
              ],
              [
                13.327238238439737,
                52.51221548342396
              ],
              [
                13.327234167891518,
                52.512215222412586
              ],
              [
                13.327233988088063,
                52.51221626619495
              ],
              [
                13.327239760865531,
                52.51221663635653
              ],
              [
                13.32724116802284,
                52.512208467624845
              ],
              [
                13.327253749717519,
                52.51220927438728
              ],
              [
                13.32725156080588,
                52.51222198130328
              ],
              [
                13.327249997296791,
                52.5122310576718
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16BEA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327333857045476,
                52.5122585505832
              ],
              [
                13.327247709436588,
                52.51225302663318
              ],
              [
                13.327251477496175,
                52.512231152585045
              ],
              [
                13.32725304100527,
                52.51222207621651
              ],
              [
                13.327255229916906,
                52.512209369300514
              ],
              [
                13.327263445023581,
                52.51220989606894
              ],
              [
                13.327280023257167,
                52.51221095909712
              ],
              [
                13.32731887849369,
                52.51221345056944
              ],
              [
                13.327318096739596,
                52.51221798875373
              ],
              [
                13.327319798969057,
                52.51221809790395
              ],
              [
                13.327320580723157,
                52.51221355971965
              ],
              [
                13.3273368629182,
                52.512214603765216
              ],
              [
                13.327336315690335,
                52.5122177804942
              ],
              [
                13.327340830299033,
                52.51221806997955
              ],
              [
                13.327333857045476,
                52.5122585505832
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16C01',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327408606580892,
                52.512251184163254
              ],
              [
                13.327408606580892,
                52.512251184163254
              ],
              [
                13.327408606580892,
                52.512251184163254
              ],
              [
                13.3273418495228,
                52.51224690388965
              ],
              [
                13.3273418495228,
                52.51224690388965
              ],
              [
                13.3273418495228,
                52.51224690388965
              ],
              [
                13.327347650114543,
                52.51221323072058
              ],
              [
                13.327332330048952,
                52.51221224836861
              ],
              [
                13.327332408224343,
                52.512211794550176
              ],
              [
                13.327333893556544,
                52.51220317200004
              ],
              [
                13.327335066186809,
                52.512196364723614
              ],
              [
                13.327350830312357,
                52.51219737554953
              ],
              [
                13.327356818538735,
                52.51216261305772
              ],
              [
                13.327365403696557,
                52.51216316355447
              ],
              [
                13.327380057673073,
                52.51216410319548
              ],
              [
                13.327404480967967,
                52.51216566926382
              ],
              [
                13.327418394845449,
                52.51216656144823
              ],
              [
                13.327440005762092,
                52.51216794718145
              ],
              [
                13.327439849411638,
                52.512168854818306
              ],
              [
                13.327438285906684,
                52.51217793118694
              ],
              [
                13.327437074189902,
                52.51218496537261
              ],
              [
                13.327420199912554,
                52.51218388336174
              ],
              [
                13.327408606580892,
                52.512251184163254
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16C13',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327423311287667,
                52.512165821388194
              ],
              [
                13.327418574648464,
                52.51216551766585
              ],
              [
                13.327404660770975,
                52.51216462548143
              ],
              [
                13.32740022017184,
                52.51216434074174
              ],
              [
                13.327356998341735,
                52.51216156927533
              ],
              [
                13.327359687568112,
                52.51214595792124
              ],
              [
                13.327363361798724,
                52.512124628454806
              ],
              [
                13.327364526607251,
                52.51211786656012
              ],
              [
                13.327430839554033,
                52.51212211867302
              ],
              [
                13.32742968256288,
                52.512128835185855
              ],
              [
                13.327423311287667,
                52.512165821388194
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16C1C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327479114820804,
                52.51216939961714
              ],
              [
                13.327464016782212,
                52.51216843150213
              ],
              [
                13.327447734584105,
                52.512167387456564
              ],
              [
                13.327426271687195,
                52.51216601121465
              ],
              [
                13.327432642962425,
                52.51212902501233
              ],
              [
                13.327435603362007,
                52.5121292148388
              ],
              [
                13.327436752535654,
                52.51212254370778
              ],
              [
                13.32748663527037,
                52.51212574228384
              ],
              [
                13.327479114820804,
                52.51216939961714
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16C25',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327529145578506,
                52.51217260768453
              ],
              [
                13.327515231698777,
                52.51217171550011
              ],
              [
                13.327498949499454,
                52.5121706714545
              ],
              [
                13.327480595020683,
                52.512169494530376
              ],
              [
                13.327488115470267,
                52.512125837197075
              ],
              [
                13.32753444572871,
                52.51212880798139
              ],
              [
                13.32753366397783,
                52.512133346165704
              ],
              [
                13.327535884277838,
                52.512133488535575
              ],
              [
                13.327529145578506,
                52.51217260768453
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16C3A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.3276366527108,
                52.51215665145325
              ],
              [
                13.327635167382295,
                52.512165274003465
              ],
              [
                13.327634713966113,
                52.51216790615038
              ],
              [
                13.327625388704819,
                52.51216730819697
              ],
              [
                13.327625208901834,
                52.512168351979355
              ],
              [
                13.327634386143098,
                52.51216894044144
              ],
              [
                13.32763292426633,
                52.51217742684614
              ],
              [
                13.327627003465512,
                52.51217704719317
              ],
              [
                13.327626690764525,
                52.5121788624669
              ],
              [
                13.327614553123023,
                52.51217808417832
              ],
              [
                13.327598122901328,
                52.512177030641396
              ],
              [
                13.327564966419105,
                52.51217490458487
              ],
              [
                13.327548536198593,
                52.512173851047926
              ],
              [
                13.327530625778493,
                52.51217270259778
              ],
              [
                13.327537364477843,
                52.51213358344879
              ],
              [
                13.327539584777853,
                52.51213372581867
              ],
              [
                13.327540366528735,
                52.51212918763432
              ],
              [
                13.32763805973655,
                52.51213545190806
              ],
              [
                13.32763727798554,
                52.51213999009237
              ],
              [
                13.327639498285874,
                52.51214013246224
              ],
              [
                13.3276366527108,
                52.51215665145325
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16C57',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327739970693912,
                52.51216327639743
              ],
              [
                13.327738485365154,
                52.51217189894762
              ],
              [
                13.327738031948886,
                52.51217453109453
              ],
              [
                13.327728706686184,
                52.512173933141135
              ],
              [
                13.327728526883165,
                52.51217497692352
              ],
              [
                13.327737704125823,
                52.5121755653856
              ],
              [
                13.327736242248797,
                52.51218405179029
              ],
              [
                13.327730321447081,
                52.51218367213731
              ],
              [
                13.32773000874605,
                52.51218548741104
              ],
              [
                13.32771838917285,
                52.5121847423421
              ],
              [
                13.327701810928628,
                52.51218367931379
              ],
              [
                13.327664139830526,
                52.512181263771836
              ],
              [
                13.327647709607648,
                52.512180210234874
              ],
              [
                13.327640308606478,
                52.51217973566871
              ],
              [
                13.327640621307461,
                52.51217792039497
              ],
              [
                13.327634700506586,
                52.512177540742016
              ],
              [
                13.32763616238336,
                52.51216905433734
              ],
              [
                13.327645339624766,
                52.512169642799414
              ],
              [
                13.327645519427751,
                52.51216859901702
              ],
              [
                13.327636194166324,
                52.51216800106362
              ],
              [
                13.32763664758251,
                52.5121653689167
              ],
              [
                13.327638132911016,
                52.51215674636648
              ],
              [
                13.327640978486102,
                52.51214022737549
              ],
              [
                13.32764319878645,
                52.512140369745346
              ],
              [
                13.327643980537475,
                52.51213583156101
              ],
              [
                13.327741673760059,
                52.5121420958349
              ],
              [
                13.32774089200891,
                52.51214663401922
              ],
              [
                13.32774281626949,
                52.51214675740643
              ],
              [
                13.327739970693912,
                52.51216327639743
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16CD7',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328274599688298,
                52.51221765461011
              ],
              [
                13.328273119486699,
                52.51221755969686
              ],
              [
                13.328258317470933,
                52.51221661056434
              ],
              [
                13.328240185002056,
                52.51221544787699
              ],
              [
                13.328239825395565,
                52.51221753544175
              ],
              [
                13.328231462257024,
                52.51221699918187
              ],
              [
                13.32823390132644,
                52.51220284004684
              ],
              [
                13.328276679151383,
                52.512205583039844
              ],
              [
                13.328274599688298,
                52.51221765461011
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16CD8',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328277085662824,
                52.51220322318403
              ],
              [
                13.328276858954526,
                52.51220453925747
              ],
              [
                13.328234081129576,
                52.51220179626447
              ],
              [
                13.32823430783786,
                52.51220048019101
              ],
              [
                13.32822453850796,
                52.51219985376354
              ],
              [
                13.328221583481717,
                52.5122170081002
              ],
              [
                13.328151865995498,
                52.51221253768606
              ],
              [
                13.328155024277056,
                52.51219420342151
              ],
              [
                13.328156509606574,
                52.512185580871304
              ],
              [
                13.328157729139848,
                52.512178501303765
              ],
              [
                13.328160393502271,
                52.51217867214762
              ],
              [
                13.328162050816099,
                52.51216905119684
              ],
              [
                13.328231176222877,
                52.51217348364571
              ],
              [
                13.32822951890886,
                52.51218310459647
              ],
              [
                13.328237215956703,
                52.512183598145384
              ],
              [
                13.328237442664806,
                52.512182282071926
              ],
              [
                13.328239959007393,
                52.51218244342445
              ],
              [
                13.328255353103405,
                52.5121834305223
              ],
              [
                13.32828022049,
                52.51218502506495
              ],
              [
                13.328279071314252,
                52.51219169619589
              ],
              [
                13.328277085662824,
                52.51220322318403
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16D03',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328250499357846,
                52.512256348746824
              ],
              [
                13.328226446083226,
                52.51225480640647
              ],
              [
                13.32822939329699,
                52.51223769745177
              ],
              [
                13.328240050747855,
                52.51223838082719
              ],
              [
                13.328250412158567,
                52.51223904521998
              ],
              [
                13.328253446571741,
                52.51223923979212
              ],
              [
                13.328250499357846,
                52.512256348746824
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16D78',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32813175934566,
                52.512223269787256
              ],
              [
                13.328130547627397,
                52.51223030397289
              ],
              [
                13.328086437630565,
                52.51222747555807
              ],
              [
                13.32807015541769,
                52.512226431512325
              ],
              [
                13.32750072227059,
                52.51218991838806
              ],
              [
                13.327484292051636,
                52.512188864851154
              ],
              [
                13.327437813776722,
                52.51218588457557
              ],
              [
                13.327439174026557,
                52.5121779881349
              ],
              [
                13.327440737531516,
                52.51216891176625
              ],
              [
                13.32744089388197,
                52.51216800412939
              ],
              [
                13.327447554781086,
                52.512168431238955
              ],
              [
                13.327463836979181,
                52.51216947528453
              ],
              [
                13.327498769696419,
                52.512171715236896
              ],
              [
                13.327515051895737,
                52.512172759282485
              ],
              [
                13.32754835639554,
                52.51217489483033
              ],
              [
                13.327564786616051,
                52.51217594836726
              ],
              [
                13.327597943098263,
                52.51217807442377
              ],
              [
                13.327614373319953,
                52.512179127960714
              ],
              [
                13.327647529804558,
                52.51218125401727
              ],
              [
                13.327663960027442,
                52.512182307554234
              ],
              [
                13.327701631125532,
                52.512184723096205
              ],
              [
                13.327718209369749,
                52.512185786124476
              ],
              [
                13.327755066360554,
                52.5121881494642
              ],
              [
                13.32777164460606,
                52.512189212492494
              ],
              [
                13.327804801098276,
                52.51219133854912
              ],
              [
                13.327821231324927,
                52.51219239208614
              ],
              [
                13.327855053909857,
                52.512194560853736
              ],
              [
                13.32787133611763,
                52.51219560489943
              ],
              [
                13.32791574214082,
                52.51219845229677
              ],
              [
                13.327932320390218,
                52.512199515325115
              ],
              [
                13.327961184307522,
                52.512201366133404
              ],
              [
                13.32797776255801,
                52.51220242916178
              ],
              [
                13.328011215100407,
                52.512204574201135
              ],
              [
                13.328027793352115,
                52.51220563722953
              ],
              [
                13.328064946399794,
                52.51220801955205
              ],
              [
                13.328081376632676,
                52.5122090730891
              ],
              [
                13.328110240556267,
                52.51221092389748
              ],
              [
                13.328126522770111,
                52.5122119679432
              ],
              [
                13.328133627736271,
                52.51221242352682
              ],
              [
                13.328133322852509,
                52.51221419341867
              ],
              [
                13.32813175934566,
                52.512223269787256
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16DAC',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328272350925022,
                52.51223939666239
              ],
              [
                13.328250591961996,
                52.51223800143756
              ],
              [
                13.328240230551284,
                52.5122373370448
              ],
              [
                13.328238158269155,
                52.51223720416624
              ],
              [
                13.328238306802406,
                52.51223634191123
              ],
              [
                13.328239369987632,
                52.51223016998061
              ],
              [
                13.328240933494778,
                52.51222109361203
              ],
              [
                13.328241089845454,
                52.51222018597518
              ],
              [
                13.328241707430568,
                52.51221660080959
              ],
              [
                13.328258137667692,
                52.5122176543467
              ],
              [
                13.328272939683456,
                52.51221860347922
              ],
              [
                13.328275900086645,
                52.51221879330573
              ],
              [
                13.32827449293045,
                52.512226962037445
              ],
              [
                13.328272350925022,
                52.51223939666239
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A922',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325436032077741,
                52.51238352592872
              ],
              [
                13.325438474400226,
                52.512383682535486
              ],
              [
                13.325438474400226,
                52.512383682535486
              ],
              [
                13.325466672124147,
                52.51238549063171
              ],
              [
                13.325466672124147,
                52.51238549063171
              ],
              [
                13.325468004300115,
                52.51238557605358
              ],
              [
                13.32547514959716,
                52.512344097050516
              ],
              [
                13.325436072436217,
                52.51234159134233
              ],
              [
                13.325432163632327,
                52.51236428226311
              ],
              [
                13.325439268570467,
                52.51236473784642
              ],
              [
                13.325436032077741,
                52.51238352592872
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A923',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32547556556842,
                52.51235210738678
              ],
              [
                13.325507537792669,
                52.51235415751165
              ],
              [
                13.325501752758672,
                52.51238774007428
              ],
              [
                13.325469780534728,
                52.51238568994941
              ],
              [
                13.32547556556842,
                52.51235210738678
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A924',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32551286321954,
                52.51233366805138
              ],
              [
                13.325543947328162,
                52.51233566122838
              ],
              [
                13.325534190949993,
                52.51239229776653
              ],
              [
                13.325503106841897,
                52.512390304589545
              ],
              [
                13.32551286321954,
                52.51233366805138
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A925',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325545723562978,
                52.51233577512421
              ],
              [
                13.325536389336296,
                52.512389961042935
              ],
              [
                13.325602406066007,
                52.512394194171215
              ],
              [
                13.325605642559646,
                52.51237540608892
              ],
              [
                13.32561274749959,
                52.51237586167225
              ],
              [
                13.325618845233754,
                52.51234046383583
              ],
              [
                13.325545723562978,
                52.51233577512421
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A926',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325494040672307,
                52.51232043973287
              ],
              [
                13.325470949621725,
                52.51231895908712
              ],
              [
                13.325460588253133,
                52.5123182946948
              ],
              [
                13.325454667471151,
                52.51231791504205
              ],
              [
                13.325454745647098,
                52.51231746122363
              ],
              [
                13.32545296941251,
                52.512317347327816
              ],
              [
                13.325452891236562,
                52.512317801146224
              ],
              [
                13.325440309575049,
                52.51231699438411
              ],
              [
                13.325442951921202,
                52.512301655321565
              ],
              [
                13.32545553358277,
                52.512302462083674
              ],
              [
                13.32545390752379,
                52.51231190150679
              ],
              [
                13.325455683758374,
                52.51231201540261
              ],
              [
                13.325457309817367,
                52.51230257597949
              ],
              [
                13.325483065219492,
                52.512304227468974
              ],
              [
                13.325481439160423,
                52.5123136668921
              ],
              [
                13.325483215395087,
                52.51231378078792
              ],
              [
                13.325484841454156,
                52.51230434136481
              ],
              [
                13.325496683018702,
                52.512305100670325
              ],
              [
                13.325494040672307,
                52.51232043973287
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A927',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32549164848728,
                52.51233432657645
              ],
              [
                13.325491056409058,
                52.512334288611186
              ],
              [
                13.325480695040161,
                52.51233362421885
              ],
              [
                13.325476994551304,
                52.51233338693587
              ],
              [
                13.32547533721966,
                52.512343007886294
              ],
              [
                13.325443364996888,
                52.51234095776144
              ],
              [
                13.325443552619365,
                52.51233986859722
              ],
              [
                13.325436447681183,
                52.51233941301393
              ],
              [
                13.325440121952761,
                52.51231808354833
              ],
              [
                13.325460400630845,
                52.512319383859
              ],
              [
                13.32547076199943,
                52.51232004825132
              ],
              [
                13.325493853050006,
                52.51232152889708
              ],
              [
                13.32549164848728,
                52.51233432657645
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A928',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325508632257565,
                52.512347804053825
              ],
              [
                13.325507772320885,
                52.512352796056405
              ],
              [
                13.325487345621884,
                52.51235148625439
              ],
              [
                13.325477724350812,
                52.51235086931865
              ],
              [
                13.325475800096621,
                52.512350745931506
              ],
              [
                13.325478551893136,
                52.512334771523285
              ],
              [
                13.325480476147343,
                52.512334894910424
              ],
              [
                13.325490837516233,
                52.51233555930275
              ],
              [
                13.325493205829138,
                52.51233571116384
              ],
              [
                13.325498459253408,
                52.512305214566155
              ],
              [
                13.325513705268104,
                52.512306192172005
              ],
              [
                13.325515481502846,
                52.51230630606784
              ],
              [
                13.325510790945685,
                52.51233353517291
              ],
              [
                13.325511086984815,
                52.512333554155546
              ],
              [
                13.325510070696652,
                52.512339453794965
              ],
              [
                13.325508632257565,
                52.512347804053825
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A929',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325540646051385,
                52.51233223772517
              ],
              [
                13.325540567875404,
                52.512332691543584
              ],
              [
                13.32551570058851,
                52.51233109700198
              ],
              [
                13.325519922089729,
                52.512306590807405
              ],
              [
                13.325544789376789,
                52.512308185349
              ],
              [
                13.325542209570948,
                52.51232316135682
              ],
              [
                13.325540646051385,
                52.51233223772517
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A92A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325619314289822,
                52.51233774092536
              ],
              [
                13.325608360840654,
                52.51233703856773
              ],
              [
                13.325594447000073,
                52.51233614638374
              ],
              [
                13.325542048071076,
                52.51233278645676
              ],
              [
                13.325542126247063,
                52.512332332638366
              ],
              [
                13.325543689766628,
                52.51232325627
              ],
              [
                13.325546269572481,
                52.5123082802622
              ],
              [
                13.325623535791777,
                52.51231323473081
              ],
              [
                13.325619314289822,
                52.51233774092536
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A92B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325633260863464,
                52.512256779719266
              ],
              [
                13.325628726669185,
                52.51228310118773
              ],
              [
                13.325627663478,
                52.51228927311825
              ],
              [
                13.325624317550883,
                52.51230869654661
              ],
              [
                13.32551626326181,
                52.51230176788365
              ],
              [
                13.325515794206447,
                52.512304490794186
              ],
              [
                13.325514017971706,
                52.51230437689834
              ],
              [
                13.32549877195699,
                52.512303399292485
              ],
              [
                13.325496995722295,
                52.51230328539665
              ],
              [
                13.325497464777632,
                52.51230056248614
              ],
              [
                13.325443733680064,
                52.512297117137386
              ],
              [
                13.32544707960616,
                52.512277693708995
              ],
              [
                13.325448142797015,
                52.51227152177847
              ],
              [
                13.325452676989903,
                52.51224520030992
              ],
              [
                13.32545374017917,
                52.5122390283794
              ],
              [
                13.325458321270672,
                52.51221243461968
              ],
              [
                13.325459384458352,
                52.51220626268909
              ],
              [
                13.325464043718435,
                52.51217921511082
              ],
              [
                13.325465106904502,
                52.512173043180184
              ],
              [
                13.32546965671203,
                52.512146630947555
              ],
              [
                13.32547071989651,
                52.512140459016884
              ],
              [
                13.325475316602397,
                52.512113774493045
              ],
              [
                13.325476379785274,
                52.512107602562374
              ],
              [
                13.325481242279603,
                52.5120793750557
              ],
              [
                13.32548230546081,
                52.512073203124956
              ],
              [
                13.325484603807386,
                52.5120598608629
              ],
              [
                13.32553034185203,
                52.51206279368051
              ],
              [
                13.325529982246849,
                52.512064881245365
              ],
              [
                13.325606656386748,
                52.512069797748765
              ],
              [
                13.325607015991976,
                52.51206771018393
              ],
              [
                13.325614861029944,
                52.512068213223856
              ],
              [
                13.32564150455579,
                52.51206992166134
              ],
              [
                13.325665187690747,
                52.51207144027247
              ],
              [
                13.325651303775604,
                52.51215203842636
              ],
              [
                13.325650240590807,
                52.51215821035701
              ],
              [
                13.32564569078188,
                52.51218462258957
              ],
              [
                13.325644627595485,
                52.5121907945202
              ],
              [
                13.325639968333961,
                52.512217842098444
              ],
              [
                13.325638905145965,
                52.51222401402904
              ],
              [
                13.325634324053057,
                52.51225060778872
              ],
              [
                13.325633260863464,
                52.512256779719266
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A92C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325621537298785,
                52.51204248794269
              ],
              [
                13.325633674905005,
                52.51204326623088
              ],
              [
                13.325649957060037,
                52.51204431027603
              ],
              [
                13.325669643666174,
                52.51204557262152
              ],
              [
                13.325667865185382,
                52.512055896991
              ],
              [
                13.325666301685098,
                52.51206497335974
              ],
              [
                13.325665844361136,
                52.51206762819761
              ],
              [
                13.325642161226158,
                52.51206610958649
              ],
              [
                13.325615517700285,
                52.512064401149
              ],
              [
                13.325476231284641,
                52.51205546981757
              ],
              [
                13.325489286489505,
                52.511979682137955
              ],
              [
                13.325630793202071,
                52.511988755839255
              ],
              [
                13.325621537298785,
                52.51204248794269
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A92D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325673982374017,
                52.512020385698115
              ],
              [
                13.32567118371259,
                52.51203663239828
              ],
              [
                13.325669831286074,
                52.51204448345727
              ],
              [
                13.325650144679932,
                52.51204322111179
              ],
              [
                13.325633862524903,
                52.51204217706664
              ],
              [
                13.32562572144753,
                52.51204165504407
              ],
              [
                13.325625955972376,
                52.51204029358874
              ],
              [
                13.325623735678565,
                52.512040151218955
              ],
              [
                13.325632569437138,
                52.511988869735085
              ],
              [
                13.325639822397056,
                52.51198933480975
              ],
              [
                13.325655660493481,
                52.511990350380934
              ],
              [
                13.32567889957021,
                52.511991840518135
              ],
              [
                13.325673982374017,
                52.512020385698115
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A92E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32568069758947,
                52.511981402693934
              ],
              [
                13.32567955623821,
                52.51198802844318
              ],
              [
                13.325656317161458,
                52.51198653830602
              ],
              [
                13.325640479065017,
                52.51198552273484
              ],
              [
                13.325625529086466,
                52.51198456411156
              ],
              [
                13.325627545994436,
                52.51197285559571
              ],
              [
                13.325625769759375,
                52.51197274169987
              ],
              [
                13.325625957378664,
                52.511971652535614
              ],
              [
                13.325627630316918,
                52.51196194082088
              ],
              [
                13.325627817936123,
                52.51196085165663
              ],
              [
                13.325629594171186,
                52.51196096555243
              ],
              [
                13.325636958217325,
                52.51191821585477
              ],
              [
                13.32565708888197,
                52.51191950667423
              ],
              [
                13.325670706684873,
                52.51192037987564
              ],
              [
                13.32569098537011,
                52.51192168018644
              ],
              [
                13.325690829021076,
                52.51192258782333
              ],
              [
                13.32568918735577,
                52.51193211801074
              ],
              [
                13.32568799910233,
                52.51193901605112
              ],
              [
                13.325686357435798,
                52.5119485462385
              ],
              [
                13.325685169181467,
                52.511955444278875
              ],
              [
                13.32568352751369,
                52.51196497446624
              ],
              [
                13.325682339258478,
                52.511971872506585
              ],
              [
                13.32568069758947,
                52.511981402693934
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A932',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325742097137823,
                52.51175528181991
              ],
              [
                13.325700207588179,
                52.511752595776386
              ],
              [
                13.325684665529765,
                52.51175159918779
              ],
              [
                13.325593876037498,
                52.51174577757943
              ],
              [
                13.325593876037498,
                52.51174577757943
              ],
              [
                13.325593877315105,
                52.511745766459086
              ],
              [
                13.3255938773245,
                52.51174575531202
              ],
              [
                13.325593876065627,
                52.51174574419249
              ],
              [
                13.32559387354463,
                52.51174573315475
              ],
              [
                13.325593869773787,
                52.51174572225248
              ],
              [
                13.325593864771479,
                52.51174571153883
              ],
              [
                13.325593858562067,
                52.51174570106599
              ],
              [
                13.325593851175809,
                52.51174569088502
              ],
              [
                13.325593842648674,
                52.51174568104552
              ],
              [
                13.32559383302222,
                52.511745671595364
              ],
              [
                13.325593822343343,
                52.51174566258063
              ],
              [
                13.325593810664078,
                52.51174565404524
              ],
              [
                13.325593798041309,
                52.51174564603081
              ],
              [
                13.325593784536553,
                52.5117456385763
              ],
              [
                13.325593770215571,
                52.5117456317181
              ],
              [
                13.325593755148159,
                52.51174562548963
              ],
              [
                13.325593739407736,
                52.51174561992116
              ],
              [
                13.325593723070963,
                52.51174561503987
              ],
              [
                13.32559370621743,
                52.51174561086954
              ],
              [
                13.325593688929255,
                52.51174560743051
              ],
              [
                13.325593671290669,
                52.511745604739474
              ],
              [
                13.325593653387596,
                52.511745602809604
              ],
              [
                13.325593635307259,
                52.511745601650254
              ],
              [
                13.325593617137761,
                52.51174560126708
              ],
              [
                13.325593598967597,
                52.51174560166198
              ],
              [
                13.325593580885304,
                52.51174560283301
              ],
              [
                13.32559356297897,
                52.511745604774454
              ],
              [
                13.325593545335844,
                52.511745607476854
              ],
              [
                13.32559352804187,
                52.511745610927086
              ],
              [
                13.325593511181305,
                52.51174561510828
              ],
              [
                13.32559349483629,
                52.511745620000134
              ],
              [
                13.32559347908647,
                52.511745625578776
              ],
              [
                13.32559346400856,
                52.51174563181699
              ],
              [
                13.325593449676033,
                52.511745638684445
              ],
              [
                13.325593436158696,
                52.51174564614765
              ],
              [
                13.325593423522418,
                52.51174565417028
              ],
              [
                13.325593411828764,
                52.511745662713196
              ],
              [
                13.325593401134698,
                52.51174567173481
              ],
              [
                13.325593391492319,
                52.511745681191194
              ],
              [
                13.32559338294861,
                52.51174569103623
              ],
              [
                13.32559337554519,
                52.51174570122199
              ],
              [
                13.325593369318128,
                52.51174571169881
              ],
              [
                13.325593364297768,
                52.51174572241568
              ],
              [
                13.325593360508558,
                52.5117457333204
              ],
              [
                13.325593357968962,
                52.51174574435979
              ],
              [
                13.325566397877814,
                52.511744015623385
              ],
              [
                13.325566710573396,
                52.51174220034954
              ],
              [
                13.325540363089253,
                52.511740510894654
              ],
              [
                13.32554227053163,
                52.51172943772416
              ],
              [
                13.325508226031015,
                52.51172725472069
              ],
              [
                13.325509640977318,
                52.51171904060651
              ],
              [
                13.325745732223394,
                52.51173417926142
              ],
              [
                13.325742097137823,
                52.51175528181991
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A933',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325892337521495,
                52.51180400795192
              ],
              [
                13.325890561285737,
                52.51180389405605
              ],
              [
                13.325875315262325,
                52.51180291645008
              ],
              [
                13.325864213789183,
                52.51180220460107
              ],
              [
                13.325848967766362,
                52.51180122699509
              ],
              [
                13.325837866293641,
                52.51180051514607
              ],
              [
                13.325822620271403,
                52.511799537540114
              ],
              [
                13.32581151879911,
                52.511798825691095
              ],
              [
                13.325796272777467,
                52.51179784808514
              ],
              [
                13.325785171305602,
                52.51179713623614
              ],
              [
                13.325769925284535,
                52.51179615863018
              ],
              [
                13.325758823813102,
                52.511795446781186
              ],
              [
                13.325743577792617,
                52.511794469175236
              ],
              [
                13.3256942872627,
                52.51179130856572
              ],
              [
                13.325678449165256,
                52.51179029299451
              ],
              [
                13.325659206617445,
                52.51178905912294
              ],
              [
                13.325661114063763,
                52.51177798595255
              ],
              [
                13.325659337828597,
                52.511777872056705
              ],
              [
                13.325659525446225,
                52.51177678289241
              ],
              [
                13.325661198369565,
                52.5117670711774
              ],
              [
                13.325661385987088,
                52.51176598201308
              ],
              [
                13.325663162222257,
                52.51176609590892
              ],
              [
                13.32566505403178,
                52.511755113502176
              ],
              [
                13.325682076285757,
                52.511756205003934
              ],
              [
                13.325682232633612,
                52.51175529736703
              ],
              [
                13.325684008868837,
                52.51175541126287
              ],
              [
                13.325700439044862,
                52.51175646479936
              ],
              [
                13.325701327162486,
                52.511756521747294
              ],
              [
                13.32570117081463,
                52.51175742938419
              ],
              [
                13.325795607329587,
                52.51176348484631
              ],
              [
                13.325811445429766,
                52.511764500417556
              ],
              [
                13.325867248832362,
                52.51176807864527
              ],
              [
                13.32587953446273,
                52.51176886642484
              ],
              [
                13.32589818493816,
                52.5117700623312
              ],
              [
                13.32589668399785,
                52.51177877564563
              ],
              [
                13.325895527022622,
                52.51178549215884
              ],
              [
                13.325892337521495,
                52.51180400795192
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A934',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325904618138848,
                52.51173358481825
              ],
              [
                13.325821875159559,
                52.511728279170235
              ],
              [
                13.325816402988371,
                52.511760046462555
              ],
              [
                13.325814034674279,
                52.51175989460143
              ],
              [
                13.325813409282791,
                52.511763525149114
              ],
              [
                13.325811633047232,
                52.51176341125328
              ],
              [
                13.325795794947053,
                52.51176239568203
              ],
              [
                13.325794018711536,
                52.511762281786204
              ],
              [
                13.325794644103013,
                52.511758651238495
              ],
              [
                13.325782654513421,
                52.511757882441586
              ],
              [
                13.325782889035192,
                52.5117565209862
              ],
              [
                13.325777264289526,
                52.51175616031603
              ],
              [
                13.325777811506928,
                52.511752983586824
              ],
              [
                13.325779015384946,
                52.5117459947825
              ],
              [
                13.32577987529758,
                52.51174100277943
              ],
              [
                13.325781079174929,
                52.5117340139751
              ],
              [
                13.325781501313651,
                52.5117315633554
              ],
              [
                13.325787228888009,
                52.51173132707974
              ],
              [
                13.325796381829887,
                52.51167819191935
              ],
              [
                13.325805263007556,
                52.51167876139856
              ],
              [
                13.325815180322751,
                52.511679397317
              ],
              [
                13.325822433284687,
                52.5116798623917
              ],
              [
                13.325832350600137,
                52.51168049831015
              ],
              [
                13.325839603562274,
                52.51168096338484
              ],
              [
                13.325849520877966,
                52.51168159930329
              ],
              [
                13.325861954527696,
                52.511682396574194
              ],
              [
                13.325871871843711,
                52.511683032492655
              ],
              [
                13.32587912480625,
                52.51168349756735
              ],
              [
                13.325889042122522,
                52.511684133485794
              ],
              [
                13.325896295085258,
                52.5116845985605
              ],
              [
                13.325906212401767,
                52.51168523447896
              ],
              [
                13.325912873286073,
                52.51168566158837
              ],
              [
                13.325904618138848,
                52.51173358481825
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A935',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325899145966892,
                52.51176535211055
              ],
              [
                13.325882123707517,
                52.5117642606087
              ],
              [
                13.325881498315946,
                52.51176789115639
              ],
              [
                13.325879722080222,
                52.51176777726055
              ],
              [
                13.32586743644985,
                52.51176698948098
              ],
              [
                13.32586566021415,
                52.51176687558513
              ],
              [
                13.325866285605708,
                52.51176324503746
              ],
              [
                13.325819955459517,
                52.51176027425424
              ],
              [
                13.325825114935459,
                52.511730322235756
              ],
              [
                13.325904305443522,
                52.51173540009208
              ],
              [
                13.325899145966892,
                52.51176535211055
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A936',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326219155016657,
                52.51177550215272
              ],
              [
                13.32621757590258,
                52.51178466928559
              ],
              [
                13.326217419554622,
                52.51178557692254
              ],
              [
                13.326097079535087,
                52.51177786047868
              ],
              [
                13.32609664176084,
                52.51178040186205
              ],
              [
                13.326094865524542,
                52.5117802879662
              ],
              [
                13.326082579890322,
                52.51177950018659
              ],
              [
                13.32608080365407,
                52.51177938629072
              ],
              [
                13.3260812414283,
                52.51177684490735
              ],
              [
                13.326049417196154,
                52.51177480427343
              ],
              [
                13.326049104500306,
                52.51177661954725
              ],
              [
                13.326033562433993,
                52.5117756229586
              ],
              [
                13.326033875129838,
                52.51177380768478
              ],
              [
                13.325994945955737,
                52.51177131146745
              ],
              [
                13.325994633259908,
                52.51177312674129
              ],
              [
                13.325979091194826,
                52.511772130152636
              ],
              [
                13.325979403890653,
                52.511770314878795
              ],
              [
                13.325932777697501,
                52.51176732511288
              ],
              [
                13.32593233992337,
                52.51176986649625
              ],
              [
                13.325930563687505,
                52.511769752600394
              ],
              [
                13.325918278056228,
                52.511768964820824
              ],
              [
                13.325916501820405,
                52.51176885092497
              ],
              [
                13.325916939594519,
                52.5117663095416
              ],
              [
                13.325905986140333,
                52.5117656071839
              ],
              [
                13.325919682190104,
                52.51168609818912
              ],
              [
                13.325944549492325,
                52.51168769273095
              ],
              [
                13.325954466809536,
                52.51168832864943
              ],
              [
                13.32599680043387,
                52.51169104316708
              ],
              [
                13.326006717751836,
                52.51169167908557
              ],
              [
                13.32604446276929,
                52.51169409937232
              ],
              [
                13.326054380087946,
                52.5116947352908
              ],
              [
                13.326099082033524,
                52.51169760166968
              ],
              [
                13.326108999352963,
                52.51169823758818
              ],
              [
                13.326147632494267,
                52.511700714822915
              ],
              [
                13.32615754981442,
                52.51170135074141
              ],
              [
                13.326194850781045,
                52.51170374255431
              ],
              [
                13.32620476810187,
                52.5117043784728
              ],
              [
                13.326231115611638,
                52.51170606792795
              ],
              [
                13.326219155016657,
                52.51177550215272
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A937',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326591246594782,
                52.511810832183386
              ],
              [
                13.326566675308774,
                52.51180925662396
              ],
              [
                13.326550837191144,
                52.511808241052535
              ],
              [
                13.32647978769601,
                52.51180368521815
              ],
              [
                13.326480162931299,
                52.51180150688955
              ],
              [
                13.326411185720104,
                52.51179708393373
              ],
              [
                13.326410873024068,
                52.511798899207584
              ],
              [
                13.326395330949541,
                52.51179790261883
              ],
              [
                13.326395643645577,
                52.511796087345004
              ],
              [
                13.326355530292956,
                52.511793515196906
              ],
              [
                13.326355217596952,
                52.511795330470754
              ],
              [
                13.326339675523696,
                52.51179433388201
              ],
              [
                13.326339988219688,
                52.51179251860817
              ],
              [
                13.326299874870326,
                52.51178994646014
              ],
              [
                13.326284036758857,
                52.511788930888756
              ],
              [
                13.32624392341276,
                52.51178635874074
              ],
              [
                13.326243610716821,
                52.51178817401458
              ],
              [
                13.326228068646095,
                52.51178717742587
              ],
              [
                13.326228381342032,
                52.51178536215204
              ],
              [
                13.326223792730739,
                52.5117850679211
              ],
              [
                13.326225371844828,
                52.511775900788194
              ],
              [
                13.326237332439936,
                52.51170646656344
              ],
              [
                13.326245029465523,
                52.51170696011214
              ],
              [
                13.326254946787081,
                52.51170759603065
              ],
              [
                13.326299056666793,
                52.511710424444374
              ],
              [
                13.326308973989127,
                52.5117110603629
              ],
              [
                13.326354564069794,
                52.511713983689845
              ],
              [
                13.326364481392934,
                52.511714619608384
              ],
              [
                13.326409479398249,
                52.5117175049701
              ],
              [
                13.32641939672219,
                52.511718140888625
              ],
              [
                13.326463062553167,
                52.51172094082849
              ],
              [
                13.326472979877883,
                52.51172157674704
              ],
              [
                13.326516645712264,
                52.51172437668693
              ],
              [
                13.32652656303776,
                52.51172501260548
              ],
              [
                13.326574077390223,
                52.5117280593198
              ],
              [
                13.326583994716534,
                52.511728695238354
              ],
              [
                13.326605161547816,
                52.51173005249738
              ],
              [
                13.326591246594782,
                52.511810832183386
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A938',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326587494239096,
                52.51183261546933
              ],
              [
                13.326663132359533,
                52.5118374655348
              ],
              [
                13.326660755865356,
                52.51185126161584
              ],
              [
                13.326671561311768,
                52.511851954482346
              ],
              [
                13.32667054504734,
                52.51185785412227
              ],
              [
                13.326766017834528,
                52.51186397602501
              ],
              [
                13.326765830216416,
                52.51186506518929
              ],
              [
                13.32667035742926,
                52.511858943286555
              ],
              [
                13.326667777679718,
                52.511873919295525
              ],
              [
                13.326663633124939,
                52.51187365353852
              ],
              [
                13.32666221035324,
                52.51188191303433
              ],
              [
                13.32665643758056,
                52.511881542872786
              ],
              [
                13.32665624996228,
                52.5118826320371
              ],
              [
                13.326652401447184,
                52.51188238526272
              ],
              [
                13.326654011837142,
                52.5118730366026
              ],
              [
                13.326658156391868,
                52.51187330235961
              ],
              [
                13.326660892489762,
                52.511857418713724
              ],
              [
                13.326631732587279,
                52.5118555489233
              ],
              [
                13.326632123458227,
                52.511853279831044
              ],
              [
                13.326633899695905,
                52.511853393726895
              ],
              [
                13.326634212392635,
                52.51185157845307
              ],
              [
                13.32661881833288,
                52.51185059135559
              ],
              [
                13.326607568827889,
                52.51184987001513
              ],
              [
                13.326592174768745,
                52.51184888291767
              ],
              [
                13.326580925264194,
                52.51184816157722
              ],
              [
                13.326565531205636,
                52.51184717447975
              ],
              [
                13.32655428170153,
                52.511846453139306
              ],
              [
                13.326538887643565,
                52.51184546604185
              ],
              [
                13.326527638139893,
                52.511844744701385
              ],
              [
                13.326512244082538,
                52.511843757603955
              ],
              [
                13.326500994579304,
                52.511843036263514
              ],
              [
                13.326485600522538,
                52.51184204916606
              ],
              [
                13.326474351019739,
                52.511841327825636
              ],
              [
                13.326458956963574,
                52.51184034072819
              ],
              [
                13.326447707461217,
                52.51183961938775
              ],
              [
                13.326432313405652,
                52.511838632290335
              ],
              [
                13.326421063903728,
                52.51183791094991
              ],
              [
                13.326405669848755,
                52.51183692385248
              ],
              [
                13.326394420347269,
                52.51183620251204
              ],
              [
                13.326379026292907,
                52.51183521541462
              ],
              [
                13.326367776791857,
                52.51183449407421
              ],
              [
                13.32635238273809,
                52.5118335069768
              ],
              [
                13.326341133237474,
                52.511832785636386
              ],
              [
                13.3263257391843,
                52.51183179853898
              ],
              [
                13.326314489684119,
                52.511831077198565
              ],
              [
                13.326299095631553,
                52.511830090101164
              ],
              [
                13.326287994151537,
                52.51182937825209
              ],
              [
                13.32627260009957,
                52.511828391154694
              ],
              [
                13.326261498619987,
                52.51182767930559
              ],
              [
                13.32624610456861,
                52.511826692208224
              ],
              [
                13.32623500308946,
                52.51182598035914
              ],
              [
                13.326219609038672,
                52.51182499326177
              ],
              [
                13.326208507559944,
                52.51182428141269
              ],
              [
                13.326193113509762,
                52.51182329431533
              ],
              [
                13.326182012031467,
                52.51182258246624
              ],
              [
                13.32616661798187,
                52.511821595368886
              ],
              [
                13.326155516504006,
                52.51182088351982
              ],
              [
                13.326140122455001,
                52.511819896422445
              ],
              [
                13.32612902097758,
                52.51181918457338
              ],
              [
                13.326113626929162,
                52.51181819747603
              ],
              [
                13.326102525452162,
                52.51181748562697
              ],
              [
                13.326087131404346,
                52.51181649852963
              ],
              [
                13.326076029927773,
                52.51181578668058
              ],
              [
                13.326060635880554,
                52.51181479958324
              ],
              [
                13.326049534404413,
                52.51181408773419
              ],
              [
                13.32603414035779,
                52.51181310063684
              ],
              [
                13.326023038882077,
                52.511812388787824
              ],
              [
                13.326007644836043,
                52.511811401690466
              ],
              [
                13.325996543360764,
                52.51181068984144
              ],
              [
                13.32598114931532,
                52.5118097027441
              ],
              [
                13.32597004784047,
                52.511808990895084
              ],
              [
                13.325954653795629,
                52.51180800379775
              ],
              [
                13.325943552321206,
                52.51180729194872
              ],
              [
                13.325928158276955,
                52.51180630485141
              ],
              [
                13.325917056802956,
                52.51180559300238
              ],
              [
                13.325901662759305,
                52.51180460590508
              ],
              [
                13.325893965737619,
                52.51180411235644
              ],
              [
                13.325897155238739,
                52.51178559656336
              ],
              [
                13.32589831221398,
                52.511778880050166
              ],
              [
                13.325900032041364,
                52.51176889604403
              ],
              [
                13.325918090438742,
                52.511770053985124
              ],
              [
                13.325930376070012,
                52.5117708417647
              ],
              [
                13.326082392272776,
                52.511780589350906
              ],
              [
                13.326094677906998,
                52.5117813771305
              ],
              [
                13.326225231286625,
                52.51178974847551
              ],
              [
                13.326225387634613,
                52.51178884083857
              ],
              [
                13.326281635130117,
                52.51179244754058
              ],
              [
                13.326281822747715,
                52.51179135837628
              ],
              [
                13.326283598984492,
                52.511791472272144
              ],
              [
                13.326299437095946,
                52.51179248784349
              ],
              [
                13.32630121333277,
                52.51179260173935
              ],
              [
                13.326301025715159,
                52.51179369090365
              ],
              [
                13.326479537539115,
                52.511805137437214
              ],
              [
                13.326479162303762,
                52.51180731576582
              ],
              [
                13.326548435561357,
                52.51181175770436
              ],
              [
                13.326548623179058,
                52.511810668540065
              ],
              [
                13.326550399416533,
                52.51181078243592
              ],
              [
                13.326566237534147,
                52.51181179800735
              ],
              [
                13.326568013771656,
                52.51181191190322
              ],
              [
                13.326567826153951,
                52.51181300106751
              ],
              [
                13.326590621202428,
                52.51181446273105
              ],
              [
                13.326587494239096,
                52.51183261546933
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A93A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326663452286,
                52.5117435174342
              ],
              [
                13.326658049562813,
                52.511743171000944
              ],
              [
                13.326657971389023,
                52.511743624819395
              ],
              [
                13.32666337411221,
                52.511743971252656
              ],
              [
                13.326661091436767,
                52.51175722275176
              ],
              [
                13.326643921138626,
                52.51175612175841
              ],
              [
                13.326646203814006,
                52.511742870259305
              ],
              [
                13.32664805406164,
                52.51174298890082
              ],
              [
                13.326648132235437,
                52.51174253508237
              ],
              [
                13.326643691641124,
                52.5117422503427
              ],
              [
                13.326643613467336,
                52.511742704161165
              ],
              [
                13.326645463714955,
                52.51174282280269
              ],
              [
                13.32664318103958,
                52.5117560743018
              ],
              [
                13.32662978524695,
                52.511755215337146
              ],
              [
                13.326632067922274,
                52.511741963838055
              ],
              [
                13.32663369614016,
                52.5117420682426
              ],
              [
                13.326633774313946,
                52.51174161442414
              ],
              [
                13.32663140599703,
                52.51174146256298
              ],
              [
                13.326629045147916,
                52.51175516788055
              ],
              [
                13.326624604553741,
                52.511754883140874
              ],
              [
                13.326611874850594,
                52.51175406688721
              ],
              [
                13.326607064207016,
                52.51175375841924
              ],
              [
                13.326611082339896,
                52.511730432150244
              ],
              [
                13.326616559072622,
                52.51173078332916
              ],
              [
                13.326626180359943,
                52.511731400265084
              ],
              [
                13.32663284125124,
                52.511731827374575
              ],
              [
                13.32664246253879,
                52.51173244431051
              ],
              [
                13.326649123430249,
                52.51173287142001
              ],
              [
                13.326658744718028,
                52.51173348835592
              ],
              [
                13.326665109570012,
                52.51173389648278
              ],
              [
                13.326663452286,
                52.5117435174342
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A93C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326766440994396,
                52.5119527411022
              ],
              [
                13.326751490991224,
                52.511951782478654
              ],
              [
                13.326737725147007,
                52.511950899785695
              ],
              [
                13.326651281572463,
                52.51194535685369
              ],
              [
                13.326651578635596,
                52.51194363234358
              ],
              [
                13.326653767521094,
                52.51193092542703
              ],
              [
                13.326654392916717,
                52.51192729487944
              ],
              [
                13.326656581800579,
                52.511914587962835
              ],
              [
                13.326657207195737,
                52.51191095741523
              ],
              [
                13.326659396077964,
                52.51189825049864
              ],
              [
                13.326660021472655,
                52.511894619951
              ],
              [
                13.32666221035324,
                52.51188191303433
              ],
              [
                13.326663633124939,
                52.51187365353852
              ],
              [
                13.326667777679718,
                52.511873919295525
              ],
              [
                13.32667035742926,
                52.511858943286555
              ],
              [
                13.326765830216416,
                52.51186506518929
              ],
              [
                13.326778855962058,
                52.511865900425654
              ],
              [
                13.326781372299306,
                52.511866061778115
              ],
              [
                13.326780043337402,
                52.51187377669182
              ],
              [
                13.326777527000152,
                52.51187361533934
              ],
              [
                13.326764501254544,
                52.511872780103
              ],
              [
                13.326732490215772,
                52.511870727492365
              ],
              [
                13.326731515550938,
                52.51187176618968
              ],
              [
                13.32676431363636,
                52.5118738692673
              ],
              [
                13.326777339381955,
                52.51187470450366
              ],
              [
                13.326779855719208,
                52.511874865856115
              ],
              [
                13.326766440994396,
                52.5119527411022
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A93D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326760781154443,
                52.511985597557675
              ],
              [
                13.326745831151424,
                52.511984638934145
              ],
              [
                13.32673206530733,
                52.51198375624121
              ],
              [
                13.326645621733622,
                52.51197821330921
              ],
              [
                13.326645856257501,
                52.511976851853866
              ],
              [
                13.326648045146325,
                52.51196414493739
              ],
              [
                13.326648670542891,
                52.51196051438983
              ],
              [
                13.32665085943009,
                52.511947807473305
              ],
              [
                13.32665109395364,
                52.51194644601796
              ],
              [
                13.32673753752815,
                52.51195198894997
              ],
              [
                13.326751303372362,
                52.51195287164293
              ],
              [
                13.326766253375528,
                52.511953830266464
              ],
              [
                13.326760781154443,
                52.511985597557675
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A942',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327374012645112,
                52.5118977311553
              ],
              [
                13.327372532445445,
                52.511897636242054
              ],
              [
                13.327371985225497,
                52.51190081297123
              ],
              [
                13.327270739576049,
                52.51189432090601
              ],
              [
                13.327258157880888,
                52.51189351414355
              ],
              [
                13.32725578956183,
                52.511893362282365
              ],
              [
                13.327256180433142,
                52.5118910931901
              ],
              [
                13.327226280405654,
                52.5118891759428
              ],
              [
                13.327211626432272,
                52.51188823630181
              ],
              [
                13.32682581258898,
                52.511863497169095
              ],
              [
                13.326811158624153,
                52.5118625575282
              ],
              [
                13.326782294754954,
                52.511860706720356
              ],
              [
                13.32678155991742,
                52.511864972613836
              ],
              [
                13.326779043580169,
                52.51186481126136
              ],
              [
                13.326766017834528,
                52.51186397602501
              ],
              [
                13.32667054504734,
                52.51185785412227
              ],
              [
                13.326671561311768,
                52.511851954482346
              ],
              [
                13.32667393780599,
                52.51183815840131
              ],
              [
                13.326674985339142,
                52.511832077234004
              ],
              [
                13.32667706476978,
                52.51182000566306
              ],
              [
                13.326677799605893,
                52.51181573976956
              ],
              [
                13.326693008090272,
                52.51181671496751
              ],
              [
                13.326756694172689,
                52.51182079864423
              ],
              [
                13.326769275858673,
                52.51182160540663
              ],
              [
                13.32678881447735,
                52.51182285826114
              ],
              [
                13.32678807964109,
                52.511827124154635
              ],
              [
                13.327265517802596,
                52.51185773841627
              ],
              [
                13.327269403055372,
                52.51183518363903
              ],
              [
                13.327261113938524,
                52.511834652124904
              ],
              [
                13.327261895679033,
                52.511830113940334
              ],
              [
                13.327263068289488,
                52.51182330666348
              ],
              [
                13.32726356860317,
                52.51182040222536
              ],
              [
                13.327260016124548,
                52.51182017443358
              ],
              [
                13.32726068842094,
                52.51181627159485
              ],
              [
                13.327268385458012,
                52.51181676514367
              ],
              [
                13.327268385458012,
                52.51181676514367
              ],
              [
                13.327268667272127,
                52.51181678920413
              ],
              [
                13.327268945731191,
                52.511816825001496
              ],
              [
                13.327269219536896,
                52.51181687236888
              ],
              [
                13.327269487412641,
                52.511816931085455
              ],
              [
                13.327269748109446,
                52.5118170008774
              ],
              [
                13.327270000411842,
                52.511817081419395
              ],
              [
                13.327270243143467,
                52.511817172335846
              ],
              [
                13.327270475172607,
                52.511817273202865
              ],
              [
                13.32727069541742,
                52.51181738355018
              ],
              [
                13.327270902851033,
                52.51181750286332
              ],
              [
                13.327271096506294,
                52.51181763058598
              ],
              [
                13.327271275480287,
                52.511817766122626
              ],
              [
                13.32727143893855,
                52.51181790884137
              ],
              [
                13.327271586118973,
                52.51181805807676
              ],
              [
                13.327271716335337,
                52.511818213133
              ],
              [
                13.327271828980503,
                52.51181837328717
              ],
              [
                13.32727192352927,
                52.51181853779251
              ],
              [
                13.32727199954081,
                52.51181870588205
              ],
              [
                13.327272056660723,
                52.51181887677207
              ],
              [
                13.327272094622685,
                52.51181904966581
              ],
              [
                13.327272113249702,
                52.51181922375713
              ],
              [
                13.327272112454928,
                52.511819398234394
              ],
              [
                13.327272092242067,
                52.5118195722841
              ],
              [
                13.327272045337663,
                52.51181984457518
              ],
              [
                13.327326812719551,
                52.511823356364815
              ],
              [
                13.327325765187636,
                52.51182943753215
              ],
              [
                13.327324608211764,
                52.51183615404532
              ],
              [
                13.327322888382087,
                52.511846138051375
              ],
              [
                13.327321731405332,
                52.511852854564474
              ],
              [
                13.327320965298892,
                52.511857301985366
              ],
              [
                13.327365925500793,
                52.51186018491967
              ],
              [
                13.327378841103936,
                52.51186101309285
              ],
              [
                13.327378215710754,
                52.51186464364052
              ],
              [
                13.327403897360025,
                52.511867592485295
              ],
              [
                13.327402183443947,
                52.51187754214827
              ],
              [
                13.327376058103471,
                52.51187716902985
              ],
              [
                13.32737525290913,
                52.51188184335991
              ],
              [
                13.327376733108812,
                52.511881938273135
              ],
              [
                13.327374012645112,
                52.5118977311553
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A943',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327385674852577,
                52.511845667495045
              ],
              [
                13.327325874787478,
                52.51184183300033
              ],
              [
                13.327326828511113,
                52.511836296415154
              ],
              [
                13.327327985487003,
                52.51182957990201
              ],
              [
                13.327328845401277,
                52.51182458789898
              ],
              [
                13.327336246399184,
                52.51182506246515
              ],
              [
                13.327336434016823,
                52.511823973300864
              ],
              [
                13.32738883308433,
                52.51182733322939
              ],
              [
                13.327385674852577,
                52.511845667495045
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A944',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327408679465915,
                52.511848519088225
              ],
              [
                13.327417227619497,
                52.51184906721215
              ],
              [
                13.327422090045086,
                52.51182083970416
              ],
              [
                13.327422090045086,
                52.51182083970416
              ],
              [
                13.32742235525377,
                52.51181968654735
              ],
              [
                13.327422750854462,
                52.511818547440036
              ],
              [
                13.32742327492401,
                52.51181742791979
              ],
              [
                13.32742392491476,
                52.511816333428875
              ],
              [
                13.327424697666904,
                52.51181526928802
              ],
              [
                13.32742558942387,
                52.511814240670276
              ],
              [
                13.327426595850563,
                52.51181325257607
              ],
              [
                13.32742771205443,
                52.51181230980884
              ],
              [
                13.327428932609275,
                52.51181141695165
              ],
              [
                13.327430251581616,
                52.51181057834498
              ],
              [
                13.32743166255953,
                52.5118097980655
              ],
              [
                13.327433158683817,
                52.51180907990643
              ],
              [
                13.327434732681393,
                52.51180842735892
              ],
              [
                13.327436376900565,
                52.511807843595214
              ],
              [
                13.3274380833483,
                52.511807331453184
              ],
              [
                13.327439843729035,
                52.51180689342245
              ],
              [
                13.327441649485024,
                52.511806531632494
              ],
              [
                13.327443491837961,
                52.511806247842024
              ],
              [
                13.327445361831598,
                52.51180604343064
              ],
              [
                13.327458317736763,
                52.51180687418802
              ],
              [
                13.32747089943565,
                52.51180768095054
              ],
              [
                13.327479040535048,
                52.511808202973334
              ],
              [
                13.327466032368324,
                52.51188371836447
              ],
              [
                13.327403308901825,
                52.51187969641606
              ],
              [
                13.32740367632057,
                52.511877563469326
              ],
              [
                13.327405364882868,
                52.511867760990725
              ],
              [
                13.327406224798565,
                52.51186276898773
              ],
              [
                13.327365925500793,
                52.51186018491967
              ],
              [
                13.327367143832454,
                52.51185888654804
              ],
              [
                13.327323420120624,
                52.51185608289984
              ],
              [
                13.32732564026524,
                52.51184319445571
              ],
              [
                13.327408679465915,
                52.511848519088225
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A945',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327479228152514,
                52.51180711380903
              ],
              [
                13.327471087053103,
                52.511806591786225
              ],
              [
                13.327458505354215,
                52.51180578502371
              ],
              [
                13.32745317663476,
                52.51180544333605
              ],
              [
                13.327455365504338,
                52.51179273641918
              ],
              [
                13.32745240510465,
                52.511792546592694
              ],
              [
                13.32745232693076,
                52.511793000411174
              ],
              [
                13.327454547230525,
                52.511793142781016
              ],
              [
                13.327452436534832,
                52.511805395879435
              ],
              [
                13.32743911473638,
                52.51180454166029
              ],
              [
                13.327441225432022,
                52.511792288561885
              ],
              [
                13.327443445731756,
                52.51179243093174
              ],
              [
                13.32744352390565,
                52.51179197711327
              ],
              [
                13.327438343206278,
                52.51179164491693
              ],
              [
                13.327438265032395,
                52.51179209873541
              ],
              [
                13.327440485332119,
                52.511792241105255
              ],
              [
                13.327438374636476,
                52.511804494203666
              ],
              [
                13.327425052838299,
                52.51180363998454
              ],
              [
                13.327427163533894,
                52.511791386886124
              ],
              [
                13.327429383833579,
                52.51179152925598
              ],
              [
                13.327429462007464,
                52.511791075437515
              ],
              [
                13.327427241707776,
                52.511790933067665
              ],
              [
                13.327429055341524,
                52.51178040447936
              ],
              [
                13.327434680100765,
                52.511780765149666
              ],
              [
                13.327444301399568,
                52.51178138208573
              ],
              [
                13.327450962298816,
                52.51178180919528
              ],
              [
                13.327460583597846,
                52.511782426131354
              ],
              [
                13.32746724449726,
                52.51178285324093
              ],
              [
                13.327476865796514,
                52.51178347017697
              ],
              [
                13.327483230656112,
                52.51178387830389
              ],
              [
                13.327479228152514,
                52.51180711380903
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A946',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327842558478599,
                52.511877670842544
              ],
              [
                13.327840916820948,
                52.511887201030085
              ],
              [
                13.327840682298373,
                52.51188856248545
              ],
              [
                13.327819811469134,
                52.51188722420868
              ],
              [
                13.327819420598155,
                52.511889493300956
              ],
              [
                13.3278038784917,
                52.5118884967119
              ],
              [
                13.327804269362671,
                52.51188622761959
              ],
              [
                13.327772445050547,
                52.51188418698484
              ],
              [
                13.327756606905062,
                52.51188317141313
              ],
              [
                13.327724782595137,
                52.51188113077839
              ],
              [
                13.327724391724214,
                52.511883399870676
              ],
              [
                13.327708849619919,
                52.51188240328161
              ],
              [
                13.327709240490835,
                52.51188013418935
              ],
              [
                13.327659949819498,
                52.51187697357837
              ],
              [
                13.327659558948623,
                52.511879242670645
              ],
              [
                13.327644016845795,
                52.5118782460816
              ],
              [
                13.327644407716665,
                52.51187597698933
              ],
              [
                13.32761258341196,
                52.51187393635464
              ],
              [
                13.32761219254111,
                52.51187620544692
              ],
              [
                13.327596650439357,
                52.511875208857894
              ],
              [
                13.32759704131019,
                52.511872939765624
              ],
              [
                13.32756521700768,
                52.51187089913095
              ],
              [
                13.327549378866976,
                52.511869883559285
              ],
              [
                13.327528360026621,
                52.511868535791294
              ],
              [
                13.327542290635073,
                52.511787665342126
              ],
              [
                13.327565825816084,
                52.51178917446264
              ],
              [
                13.327575299096715,
                52.51178978190739
              ],
              [
                13.327613340240575,
                52.51179222117767
              ],
              [
                13.32762281352187,
                52.51179282862243
              ],
              [
                13.32766122471844,
                52.51179529162104
              ],
              [
                13.327670698000386,
                52.51179589906581
              ],
              [
                13.327726131504441,
                52.51179945356677
              ],
              [
                13.327735604787282,
                52.511800061011535
              ],
              [
                13.327773275889909,
                52.51180247655361
              ],
              [
                13.327782749173412,
                52.51180308399837
              ],
              [
                13.327821456419073,
                52.51180556597975
              ],
              [
                13.327830929703232,
                52.51180617342453
              ],
              [
                13.327854612914225,
                52.51180769203648
              ],
              [
                13.327842558478599,
                52.511877670842544
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A947',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.3275223756655,
                52.51187026268589
              ],
              [
                13.327508461785747,
                52.511869370501444
              ],
              [
                13.327508242898094,
                52.511870641193106
              ],
              [
                13.327506466658141,
                52.51187052729723
              ],
              [
                13.327492552778706,
                52.51186963511279
              ],
              [
                13.327490776538795,
                52.51186952121691
              ],
              [
                13.327490995426432,
                52.51186825052522
              ],
              [
                13.327474565207504,
                52.511867196988284
              ],
              [
                13.327488855415783,
                52.511784238974194
              ],
              [
                13.327507653955063,
                52.511785444372315
              ],
              [
                13.327517127234893,
                52.51178605181706
              ],
              [
                13.327536665874947,
                52.511787304671806
              ],
              [
                13.3275223756655,
                52.51187026268589
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A949',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328284657876104,
                52.51188885879391
              ],
              [
                13.32829237786618,
                52.51189008794371
              ],
              [
                13.328292284057193,
                52.51189063252585
              ],
              [
                13.328290830017542,
                52.51189907354909
              ],
              [
                13.32828971994388,
                52.511905517771105
              ],
              [
                13.328288265903236,
                52.511913958794324
              ],
              [
                13.328288172094148,
                52.511914503376445
              ],
              [
                13.32828012377244,
                52.511915180264175
              ],
              [
                13.328279685996632,
                52.51191772164751
              ],
              [
                13.328267814779693,
                52.511916960443216
              ],
              [
                13.328267502082658,
                52.51191877571703
              ],
              [
                13.328265799850824,
                52.51191866656678
              ],
              [
                13.328249991297964,
                52.511917652893224
              ],
              [
                13.328248259462153,
                52.51191754184471
              ],
              [
                13.328248572159177,
                52.51191572657089
              ],
              [
                13.328246677501182,
                52.511915605081924
              ],
              [
                13.328252243503877,
                52.51188329320805
              ],
              [
                13.328170832422481,
                52.51187807297907
              ],
              [
                13.328179369027417,
                52.511828516003604
              ],
              [
                13.328187510135248,
                52.51182903802652
              ],
              [
                13.328196983424474,
                52.51182964547134
              ],
              [
                13.328203644331039,
                52.51183007258097
              ],
              [
                13.328213117620484,
                52.5118306800258
              ],
              [
                13.328219778527206,
                52.51183110713544
              ],
              [
                13.328229251816882,
                52.51183171458029
              ],
              [
                13.32824094540899,
                52.51183246439499
              ],
              [
                13.328250418698959,
                52.51183307183984
              ],
              [
                13.328257079606038,
                52.51183349894948
              ],
              [
                13.328266552896226,
                52.511834106394325
              ],
              [
                13.328273213803465,
                52.51183453350397
              ],
              [
                13.328282687093871,
                52.511835140948826
              ],
              [
                13.328293788606244,
                52.51183585279824
              ],
              [
                13.328284657876104,
                52.51188885879391
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A94A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32824460521901,
                52.51191547220337
              ],
              [
                13.328198126891934,
                52.51191249192719
              ],
              [
                13.328182288736615,
                52.51191147635537
              ],
              [
                13.328165266420555,
                52.51191038485295
              ],
              [
                13.328170457186657,
                52.51188025130767
              ],
              [
                13.328249795985805,
                52.5118853386581
              ],
              [
                13.32824460521901,
                52.51191547220337
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A94E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328562363791583,
                52.51192566141441
              ],
              [
                13.328526838938833,
                52.511923383496146
              ],
              [
                13.328525353627722,
                52.5119320060467
              ],
              [
                13.328378665609788,
                52.5119226001427
              ],
              [
                13.328362827450297,
                52.51192158457084
              ],
              [
                13.328322862002734,
                52.5119190219129
              ],
              [
                13.328326348573022,
                52.511898781609915
              ],
              [
                13.32856436505207,
                52.51191404366202
              ],
              [
                13.328562363791583,
                52.51192566141441
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A94F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328383844072691,
                52.511999832293455
              ],
              [
                13.328382436930717,
                52.512008001025464
              ],
              [
                13.328379810264288,
                52.5120232493252
              ],
              [
                13.32836944885152,
                52.51202258493239
              ],
              [
                13.328355534954614,
                52.5120216927478
              ],
              [
                13.328326819040626,
                52.512019851430615
              ],
              [
                13.328327006659707,
                52.512018762266344
              ],
              [
                13.328328617056377,
                52.512009413606414
              ],
              [
                13.32832980530979,
                52.51200251556602
              ],
              [
                13.328331415705263,
                52.51199316690605
              ],
              [
                13.32833260395779,
                52.51198626886566
              ],
              [
                13.328334214352074,
                52.51197692020563
              ],
              [
                13.328336778473581,
                52.511962034960554
              ],
              [
                13.328355280995657,
                52.511963221376256
              ],
              [
                13.328371119154948,
                52.5119642369481
              ],
              [
                13.328389769698134,
                52.51196543285515
              ],
              [
                13.328383844072691,
                52.511999832293455
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A955',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328480446382644,
                52.5122452611766
              ],
              [
                13.328346192070809,
                52.512236652544274
              ],
              [
                13.328298677596294,
                52.51223360582882
              ],
              [
                13.328292830799812,
                52.512233230921474
              ],
              [
                13.328293698546423,
                52.51222819353692
              ],
              [
                13.328295105702647,
                52.51222002480519
              ],
              [
                13.328296723931677,
                52.51221063076371
              ],
              [
                13.328316706654164,
                52.51221191209263
              ],
              [
                13.328331952731714,
                52.51221288969917
              ],
              [
                13.328350085203025,
                52.512214052386526
              ],
              [
                13.328359247337671,
                52.51216086486633
              ],
              [
                13.328493501652485,
                52.51216947349875
              ],
              [
                13.328480446382644,
                52.5122452611766
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A956',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32842506738697,
                52.51228135318814
              ],
              [
                13.32841766637742,
                52.51228087862186
              ],
              [
                13.328407304964182,
                52.51228021422907
              ],
              [
                13.32838954254187,
                52.51227907526999
              ],
              [
                13.328392528846043,
                52.51226173940612
              ],
              [
                13.328402594218685,
                52.512262384816275
              ],
              [
                13.328402860015277,
                52.51226084183364
              ],
              [
                13.328392794642637,
                52.512260196423476
              ],
              [
                13.328395296256701,
                52.51224567423383
              ],
              [
                13.32841720324451,
                52.51224707895002
              ],
              [
                13.328414701630354,
                52.512261601139684
              ],
              [
                13.328411741226567,
                52.51226141131316
              ],
              [
                13.328411475429965,
                52.512262954295814
              ],
              [
                13.32841769227793,
                52.512263352931484
              ],
              [
                13.328417958074533,
                52.51226180994886
              ],
              [
                13.328416477872631,
                52.512261715035585
              ],
              [
                13.3284189794868,
                52.51224719284593
              ],
              [
                13.328430821102149,
                52.512247952151995
              ],
              [
                13.328428319487934,
                52.512262474341625
              ],
              [
                13.328426839286003,
                52.51226237942838
              ],
              [
                13.3284265734894,
                52.51226392241103
              ],
              [
                13.32842805369133,
                52.5122640173243
              ],
              [
                13.32842506738697,
                52.51228135318814
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A957',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32838599005746,
                52.51227884747819
              ],
              [
                13.328343064205637,
                52.5122760949938
              ],
              [
                13.328343337819977,
                52.5122745066293
              ],
              [
                13.328344823154584,
                52.51226588407923
              ],
              [
                13.328344940417816,
                52.51226520335159
              ],
              [
                13.328387866269768,
                52.512267955835995
              ],
              [
                13.32838599005746,
                52.51227884747819
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A958',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32834312092258,
                52.512265774929006
              ],
              [
                13.328341635587968,
                52.51227439747907
              ],
              [
                13.328341361973646,
                52.512275985843544
              ],
              [
                13.32832396960345,
                52.51227487061282
              ],
              [
                13.328301322517833,
                52.51227341844004
              ],
              [
                13.328282523957126,
                52.51227221304171
              ],
              [
                13.32828868418297,
                52.512236452149715
              ],
              [
                13.32829808346335,
                52.51223705484888
              ],
              [
                13.328345597937822,
                52.51224010156433
              ],
              [
                13.328347522200097,
                52.51224022495157
              ],
              [
                13.328346373021567,
                52.51224689608243
              ],
              [
                13.328343949582685,
                52.51226096445366
              ],
              [
                13.32834312092258,
                52.512265774929006
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A959',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328443705357591,
                52.51239643019074
              ],
              [
                13.32844264216457,
                52.51240260212119
              ],
              [
                13.328438061050079,
                52.51242919588028
              ],
              [
                13.328436997855485,
                52.51243536781068
              ],
              [
                13.328434871465376,
                52.512447711671534
              ],
              [
                13.32842895065768,
                52.51244733201852
              ],
              [
                13.328414148638691,
                52.51244638288597
              ],
              [
                13.328275009675714,
                52.51243746104017
              ],
              [
                13.328260207660046,
                52.51243651190768
              ],
              [
                13.328254286853861,
                52.51243613225467
              ],
              [
                13.328256413243325,
                52.5124237883938
              ],
              [
                13.328257476437592,
                52.51241761646339
              ],
              [
                13.328262057550685,
                52.51239102270425
              ],
              [
                13.328263120743372,
                52.512384850773785
              ],
              [
                13.32826778002541,
                52.51235780319611
              ],
              [
                13.328268843216486,
                52.51235163126562
              ],
              [
                13.328273393045464,
                52.51232521903352
              ],
              [
                13.328274456234945,
                52.51231904710302
              ],
              [
                13.328278161760652,
                52.512297536109784
              ],
              [
                13.328279537651127,
                52.51228954890553
              ],
              [
                13.32828214871462,
                52.51227439137014
              ],
              [
                13.328300947275311,
                52.51227559676846
              ],
              [
                13.32832359436092,
                52.51227704894125
              ],
              [
                13.328406929721597,
                52.51228239255749
              ],
              [
                13.328417291134821,
                52.51228305695029
              ],
              [
                13.328434905537666,
                52.51228418641802
              ],
              [
                13.328445266951315,
                52.51228485081081
              ],
              [
                13.328462733334677,
                52.512285970787254
              ],
              [
                13.32846070076982,
                52.51229777006627
              ],
              [
                13.328459637581599,
                52.512303941996834
              ],
              [
                13.328455040852642,
                52.51233062652003
              ],
              [
                13.328453977662834,
                52.51233679845056
              ],
              [
                13.328449427832455,
                52.512363210682594
              ],
              [
                13.328448364641062,
                52.51236938261307
              ],
              [
                13.328443705357591,
                52.51239643019074
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A95A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328416124782983,
                52.51255653732577
              ],
              [
                13.328409019813986,
                52.51255608174216
              ],
              [
                13.328406049105162,
                52.512573326841476
              ],
              [
                13.328239674436446,
                52.512562658592
              ],
              [
                13.328242645144417,
                52.51254541349267
              ],
              [
                13.32823554017723,
                52.512544957909064
              ],
              [
                13.32824155976247,
                52.51251001389178
              ],
              [
                13.32825280929401,
                52.51251073523248
              ],
              [
                13.328266723188545,
                52.51251162741705
              ],
              [
                13.328352722904636,
                52.51251714187694
              ],
              [
                13.328357350943717,
                52.51249027582723
              ],
              [
                13.32835971926654,
                52.51249042768842
              ],
              [
                13.328376445546663,
                52.5124915002082
              ],
              [
                13.3284267724097,
                52.512494727258826
              ],
              [
                13.328416124782983,
                52.51255653732577
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A95B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328349608208173,
                52.512514372702086
              ],
              [
                13.328267160976205,
                52.512509086033965
              ],
              [
                13.328253247081658,
                52.51250819384941
              ],
              [
                13.32824199755011,
                52.51250747250871
              ],
              [
                13.328246187800685,
                52.51248314784203
              ],
              [
                13.32835379845951,
                52.512490048035424
              ],
              [
                13.328349608208173,
                52.512514372702086
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A95C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328377758908118,
                52.51248387605893
              ],
              [
                13.328361032627958,
                52.51248280353918
              ],
              [
                13.32824750116184,
                52.51247552369275
              ],
              [
                13.32825081583321,
                52.512456281792126
              ],
              [
                13.328251879029063,
                52.512450109861746
              ],
              [
                13.328254130501657,
                52.512437039891466
              ],
              [
                13.328260051307836,
                52.51243741954448
              ],
              [
                13.328274853323506,
                52.51243836867699
              ],
              [
                13.328384388249399,
                52.51244539225769
              ],
              [
                13.328377758908118,
                52.51248387605893
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A95D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328428085771264,
                52.51248710310956
              ],
              [
                13.32837923910993,
                52.51248397097217
              ],
              [
                13.32838586845122,
                52.51244548717094
              ],
              [
                13.328413992286436,
                52.51244729052278
              ],
              [
                13.328428794305426,
                52.51244823965533
              ],
              [
                13.328434715113117,
                52.512448619308344
              ],
              [
                13.328432463639823,
                52.51246168927859
              ],
              [
                13.32843140044365,
                52.51246786120897
              ],
              [
                13.328428085771264,
                52.51248710310956
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1A95F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32842787977061,
                52.51228153352332
              ],
              [
                13.328435280780276,
                52.51228200808961
              ],
              [
                13.328445642193934,
                52.512282672482414
              ],
              [
                13.328463108577301,
                52.51228379245883
              ],
              [
                13.32846609488185,
                52.51226645659497
              ],
              [
                13.328456325548673,
                52.51226583016748
              ],
              [
                13.328456591345297,
                52.512264287184834
              ],
              [
                13.328466360678476,
                52.51226491361233
              ],
              [
                13.328468862292857,
                52.5122503914227
              ],
              [
                13.328447991444772,
                52.51224905314576
              ],
              [
                13.32844548983048,
                52.51226357533542
              ],
              [
                13.328447710133434,
                52.51226371770529
              ],
              [
                13.328447444336826,
                52.51226526068794
              ],
              [
                13.328441227488577,
                52.51226486205226
              ],
              [
                13.328441493285196,
                52.51226331906962
              ],
              [
                13.328442973487158,
                52.512263413982886
              ],
              [
                13.328445475101432,
                52.51224889179325
              ],
              [
                13.328433633485828,
                52.51224813248718
              ],
              [
                13.328431131871593,
                52.51226265467682
              ],
              [
                13.328432612073533,
                52.51226274959008
              ],
              [
                13.328432346276925,
                52.512264292572716
              ],
              [
                13.328430866074989,
                52.512264197659455
              ],
              [
                13.32842787977061,
                52.51228153352332
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1DB5F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326737539113893,
                52.51212052153713
              ],
              [
                13.326735124148966,
                52.51213454076942
              ],
              [
                13.326656673645102,
                52.51212951036885
              ],
              [
                13.326626108371967,
                52.5121275504633
              ],
              [
                13.3266050147333,
                52.51212619789759
              ],
              [
                13.326607219272143,
                52.51211340021776
              ],
              [
                13.326622169272053,
                52.512114358841266
              ],
              [
                13.326624733414965,
                52.512099473596535
              ],
              [
                13.326618072523852,
                52.51209904648708
              ],
              [
                13.326621824924915,
                52.51207726320211
              ],
              [
                13.32662848581608,
                52.512077690311585
              ],
              [
                13.326632488373136,
                52.51205445480752
              ],
              [
                13.326725444817209,
                52.51206041535764
              ],
              [
                13.32674217105769,
                52.51206148787703
              ],
              [
                13.326747647791386,
                52.51206183905594
              ],
              [
                13.326743754678546,
                52.51208443921414
              ],
              [
                13.326737539113893,
                52.51212052153713
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22A18',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325746093667425,
                52.511755538085545
              ],
              [
                13.32577510551267,
                52.511757398384276
              ],
              [
                13.32577959269331,
                52.511731349204545
              ],
              [
                13.325750580847847,
                52.51172948890583
              ],
              [
                13.325746093667425,
                52.511755538085545
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22B0B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328289954620818,
                52.51191718712969
              ],
              [
                13.328314673988773,
                52.51191877218106
              ],
              [
                13.328319810034007,
                52.51188895630868
              ],
              [
                13.328295090665833,
                52.511887371257316
              ],
              [
                13.328289954620818,
                52.51191718712969
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22B8A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328345526733935,
                52.512261799713386
              ],
              [
                13.328386380303353,
                52.51226441931922
              ],
              [
                13.32838967148967,
                52.512245313563476
              ],
              [
                13.328348817920025,
                52.51224269395763
              ],
              [
                13.328345526733935,
                52.512261799713386
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22C46',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325771139965187,
                52.5121287973454
              ],
              [
                13.325745532571649,
                52.512127155347095
              ],
              [
                13.325749980740426,
                52.51210133307819
              ],
              [
                13.32577558813416,
                52.512102975076495
              ],
              [
                13.325771139965187,
                52.5121287973454
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22C7E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328188565716333,
                52.51228357796788
              ],
              [
                13.328164142393579,
                52.51228201189925
              ],
              [
                13.328168676573622,
                52.51225569043058
              ],
              [
                13.328193099896573,
                52.51225725649921
              ],
              [
                13.328188565716333,
                52.51228357796788
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22F14',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32808405229324,
                52.51230582862151
              ],
              [
                13.328131233707426,
                52.512308853981324
              ],
              [
                13.328141302719448,
                52.51225040216828
              ],
              [
                13.328131237350652,
                52.512249756758166
              ],
              [
                13.328134364367475,
                52.51223160402107
              ],
              [
                13.3280972483211,
                52.51222922407135
              ],
              [
                13.328095583183497,
                52.5122388904129
              ],
              [
                13.328094199479905,
                52.51224692299
              ],
              [
                13.328094199479905,
                52.51224692299
              ],
              [
                13.32808405229324,
                52.51230582862151
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22F26',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328032245253961,
                52.51230250665785
              ],
              [
                13.328082202041774,
                52.51230570997997
              ],
              [
                13.328091997438879,
                52.512248846531364
              ],
              [
                13.328073568949,
                52.51224766486238
              ],
              [
                13.328060477678436,
                52.51224682542578
              ],
              [
                13.328042040650228,
                52.51224564320924
              ],
              [
                13.328042040650228,
                52.51224564320924
              ],
              [
                13.328032245253961,
                52.51230250665785
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22F39',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327980438218589,
                52.51229918469424
              ],
              [
                13.328028359726169,
                52.51230225751056
              ],
              [
                13.32802945418511,
                52.51229590405265
              ],
              [
                13.328031489461582,
                52.512296034558375
              ],
              [
                13.32804019039887,
                52.51224552456768
              ],
              [
                13.328021761910389,
                52.51224434289869
              ],
              [
                13.328008662131245,
                52.512243502916476
              ],
              [
                13.327990226448161,
                52.5122423628442
              ],
              [
                13.327987391935675,
                52.51225881754539
              ],
              [
                13.327986102017803,
                52.512266305547996
              ],
              [
                13.327980438218589,
                52.51229918469424
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22F4B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327930962503467,
                52.512296012219004
              ],
              [
                13.327978587967396,
                52.51229906605266
              ],
              [
                13.327984157977875,
                52.512266731489966
              ],
              [
                13.32798554168446,
                52.51225869890383
              ],
              [
                13.327988383362776,
                52.512242202604035
              ],
              [
                13.327969954846083,
                52.51224102093315
              ],
              [
                13.327956855067931,
                52.51224018095096
              ],
              [
                13.327938426581682,
                52.51223899928199
              ],
              [
                13.327935584902042,
                52.51225549559086
              ],
              [
                13.327934201197136,
                52.51226352816793
              ],
              [
                13.327929725645891,
                52.512289509272705
              ],
              [
                13.327932056962226,
                52.51228965876108
              ],
              [
                13.327930962503467,
                52.512296012219004
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22F5D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327876824159599,
                52.512292540767106
              ],
              [
                13.327925041700098,
                52.51229563256603
              ],
              [
                13.327926136158853,
                52.51228927910808
              ],
              [
                13.32792787539484,
                52.51228939063115
              ],
              [
                13.327932444768344,
                52.51226286494501
              ],
              [
                13.327933688524235,
                52.512255957996615
              ],
              [
                13.327936576330602,
                52.51223888064042
              ],
              [
                13.3279181478153,
                52.51223769896957
              ],
              [
                13.32790504803813,
                52.51223685898735
              ],
              [
                13.327886619553288,
                52.512235677318415
              ],
              [
                13.327876824159599,
                52.512292540767106
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22F6F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327827348451954,
                52.51228936829195
              ],
              [
                13.327874973908685,
                52.51229242212554
              ],
              [
                13.327885074968247,
                52.51223378424682
              ],
              [
                13.327886458672925,
                52.51222575166062
              ],
              [
                13.327888169931844,
                52.51221581757523
              ],
              [
                13.32787263940108,
                52.51221482172852
              ],
              [
                13.327857981777683,
                52.51221388185425
              ],
              [
                13.327842468734904,
                52.51221288712883
              ],
              [
                13.327840795782045,
                52.512222598843216
              ],
              [
                13.32783654020517,
                52.51222232596763
              ],
              [
                13.327834433379559,
                52.51223455636517
              ],
              [
                13.327833143484778,
                52.51224204436924
              ],
              [
                13.327826111594531,
                52.512282865345654
              ],
              [
                13.327828442910516,
                52.51228301483402
              ],
              [
                13.327827348451954,
                52.51228936829195
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22F81',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327773210116264,
                52.512285896840126
              ],
              [
                13.327821427649475,
                52.512288988638986
              ],
              [
                13.32782252210804,
                52.512282635181045
              ],
              [
                13.327824261343757,
                52.5122827467041
              ],
              [
                13.327831293233983,
                52.512241925727665
              ],
              [
                13.327832676939266,
                52.51223389314153
              ],
              [
                13.327834689954363,
                52.51222220732608
              ],
              [
                13.327830434377528,
                52.5122219344505
              ],
              [
                13.327831372482004,
                52.512216488629356
              ],
              [
                13.32782145513781,
                52.51221585271063
              ],
              [
                13.327822189986135,
                52.512211586817386
              ],
              [
                13.327817046289018,
                52.51221125699388
              ],
              [
                13.32780572275447,
                52.512210530907566
              ],
              [
                13.327786406137148,
                52.51220929228978
              ],
              [
                13.327773210116264,
                52.512285896840126
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22F93',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327723882436139,
                52.51228273385639
              ],
              [
                13.327771359865633,
                52.512285778198574
              ],
              [
                13.32778455588647,
                52.51220917364822
              ],
              [
                13.32776523926974,
                52.51220793503041
              ],
              [
                13.327753915736052,
                52.5122072089441
              ],
              [
                13.32774063093696,
                52.51220635709775
              ],
              [
                13.327738957984394,
                52.512216068812165
              ],
              [
                13.327732926167641,
                52.51221568204068
              ],
              [
                13.327730913151324,
                52.51222736786521
              ],
              [
                13.327729623234893,
                52.51223485586787
              ],
              [
                13.32772265391001,
                52.51227531378193
              ],
              [
                13.32772249755884,
                52.51227622141876
              ],
              [
                13.327724976894512,
                52.51227638039844
              ],
              [
                13.327723882436139,
                52.51228273385639
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22FB7',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327565982076578,
                52.51227260898665
              ],
              [
                13.327614347615238,
                52.51227571027669
              ],
              [
                13.32761544207341,
                52.51226935681875
              ],
              [
                13.327617033288591,
                52.512269458850476
              ],
              [
                13.327621496329384,
                52.51224355035657
              ],
              [
                13.327622786257018,
                52.51223606235161
              ],
              [
                13.327624162927542,
                52.512228070603
              ],
              [
                13.327625452789903,
                52.512220582596896
              ],
              [
                13.327625734219765,
                52.51221894885961
              ],
              [
                13.327575777465093,
                52.51221574553784
              ],
              [
                13.327575527307026,
                52.51221719774713
              ],
              [
                13.327574237420412,
                52.512224685752386
              ],
              [
                13.32756598208574,
                52.512272608987246
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22FB8',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327516802431358,
                52.51226945549439
              ],
              [
                13.32756413182651,
                52.512272490345126
              ],
              [
                13.327572387161156,
                52.51222456711027
              ],
              [
                13.327573677047637,
                52.512217079105696
              ],
              [
                13.327577327835032,
                52.512195885794156
              ],
              [
                13.327558011231815,
                52.51219464717689
              ],
              [
                13.327546687701545,
                52.51219392109061
              ],
              [
                13.327533550926471,
                52.51219307873564
              ],
              [
                13.327531877974499,
                52.512202790450054
              ],
              [
                13.327525698139567,
                52.51220239418729
              ],
              [
                13.32751526953444,
                52.512262933565445
              ],
              [
                13.327517896889352,
                52.51226310203644
              ],
              [
                13.327516802431358,
                52.51226945549439
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22FBA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327462368080234,
                52.512265965060166
              ],
              [
                13.327510881631579,
                52.51226907584145
              ],
              [
                13.327511976089552,
                52.512262722383525
              ],
              [
                13.3275134192845,
                52.512262814923886
              ],
              [
                13.32752384788959,
                52.51220227554574
              ],
              [
                13.327521516574643,
                52.512202126057396
              ],
              [
                13.327523189526595,
                52.51219241434299
              ],
              [
                13.327500542467389,
                52.512190962170486
              ],
              [
                13.327484112248442,
                52.512189908633566
              ],
              [
                13.327475564094147,
                52.51218936050962
              ],
              [
                13.327473793423168,
                52.51219963949706
              ],
              [
                13.32747250350795,
                52.51220712749973
              ],
              [
                13.327462368080234,
                52.512265965060166
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22FBB',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327412596362558,
                52.51226277360269
              ],
              [
                13.327413690820357,
                52.51225642014473
              ],
              [
                13.327410730420896,
                52.512256230318265
              ],
              [
                13.327421018308574,
                52.512196507813194
              ],
              [
                13.327424348758026,
                52.51219672136797
              ],
              [
                13.327426162425287,
                52.5121861927804
              ],
              [
                13.327473713844313,
                52.51218924186808
              ],
              [
                13.32747194317334,
                52.51219952085551
              ],
              [
                13.327470559469614,
                52.5122075534417
              ],
              [
                13.32746051783044,
                52.51226584641865
              ],
              [
                13.327412596362558,
                52.51226277360269
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22FBC',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32762026841592,
                52.51227608992965
              ],
              [
                13.32766774583824,
                52.51227913427178
              ],
              [
                13.327673397170186,
                52.51224632773922
              ],
              [
                13.327674687041993,
                52.51223883974285
              ],
              [
                13.327675586842972,
                52.5122336162837
              ],
              [
                13.327657306354713,
                52.51223244410424
              ],
              [
                13.327644206582542,
                52.51223160412208
              ],
              [
                13.327625630084693,
                52.51223041296188
              ],
              [
                13.327624730285358,
                52.51223563641197
              ],
              [
                13.327623440368269,
                52.5122431244146
              ],
              [
                13.32761888353881,
                52.51226957749202
              ],
              [
                13.327621362874103,
                52.51226973647169
              ],
              [
                13.32762026841592,
                52.51227608992965
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '22FBF',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32768005596966,
                52.512218532007914
              ],
              [
                13.327730012731934,
                52.51222173532977
              ],
              [
                13.327731075917116,
                52.51221556339913
              ],
              [
                13.327728596581407,
                52.512215404419464
              ],
              [
                13.327730269533957,
                52.51220569270509
              ],
              [
                13.327682792107144,
                52.51220264836289
              ],
              [
                13.327681607750742,
                52.5122095237121
              ],
              [
                13.327680317885992,
                52.51221701173637
              ],
              [
                13.32768005596966,
                52.512218532007914
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '231BE',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32788681499184,
                52.51223454277235
              ],
              [
                13.327905243476694,
                52.512235724441304
              ],
              [
                13.327918343253863,
                52.51223656442351
              ],
              [
                13.327957050506509,
                52.51223904640486
              ],
              [
                13.327970150284674,
                52.512239886387086
              ],
              [
                13.328008857569845,
                52.51224236837038
              ],
              [
                13.32802195734899,
                52.512243208352636
              ],
              [
                13.328060664607483,
                52.51224569033407
              ],
              [
                13.328073764387621,
                52.512246530316304
              ],
              [
                13.3280921928775,
                52.512247711985296
              ],
              [
                13.328092349228404,
                52.512246804348436
              ],
              [
                13.328093732931992,
                52.51223877177134
              ],
              [
                13.328095398069582,
                52.512229105429775
              ],
              [
                13.328086257827191,
                52.512228519340454
              ],
              [
                13.328069975614312,
                52.512227475294736
              ],
              [
                13.328049696858825,
                52.51222617498325
              ],
              [
                13.328048023905378,
                52.51223588669761
              ],
              [
                13.328037662497728,
                52.51223522230485
              ],
              [
                13.328039335451141,
                52.512225510590504
              ],
              [
                13.327972527305572,
                52.512221226727696
              ],
              [
                13.327954371914037,
                52.51222006257015
              ],
              [
                13.327953715240923,
                52.512223874644924
              ],
              [
                13.32794542611593,
                52.51222334313075
              ],
              [
                13.32794440983588,
                52.51222924277032
              ],
              [
                13.327934048429789,
                52.5122285783776
              ],
              [
                13.327935721382907,
                52.51221886666324
              ],
              [
                13.327890016452784,
                52.512215957870026
              ],
              [
                13.32788830892387,
                52.51222587030218
              ],
              [
                13.327886925219191,
                52.512233902888376
              ],
              [
                13.32788681499184,
                52.51223454277235
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '24DA1',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327078063959712,
                52.51217772873924
              ],
              [
                13.327131388131114,
                52.51218114798824
              ],
              [
                13.3271330141771,
                52.512171708564885
              ],
              [
                13.327140415172783,
                52.51217218313102
              ],
              [
                13.32713878912678,
                52.51218162255438
              ],
              [
                13.327144512301471,
                52.5121819895354
              ],
              [
                13.32715503431255,
                52.512182664227055
              ],
              [
                13.327158012790592,
                52.51216537374485
              ],
              [
                13.327159492989779,
                52.51216546865807
              ],
              [
                13.327159492989779,
                52.51216546865807
              ],
              [
                13.327156514511728,
                52.51218275914027
              ],
              [
                13.327158327755729,
                52.512182875409
              ],
              [
                13.327170169349298,
                52.51218363471482
              ],
              [
                13.327187302655355,
                52.51218473333543
              ],
              [
                13.327190601652072,
                52.51216558219765
              ],
              [
                13.327185272934779,
                52.512165240510015
              ],
              [
                13.327186625366231,
                52.51215738945114
              ],
              [
                13.327161165939671,
                52.51215575694361
              ],
              [
                13.327160853238814,
                52.51215757221735
              ],
              [
                13.32715937303962,
                52.51215747730412
              ],
              [
                13.327159685740478,
                52.512155662030395
              ],
              [
                13.327082715387032,
                52.512150726542544
              ],
              [
                13.327080424852948,
                52.51216402342263
              ],
              [
                13.327080346677702,
                52.51216447724104
              ],
              [
                13.327078063959712,
                52.51217772873924
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '24DC5',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326923566032303,
                52.51215827834349
              ],
              [
                13.326925194250855,
                52.512158382748055
              ],
              [
                13.32692507698799,
                52.51215906347568
              ],
              [
                13.326937436647079,
                52.51215985600109
              ],
              [
                13.326950758435569,
                52.51216071022009
              ],
              [
                13.32696963096971,
                52.51216192036369
              ],
              [
                13.326982952758822,
                52.51216277458272
              ],
              [
                13.327001825293848,
                52.512163984726335
              ],
              [
                13.327015147083598,
                52.51216483894537
              ],
              [
                13.327021437928847,
                52.51216524232656
              ],
              [
                13.327021555191722,
                52.512164561598944
              ],
              [
                13.327027179947532,
                52.5121649222692
              ],
              [
                13.327030424219277,
                52.51214608880424
              ],
              [
                13.326926810303487,
                52.512139444878535
              ],
              [
                13.326923566032303,
                52.51215827834349
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '24E50',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326415362564576,
                52.512199838456674
              ],
              [
                13.326513055611498,
                52.51220610272871
              ],
              [
                13.326514150067664,
                52.512199749270714
              ],
              [
                13.32651711046325,
                52.51219993909716
              ],
              [
                13.326528258263066,
                52.51213522458891
              ],
              [
                13.326521227323434,
                52.512134773751136
              ],
              [
                13.326522822097123,
                52.51212551585511
              ],
              [
                13.326470645126236,
                52.51212217016433
              ],
              [
                13.326455843149402,
                52.51212122103222
              ],
              [
                13.32642993969071,
                52.51211956005099
              ],
              [
                13.326428344917273,
                52.512128817947016
              ],
              [
                13.326425569546762,
                52.51212863998474
              ],
              [
                13.326414421748902,
                52.512193354493036
              ],
              [
                13.326416457020567,
                52.51219348499871
              ],
              [
                13.326415362564576,
                52.512199838456674
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '24E63',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326311748742143,
                52.512193194531996
              ],
              [
                13.326359670133078,
                52.51219626734715
              ],
              [
                13.326368066166655,
                52.51214752723867
              ],
              [
                13.326369449866904,
                52.51213949466148
              ],
              [
                13.326370075268683,
                52.512135864114036
              ],
              [
                13.326320118605512,
                52.51213266079318
              ],
              [
                13.326318579335325,
                52.51214159647814
              ],
              [
                13.326317035373998,
                52.51215055939216
              ],
              [
                13.326315651672969,
                52.512158591969346
              ],
              [
                13.326310807926586,
                52.51218671056834
              ],
              [
                13.326312843197943,
                52.512186841074005
              ],
              [
                13.326311748742143,
                52.512193194531996
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '24E64',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326257906565598,
                52.51218974206403
              ],
              [
                13.326305827952767,
                52.51219281487914
              ],
              [
                13.326306922408548,
                52.512186461421166
              ],
              [
                13.326308957679906,
                52.512186591926834
              ],
              [
                13.32631380142627,
                52.51215847332783
              ],
              [
                13.326315185127296,
                52.51215044075063
              ],
              [
                13.326316313981199,
                52.51214388759432
              ],
              [
                13.326297885509444,
                52.51214270592393
              ],
              [
                13.32628478576326,
                52.512141865942034
              ],
              [
                13.32626635732197,
                52.51214068427353
              ],
              [
                13.326265226906227,
                52.51214724649714
              ],
              [
                13.326263843203753,
                52.51215527908339
              ],
              [
                13.326257906565598,
                52.51218974206403
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '24E65',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326208134935372,
                52.51218655060746
              ],
              [
                13.326256056319059,
                52.51218962342252
              ],
              [
                13.326261992957193,
                52.51215516044187
              ],
              [
                13.32626337665966,
                52.512147127855634
              ],
              [
                13.32626492218279,
                52.5121381558743
              ],
              [
                13.326266305884177,
                52.51213012328804
              ],
              [
                13.326266461452839,
                52.51212922018936
              ],
              [
                13.326216504797221,
                52.51212601686859
              ],
              [
                13.326215879395605,
                52.512129647416046
              ],
              [
                13.32621449573857,
                52.51213768000517
              ],
              [
                13.32620719416434,
                52.51218006664665
              ],
              [
                13.326209229390978,
                52.51218019714945
              ],
              [
                13.326208134935372,
                52.51218655060746
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '24E66',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326154292766972,
                52.51218309813958
              ],
              [
                13.32620221414689,
                52.51218617095462
              ],
              [
                13.32620330860248,
                52.51217981749662
              ],
              [
                13.326205343873523,
                52.512179948002284
              ],
              [
                13.326212645447734,
                52.5121375613608
              ],
              [
                13.32621402914917,
                52.51212952877454
              ],
              [
                13.326216491667424,
                52.51211523349387
              ],
              [
                13.326206611351516,
                52.51211459994821
              ],
              [
                13.326208206124356,
                52.51210534205217
              ],
              [
                13.326168129788163,
                52.51210277227709
              ],
              [
                13.326154292766972,
                52.51218309813958
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251AA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326104521144265,
                52.51217990668307
              ],
              [
                13.326152442520709,
                52.51218297949807
              ],
              [
                13.326166279541855,
                52.51210265363558
              ],
              [
                13.326142463304848,
                52.51210112649074
              ],
              [
                13.326130518164845,
                52.5121003606121
              ],
              [
                13.326119098251187,
                52.51209962834427
              ],
              [
                13.326117503490154,
                52.51210888617321
              ],
              [
                13.326114728120901,
                52.512108708210945
              ],
              [
                13.326114330040024,
                52.51211113674154
              ],
              [
                13.326113133957625,
                52.51211808017739
              ],
              [
                13.326111194599628,
                52.51212922082233
              ],
              [
                13.326109935984684,
                52.51213652726277
              ],
              [
                13.326103580328937,
                52.51217342271941
              ],
              [
                13.326105615599678,
                52.51217355322507
              ],
              [
                13.326104521144265,
                52.51217990668307
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251AB',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326050678984,
                52.51217645421528
              ],
              [
                13.326098600356676,
                52.51217952703024
              ],
              [
                13.326099694812076,
                52.51217317357224
              ],
              [
                13.326101730082804,
                52.51217330407788
              ],
              [
                13.326108148272482,
                52.51213604560285
              ],
              [
                13.326109344353481,
                52.51212910218081
              ],
              [
                13.326110063568532,
                52.51212492703309
              ],
              [
                13.326090747007555,
                52.512123688453016
              ],
              [
                13.326078535389682,
                52.512122905382796
              ],
              [
                13.326060106924324,
                52.512121723712454
              ],
              [
                13.326050678984,
                52.51217645421528
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251AC',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326000907368822,
                52.51217326275882
              ],
              [
                13.326048828738022,
                52.512176335573784
              ],
              [
                13.326058256678316,
                52.51212160507095
              ],
              [
                13.326038052034711,
                52.512120309512184
              ],
              [
                13.326026728501198,
                52.51211958342437
              ],
              [
                13.32600830003724,
                52.51211840175405
              ],
              [
                13.326007675022515,
                52.51212203005592
              ],
              [
                13.32600638513304,
                52.51212951805101
              ],
              [
                13.325999966553605,
                52.51216677879518
              ],
              [
                13.326002001824039,
                52.512166909300845
              ],
              [
                13.326000907368822,
                52.51217326275882
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251AD',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32594706521669,
                52.51216981029114
              ],
              [
                13.32599498658213,
                52.51217288310604
              ],
              [
                13.325996081037333,
                52.51216652964801
              ],
              [
                13.325998116307758,
                52.51216666015368
              ],
              [
                13.32600453488717,
                52.51212939940951
              ],
              [
                13.32600573096797,
                52.51212245598747
              ],
              [
                13.326006449792013,
                52.51211828310882
              ],
              [
                13.325987133245544,
                52.51211704446152
              ],
              [
                13.325975809731965,
                52.512116318429364
              ],
              [
                13.325956493155358,
                52.512115079788266
              ],
              [
                13.325955868140035,
                52.51211870809385
              ],
              [
                13.325954578261756,
                52.51212619610801
              ],
              [
                13.325947065230894,
                52.51216981029204
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251AE',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325945214970986,
                52.51216969164963
              ],
              [
                13.325897293609039,
                52.512166618834776
              ],
              [
                13.325898388064058,
                52.51216026537677
              ],
              [
                13.325896352793931,
                52.51216013487113
              ],
              [
                13.325904686276747,
                52.512111757826226
              ],
              [
                13.325924002822285,
                52.51211299646542
              ],
              [
                13.32593532632968,
                52.51211372252852
              ],
              [
                13.325954642909627,
                52.51211496114676
              ],
              [
                13.325953924017586,
                52.512119134021034
              ],
              [
                13.325952727755723,
                52.512126077431446
              ],
              [
                13.325950540076294,
                52.512138778685156
              ],
              [
                13.325945214970986,
                52.51216969164963
              ],
              [
                13.325944985049127,
                52.512169676906574
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251B0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.3257936798649,
                52.512159974910894
              ],
              [
                13.325841601219626,
                52.512163047725686
              ],
              [
                13.325852377634897,
                52.51210048887033
              ],
              [
                13.325853573684725,
                52.51209354544635
              ],
              [
                13.325855438117728,
                52.51208272185556
              ],
              [
                13.325838664528144,
                52.5120816462989
              ],
              [
                13.32582230798075,
                52.512080597483795
              ],
              [
                13.325813141623446,
                52.51208000971832
              ],
              [
                13.32581024914475,
                52.51209680100045
              ],
              [
                13.325804624398932,
                52.51209644033029
              ],
              [
                13.3257936798649,
                52.512159974910894
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251B1',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325857288480478,
                52.51208284049538
              ],
              [
                13.325855423999764,
                52.51209366409233
              ],
              [
                13.32585422792032,
                52.51210060751438
              ],
              [
                13.325853074839953,
                52.512107301318146
              ],
              [
                13.32587239138799,
                52.51210853993445
              ],
              [
                13.325883714890589,
                52.51210926602043
              ],
              [
                13.325924198264156,
                52.51211186189648
              ],
              [
                13.325935521767619,
                52.51211258798244
              ],
              [
                13.325976005159053,
                52.51211518385946
              ],
              [
                13.325987328667704,
                52.512115909833405
              ],
              [
                13.32602692393981,
                52.512118448874574
              ],
              [
                13.32603812230058,
                52.51211916693606
              ],
              [
                13.326079618945778,
                52.51212182778462
              ],
              [
                13.326090942451808,
                52.51212255387063
              ],
              [
                13.326110259006535,
                52.512123792487
              ],
              [
                13.326111263672962,
                52.51211796025096
              ],
              [
                13.326112459755349,
                52.51211101681512
              ],
              [
                13.326112877874746,
                52.512108589569436
              ],
              [
                13.326110102505515,
                52.51210841160719
              ],
              [
                13.326111697278085,
                52.51209915371112
              ],
              [
                13.326052933462712,
                52.51209538565691
              ],
              [
                13.326051338690291,
                52.512104643552966
              ],
              [
                13.326006488728915,
                52.51210176768291
              ],
              [
                13.326008083501211,
                52.512092509786854
              ],
              [
                13.325911870722418,
                52.512086340428745
              ],
              [
                13.32591027595039,
                52.51209559832482
              ],
              [
                13.325902874967978,
                52.51209512375881
              ],
              [
                13.325904469739992,
                52.51208586586273
              ],
              [
                13.325857288480478,
                52.51208284049538
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251B2',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325843451465039,
                52.512163166367166
              ],
              [
                13.325891372823232,
                52.51216623918198
              ],
              [
                13.325892467278244,
                52.51215988572395
              ],
              [
                13.32589450254836,
                52.51216001622961
              ],
              [
                13.325902836183836,
                52.51211163919231
              ],
              [
                13.32588351945838,
                52.51211040053334
              ],
              [
                13.325872195946124,
                52.51210967450341
              ],
              [
                13.325852879381651,
                52.512108435982675
              ],
              [
                13.325843451465039,
                52.512163166367166
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251B3',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326361520379896,
                52.51219638598866
              ],
              [
                13.326409441774306,
                52.51219945880385
              ],
              [
                13.326410536230282,
                52.512193105345865
              ],
              [
                13.326412571501937,
                52.512193235851534
              ],
              [
                13.326423719299761,
                52.51212852134322
              ],
              [
                13.326420943929266,
                52.512128343380944
              ],
              [
                13.326422538702689,
                52.51211908548493
              ],
              [
                13.326404999588677,
                52.512117960842055
              ],
              [
                13.32639019761326,
                52.51211701170994
              ],
              [
                13.326375357405954,
                52.51211606012631
              ],
              [
                13.326372120953568,
                52.51213484820946
              ],
              [
                13.326371300113749,
                52.512139613303
              ],
              [
                13.326361520379896,
                52.51219638598866
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '251D7',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326320314043532,
                52.5121315262471
              ],
              [
                13.326370270706718,
                52.512134729567926
              ],
              [
                13.326373507159094,
                52.5121159414848
              ],
              [
                13.32633853749429,
                52.5121136991602
              ],
              [
                13.326336942721092,
                52.51212295705624
              ],
              [
                13.326321955722477,
                52.512121996059996
              ],
              [
                13.326321754148829,
                52.51212316622484
              ],
              [
                13.326320558069206,
                52.51213010964251
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '252E9',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326216700235202,
                52.51212488232251
              ],
              [
                13.326266656890834,
                52.512128085643276
              ],
              [
                13.326266906269701,
                52.512126637962474
              ],
              [
                13.326268102380084,
                52.51211969453935
              ],
              [
                13.326269893372244,
                52.512109297562
              ],
              [
                13.326222712056412,
                52.5121062722016
              ],
              [
                13.32622111728354,
                52.51211553009766
              ],
              [
                13.326218341913865,
                52.51211535213539
              ],
              [
                13.326216700235202,
                52.51212488232251
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '252EA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326266552760075,
                52.51213954972746
              ],
              [
                13.326284981201367,
                52.51214073139596
              ],
              [
                13.326298080948783,
                52.51214157139916
              ],
              [
                13.326316509415696,
                52.51214275306927
              ],
              [
                13.326316822898908,
                52.51214093325451
              ],
              [
                13.326318018987115,
                52.51213398983222
              ],
              [
                13.326318463803927,
                52.51213140760602
              ],
              [
                13.326319903902112,
                52.512123047583316
              ],
              [
                13.326320105475768,
                52.51212187741849
              ],
              [
                13.326317330105693,
                52.5121216994562
              ],
              [
                13.32631892487883,
                52.51211244156016
              ],
              [
                13.326271743589228,
                52.51210941620162
              ],
              [
                13.32626995259655,
                52.51211981318195
              ],
              [
                13.326268756516285,
                52.512126756604
              ],
              [
                13.326268062320503,
                52.512130786511676
              ],
              [
                13.326266866239639,
                52.5121377299337
              ],
              [
                13.326266552760075,
                52.51213954972746
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2727A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327264601988485,
                52.51209067415
              ],
              [
                13.327337131763844,
                52.51209532489838
              ],
              [
                13.327338617088158,
                52.51208670234807
              ],
              [
                13.327340805987642,
                52.51207399542271
              ],
              [
                13.32734143138682,
                52.512070364875214
              ],
              [
                13.327343620281543,
                52.51205765796798
              ],
              [
                13.327343835263914,
                52.5120564099582
              ],
              [
                13.32727130548773,
                52.5120517592098
              ],
              [
                13.327269187270133,
                52.5120640577101
              ],
              [
                13.327267897048776,
                52.51207154569281
              ],
              [
                13.32726460197368,
                52.512090674149064
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2727B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.3272715009248,
                52.512050624663694
              ],
              [
                13.327344030701015,
                52.51205527541209
              ],
              [
                13.327346434576459,
                52.51204132049503
              ],
              [
                13.327347059974697,
                52.51203768994749
              ],
              [
                13.327349248867717,
                52.51202498303112
              ],
              [
                13.32734946384821,
                52.51202373503041
              ],
              [
                13.327276934071326,
                52.512019084282
              ],
              [
                13.327274909344137,
                52.51203083818873
              ],
              [
                13.327273525637468,
                52.51203887076511
              ],
              [
                13.3272715009248,
                52.512050624663694
              ],
              [
                13.327276020807254,
                52.51205091448725
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2727C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32726032924455,
                52.51201687247071
              ],
              [
                13.32734965928501,
                52.512022600484315
              ],
              [
                13.327349874265485,
                52.512021352483586
              ],
              [
                13.327352063156866,
                52.512008645567164
              ],
              [
                13.327352688552606,
                52.512005015028684
              ],
              [
                13.327354877443913,
                52.511992308103146
              ],
              [
                13.327355131511382,
                52.511990833193224
              ],
              [
                13.327265801464774,
                52.51198510517924
              ],
              [
                13.32726365198846,
                52.511997585207375
              ],
              [
                13.327262455914738,
                52.512004528638656
              ],
              [
                13.32726032924455,
                52.51201687247071
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2727E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327265996906576,
                52.51198397063349
              ],
              [
                13.327355326947895,
                52.51198969864711
              ],
              [
                13.327355581013784,
                52.51198822374622
              ],
              [
                13.327357769903415,
                52.51197551682067
              ],
              [
                13.327358395298203,
                52.51197188628217
              ],
              [
                13.327360584184627,
                52.511959179365654
              ],
              [
                13.327360799166035,
                52.511957931355845
              ],
              [
                13.327271469123886,
                52.511952203342204
              ],
              [
                13.327269350915888,
                52.5119645018427
              ],
              [
                13.327268154502724,
                52.51197144524296
              ],
              [
                13.327265996906576,
                52.51198397063349
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '27280',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327222269491264,
                52.51208795970888
              ],
              [
                13.327228189083753,
                52.51208833928454
              ],
              [
                13.327246612017142,
                52.51208952059846
              ],
              [
                13.327262751739214,
                52.51209055550847
              ],
              [
                13.327266140593013,
                52.51207088247726
              ],
              [
                13.327267336652408,
                52.512063939063296
              ],
              [
                13.327271769181545,
                52.51203820754958
              ],
              [
                13.32727296507278,
                52.51203126412488
              ],
              [
                13.327275083611244,
                52.51201896562695
              ],
              [
                13.32725828355851,
                52.51201788837526
              ],
              [
                13.327260605345158,
                52.51200440997656
              ],
              [
                13.327266304244183,
                52.51197132661001
              ],
              [
                13.32726759424802,
                52.51196383860212
              ],
              [
                13.3272696189671,
                52.5119520847066
              ],
              [
                13.327263439041955,
                52.51195168843793
              ],
              [
                13.327252115512122,
                52.51195096246372
              ],
              [
                13.327245935664846,
                52.51195056620004
              ],
              [
                13.327222268286407,
                52.51208795963162
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '27A51',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326741170022352,
                52.5121341943128
              ],
              [
                13.32676544527464,
                52.512135750889605
              ],
              [
                13.326766883697637,
                52.512127400630405
              ],
              [
                13.32679959608173,
                52.51212949821258
              ],
              [
                13.32680080779639,
                52.51212246402685
              ],
              [
                13.326768983501639,
                52.512120423390726
              ],
              [
                13.326754995642217,
                52.51211952646174
              ],
              [
                13.32674974095302,
                52.51211918952078
              ],
              [
                13.326750561791702,
                52.51211442442719
              ],
              [
                13.326744640998506,
                52.51211404477431
              ],
              [
                13.326741170022352,
                52.5121341943128
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '27AAE',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326723849919093,
                52.51205664244028
              ],
              [
                13.32672607021647,
                52.512056784810106
              ],
              [
                13.326742796456967,
                52.512057857329495
              ],
              [
                13.326748273190667,
                52.51205820850841
              ],
              [
                13.32675492977937,
                52.51201956585912
              ],
              [
                13.326749453045606,
                52.51201921468021
              ],
              [
                13.326734503042852,
                52.51201825605667
              ],
              [
                13.32673050650752,
                52.51201799979097
              ],
              [
                13.326727776264631,
                52.51203384941916
              ],
              [
                13.326726580176494,
                52.512040792831286
              ],
              [
                13.326723849919093,
                52.51205664244028
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '27AC0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326633113772303,
                52.51205082426001
              ],
              [
                13.32672199967128,
                52.512056523798755
              ],
              [
                13.326724729899064,
                52.51204067418783
              ],
              [
                13.326726019661638,
                52.51203318617574
              ],
              [
                13.326728656107962,
                52.512017881139734
              ],
              [
                13.326639770359701,
                52.512012181610665
              ],
              [
                13.326639555377563,
                52.51201342962046
              ],
              [
                13.326637366483812,
                52.51202613653684
              ],
              [
                13.326636741086924,
                52.5120297670753
              ],
              [
                13.326634552189969,
                52.5120424740007
              ],
              [
                13.326633113772303,
                52.51205082426001
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '27AD2',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326639965796566,
                52.512011047064576
              ],
              [
                13.32672455912163,
                52.51201647135502
              ],
              [
                13.32672455912163,
                52.51201647135502
              ],
              [
                13.32672455912163,
                52.51201647135502
              ],
              [
                13.326727390999578,
                52.51200003179099
              ],
              [
                13.326730027440357,
                52.511984726763956
              ],
              [
                13.326731877689754,
                52.51198484539641
              ],
              [
                13.326645434116072,
                52.51197930246442
              ],
              [
                13.32664518395568,
                52.51198075469251
              ],
              [
                13.326642995065187,
                52.511993461608945
              ],
              [
                13.326642369669239,
                52.511997092147425
              ],
              [
                13.326640180777115,
                52.512009799063854
              ],
              [
                13.326639965796566,
                52.512011047064576
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2A549',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32551303339893,
                52.51189916582678
              ],
              [
                13.325609246122392,
                52.5119053351844
              ],
              [
                13.325635794078087,
                52.51175121843699
              ],
              [
                13.325539581350293,
                52.51174504907926
              ],
              [
                13.32551303339893,
                52.51189916582678
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2A5FE',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325637114566343,
                52.51191730821786
              ],
              [
                13.325691141719155,
                52.511920772549544
              ],
              [
                13.32569729404845,
                52.51188505703754
              ],
              [
                13.325643266895081,
                52.511881592705876
              ],
              [
                13.325641085829243,
                52.51189425424063
              ],
              [
                13.325639522340415,
                52.51190333060964
              ],
              [
                13.325637114566343,
                52.51191730821786
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2A5FF',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325643423243838,
                52.51188068506895
              ],
              [
                13.325697450397222,
                52.511884149400636
              ],
              [
                13.325705080209374,
                52.51183985671967
              ],
              [
                13.325651053055285,
                52.511836392387956
              ],
              [
                13.325645987217914,
                52.51186580066325
              ],
              [
                13.325644423875733,
                52.51187487619278
              ],
              [
                13.325643423243838,
                52.51188068506895
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2A600',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32565120940372,
                52.51183548475104
              ],
              [
                13.325705236557821,
                52.511838949082744
              ],
              [
                13.325712850719388,
                52.51179474716511
              ],
              [
                13.325658823564591,
                52.51179128283338
              ],
              [
                13.325657830753924,
                52.5117970463278
              ],
              [
                13.325656267272045,
                52.51180612269697
              ],
              [
                13.32565120940372,
                52.51183548475104
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2BFFE',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327209072329353,
                52.5121645641834
              ],
              [
                13.327267503198978,
                52.51216831088316
              ],
              [
                13.327270442587073,
                52.51215124731013
              ],
              [
                13.327212011717156,
                52.512147500610354
              ],
              [
                13.327209072329353,
                52.5121645641834
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2C010',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327323420120624,
                52.51185608289984
              ],
              [
                13.327385721778846,
                52.5118600778015
              ],
              [
                13.327387941923693,
                52.511847189357354
              ],
              [
                13.32732564026524,
                52.51184319445571
              ],
              [
                13.327323420120624,
                52.51185608289984
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2CC30',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327146332647093,
                52.51215173163538
              ],
              [
                13.327193254962058,
                52.51215474038473
              ],
              [
                13.327194427589944,
                52.51214793310823
              ],
              [
                13.327147505274894,
                52.512144924358886
              ],
              [
                13.327146332647093,
                52.51215173163538
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2D02B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325586979007364,
                52.51194685396785
              ],
              [
                13.32560192898509,
                52.51194781259109
              ],
              [
                13.32559812969666,
                52.51196986816754
              ],
              [
                13.325595925169509,
                52.51198266584766
              ],
              [
                13.32549971244823,
                52.511976496490114
              ],
              [
                13.325502167134006,
                52.51196224659095
              ],
              [
                13.325503214674587,
                52.5119561654238
              ],
              [
                13.325505716262834,
                52.511941643233506
              ],
              [
                13.32552791919754,
                52.511943066931416
              ],
              [
                13.325528294435648,
                52.51194088860286
              ],
              [
                13.32550609150093,
                52.511939464904984
              ],
              [
                13.325508733801456,
                52.511924125841475
              ],
              [
                13.325509781340203,
                52.51191804467426
              ],
              [
                13.325512705066206,
                52.51190107186425
              ],
              [
                13.325608917789609,
                52.51190724122187
              ],
              [
                13.325606455292858,
                52.51192153650301
              ],
              [
                13.325602304223246,
                52.511945634262574
              ],
              [
                13.32558735424551,
                52.511944675639306
              ],
              [
                13.325586979007364,
                52.51194685396785
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2D325',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325639522340415,
                52.51190333060964
              ],
              [
                13.325639279999583,
                52.51190473744684
              ],
              [
                13.325633359215956,
                52.51190435779406
              ],
              [
                13.325632929256388,
                52.511906853795516
              ],
              [
                13.325632929256388,
                52.511906853795516
              ],
              [
                13.325632677036918,
                52.511908443531716
              ],
              [
                13.325632600923587,
                52.511908759833
              ],
              [
                13.325632600923587,
                52.511908759833
              ],
              [
                13.32562598735697,
                52.51194715287369
              ],
              [
                13.32562598735697,
                52.51194715287369
              ],
              [
                13.325625612118799,
                52.51194933120221
              ],
              [
                13.325625612118799,
                52.51194933120221
              ],
              [
                13.325623485768453,
                52.51196167506394
              ],
              [
                13.325623391958853,
                52.5119622196461
              ],
              [
                13.325627536507325,
                52.511962485403025
              ],
              [
                13.325625957378664,
                52.511971652535614
              ],
              [
                13.325621812830214,
                52.511971386778654
              ],
              [
                13.325619610869074,
                52.511984169562126
              ],
              [
                13.32561960922223,
                52.51198416945651
              ],
              [
                13.325618949988197,
                52.51198799642811
              ],
              [
                13.325603111892622,
                52.511986980856925
              ],
              [
                13.32560376856054,
                52.511983168782
              ],
              [
                13.325597405365324,
                52.511982760760866
              ],
              [
                13.325599609892484,
                52.51196996308074
              ],
              [
                13.32560151972422,
                52.511958876154885
              ],
              [
                13.325600039729185,
                52.51195878007604
              ],
              [
                13.325601618856965,
                52.511949612943404
              ],
              [
                13.325603099052804,
                52.511949707856594
              ],
              [
                13.325603784419084,
                52.51194572917576
              ],
              [
                13.32560408699243,
                52.51194397268004
              ],
              [
                13.325602606796592,
                52.511943877766846
              ],
              [
                13.325604185923295,
                52.51193471063423
              ],
              [
                13.325605666119138,
                52.511934805547426
              ],
              [
                13.325607933552504,
                52.51192164265622
              ],
              [
                13.325610397985459,
                52.51190733613506
              ],
              [
                13.325610397985459,
                52.51190733613506
              ],
              [
                13.325610397985459,
                52.51190733613506
              ],
              [
                13.325610687231011,
                52.5119056570068
              ],
              [
                13.32561072631824,
                52.5119054300976
              ],
              [
                13.32561072631824,
                52.5119054300976
              ],
              [
                13.325616159439848,
                52.5118738897153
              ],
              [
                13.325614679243987,
                52.5118737948021
              ],
              [
                13.325616258365626,
                52.51186462766934
              ],
              [
                13.325617738561492,
                52.51186472258253
              ],
              [
                13.325628127920044,
                52.51180441007842
              ],
              [
                13.325626647718718,
                52.5118043151967
              ],
              [
                13.325628226835338,
                52.51179514806387
              ],
              [
                13.325629707074318,
                52.511795242727
              ],
              [
                13.325637274274012,
                52.511751313350175
              ],
              [
                13.32565947721316,
                52.51175273704813
              ],
              [
                13.325657053820855,
                52.51176680542046
              ],
              [
                13.325661198369565,
                52.5117670711774
              ],
              [
                13.325659525446225,
                52.51177678289241
              ],
              [
                13.325655380897528,
                52.51177651713545
              ],
              [
                13.32565194123974,
                52.51179648514763
              ],
              [
                13.325651902152696,
                52.511796712056864
              ],
              [
                13.32565782293652,
                52.511797091709646
              ],
              [
                13.325656243819815,
                52.51180625884248
              ],
              [
                13.325650323036015,
                52.511805879189694
              ],
              [
                13.32565029958378,
                52.51180601533524
              ],
              [
                13.325646930278113,
                52.51182557491068
              ],
              [
                13.325645288619963,
                52.51183510509825
              ],
              [
                13.325640066434218,
                52.51186542101045
              ],
              [
                13.325639941499904,
                52.511866146280475
              ],
              [
                13.325645862283611,
                52.51186652593326
              ],
              [
                13.325644423875733,
                52.51187487619278
              ],
              [
                13.32564428316188,
                52.511875693066
              ],
              [
                13.3256383623782,
                52.51187531341323
              ],
              [
                13.325635165045592,
                52.51189387458786
              ],
              [
                13.325634936333529,
                52.51189519053273
              ],
              [
                13.325640859123407,
                52.51189557031415
              ],
              [
                13.325639522340415,
                52.51190333060964
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2F7C6',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32673376359128,
                52.51181795179693
              ],
              [
                13.326756928694858,
                52.511819437188855
              ],
              [
                13.326771730678391,
                52.51182038632108
              ],
              [
                13.32677393518608,
                52.51180758864057
              ],
              [
                13.326735968098832,
                52.51180515411644
              ],
              [
                13.32673376359128,
                52.51181795179693
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2F88D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326726409369451,
                52.512016589996534
              ],
              [
                13.326755125216277,
                52.512018431313
              ],
              [
                13.326760593535292,
                52.51198668672196
              ],
              [
                13.326731877688191,
                52.511984845405465
              ],
              [
                13.326726409369451,
                52.512016589996534
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2F93D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327361209582074,
                52.511955548808984
              ],
              [
                13.327361209580523,
                52.51195554881806
              ],
              [
                13.327360994602254,
                52.51195679680972
              ],
              [
                13.32726452261408,
                52.51195061065835
              ],
              [
                13.327252310948264,
                52.51194982774402
              ],
              [
                13.327246132325008,
                52.511949431620096
              ],
              [
                13.327254085362387,
                52.51190325552463
              ],
              [
                13.327256453681448,
                52.51190340738579
              ],
              [
                13.327269775476298,
                52.51190426160488
              ],
              [
                13.32730347005472,
                52.511906422165985
              ],
              [
                13.327304444720372,
                52.51190538346867
              ],
              [
                13.32726996309462,
                52.51190317244061
              ],
              [
                13.327256641299767,
                52.51190231822152
              ],
              [
                13.327254272980712,
                52.51190216636033
              ],
              [
                13.327255601943573,
                52.51189445144667
              ],
              [
                13.32725797026264,
                52.51189460330785
              ],
              [
                13.327270551957797,
                52.511895410070316
              ],
              [
                13.327366320868485,
                52.51190155095656
              ],
              [
                13.327363428418758,
                52.51191834223923
              ],
              [
                13.327367572977778,
                52.511918607996286
              ],
              [
                13.327366212745442,
                52.51192650442826
              ],
              [
                13.327366212743883,
                52.51192650443735
              ],
              [
                13.327364023862291,
                52.51193921134485
              ],
              [
                13.32736402386072,
                52.51193921135394
              ],
              [
                13.327363398466872,
                52.511942841892456
              ],
              [
                13.327363398465307,
                52.51194284190151
              ],
              [
                13.327361209582074,
                52.511955548808984
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2FBE0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325779977592257,
                52.51209747526459
              ],
              [
                13.325775833042895,
                52.51209720950764
              ],
              [
                13.325775317086967,
                52.512100204709306
              ],
              [
                13.32577280075343,
                52.51210004335687
              ],
              [
                13.32575859087014,
                52.51209913219019
              ],
              [
                13.325750597810922,
                52.51209861965891
              ],
              [
                13.325751113766827,
                52.512095624457245
              ],
              [
                13.325746969217635,
                52.512095358700286
              ],
              [
                13.325740910640105,
                52.51213052962891
              ],
              [
                13.325773919014384,
                52.5121326461932
              ],
              [
                13.325779977592257,
                52.51209747526459
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2FBE1',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.325792705125972,
                52.512148258096346
              ],
              [
                13.325788133700676,
                52.51214796496725
              ],
              [
                13.325724912141922,
                52.51214391107194
              ],
              [
                13.325720619573294,
                52.51214363582367
              ],
              [
                13.325732470930946,
                52.512074836949054
              ],
              [
                13.325779393149094,
                52.5120778456974
              ],
              [
                13.32577949477668,
                52.51207725573344
              ],
              [
                13.325804658112665,
                52.51207886925781
              ],
              [
                13.325792705125972,
                52.512148258096346
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2FF89',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.3281696928546,
                52.51224979079102
              ],
              [
                13.32816569631093,
                52.51224953452526
              ],
              [
                13.328159614256059,
                52.51228484159874
              ],
              [
                13.32819203066624,
                52.51228692019891
              ],
              [
                13.328198112721445,
                52.51225161312544
              ],
              [
                13.328194116177594,
                52.51225135685965
              ],
              [
                13.328193631489755,
                52.5122541705339
              ],
              [
                13.32818978296608,
                52.51225392375946
              ],
              [
                13.3281760170931,
                52.51225304106621
              ],
              [
                13.328169208166782,
                52.51225260446526
              ],
              [
                13.3281696928546,
                52.51224979079102
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2FF8A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328237418681491,
                52.512236284963286
              ],
              [
                13.32822202458592,
                52.51223529786545
              ],
              [
                13.328210337346503,
                52.51230314372004
              ],
              [
                13.328206192782451,
                52.512302877962966
              ],
              [
                13.32820627095808,
                52.51230242414451
              ],
              [
                13.328140401997032,
                52.51229820050488
              ],
              [
                13.328140323821412,
                52.512298654323324
              ],
              [
                13.3281367713383,
                52.512298426531515
              ],
              [
                13.328148458576258,
                52.51223058067688
              ],
              [
                13.328131584281394,
                52.512229498665825
              ],
              [
                13.328134210973289,
                52.51221425036663
              ],
              [
                13.32813436732393,
                52.51221334272978
              ],
              [
                13.328240201724535,
                52.51222012902724
              ],
              [
                13.328240045373864,
                52.51222103666408
              ],
              [
                13.328238481866723,
                52.51223011303266
              ],
              [
                13.328237418681491,
                52.512236284963286
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2FFD9',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32762483454018,
                52.512208751001374
              ],
              [
                13.327627461895522,
                52.51220891947236
              ],
              [
                13.327625929660222,
                52.5122178142327
              ],
              [
                13.327575972982883,
                52.51221461099685
              ],
              [
                13.327579178092812,
                52.51219600443618
              ],
              [
                13.327626507492425,
                52.51219903928697
              ],
              [
                13.32762483454018,
                52.512208751001374
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '30036',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32767837379635,
                52.512217437656744
              ],
              [
                13.32767764207471,
                52.512221685397215
              ],
              [
                13.327676055898705,
                52.51223089335497
              ],
              [
                13.327675782281561,
                52.51223248173762
              ],
              [
                13.327657501793306,
                52.51223130955816
              ],
              [
                13.32764440202112,
                52.51223046957601
              ],
              [
                13.327625825523267,
                52.51222927841582
              ],
              [
                13.327626013145856,
                52.512228189242514
              ],
              [
                13.32762739685062,
                52.51222015665631
              ],
              [
                13.327627779908477,
                52.51221793295511
              ],
              [
                13.327629147979085,
                52.51220999112354
              ],
              [
                13.327629312145776,
                52.512209038113916
              ],
              [
                13.327635195941596,
                52.512209415394025
              ],
              [
                13.327636180950968,
                52.5122036972818
              ],
              [
                13.327643581952087,
                52.51220417184801
              ],
              [
                13.32764426989499,
                52.512200178245834
              ],
              [
                13.327650301710966,
                52.512200565017295
              ],
              [
                13.327661625242952,
                52.51220129110358
              ],
              [
                13.32768094185675,
                52.51220252972134
              ],
              [
                13.327679757500343,
                52.512209405070536
              ],
              [
                13.32767837379635,
                52.512217437656744
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '30163',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327332425627144,
                52.51212264476825
              ],
              [
                13.327363361798724,
                52.512124628454806
              ],
              [
                13.327359687568112,
                52.51214595792124
              ],
              [
                13.327359484312698,
                52.51214713784916
              ],
              [
                13.327336467208985,
                52.5121456619484
              ],
              [
                13.327331854871801,
                52.5121724372359
              ],
              [
                13.327281195043764,
                52.51216918883056
              ],
              [
                13.327269205428316,
                52.512168420033376
              ],
              [
                13.32727237934197,
                52.51214999500502
              ],
              [
                13.32721224624268,
                52.51214613915503
              ],
              [
                13.327219282792436,
                52.51210529090493
              ],
              [
                13.327221642886649,
                52.51209159017911
              ],
              [
                13.327273584546798,
                52.512094920778246
              ],
              [
                13.327336506363952,
                52.51209895544588
              ],
              [
                13.327332425627144,
                52.51212264476825
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '301E6',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327727789081262,
                52.51223464391043
              ],
              [
                13.327720647308343,
                52.51227610277721
              ],
              [
                13.327719056092919,
                52.51227600074548
              ],
              [
                13.327717961634562,
                52.51228235420342
              ],
              [
                13.327669596554857,
                52.51227925294321
              ],
              [
                13.327675154054328,
                52.51224699099137
              ],
              [
                13.327676537758638,
                52.512238958414315
              ],
              [
                13.327678202634347,
                52.51222929205586
              ],
              [
                13.327679492325093,
                52.51222180403876
              ],
              [
                13.327679860531196,
                52.51221966655397
              ],
              [
                13.327729817293449,
                52.51222286987584
              ],
              [
                13.327729062900804,
                52.512227249223656
              ],
              [
                13.327727789081262,
                52.51223464391043
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '30E95',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327204504476523,
                52.51215546172528
              ],
              [
                13.327193254962058,
                52.51215474038473
              ],
              [
                13.327194427589944,
                52.51214793310823
              ],
              [
                13.327147505274894,
                52.512144924358886
              ],
              [
                13.327147739800404,
                52.512143562903574
              ],
              [
                13.327143669252726,
                52.5121433018922
              ],
              [
                13.327142262099429,
                52.512151470624005
              ],
              [
                13.327113398216593,
                52.51214961981605
              ],
              [
                13.327113601472094,
                52.512148439888136
              ],
              [
                13.327109530924613,
                52.512148178876764
              ],
              [
                13.327109327669111,
                52.51214935880467
              ],
              [
                13.327073062792637,
                52.51214703343063
              ],
              [
                13.327073266048112,
                52.51214585350271
              ],
              [
                13.32706919550087,
                52.51214559249132
              ],
              [
                13.327068992245392,
                52.51214677241924
              ],
              [
                13.327065439767816,
                52.512146544627484
              ],
              [
                13.327065861913804,
                52.512144094007965
              ],
              [
                13.327062901515838,
                52.512143904181514
              ],
              [
                13.327045139128321,
                52.5121427652228
              ],
              [
                13.32703226139765,
                52.51214193947774
              ],
              [
                13.32703257935115,
                52.51214443755389
              ],
              [
                13.327030359052781,
                52.51214429518406
              ],
              [
                13.327031766205815,
                52.51213612645225
              ],
              [
                13.32702776966876,
                52.51213587018653
              ],
              [
                13.327026362515733,
                52.51214403891835
              ],
              [
                13.32699453824012,
                52.51214199828399
              ],
              [
                13.326994741495579,
                52.512140818356066
              ],
              [
                13.3269906709488,
                52.512140557344715
              ],
              [
                13.326990467693342,
                52.51214173727263
              ],
              [
                13.326954942922523,
                52.512139459355254
              ],
              [
                13.326955146177959,
                52.512138279427326
              ],
              [
                13.326951075631419,
                52.51213801841597
              ],
              [
                13.326950872375978,
                52.51213919834388
              ],
              [
                13.32691830800453,
                52.512137110252965
              ],
              [
                13.326919715157297,
                52.51212894152116
              ],
              [
                13.326917038829018,
                52.51212876990978
              ],
              [
                13.326917508314134,
                52.512126044477874
              ],
              [
                13.326919087451403,
                52.5121168773455
              ],
              [
                13.326923449184912,
                52.51209155679804
              ],
              [
                13.327157850656778,
                52.512106587074996
              ],
              [
                13.327158225896937,
                52.51210440874649
              ],
              [
                13.327154525398978,
                52.51210417146343
              ],
              [
                13.327157496048871,
                52.51208692636282
              ],
              [
                13.327161196546854,
                52.5120871636459
              ],
              [
                13.327161603056663,
                52.51208480379001
              ],
              [
                13.32715790255868,
                52.512084566506935
              ],
              [
                13.32719175216344,
                52.51188806312013
              ],
              [
                13.327211438814054,
                52.51188932546609
              ],
              [
                13.327226092787438,
                52.51189026510711
              ],
              [
                13.327245779439046,
                52.51189152745309
              ],
              [
                13.327244935156916,
                52.511896428692395
              ],
              [
                13.32724907971678,
                52.51189669444035
              ],
              [
                13.327216074389248,
                52.51208829659686
              ],
              [
                13.327212373890978,
                52.51208805931378
              ],
              [
                13.327211967381135,
                52.51209041916965
              ],
              [
                13.327215667879406,
                52.512090656452735
              ],
              [
                13.32721269722921,
                52.51210790155333
              ],
              [
                13.327208996730956,
                52.51210766427026
              ],
              [
                13.32720862149077,
                52.51210984259876
              ],
              [
                13.327212321989018,
                52.51211007988181
              ],
              [
                13.327211383888379,
                52.51211552570307
              ],
              [
                13.327215972506233,
                52.51211581993407
              ],
              [
                13.327214948412754,
                52.512121764955566
              ],
              [
                13.327213322370634,
                52.51213120437903
              ],
              [
                13.327212298276434,
                52.51213714940051
              ],
              [
                13.327207709658614,
                52.512136855169494
              ],
              [
                13.327204504476523,
                52.51215546172528
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '30F72',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326653743088599,
                52.51179988168703
              ],
              [
                13.326734487900536,
                52.511805059203205
              ],
              [
                13.326732283392992,
                52.51181785688371
              ],
              [
                13.32672987743847,
                52.51181907910257
              ],
              [
                13.32665596668329,
                52.51181433979955
              ],
              [
                13.326654872246516,
                52.51182069325798
              ],
              [
                13.326653265842305,
                52.51183050169625
              ],
              [
                13.3266525426586,
                52.51183421704796
              ],
              [
                13.326593852806031,
                52.51183045373884
              ],
              [
                13.32660682968551,
                52.511755119874636
              ],
              [
                13.326611640329082,
                52.511755428342596
              ],
              [
                13.326624370032231,
                52.511756244596285
              ],
              [
                13.326639505057495,
                52.51175721508396
              ],
              [
                13.32665351369134,
                52.51176514880909
              ],
              [
                13.32665786798831,
                52.51177593565858
              ],
              [
                13.326653743088599,
                52.51179988168703
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '30FDA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327074133011287,
                52.51216862123741
              ],
              [
                13.327064511717841,
                52.512168004301444
              ],
              [
                13.32704689735019,
                52.51216687483404
              ],
              [
                13.327032983480272,
                52.512165982649734
              ],
              [
                13.32703310074315,
                52.512165301922096
              ],
              [
                13.327031472524354,
                52.51216519751755
              ],
              [
                13.327034716796128,
                52.512146364052604
              ],
              [
                13.327044412099116,
                52.51214698573421
              ],
              [
                13.32706217448661,
                52.51214812469293
              ],
              [
                13.327077494546195,
                52.512149107044834
              ],
              [
                13.327074133011287,
                52.51216862123741
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3134B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326917607252732,
                52.51211678243227
              ],
              [
                13.326919087451403,
                52.5121168773455
              ],
              [
                13.326917508314134,
                52.512126044477874
              ],
              [
                13.326916028115464,
                52.51212594956467
              ],
              [
                13.326915559064672,
                52.51212867247526
              ],
              [
                13.326915570601036,
                52.512128675764146
              ],
              [
                13.32691416344828,
                52.51213684449594
              ],
              [
                13.326876862443111,
                52.512134452682744
              ],
              [
                13.326877065698529,
                52.51213327275482
              ],
              [
                13.326873217182243,
                52.51213302598044
              ],
              [
                13.326873013926818,
                52.51213420590835
              ],
              [
                13.326836008963593,
                52.51213183307781
              ],
              [
                13.326836212218987,
                52.5121306531499
              ],
              [
                13.326832215683087,
                52.51213039688419
              ],
              [
                13.326832012427687,
                52.512131576812116
              ],
              [
                13.326804036676968,
                52.51212978295222
              ],
              [
                13.326805443829459,
                52.51212161422042
              ],
              [
                13.3268010032342,
                52.51212132948077
              ],
              [
                13.32680080779639,
                52.51212246402685
              ],
              [
                13.326768983501639,
                52.512120423390726
              ],
              [
                13.326754995642217,
                52.51211952646174
              ],
              [
                13.32674974095302,
                52.51211918952078
              ],
              [
                13.326750561791702,
                52.51211442442719
              ],
              [
                13.326744640998506,
                52.51211404477431
              ],
              [
                13.326745938704978,
                52.51210651138829
              ],
              [
                13.326741498110104,
                52.51210622664861
              ],
              [
                13.326742584743492,
                52.51209991857238
              ],
              [
                13.326741104545206,
                52.512099823659156
              ],
              [
                13.326742668045929,
                52.51209074729044
              ],
              [
                13.326744148244213,
                52.51209084220364
              ],
              [
                13.326745234876837,
                52.51208453412736
              ],
              [
                13.326749675471737,
                52.512084818867024
              ],
              [
                13.326750425951639,
                52.51208046221001
              ],
              [
                13.32675412644742,
                52.51208069949306
              ],
              [
                13.32675450168732,
                52.512078521164575
              ],
              [
                13.326750801191539,
                52.51207828388151
              ],
              [
                13.326753771839405,
                52.51206103878084
              ],
              [
                13.326757472335204,
                52.512061276063896
              ],
              [
                13.326757878844733,
                52.512058916208034
              ],
              [
                13.32675417834893,
                52.51205867892496
              ],
              [
                13.326787246188358,
                52.511866713722085
              ],
              [
                13.326790650644684,
                52.511866932022485
              ],
              [
                13.32679143238679,
                52.51186239383796
              ],
              [
                13.326798069904719,
                52.5118628194486
              ],
              [
                13.3267982575228,
                52.51186173028431
              ],
              [
                13.326811158624153,
                52.5118625575282
              ],
              [
                13.32682581258898,
                52.511863497169095
              ],
              [
                13.326825624970894,
                52.511864586333395
              ],
              [
                13.326845607650704,
                52.511865867661896
              ],
              [
                13.326811758065867,
                52.51206237104924
              ],
              [
                13.326808057569774,
                52.5120621337662
              ],
              [
                13.326807651060207,
                52.5120644936221
              ],
              [
                13.326811351556302,
                52.512064730905145
              ],
              [
                13.32680838090815,
                52.51208197600579
              ],
              [
                13.326804680412076,
                52.512081738722735
              ],
              [
                13.326804305172143,
                52.51208391705124
              ],
              [
                13.326808005668209,
                52.51208415433428
              ],
              [
                13.326921968986237,
                52.51209146188483
              ],
              [
                13.326917607252732,
                52.51211678243227
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '313D6',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326863601327263,
                52.512156268614376
              ],
              [
                13.326862287982982,
                52.51216389276401
              ],
              [
                13.326850150355023,
                52.51216311447559
              ],
              [
                13.326849970551901,
                52.51216415825799
              ],
              [
                13.326838128963873,
                52.512163398952225
              ],
              [
                13.326838308766982,
                52.51216235516982
              ],
              [
                13.326836828568496,
                52.5121622602566
              ],
              [
                13.326838196635393,
                52.51215431843406
              ],
              [
                13.32683947089153,
                52.5121469211936
              ],
              [
                13.326835030296067,
                52.51214663645393
              ],
              [
                13.326834483069556,
                52.51214981318296
              ],
              [
                13.32682848826572,
                52.512149428784404
              ],
              [
                13.32682841009049,
                52.51214988260287
              ],
              [
                13.326834404894333,
                52.512150267001424
              ],
              [
                13.326833756039925,
                52.51215403369441
              ],
              [
                13.326832387973042,
                52.51216197551694
              ],
              [
                13.326816105789963,
                52.51216093147152
              ],
              [
                13.32681663738171,
                52.51215784550617
              ],
              [
                13.326817731835074,
                52.51215149204813
              ],
              [
                13.326812551140522,
                52.51215115985187
              ],
              [
                13.32681294201664,
                52.51214889075971
              ],
              [
                13.32681952889972,
                52.51214931312353
              ],
              [
                13.326819607074945,
                52.51214885930508
              ],
              [
                13.326808246551913,
                52.51214813084611
              ],
              [
                13.326808168376692,
                52.51214858466456
              ],
              [
                13.326812201917422,
                52.51214884330308
              ],
              [
                13.326811811041303,
                52.512151112395266
              ],
              [
                13.326810330842866,
                52.51215101748204
              ],
              [
                13.32680923638952,
                52.51215737094007
              ],
              [
                13.326808704797772,
                52.5121604569054
              ],
              [
                13.32679542001701,
                52.512159605059246
              ],
              [
                13.326797436938175,
                52.5121478965437
              ],
              [
                13.326799287186182,
                52.51214801518525
              ],
              [
                13.3267993653614,
                52.512147561366795
              ],
              [
                13.326794924766185,
                52.512147276627125
              ],
              [
                13.326794846590968,
                52.51214773044557
              ],
              [
                13.326796696838974,
                52.51214784908711
              ],
              [
                13.326794679917818,
                52.512159557602644
              ],
              [
                13.32678202422162,
                52.51215874609463
              ],
              [
                13.326784041142743,
                52.51214703757909
              ],
              [
                13.32678596540063,
                52.512147160966265
              ],
              [
                13.326786043575849,
                52.51214670714781
              ],
              [
                13.326781454960887,
                52.512146412916834
              ],
              [
                13.326781376785668,
                52.512146866735286
              ],
              [
                13.326783301043557,
                52.51214699012247
              ],
              [
                13.326781284122436,
                52.51215869863801
              ],
              [
                13.326764520876107,
                52.51215762374579
              ],
              [
                13.326766537797173,
                52.512145915230256
              ],
              [
                13.326770756362452,
                52.51214618573293
              ],
              [
                13.326772495595502,
                52.51214629725595
              ],
              [
                13.32677257377072,
                52.51214584343752
              ],
              [
                13.326770834537667,
                52.512145731914494
              ],
              [
                13.32677322669841,
                52.51213184507042
              ],
              [
                13.326836875230562,
                52.51213592633891
              ],
              [
                13.326836171653946,
                52.512140010704826
              ],
              [
                13.326840612249436,
                52.51214029544451
              ],
              [
                13.326841315826051,
                52.512136211078584
              ],
              [
                13.326866775240726,
                52.512137843586004
              ],
              [
                13.326863601327263,
                52.512156268614376
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '31740',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328098754369153,
                52.51220913207013
              ],
              [
                13.328081556435897,
                52.51220802930672
              ],
              [
                13.328081376632676,
                52.5122090730891
              ],
              [
                13.328064946399794,
                52.51220801955205
              ],
              [
                13.328065126203002,
                52.51220697576966
              ],
              [
                13.32805476479508,
                52.512206311376914
              ],
              [
                13.328055077496288,
                52.5122044961032
              ],
              [
                13.328049156691824,
                52.5122041164502
              ],
              [
                13.328050618569636,
                52.51219563004556
              ],
              [
                13.32805979581659,
                52.51219621850768
              ],
              [
                13.328059975619707,
                52.51219517472532
              ],
              [
                13.32805065035263,
                52.512194576771854
              ],
              [
                13.328051103769138,
                52.512191944624945
              ],
              [
                13.328049623568031,
                52.512191849711684
              ],
              [
                13.328051108897576,
                52.51218322716152
              ],
              [
                13.328052589098698,
                52.51218332207477
              ],
              [
                13.328055434675791,
                52.512166803083815
              ],
              [
                13.32805765497748,
                52.512166945453664
              ],
              [
                13.32805843672905,
                52.51216240726935
              ],
              [
                13.328106274826645,
                52.51216547473704
              ],
              [
                13.328098754369153,
                52.51220913207013
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3182C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328148609552585,
                52.51221232887695
              ],
              [
                13.328126702573341,
                52.512210924160826
              ],
              [
                13.328126522770111,
                52.5122119679432
              ],
              [
                13.328110240556267,
                52.51221092389748
              ],
              [
                13.328110420359495,
                52.51220988011509
              ],
              [
                13.328100530610616,
                52.51220924596604
              ],
              [
                13.32810805106812,
                52.512165588632946
              ],
              [
                13.328156130010708,
                52.512168671543854
              ],
              [
                13.328153253163636,
                52.51218537206217
              ],
              [
                13.328156509606574,
                52.512185580871304
              ],
              [
                13.328155024277056,
                52.51219420342151
              ],
              [
                13.32815176783412,
                52.512193994612346
              ],
              [
                13.328148609552585,
                52.51221232887695
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '31DBF',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327946886705188,
                52.51189537251092
              ],
              [
                13.32793186266615,
                52.51189440914145
              ],
              [
                13.327931487429936,
                52.51189658747003
              ],
              [
                13.327915649280785,
                52.51189557189828
              ],
              [
                13.327916024516982,
                52.51189339356971
              ],
              [
                13.327846307060968,
                52.511888923155766
              ],
              [
                13.327846541583543,
                52.511887561700426
              ],
              [
                13.327840916820948,
                52.511887201030085
              ],
              [
                13.327842558478599,
                52.511877670842544
              ],
              [
                13.32784818324121,
                52.51187803151289
              ],
              [
                13.327860237676957,
                52.511808052706805
              ],
              [
                13.327885771139925,
                52.51180968996033
              ],
              [
                13.327895244424983,
                52.51181029740512
              ],
              [
                13.32793313756651,
                52.51181272718427
              ],
              [
                13.327942610852217,
                52.51181333462904
              ],
              [
                13.327960817323557,
                52.51181450206202
              ],
              [
                13.327946886705188,
                52.51189537251092
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '32233',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328159641655336,
                52.5119100241826
              ],
              [
                13.328139214876792,
                52.51190871437971
              ],
              [
                13.328138824005585,
                52.51191098347199
              ],
              [
                13.328122985851632,
                52.51190996790018
              ],
              [
                13.328123376722838,
                52.51190769880791
              ],
              [
                13.328091404375778,
                52.51190564868167
              ],
              [
                13.328091013504586,
                52.511907917773954
              ],
              [
                13.328075471391974,
                52.51190692118481
              ],
              [
                13.328075862263148,
                52.51190465209255
              ],
              [
                13.328059876090537,
                52.511903627029426
              ],
              [
                13.328060048073832,
                52.51190262862883
              ],
              [
                13.328044505961929,
                52.51190163203971
              ],
              [
                13.328044115090792,
                52.51190390113199
              ],
              [
                13.32802857297925,
                52.511902904542836
              ],
              [
                13.328028963850372,
                52.51190063545058
              ],
              [
                13.327979525135957,
                52.511897465348056
              ],
              [
                13.32797913426487,
                52.51189973444032
              ],
              [
                13.327963592154797,
                52.511898737851205
              ],
              [
                13.327963983025873,
                52.51189646875893
              ],
              [
                13.327948662946282,
                52.51189548640681
              ],
              [
                13.327962593564688,
                52.51181461595792
              ],
              [
                13.327980800036563,
                52.51181578339089
              ],
              [
                13.327990273322927,
                52.511816390835676
              ],
              [
                13.328046298933263,
                52.5118199833022
              ],
              [
                13.328055772220536,
                52.511820590747014
              ],
              [
                13.328093517350817,
                52.51182301103495
              ],
              [
                13.328102990638744,
                52.511823618479745
              ],
              [
                13.32814325211389,
                52.51182620012022
              ],
              [
                13.3281527254025,
                52.511826807565036
              ],
              [
                13.32817374426207,
                52.51182815533325
              ],
              [
                13.328159641655336,
                52.5119100241826
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '32747',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328104231215326,
                52.51193066142466
              ],
              [
                13.328101864212872,
                52.511944402369856
              ],
              [
                13.328097127568931,
                52.51194409864745
              ],
              [
                13.328081881496448,
                52.511943121040964
              ],
              [
                13.328070631967448,
                52.51194239970025
              ],
              [
                13.328055385895546,
                52.51194142209376
              ],
              [
                13.328044136366998,
                52.511940700753065
              ],
              [
                13.32802889029568,
                52.51193972314661
              ],
              [
                13.328017640767559,
                52.51193900180592
              ],
              [
                13.328002394696838,
                52.511938024199424
              ],
              [
                13.327991145169156,
                52.51193730285874
              ],
              [
                13.32797589909902,
                52.51193632525227
              ],
              [
                13.327964649571772,
                52.511935603911596
              ],
              [
                13.327949403502233,
                52.51193462630514
              ],
              [
                13.327938153975406,
                52.511933904964444
              ],
              [
                13.327922907906455,
                52.51193292735799
              ],
              [
                13.327911658380074,
                52.51193220601732
              ],
              [
                13.327896412311716,
                52.51193122841089
              ],
              [
                13.327885162785764,
                52.51193050707021
              ],
              [
                13.327869916717992,
                52.511929529463764
              ],
              [
                13.327858667192473,
                52.511928808123095
              ],
              [
                13.3278434211253,
                52.511927830516676
              ],
              [
                13.327832171600209,
                52.51192710917599
              ],
              [
                13.327816925533615,
                52.511926131569574
              ],
              [
                13.327805676008973,
                52.51192541022891
              ],
              [
                13.327790429942963,
                52.51192443262249
              ],
              [
                13.32777918041876,
                52.511923711281824
              ],
              [
                13.327763934353344,
                52.51192273367542
              ],
              [
                13.327752684829568,
                52.511922012334786
              ],
              [
                13.327737438764737,
                52.51192103472837
              ],
              [
                13.327726189241403,
                52.51192031338772
              ],
              [
                13.327710943177163,
                52.51191933578132
              ],
              [
                13.327699693654255,
                52.51191861444068
              ],
              [
                13.32768444759061,
                52.51191763683426
              ],
              [
                13.327673198068135,
                52.51191691549364
              ],
              [
                13.32765795200508,
                52.511915937887245
              ],
              [
                13.327646702483037,
                52.51191521654662
              ],
              [
                13.327631456420573,
                52.51191423894023
              ],
              [
                13.327620206898974,
                52.511913517599595
              ],
              [
                13.32760496083709,
                52.51191253999323
              ],
              [
                13.327593711315929,
                52.5119118186526
              ],
              [
                13.327578465254632,
                52.51191084104622
              ],
              [
                13.327567215733898,
                52.5119101197056
              ],
              [
                13.327551969673198,
                52.51190914209925
              ],
              [
                13.327540720152898,
                52.511908420758616
              ],
              [
                13.327525474092793,
                52.51190744315226
              ],
              [
                13.327528194557193,
                52.51189165027014
              ],
              [
                13.327470984829537,
                52.51188798187348
              ],
              [
                13.327473971083885,
                52.51187064600856
              ],
              [
                13.327492177542714,
                52.51187181344136
              ],
              [
                13.327492552778706,
                52.51186963511279
              ],
              [
                13.327506466658141,
                52.51187052729723
              ],
              [
                13.327506091422146,
                52.511872705625805
              ],
              [
                13.327527406301815,
                52.51187407237645
              ],
              [
                13.327527765902996,
                52.51187198481154
              ],
              [
                13.327547008503284,
                52.51187321868365
              ],
              [
                13.327547227390955,
                52.511871947992006
              ],
              [
                13.327549003631004,
                52.51187206188787
              ],
              [
                13.327549378866976,
                52.511869883559285
              ],
              [
                13.32756521700768,
                52.51187089913095
              ],
              [
                13.327564841771697,
                52.51187307745955
              ],
              [
                13.327566618011796,
                52.511873191355434
              ],
              [
                13.32756639912413,
                52.511874462047096
              ],
              [
                13.327754236540624,
                52.511886506537486
              ],
              [
                13.327754455428366,
                52.51188523584582
              ],
              [
                13.32775623166896,
                52.51188534974172
              ],
              [
                13.327756606905062,
                52.51188317141313
              ],
              [
                13.327772445050547,
                52.51188418698484
              ],
              [
                13.327772069814428,
                52.51188636531344
              ],
              [
                13.327773846055068,
                52.51188647920934
              ],
              [
                13.327773627167318,
                52.511887749900986
              ],
              [
                13.327913654151972,
                52.51189672869404
              ],
              [
                13.327913873039774,
                52.51189545800239
              ],
              [
                13.327915649280785,
                52.51189557189828
              ],
              [
                13.327931487429936,
                52.51189658747003
              ],
              [
                13.327933263670987,
                52.51189670136591
              ],
              [
                13.327933044783176,
                52.511897972057604
              ],
              [
                13.328043911837787,
                52.511905081059936
              ],
              [
                13.328043739854467,
                52.51190607946055
              ],
              [
                13.328083039195208,
                52.511908599407356
              ],
              [
                13.328107758555703,
                52.511910184458664
              ],
              [
                13.328105700892259,
                52.51192212963777
              ],
              [
                13.328104231215326,
                52.51193066142466
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '32EBC',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328392630880693,
                52.5119488230999
              ],
              [
                13.328390207474836,
                52.51196289147183
              ],
              [
                13.328371556931645,
                52.51196169556479
              ],
              [
                13.328371119154948,
                52.5119642369481
              ],
              [
                13.328355280995657,
                52.511963221376256
              ],
              [
                13.328355718772345,
                52.511960679992924
              ],
              [
                13.328309092417632,
                52.51195769022537
              ],
              [
                13.328293846340442,
                52.511956712618826
              ],
              [
                13.328282596807961,
                52.51195599127809
              ],
              [
                13.328267350731354,
                52.511955013671546
              ],
              [
                13.328256101199312,
                52.511954292330806
              ],
              [
                13.32824085512329,
                52.51195331472429
              ],
              [
                13.328234638276857,
                52.51195291608863
              ],
              [
                13.328229605591687,
                52.511952593383555
              ],
              [
                13.328214359516256,
                52.51195161577704
              ],
              [
                13.328203109985086,
                52.51195089443631
              ],
              [
                13.328187863910243,
                52.511949916829785
              ],
              [
                13.328176614379515,
                52.51194919548908
              ],
              [
                13.328161368305254,
                52.511948217882576
              ],
              [
                13.328151598976303,
                52.51194759145512
              ],
              [
                13.32815011877496,
                52.51194749654186
              ],
              [
                13.3281348727013,
                52.51194651893535
              ],
              [
                13.328123623171434,
                52.51194579759465
              ],
              [
                13.328108377098358,
                52.51194481998815
              ],
              [
                13.328103640454364,
                52.51194451626575
              ],
              [
                13.328106007456828,
                52.51193077532055
              ],
              [
                13.328104231215326,
                52.51193066142466
              ],
              [
                13.328105700892259,
                52.51192212963777
              ],
              [
                13.328107477133765,
                52.51192224353367
              ],
              [
                13.328109534797205,
                52.51191029835458
              ],
              [
                13.32812100635705,
                52.51191103393227
              ],
              [
                13.328121209610089,
                52.51190985400429
              ],
              [
                13.328122985851632,
                52.51190996790018
              ],
              [
                13.328138824005585,
                52.51191098347199
              ],
              [
                13.328140600247178,
                52.51191109736789
              ],
              [
                13.328140396994122,
                52.51191227729588
              ],
              [
                13.328159269561262,
                52.51191348743984
              ],
              [
                13.328179918370608,
                52.51191481147972
              ],
              [
                13.328180231067627,
                52.51191299620588
              ],
              [
                13.328182007309328,
                52.51191311010179
              ],
              [
                13.328182288736615,
                52.51191147635537
              ],
              [
                13.328198126891934,
                52.51191249192719
              ],
              [
                13.328197845464643,
                52.511914125673606
              ],
              [
                13.32819962170638,
                52.51191423956952
              ],
              [
                13.328199309009351,
                52.51191605484334
              ],
              [
                13.328240532621011,
                52.51191869817747
              ],
              [
                13.328249709870624,
                52.51191928663965
              ],
              [
                13.328249991297964,
                52.511917652893224
              ],
              [
                13.328265799850824,
                52.51191866656678
              ],
              [
                13.32826551842347,
                52.51192030031323
              ],
              [
                13.328359910885636,
                52.51192635293157
              ],
              [
                13.328360129773612,
                52.5119250822399
              ],
              [
                13.328362202056136,
                52.511925215118445
              ],
              [
                13.328362827450297,
                52.51192158457084
              ],
              [
                13.328378665609788,
                52.5119226001427
              ],
              [
                13.32837804021562,
                52.51192623069031
              ],
              [
                13.328380112498191,
                52.511926363568854
              ],
              [
                13.328379893610203,
                52.51192763426054
              ],
              [
                13.328396101820559,
                52.51192867356072
              ],
              [
                13.328394256906975,
                52.51193938367615
              ],
              [
                13.328392630880693,
                52.5119488230999
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '33236',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328366042151481,
                52.51210578199174
              ],
              [
                13.328363384203316,
                52.512121211818624
              ],
              [
                13.328352578730305,
                52.51212051895185
              ],
              [
                13.328352578730305,
                52.51212051895185
              ],
              [
                13.328352578730305,
                52.51212051895185
              ],
              [
                13.32833896087415,
                52.51211964574989
              ],
              [
                13.328309948920639,
                52.5121177854501
              ],
              [
                13.328310433605402,
                52.51211497177579
              ],
              [
                13.32831201273919,
                52.51210580464335
              ],
              [
                13.328313200997844,
                52.51209890660311
              ],
              [
                13.328314780130471,
                52.51208973947064
              ],
              [
                13.328315630584838,
                52.51208480243736
              ],
              [
                13.328369065868026,
                52.51208822880588
              ],
              [
                13.328367652554451,
                52.51209643333193
              ],
              [
                13.328366042151481,
                52.51210578199174
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '335AA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328489766343054,
                52.512129040374035
              ],
              [
                13.32839125928867,
                52.512122723921514
              ],
              [
                13.328393886179065,
                52.51210747439071
              ],
              [
                13.328395480947133,
                52.51209821649456
              ],
              [
                13.32840364218456,
                52.512050839080736
              ],
              [
                13.328405236948495,
                52.512041581184505
              ],
              [
                13.328407738537615,
                52.51202705899433
              ],
              [
                13.32850624559477,
                52.51203337544694
              ],
              [
                13.328503650195582,
                52.512048442219246
              ],
              [
                13.328502680829162,
                52.51205406956792
              ],
              [
                13.328498662646334,
                52.51207739583582
              ],
              [
                13.328497693278644,
                52.512083023184466
              ],
              [
                13.32849364382053,
                52.51210653097959
              ],
              [
                13.328492674451537,
                52.5121121583282
              ],
              [
                13.328489766343054,
                52.512129040374035
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3391E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328515493644192,
                52.51197968872485
              ],
              [
                13.328514524280823,
                52.51198531607358
              ],
              [
                13.328510529563012,
                52.51200850619618
              ],
              [
                13.328509560198354,
                52.512014133544895
              ],
              [
                13.328506683372767,
                52.512030834063665
              ],
              [
                13.32840817631555,
                52.51202451761104
              ],
              [
                13.328410622243078,
                52.512010318535765
              ],
              [
                13.328412217004061,
                52.51200106063949
              ],
              [
                13.328420887488154,
                52.5119507267316
              ],
              [
                13.328422482244786,
                52.511941468835204
              ],
              [
                13.328424858743615,
                52.51192767275432
              ],
              [
                13.328523365803623,
                52.511933989206995
              ],
              [
                13.328520551528065,
                52.5119503266712
              ],
              [
                13.328519582165987,
                52.51195595401998
              ],
              [
                13.328515493644192,
                52.51197968872485
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '33C93',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328369253487669,
                52.51208713964164
              ],
              [
                13.328315818204466,
                52.51208371327311
              ],
              [
                13.32831596838825,
                52.512082841430335
              ],
              [
                13.328317563154684,
                52.512073583534175
              ],
              [
                13.32831875141158,
                52.51206668549388
              ],
              [
                13.328320346176833,
                52.512057427597696
              ],
              [
                13.328321534432858,
                52.51205052955737
              ],
              [
                13.328323113561996,
                52.51204136242484
              ],
              [
                13.328324301817144,
                52.512034464384534
              ],
              [
                13.32832588094512,
                52.51202529725195
              ],
              [
                13.328326631421543,
                52.5120209405949
              ],
              [
                13.328369261232424,
                52.51202367409668
              ],
              [
                13.32838006670573,
                52.51202436696344
              ],
              [
                13.3283774244024,
                52.51203970602684
              ],
              [
                13.328375814003609,
                52.51204905468675
              ],
              [
                13.328369253487669,
                52.51208713964164
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '34266',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328357177105296,
                52.512157245002356
              ],
              [
                13.328303741823186,
                52.51215381863387
              ],
              [
                13.32830976130072,
                52.512118874614345
              ],
              [
                13.32836319658337,
                52.51212230098287
              ],
              [
                13.328357177105296,
                52.512157245002356
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '34443',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328262508628928,
                52.512296532402154
              ],
              [
                13.328261805048392,
                52.51230061676796
              ],
              [
                13.328240786186512,
                52.51229926899978
              ],
              [
                13.328240598565026,
                52.512300358164005
              ],
              [
                13.328218839603064,
                52.51229896293922
              ],
              [
                13.328226266279655,
                52.512255850188865
              ],
              [
                13.328251281685274,
                52.51225745422281
              ],
              [
                13.328250234133849,
                52.512263535389735
              ],
              [
                13.328250974234606,
                52.51226358284635
              ],
              [
                13.328255148803521,
                52.51223934894237
              ],
              [
                13.32827217112159,
                52.512240440444785
              ],
              [
                13.328266902095493,
                52.51227102780671
              ],
              [
                13.328249879777585,
                52.51226993630431
              ],
              [
                13.328250036128603,
                52.512269028667454
              ],
              [
                13.328249296027828,
                52.512268981210845
              ],
              [
                13.328249061501296,
                52.512270342666106
              ],
              [
                13.328266823919979,
                52.512271481625135
              ],
              [
                13.328264885166641,
                52.51228273632206
              ],
              [
                13.328261776743346,
                52.51228253700424
              ],
              [
                13.328260901176993,
                52.512287619770575
              ],
              [
                13.328264009600286,
                52.51228781908841
              ],
              [
                13.328263884519362,
                52.51228854519789
              ],
              [
                13.328262508628928,
                52.512296532402154
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '346B6',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327787690154182,
                52.51218918605152
              ],
              [
                13.327771824409187,
                52.51218816871012
              ],
              [
                13.32777164460606,
                52.512189212492494
              ],
              [
                13.327755066360554,
                52.5121881494642
              ],
              [
                13.327755246163663,
                52.51218710568181
              ],
              [
                13.327743626590047,
                52.51218636061287
              ],
              [
                13.32774393929109,
                52.512184545339124
              ],
              [
                13.327738018489315,
                52.51218416568618
              ],
              [
                13.327739480366347,
                52.5121756792815
              ],
              [
                13.32774865760914,
                52.51217626774359
              ],
              [
                13.327748837412157,
                52.5121752239612
              ],
              [
                13.32773951214932,
                52.51217462600779
              ],
              [
                13.32773996556559,
                52.51217199386087
              ],
              [
                13.327738485365154,
                52.51217189894762
              ],
              [
                13.327739970693912,
                52.51216327639743
              ],
              [
                13.327741450894354,
                52.51216337131067
              ],
              [
                13.327744296469936,
                52.51214685231968
              ],
              [
                13.327746812810718,
                52.512147013672184
              ],
              [
                13.32774759456188,
                52.51214247548788
              ],
              [
                13.327795210607693,
                52.51214552871833
              ],
              [
                13.327787690154182,
                52.51218918605152
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '34850',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32784194202749,
                52.51218119400417
              ],
              [
                13.327832616763382,
                52.51218059605074
              ],
              [
                13.32783243696033,
                52.51218163983312
              ],
              [
                13.327841614204374,
                52.512182228295224
              ],
              [
                13.327840152327092,
                52.51219071469989
              ],
              [
                13.327834231524488,
                52.51219033504691
              ],
              [
                13.327833918823394,
                52.51219215032065
              ],
              [
                13.327821411128065,
                52.512191348303745
              ],
              [
                13.327821231324927,
                52.51219239208614
              ],
              [
                13.327804801098276,
                52.51219133854912
              ],
              [
                13.327804980901409,
                52.51219029476674
              ],
              [
                13.327789466394835,
                52.51218929994742
              ],
              [
                13.32779698684837,
                52.5121456426142
              ],
              [
                13.327845287799231,
                52.51214873976189
              ],
              [
                13.327844506047938,
                52.51215327794623
              ],
              [
                13.327846726348948,
                52.51215342031608
              ],
              [
                13.32784194202749,
                52.51218119400417
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '349EA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32789108217,
                52.51219581574157
              ],
              [
                13.327871515920783,
                52.51219456111705
              ],
              [
                13.32787133611763,
                52.51219560489943
              ],
              [
                13.327855053909857,
                52.512194560853736
              ],
              [
                13.327855233713002,
                52.51219351707135
              ],
              [
                13.327847536669449,
                52.512193023522464
              ],
              [
                13.32784784937056,
                52.51219120824875
              ],
              [
                13.327841928567889,
                52.5121908285958
              ],
              [
                13.327843390445171,
                52.51218234219111
              ],
              [
                13.32785256768936,
                52.512182930653246
              ],
              [
                13.327852747492411,
                52.51218188687084
              ],
              [
                13.327843422228153,
                52.512181288917425
              ],
              [
                13.327848206549625,
                52.51215351522932
              ],
              [
                13.327850426850642,
                52.512153657599185
              ],
              [
                13.327851208601942,
                52.51214911941488
              ],
              [
                13.32789860262484,
                52.512152158408405
              ],
              [
                13.32789108217,
                52.51219581574157
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '34B84',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32794555608168,
                52.5121878379313
              ],
              [
                13.327936230816155,
                52.51218723997787
              ],
              [
                13.327936051013083,
                52.51218828376024
              ],
              [
                13.327945228258507,
                52.51218887222235
              ],
              [
                13.327943766380963,
                52.51219735862702
              ],
              [
                13.327937845577464,
                52.51219697897404
              ],
              [
                13.327937532876314,
                52.51219879424775
              ],
              [
                13.327932500193386,
                52.512198471542725
              ],
              [
                13.327932320390218,
                52.512199515325115
              ],
              [
                13.32791574214082,
                52.51219845229677
              ],
              [
                13.327915921943982,
                52.51219740851436
              ],
              [
                13.327892858410923,
                52.51219592963749
              ],
              [
                13.327900378865774,
                52.51215227230431
              ],
              [
                13.327948901854064,
                52.51215538368906
              ],
              [
                13.32794812010263,
                52.51215992187337
              ],
              [
                13.327950340403977,
                52.51216006424324
              ],
              [
                13.32794555608168,
                52.5121878379313
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '34D1E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327995140291856,
                52.51220248814276
              ],
              [
                13.327977942361201,
                52.51220138537938
              ],
              [
                13.32797776255801,
                52.51220242916178
              ],
              [
                13.327961184307522,
                52.512201366133404
              ],
              [
                13.327961364110706,
                52.51220032235102
              ],
              [
                13.327951150724438,
                52.51219966744962
              ],
              [
                13.327951463425597,
                52.5121978521759
              ],
              [
                13.32794554262203,
                52.51219747252292
              ],
              [
                13.327947004499565,
                52.512188986118254
              ],
              [
                13.327956181745146,
                52.512189574580376
              ],
              [
                13.327956361548225,
                52.51218853079799
              ],
              [
                13.327947036282557,
                52.512187932844554
              ],
              [
                13.327951820604872,
                52.51216015915649
              ],
              [
                13.327954040906235,
                52.51216030152636
              ],
              [
                13.327954822657661,
                52.51215576334203
              ],
              [
                13.328002660748016,
                52.512158830809625
              ],
              [
                13.327995140291856,
                52.51220248814276
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '34EBA',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328051108897576,
                52.51218322716152
              ],
              [
                13.328049623568031,
                52.512191849711684
              ],
              [
                13.328049170151523,
                52.51219448185859
              ],
              [
                13.328039844884593,
                52.512193883905134
              ],
              [
                13.328039665081477,
                52.51219492768753
              ],
              [
                13.328048842328299,
                52.51219551614966
              ],
              [
                13.328047380450503,
                52.51220400255432
              ],
              [
                13.328041459646105,
                52.512203622901325
              ],
              [
                13.328041146944898,
                52.512205438175044
              ],
              [
                13.328027973155312,
                52.51220459344714
              ],
              [
                13.328027793352115,
                52.51220563722953
              ],
              [
                13.328011215100407,
                52.512204574201135
              ],
              [
                13.328011394903607,
                52.512203530418766
              ],
              [
                13.327996916533056,
                52.512202602038656
              ],
              [
                13.328004436989247,
                52.51215894470553
              ],
              [
                13.328052515924554,
                52.51216202761639
              ],
              [
                13.32805173417299,
                52.51216656580069
              ],
              [
                13.328053954474663,
                52.51216670817056
              ],
              [
                13.328051108897576,
                52.51218322716152
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3A18E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328487045852436,
                52.512144833255626
              ],
              [
                13.32848607648174,
                52.51215046060421
              ],
              [
                13.3284835123387,
                52.51216534584882
              ],
              [
                13.328362875882162,
                52.512157610418384
              ],
              [
                13.328366112330045,
                52.51213882233522
              ],
              [
                13.328360413553147,
                52.512138456919196
              ],
              [
                13.328362023958528,
                52.5121291082594
              ],
              [
                13.328367722735445,
                52.51212947367544
              ],
              [
                13.328368676470358,
                52.512123937090514
              ],
              [
                13.328371740928437,
                52.51210614740777
              ],
              [
                13.328366042151481,
                52.51210578199174
              ],
              [
                13.328367652554451,
                52.51209643333193
              ],
              [
                13.328373351331422,
                52.51209679874797
              ],
              [
                13.328381512780656,
                52.51204942010278
              ],
              [
                13.328375814003609,
                52.51204905468675
              ],
              [
                13.3283774244024,
                52.51203970602684
              ],
              [
                13.32838312317947,
                52.51204007144288
              ],
              [
                13.328386046911483,
                52.51202309863309
              ],
              [
                13.32838849283892,
                52.51200889955781
              ],
              [
                13.328382350001245,
                52.51200850566782
              ],
              [
                13.328382436930717,
                52.512008001025464
              ],
              [
                13.328383844072691,
                52.511999832293455
              ],
              [
                13.328383960397051,
                52.51199915700783
              ],
              [
                13.328390103234748,
                52.51199955089786
              ],
              [
                13.328393614207306,
                52.511979169007546
              ],
              [
                13.32839065380358,
                52.51197897918102
              ],
              [
                13.328392264197204,
                52.51196963052098
              ],
              [
                13.328395224600946,
                52.511969820347495
              ],
              [
                13.328398773718474,
                52.51194921698993
              ],
              [
                13.328392630880693,
                52.5119488230999
              ],
              [
                13.328394256906975,
                52.51193938367615
              ],
              [
                13.328400399744762,
                52.51193977756617
              ],
              [
                13.328402729338913,
                52.511926253776345
              ],
              [
                13.328421602299361,
                52.51192746394515
              ],
              [
                13.328419225800546,
                52.51194126002603
              ],
              [
                13.328422482244786,
                52.511941468835204
              ],
              [
                13.328420887488154,
                52.5119507267316
              ],
              [
                13.328417631043918,
                52.51195051792242
              ],
              [
                13.328408960559882,
                52.51200085183032
              ],
              [
                13.328412217004061,
                52.51200106063949
              ],
              [
                13.328410622243078,
                52.512010318535765
              ],
              [
                13.328407365798913,
                52.51201010972661
              ],
              [
                13.32840198050435,
                52.51204137237533
              ],
              [
                13.328405236948495,
                52.512041581184505
              ],
              [
                13.32840364218456,
                52.512050839080736
              ],
              [
                13.328400385740427,
                52.51205063027156
              ],
              [
                13.32839222450304,
                52.5120980076854
              ],
              [
                13.328395480947133,
                52.51209821649456
              ],
              [
                13.328393886179065,
                52.51210747439071
              ],
              [
                13.328390629734976,
                52.51210726558155
              ],
              [
                13.328387549429713,
                52.5121251472593
              ],
              [
                13.32848931292809,
                52.512131672520994
              ],
              [
                13.328487045852436,
                52.512144833255626
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3A901',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32656168664739,
                52.5121253467061
              ],
              [
                13.326559984419811,
                52.51212523755591
              ],
              [
                13.326559796799417,
                52.51212632672015
              ],
              [
                13.326558242591634,
                52.51212622706128
              ],
              [
                13.326558156598955,
                52.51212672626155
              ],
              [
                13.326548535312744,
                52.51212610932564
              ],
              [
                13.326548378962414,
                52.51212701696253
              ],
              [
                13.326533576983897,
                52.512126067830366
              ],
              [
                13.32653373333422,
                52.51212516019349
              ],
              [
                13.32653299323531,
                52.512125112736896
              ],
              [
                13.326523001900007,
                52.512124472072706
              ],
              [
                13.326470824929098,
                52.51212112638193
              ],
              [
                13.326470645126236,
                52.51212217016433
              ],
              [
                13.326455843149402,
                52.51212122103222
              ],
              [
                13.326456022952256,
                52.51212017724979
              ],
              [
                13.326405179391516,
                52.51211691705966
              ],
              [
                13.326404999588677,
                52.512117960842055
              ],
              [
                13.32639019761326,
                52.51211701170994
              ],
              [
                13.3263903774161,
                52.51211596792753
              ],
              [
                13.32635662768606,
                52.51211380382763
              ],
              [
                13.326342417790654,
                52.51211289266081
              ],
              [
                13.326303363854867,
                52.51211038844451
              ],
              [
                13.32630318405207,
                52.51211143222694
              ],
              [
                13.326289911238899,
                52.512110581147624
              ],
              [
                13.326290091041695,
                52.51210953736523
              ],
              [
                13.326192648317452,
                52.51210328914266
              ],
              [
                13.326192468514677,
                52.512104332925084
              ],
              [
                13.32618114497633,
                52.51210360683706
              ],
              [
                13.326181324779101,
                52.51210256305466
              ],
              [
                13.326142643076983,
                52.51210008270627
              ],
              [
                13.326142463274202,
                52.51210112648876
              ],
              [
                13.326130518164845,
                52.5121003606121
              ],
              [
                13.326130697979151,
                52.51209931676258
              ],
              [
                13.326049782822652,
                52.51209412831979
              ],
              [
                13.326049603019925,
                52.512095172102185
              ],
              [
                13.326051231236395,
                52.51209527650672
              ],
              [
                13.326049816266787,
                52.51210349062038
              ],
              [
                13.326024208862801,
                52.51210184862196
              ],
              [
                13.32602562383234,
                52.512093634508325
              ],
              [
                13.32603494907186,
                52.51209423246149
              ],
              [
                13.326035128874587,
                52.512093188679074
              ],
              [
                13.325864092160403,
                52.51208222145863
              ],
              [
                13.325838844690526,
                52.51208060253955
              ],
              [
                13.325838664887872,
                52.51208164632195
              ],
              [
                13.325838664528144,
                52.5120816462989
              ],
              [
                13.32582230798075,
                52.512080597483795
              ],
              [
                13.325822487783409,
                52.512079553701405
              ],
              [
                13.325813321426107,
                52.512078965935906
              ],
              [
                13.325804736287735,
                52.51207841543937
              ],
              [
                13.325803996189606,
                52.512078367982745
              ],
              [
                13.325803918014541,
                52.5120788218012
              ],
              [
                13.325789116052098,
                52.51207787266922
              ],
              [
                13.325789194227164,
                52.512077418850765
              ],
              [
                13.325779572951749,
                52.512076801915
              ],
              [
                13.325710595811959,
                52.512072378960006
              ],
              [
                13.325710416009326,
                52.512073422742425
              ],
              [
                13.325699314538982,
                52.512072710893456
              ],
              [
                13.325699494341608,
                52.512071667111044
              ],
              [
                13.325692167371274,
                52.51207119729072
              ],
              [
                13.32569198756865,
                52.51207224107313
              ],
              [
                13.325678073726221,
                52.512071348889094
              ],
              [
                13.325678253528842,
                52.51207030510669
              ],
              [
                13.32567647729366,
                52.51207019121085
              ],
              [
                13.325677020610208,
                52.51206703717272
              ],
              [
                13.325667781881059,
                52.512065068272946
              ],
              [
                13.325666301685098,
                52.51206497335974
              ],
              [
                13.325667865185382,
                52.512055896991
              ],
              [
                13.325669345381352,
                52.51205599190419
              ],
              [
                13.32567905316061,
                52.51205523789335
              ],
              [
                13.325679275959342,
                52.5120539445108
              ],
              [
                13.32576231495952,
                52.51205926914112
              ],
              [
                13.325763597029319,
                52.51205182651873
              ],
              [
                13.32577973116781,
                52.51205286107259
              ],
              [
                13.325778449097971,
                52.512060303694966
              ],
              [
                13.325809385199364,
                52.512062287380814
              ],
              [
                13.325810010599374,
                52.51205865683329
              ],
              [
                13.325815931384499,
                52.51205903648608
              ],
              [
                13.325816588054401,
                52.51205522441122
              ],
              [
                13.325833166253032,
                52.512056287439044
              ],
              [
                13.325832509583119,
                52.51206009951392
              ],
              [
                13.325921765427195,
                52.512065822779896
              ],
              [
                13.325922422097214,
                52.51206201070502
              ],
              [
                13.325938852278755,
                52.51206306424155
              ],
              [
                13.325938195608718,
                52.512066876316425
              ],
              [
                13.325975200523498,
                52.51206924914646
              ],
              [
                13.325975857193573,
                52.512065437071605
              ],
              [
                13.325992287376387,
                52.512066490608156
              ],
              [
                13.325991630706287,
                52.512070302683014
              ],
              [
                13.326081478649835,
                52.51207606391447
              ],
              [
                13.326082135320028,
                52.51207225183961
              ],
              [
                13.326098565505399,
                52.512073305376184
              ],
              [
                13.32609790883518,
                52.512077117451035
              ],
              [
                13.32612381228135,
                52.51207877843214
              ],
              [
                13.326124468951594,
                52.51207496635727
              ],
              [
                13.326140899137977,
                52.51207601989386
              ],
              [
                13.32614024246771,
                52.51207983196872
              ],
              [
                13.32623956369276,
                52.5120862006449
              ],
              [
                13.326240220363134,
                52.51208238857004
              ],
              [
                13.326256650552288,
                52.51208344210666
              ],
              [
                13.326255993881903,
                52.51208725418152
              ],
              [
                13.326291666636246,
                52.512089541589866
              ],
              [
                13.326292323306676,
                52.512085729514986
              ],
              [
                13.32630875349708,
                52.51208678305163
              ],
              [
                13.326308096826626,
                52.51209059512647
              ],
              [
                13.326389063626442,
                52.5120957868791
              ],
              [
                13.326389720296984,
                52.51209197480424
              ],
              [
                13.326406002469968,
                52.51209301884956
              ],
              [
                13.326405345799406,
                52.51209683092443
              ],
              [
                13.326457744795025,
                52.512100190852166
              ],
              [
                13.326458401465642,
                52.51209637877729
              ],
              [
                13.326473203442541,
                52.51209732790942
              ],
              [
                13.32647254677191,
                52.51210113998429
              ],
              [
                13.326503926963975,
                52.5121031521444
              ],
              [
                13.326504583634641,
                52.51209934006955
              ],
              [
                13.326519385612546,
                52.51210028920168
              ],
              [
                13.326518728941869,
                52.51210410127655
              ],
              [
                13.326523391564956,
                52.512104400253165
              ],
              [
                13.326522914696879,
                52.51210716854562
              ],
              [
                13.32656265800933,
                52.51210971696544
              ],
              [
                13.326561563557712,
                52.512116070423545
              ],
              [
                13.32656326578529,
                52.51211617957373
              ],
              [
                13.32656168664739,
                52.5121253467061
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3B538',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328348996903546,
                52.51220994488843
              ],
              [
                13.328347072641256,
                52.512209821501195
              ],
              [
                13.328346728669965,
                52.512211818302305
              ],
              [
                13.32833229670303,
                52.51221089289807
              ],
              [
                13.328317050625477,
                52.51220991529155
              ],
              [
                13.328294329529934,
                52.5122084583731
              ],
              [
                13.32829512691782,
                52.51220382942511
              ],
              [
                13.328298973139345,
                52.512181501558295
              ],
              [
                13.328321694235065,
                52.512182958476735
              ],
              [
                13.328321780227784,
                52.51218245927648
              ],
              [
                13.328337026305459,
                52.512183436883
              ],
              [
                13.32833694031274,
                52.51218393608327
              ],
              [
                13.32835137227979,
                52.512184861487505
              ],
              [
                13.328351200294339,
                52.51218585988805
              ],
              [
                13.32835312455663,
                52.512185983275295
              ],
              [
                13.328348996903546,
                52.51220994488843
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3B5D5',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.328353468527524,
                52.51218398647418
              ],
              [
                13.328337112298179,
                52.51218293768272
              ],
              [
                13.328337026305459,
                52.512183436883
              ],
              [
                13.328321780227784,
                52.51218245927648
              ],
              [
                13.328321866220493,
                52.51218196007619
              ],
              [
                13.328299145124769,
                52.51218050315773
              ],
              [
                13.328303147692411,
                52.51215726765396
              ],
              [
                13.328325868788285,
                52.51215872457244
              ],
              [
                13.328326462919083,
                52.5121552755523
              ],
              [
                13.32834170899689,
                52.51215625315885
              ],
              [
                13.32834111486608,
                52.512159702178955
              ],
              [
                13.328357471095536,
                52.51216075097043
              ],
              [
                13.328353468527524,
                52.51218398647418
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3B7F2',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326854716615856,
                52.51222782717187
              ],
              [
                13.326804389867892,
                52.51222460012235
              ],
              [
                13.326805484324598,
                52.51221824666439
              ],
              [
                13.32680400412618,
                52.51221815175117
              ],
              [
                13.326809069893063,
                52.512188744317015
              ],
              [
                13.326809661972437,
                52.512188782282294
              ],
              [
                13.326809841775763,
                52.51218773849992
              ],
              [
                13.326806215289615,
                52.512187505962515
              ],
              [
                13.32680739573734,
                52.51218065330422
              ],
              [
                13.326821901682056,
                52.51218158345378
              ],
              [
                13.32683833188511,
                52.51218263699053
              ],
              [
                13.32686223709116,
                52.51218416983905
              ],
              [
                13.32686004818098,
                52.5121968767551
              ],
              [
                13.326854716615856,
                52.51222782717187
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '3BED6',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327068605335919,
                52.512241542132635
              ],
              [
                13.327019758771131,
                52.51223840999619
              ],
              [
                13.327020853228234,
                52.512232056538224
              ],
              [
                13.327014932432725,
                52.51223167688533
              ],
              [
                13.327013837975636,
                52.51223803034332
              ],
              [
                13.326964251315367,
                52.51223485075031
              ],
              [
                13.326970880592912,
                52.512196366947634
              ],
              [
                13.326992565505343,
                52.51219775742637
              ],
              [
                13.32699279221393,
                52.512196441352906
              ],
              [
                13.327009370440965,
                52.51219750438103
              ],
              [
                13.327009143732376,
                52.51219882045447
              ],
              [
                13.327023649681355,
                52.51219975060408
              ],
              [
                13.32702246923291,
                52.51220660326236
              ],
              [
                13.327018250666093,
                52.512206332759675
              ],
              [
                13.327018070862659,
                52.512207376542044
              ],
              [
                13.327022067399641,
                52.51220763280776
              ],
              [
                13.327023991658201,
                52.51220775619495
              ],
              [
                13.327024570156187,
                52.51220439793858
              ],
              [
                13.327029750852331,
                52.51220473013486
              ],
              [
                13.327030532606228,
                52.512200191950576
              ],
              [
                13.327060728664568,
                52.51220212818038
              ],
              [
                13.32707523461464,
                52.51220305833
              ],
              [
                13.327068605335919,
                52.512241542132635
              ]
            ]
          }
        }
      ]
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCFloor',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': 'd6152601-0154-581a-8bb5-0d1643972a21',
    'name': 'H_5',
    'type': 'floor',
    'inPlace': {
      'geo': {
        'point': {
          'type': 'Point',
          'coordinates': [
            13.32687,
            52.51211
          ]
        },
        'polygon': {
          'type': 'Polygon',
          'coordinates': [
            [
              [
                13.3254469,
                52.5123792
              ],
              [
                13.3256455,
                52.5123886
              ],
              [
                13.3256706,
                52.5122521
              ],
              [
                13.3274413,
                52.5123518
              ],
              [
                13.3274581,
                52.5126071
              ],
              [
                13.327813,
                52.5126284
              ],
              [
                13.3279179,
                52.5123973
              ],
              [
                13.3282613,
                52.5124033
              ],
              [
                13.3282347,
                52.5125532
              ],
              [
                13.328432,
                52.5125591
              ],
              [
                13.3285734,
                52.5118008
              ],
              [
                13.3283154,
                52.5117772
              ],
              [
                13.3282977,
                52.5118288
              ],
              [
                13.3258416,
                52.5116718
              ],
              [
                13.3258416,
                52.5116266
              ],
              [
                13.325554,
                52.5116065
              ],
              [
                13.3254469,
                52.5123792
              ]
            ]
          ]
        }
      },
      'type': 'building',
      'categories': [
        'education'
      ],
      'name': 'Hauptgebäude',
      'alternateNames': [
        'H'
      ],
      'uid': 'ebe95e80-f826-5a70-a844-436b561d5181',
      'address': {
        'addressCountry': 'Germany',
        'addressLocality': 'Berlin',
        'addressRegion': 'Berlin',
        'postalCode': '10623',
        'streetAddress': 'Straße des 17. Juni 135'
      },
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'floorName': '5',
    'plan': {
      'type': 'FeatureCollection',
      'crs': {
        'type': 'name',
        'properties': {
          'name': 'urn:ogc:def:crs:EPSG::3857'
        }
      },
      'features': [
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '147B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326591029418713,
                52.5121937700983
              ],
              [
                13.326591029418713,
                52.5121937700983
              ],
              [
                13.326591029418713,
                52.5121937700983
              ],
              [
                13.326524600245316,
                52.51218951052837
              ],
              [
                13.326524600245316,
                52.51218951052837
              ],
              [
                13.326524600245316,
                52.51218951052837
              ],
              [
                13.32653610454212,
                52.51212272647053
              ],
              [
                13.326577326216732,
                52.51212536968581
              ],
              [
                13.326586074701945,
                52.512125930655976
              ],
              [
                13.326600902643253,
                52.51212688145283
              ],
              [
                13.326602533716805,
                52.512126986040506
              ],
              [
                13.326599418289492,
                52.51214507155161
              ],
              [
                13.326605349466083,
                52.51214545187036
              ],
              [
                13.326615877304665,
                52.512146126936116
              ],
              [
                13.326614919880111,
                52.51215168492244
              ],
              [
                13.326613248185867,
                52.51216138934299
              ],
              [
                13.326613096213629,
                52.512162271563064
              ],
              [
                13.326602568375092,
                52.51216159649728
              ],
              [
                13.326596637198529,
                52.512161216178534
              ],
              [
                13.326591029418713,
                52.5121937700983
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1484',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326645369560328,
                52.51220902843097
              ],
              [
                13.326594954558116,
                52.51220579572164
              ],
              [
                13.326601664140153,
                52.51216684570659
              ],
              [
                13.326608040155001,
                52.51216725454923
              ],
              [
                13.32660857205795,
                52.51216416677906
              ],
              [
                13.326621472367263,
                52.51216499397235
              ],
              [
                13.32663793138295,
                52.51216604935687
              ],
              [
                13.326652611045926,
                52.51216699064579
              ],
              [
                13.326645369560328,
                52.51220902843097
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1497',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32673433722036,
                52.51221473321232
              ],
              [
                13.326701715743676,
                52.512212641459136
              ],
              [
                13.326702726361164,
                52.51220677469585
              ],
              [
                13.32669679518375,
                52.51220639437711
              ],
              [
                13.32669578456626,
                52.512212261140384
              ],
              [
                13.326646852354571,
                52.512209123510694
              ],
              [
                13.326654093840181,
                52.51216708572548
              ],
              [
                13.326667438988641,
                52.51216794144267
              ],
              [
                13.326683898005438,
                52.51216899682721
              ],
              [
                13.326697687992787,
                52.512169881068324
              ],
              [
                13.326696928131307,
                52.51217429216857
              ],
              [
                13.326701376514377,
                52.51217457740764
              ],
              [
                13.326700768625075,
                52.512178106287806
              ],
              [
                13.326702399698878,
                52.51217821087547
              ],
              [
                13.326702247726539,
                52.51217909309553
              ],
              [
                13.326700728002786,
                52.512187915295996
              ],
              [
                13.326699694590294,
                52.5121939143923
              ],
              [
                13.32673750584741,
                52.512196338924404
              ],
              [
                13.32673433722036,
                52.51221473321232
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '14A4',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326737657819868,
                52.51219545670432
              ],
              [
                13.326701329357086,
                52.512193127251955
              ],
              [
                13.326702210797144,
                52.512188010375674
              ],
              [
                13.326703730520888,
                52.51217918817521
              ],
              [
                13.326703882493232,
                52.51217830595515
              ],
              [
                13.326706551523102,
                52.51217847709859
              ],
              [
                13.326706749087133,
                52.51217733021254
              ],
              [
                13.32670289382177,
                52.51217708300535
              ],
              [
                13.326704064008531,
                52.512170289910976
              ],
              [
                13.32672185754116,
                52.51217143086727
              ],
              [
                13.326738316559272,
                52.51217248625181
              ],
              [
                13.326741578707042,
                52.51217269542714
              ],
              [
                13.326737657819868,
                52.51219545670432
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '14B1',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32680652197437,
                52.51221343027415
              ],
              [
                13.326803556385192,
                52.51221324011476
              ],
              [
                13.326802545767531,
                52.512219106878064
              ],
              [
                13.326735820014786,
                52.51221482829199
              ],
              [
                13.326743061501494,
                52.51217279050682
              ],
              [
                13.326774496744584,
                52.51217480619627
              ],
              [
                13.32679095576395,
                52.51217586158083
              ],
              [
                13.326804449194528,
                52.51217672680602
              ],
              [
                13.326803689332912,
                52.51218113790625
              ],
              [
                13.326808730834513,
                52.512181461177214
              ],
              [
                13.326808122945108,
                52.51218499005738
              ],
              [
                13.326811385093208,
                52.512185199232704
              ],
              [
                13.32680652197437,
                52.51221343027415
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '14C0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326655481048007,
                52.512147283860756
              ],
              [
                13.326653330996335,
                52.51214714599519
              ],
              [
                13.326641468642366,
                52.51214638535771
              ],
              [
                13.326609143728852,
                52.512144312620514
              ],
              [
                13.326610906605039,
                52.51213407886789
              ],
              [
                13.326607347899051,
                52.51213385067663
              ],
              [
                13.32660844969624,
                52.51212745458125
              ],
              [
                13.32665834572162,
                52.51213065401275
              ],
              [
                13.326655481048007,
                52.512147283860756
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '14CD',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326640949377554,
                52.5121285127654
              ],
              [
                13.326608624464038,
                52.51212644002819
              ],
              [
                13.326609019591208,
                52.51212414625604
              ],
              [
                13.32661053931071,
                52.512115324055486
              ],
              [
                13.326611929853511,
                52.512107251741966
              ],
              [
                13.326644254767206,
                52.512109324479155
              ],
              [
                13.326640949377554,
                52.5121285127654
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '14E1',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326713460857988,
                52.51212531305396
              ],
              [
                13.32669522248724,
                52.512124143573786
              ],
              [
                13.32669222863834,
                52.512141523308856
              ],
              [
                13.326707797979116,
                52.512142521645586
              ],
              [
                13.326706886146471,
                52.51214781496591
              ],
              [
                13.326706415032849,
                52.51215054984806
              ],
              [
                13.326687731823998,
                52.51214935184399
              ],
              [
                13.326675869469437,
                52.51214859120647
              ],
              [
                13.326657186261418,
                52.5121473932024
              ],
              [
                13.326660225702849,
                52.512129748801335
              ],
              [
                13.326641690774663,
                52.512128560305214
              ],
              [
                13.326644775805118,
                52.512110651238075
              ],
              [
                13.326715208535239,
                52.51211516752332
              ],
              [
                13.326713460857988,
                52.51212531305396
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '14F0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326744300430132,
                52.51215297913416
              ],
              [
                13.326726581036619,
                52.51215184293184
              ],
              [
                13.326714718681389,
                52.51215108229433
              ],
              [
                13.326712346210371,
                52.51215093016683
              ],
              [
                13.32671281732399,
                52.51214819528467
              ],
              [
                13.326713865931527,
                52.5121421079663
              ],
              [
                13.326707934753998,
                52.51214172764753
              ],
              [
                13.326700001804124,
                52.512141218971195
              ],
              [
                13.326702684110419,
                52.51212564778723
              ],
              [
                13.326722479415503,
                52.51212691710111
              ],
              [
                13.326735083168098,
                52.512127725278475
              ],
              [
                13.32674850245788,
                52.51212858574967
              ],
              [
                13.326744300430132,
                52.51215297913416
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '14F9',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326749047924283,
                52.512127594966536
              ],
              [
                13.326735257935875,
                52.512126710725404
              ],
              [
                13.326722654183277,
                52.51212590254806
              ],
              [
                13.326714202255188,
                52.51212536059383
              ],
              [
                13.326715949932437,
                52.51211521506316
              ],
              [
                13.32675079560163,
                52.512117449435884
              ],
              [
                13.326749047924283,
                52.512127594966536
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1501',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326867298241233,
                52.512160865994545
              ],
              [
                13.326854694486215,
                52.51216005781716
              ],
              [
                13.326842090731434,
                52.51215924963977
              ],
              [
                13.326840607936772,
                52.512159154560116
              ],
              [
                13.326845296275607,
                52.512131938071455
              ],
              [
                13.326846679220473,
                52.51212390986895
              ],
              [
                13.326873369525208,
                52.51212562130344
              ],
              [
                13.326867298241233,
                52.512160865994545
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1518',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326922961701435,
                52.51214668514466
              ],
              [
                13.326919996111766,
                52.512146494985274
              ],
              [
                13.326912656277361,
                52.51214602434078
              ],
              [
                13.326907985473698,
                52.51214572483975
              ],
              [
                13.32690605542785,
                52.51215692903442
              ],
              [
                13.326916583271045,
                52.512157604100246
              ],
              [
                13.326915489071368,
                52.512163956084606
              ],
              [
                13.326902143918069,
                52.51216310036736
              ],
              [
                13.326890281559805,
                52.51216233972983
              ],
              [
                13.326876195009623,
                52.512161436472724
              ],
              [
                13.326877289209227,
                52.512155084488356
              ],
              [
                13.326887817051986,
                52.51215575955417
              ],
              [
                13.32688974709777,
                52.51214455535951
              ],
              [
                13.326885076294255,
                52.51214425585847
              ],
              [
                13.326877736460222,
                52.512143785213986
              ],
              [
                13.326874770870745,
                52.5121435950546
              ],
              [
                13.326877817909445,
                52.51212590654252
              ],
              [
                13.326928232932666,
                52.51212913925212
              ],
              [
                13.326927966981735,
                52.512130683137215
              ],
              [
                13.326927131135829,
                52.51213553534751
              ],
              [
                13.326926849987606,
                52.512137167454604
              ],
              [
                13.32692462579534,
                52.512137024835084
              ],
              [
                13.326922961701435,
                52.51214668514466
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '151F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326886286605971,
                52.512154635659485
              ],
              [
                13.326873015592971,
                52.512153784696224
              ],
              [
                13.326874596102867,
                52.51214460960767
              ],
              [
                13.326877561692346,
                52.51214479976706
              ],
              [
                13.326884901526372,
                52.51214527041154
              ],
              [
                13.326887867115898,
                52.512145460570935
              ],
              [
                13.326886286605971,
                52.512154635659485
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1526',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32692120642353,
                52.51215687478627
              ],
              [
                13.32690793540985,
                52.51215602382301
              ],
              [
                13.326909515919837,
                52.51214684873446
              ],
              [
                13.326912481509465,
                52.51214703889384
              ],
              [
                13.326919821343864,
                52.51214750953833
              ],
              [
                13.326922786933542,
                52.512147699697714
              ],
              [
                13.32692120642353,
                52.51215687478627
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1555',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326923839379715,
                52.51217292059005
              ],
              [
                13.326922319656806,
                52.51218174279054
              ],
              [
                13.326922167684483,
                52.51218262501059
              ],
              [
                13.326905263823523,
                52.51218154110208
              ],
              [
                13.326905157442898,
                52.512182158656124
              ],
              [
                13.326899967661104,
                52.512181825877185
              ],
              [
                13.326883656918604,
                52.51218078000055
              ],
              [
                13.326844955976572,
                52.51217829842059
              ],
              [
                13.326828496955917,
                52.512177243035985
              ],
              [
                13.326791130532104,
                52.51217484702778
              ],
              [
                13.326774671512728,
                52.5121737916432
              ],
              [
                13.326738491327397,
                52.512171471698764
              ],
              [
                13.326722032309297,
                52.51217041631419
              ],
              [
                13.32668407277355,
                52.51216798227414
              ],
              [
                13.326667613756756,
                52.512166926889606
              ],
              [
                13.326638106151059,
                52.512165034803814
              ],
              [
                13.326621647135362,
                52.51216397941928
              ],
              [
                13.32661408488504,
                52.51216349451287
              ],
              [
                13.326614282448958,
                52.512162347626806
              ],
              [
                13.326614434421206,
                52.512161465406756
              ],
              [
                13.326616106115452,
                52.5121517609862
              ],
              [
                13.326617063540002,
                52.51214620299989
              ],
              [
                13.326617124328857,
                52.51214585011185
              ],
              [
                13.326641293874415,
                52.51214739991077
              ],
              [
                13.326653156228385,
                52.51214816054826
              ],
              [
                13.326675694701473,
                52.51214960575953
              ],
              [
                13.326687557056033,
                52.51215036639704
              ],
              [
                13.326714543913413,
                52.51215209684739
              ],
              [
                13.32672640626864,
                52.5121528574849
              ],
              [
                13.326841915963419,
                52.51216026419284
              ],
              [
                13.326854519718207,
                52.51216107237023
              ],
              [
                13.326890106791778,
                52.512163354282855
              ],
              [
                13.326901969150045,
                52.51216411492042
              ],
              [
                13.326908196888215,
                52.512164514255126
              ],
              [
                13.326908060113226,
                52.51216530825319
              ],
              [
                13.326924963974259,
                52.512166392161674
              ],
              [
                13.326923839379715,
                52.51217292059005
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '156A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326858891963788,
                52.51222271990634
              ],
              [
                13.326808476945866,
                52.512219487196816
              ],
              [
                13.32680948756354,
                52.51221362043354
              ],
              [
                13.326808004768953,
                52.51221352535386
              ],
              [
                13.326812867887819,
                52.51218529431242
              ],
              [
                13.326814054123494,
                52.512185370376145
              ],
              [
                13.326814251687567,
                52.5121842234901
              ],
              [
                13.32681024814214,
                52.51218396677495
              ],
              [
                13.326811418329115,
                52.51217717368059
              ],
              [
                13.326828322187758,
                52.51217825758907
              ],
              [
                13.326844781208408,
                52.51217931297363
              ],
              [
                13.326866133452008,
                52.5121806821212
              ],
              [
                13.326858891963788,
                52.51222271990634
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '157B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326913876310833,
                52.512220314043894
              ],
              [
                13.326911059000683,
                52.51222013339247
              ],
              [
                13.32691004838283,
                52.512226000155756
              ],
              [
                13.326860374758489,
                52.51222281498604
              ],
              [
                13.32686761624673,
                52.51218077720091
              ],
              [
                13.326883482150427,
                52.51218179455362
              ],
              [
                13.326899792892924,
                52.51218284043024
              ],
              [
                13.326920107181857,
                52.512184143022054
              ],
              [
                13.326913876310833,
                52.512220314043894
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '157E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326746005643756,
                52.512153088475785
              ],
              [
                13.326813176236039,
                52.51215739558578
              ],
              [
                13.326814695957474,
                52.51214857338526
              ],
              [
                13.326820627135929,
                52.512148953704035
              ],
              [
                13.326819107414487,
                52.51215777590456
              ],
              [
                13.326836159552792,
                52.51215886932103
              ],
              [
                13.326842230836453,
                52.512123624629886
              ],
              [
                13.326751483808694,
                52.512117805752766
              ],
              [
                13.326749789321513,
                52.51212764250637
              ],
              [
                13.326750382439297,
                52.512127680538256
              ],
              [
                13.326746005643756,
                52.512153088475785
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '15A0',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326970049528247,
                52.5122083957921
              ],
              [
                13.326966394587922,
                52.51222961318413
              ],
              [
                13.32691597956209,
                52.512226380474516
              ],
              [
                13.326916990179956,
                52.51222051371125
              ],
              [
                13.326915359105653,
                52.512220409123586
              ],
              [
                13.326920845312157,
                52.51218856097998
              ],
              [
                13.326939676806834,
                52.51218976849209
              ],
              [
                13.326956284109652,
                52.51219083338468
              ],
              [
                13.32697289141287,
                52.51219189827726
              ],
              [
                13.32697156925291,
                52.51219957359165
              ],
              [
                13.326970049528247,
                52.5122083957921
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '15AC',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327019303030543,
                52.51222707421019
              ],
              [
                13.327018292412504,
                52.512232940973455
              ],
              [
                13.326967877382865,
                52.51222970826382
              ],
              [
                13.326971532323192,
                52.51220849087181
              ],
              [
                13.326973052047858,
                52.51219966867134
              ],
              [
                13.326974374207818,
                52.51219199335695
              ],
              [
                13.326997802368453,
                52.51219349561614
              ],
              [
                13.327011147523603,
                52.51219435133341
              ],
              [
                13.327029089343709,
                52.51219550179772
              ],
              [
                13.327027843169853,
                52.512202736002095
              ],
              [
                13.327023543064167,
                52.512202460270984
              ],
              [
                13.327019303030543,
                52.51222707421019
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '15BB',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327076871561333,
                52.5122148886422
              ],
              [
                13.32707315583133,
                52.51223645892223
              ],
              [
                13.327024223592703,
                52.512233321292236
              ],
              [
                13.327025234210756,
                52.51222745452897
              ],
              [
                13.327020785825592,
                52.512227169289886
              ],
              [
                13.327024828295041,
                52.51220370223673
              ],
              [
                13.327029573239257,
                52.51220400649176
              ],
              [
                13.32703025711524,
                52.51220003650156
              ],
              [
                13.327035891736536,
                52.5122003978044
              ],
              [
                13.327036651598613,
                52.512195986704164
              ],
              [
                13.327049700195486,
                52.51219682340552
              ],
              [
                13.327066159221456,
                52.51219787879016
              ],
              [
                13.327079652657455,
                52.51219874401539
              ],
              [
                13.327078315300014,
                52.51220650755176
              ],
              [
                13.327076871561333,
                52.5122148886422
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '15E2',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327080431196604,
                52.512191613620104
              ],
              [
                13.32708027922424,
                52.51219249584015
              ],
              [
                13.3270798992933,
                52.51219470139028
              ],
              [
                13.327076637143913,
                52.51219449221494
              ],
              [
                13.32707612043779,
                52.5121974917631
              ],
              [
                13.327066333989722,
                52.51219686423708
              ],
              [
                13.327049874963746,
                52.51219580885246
              ],
              [
                13.327011322291852,
                52.51219333678034
              ],
              [
                13.326997977136692,
                52.512192481063074
              ],
              [
                13.326956458877875,
                52.512189818831594
              ],
              [
                13.326939851575053,
                52.51218875393903
              ],
              [
                13.32692561674439,
                52.512187841173976
              ],
              [
                13.326926133450389,
                52.51218484162581
              ],
              [
                13.326923019581207,
                52.51218464195847
              ],
              [
                13.326923353920344,
                52.51218270107436
              ],
              [
                13.326923505892672,
                52.51218181885428
              ],
              [
                13.326925025615584,
                52.51217299665378
              ],
              [
                13.326926150210136,
                52.51216646822544
              ],
              [
                13.326926484548997,
                52.51216452734134
              ],
              [
                13.326929153579739,
                52.512164698484774
              ],
              [
                13.326930414948801,
                52.51215737605837
              ],
              [
                13.32694109107187,
                52.51215806063217
              ],
              [
                13.32695443622593,
                52.51215891634943
              ],
              [
                13.326973267721542,
                52.512160123861534
              ],
              [
                13.326986612876237,
                52.512160979578816
              ],
              [
                13.32700544437274,
                52.51216218709095
              ],
              [
                13.327018789528054,
                52.51216304280821
              ],
              [
                13.327028279416433,
                52.51216365131829
              ],
              [
                13.32702713962493,
                52.51217026796864
              ],
              [
                13.327038408867542,
                52.51217099057435
              ],
              [
                13.327039548659055,
                52.51216437392397
              ],
              [
                13.327049854085047,
                52.512165034727865
              ],
              [
                13.327063199241223,
                52.512165890445154
              ],
              [
                13.327081363481993,
                52.512167055171425
              ],
              [
                13.327080147704276,
                52.51217411293183
              ],
              [
                13.327083409853692,
                52.512174322107185
              ],
              [
                13.327083075514745,
                52.51217626299127
              ],
              [
                13.327081950919894,
                52.51218279141965
              ],
              [
                13.327080431196604,
                52.512191613620104
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '15E9',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327081235437406,
                52.512146041189965
              ],
              [
                13.3269282109913,
                52.51213622896529
              ],
              [
                13.326928317371706,
                52.512135611411274
              ],
              [
                13.326929153217614,
                52.51213075920096
              ],
              [
                13.326929419168552,
                52.51212921531586
              ],
              [
                13.327082443614962,
                52.51213902754054
              ],
              [
                13.327081235437406,
                52.512146041189965
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '15F3',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32713689664995,
                52.51217538807724
              ],
              [
                13.327136425535981,
                52.51217812295939
              ],
              [
                13.327134497902097,
                52.51217799935578
              ],
              [
                13.32712233898075,
                52.512177219702245
              ],
              [
                13.327085046680647,
                52.51217482844784
              ],
              [
                13.327085289836226,
                52.51217341689578
              ],
              [
                13.327082101826566,
                52.512173212474416
              ],
              [
                13.32708319602649,
                52.51216686049004
              ],
              [
                13.327137762891606,
                52.51217035942294
              ],
              [
                13.32713689664995,
                52.51217538807724
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1602',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32712828847514,
                52.51223406256793
              ],
              [
                13.327126805679864,
                52.512233967488235
              ],
              [
                13.327125795061633,
                52.512239834251496
              ],
              [
                13.3270746386265,
                52.51223655400194
              ],
              [
                13.327078354356503,
                52.51221498372188
              ],
              [
                13.327079798095196,
                52.51220660263147
              ],
              [
                13.32708188011738,
                52.512194516216866
              ],
              [
                13.327104270325025,
                52.51219595192032
              ],
              [
                13.327120729352304,
                52.51219700730497
              ],
              [
                13.327136150423264,
                52.51219799613382
              ],
              [
                13.327135162602577,
                52.51220373056411
              ],
              [
                13.327131307334811,
                52.512203483356885
              ],
              [
                13.327131109770642,
                52.512204630242955
              ],
              [
                13.327133333963575,
                52.51220477286251
              ],
              [
                13.32712828847514,
                52.51223406256793
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1611',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327201304789067,
                52.51222358111293
              ],
              [
                13.327197710636325,
                52.51224444561691
              ],
              [
                13.327131726242774,
                52.51224021457029
              ],
              [
                13.327132736861001,
                52.51223434780702
              ],
              [
                13.32712977127042,
                52.51223415764762
              ],
              [
                13.327134816758877,
                52.51220486794221
              ],
              [
                13.327136892672295,
                52.512205001053786
              ],
              [
                13.327137348589602,
                52.51220235439363
              ],
              [
                13.327142538373181,
                52.51220268717259
              ],
              [
                13.327143267840754,
                52.51219845251638
              ],
              [
                13.327158392353157,
                52.51219942232931
              ],
              [
                13.327174851381729,
                52.512200477713975
              ],
              [
                13.327204952128717,
                52.512202407831886
              ],
              [
                13.327202748528098,
                52.5122152000225
              ],
              [
                13.327201304789067,
                52.51222358111293
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1628',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327254685427022,
                52.51222700398215
              ],
              [
                13.327251091273965,
                52.51224786848612
              ],
              [
                13.327238487511925,
                52.51224706030867
              ],
              [
                13.327239498130345,
                52.51224119354539
              ],
              [
                13.327233566948273,
                52.512240813226605
              ],
              [
                13.327232556329868,
                52.51224667998985
              ],
              [
                13.327199193431758,
                52.51224454069661
              ],
              [
                13.327202787584506,
                52.51222367619262
              ],
              [
                13.327204231323549,
                52.512215295102216
              ],
              [
                13.327206434924173,
                52.512202502911585
              ],
              [
                13.32721533169695,
                52.51220307338979
              ],
              [
                13.327231642447346,
                52.512204119266485
              ],
              [
                13.327243208252417,
                52.51220486088814
              ],
              [
                13.32724223562878,
                52.512210507096434
              ],
              [
                13.327238528639963,
                52.512210269397194
              ],
              [
                13.327238315878516,
                52.512211504505245
              ],
              [
                13.327244098781069,
                52.51221187531608
              ],
              [
                13.327244554698462,
                52.51220922865595
              ],
              [
                13.327249744482854,
                52.512209561434894
              ],
              [
                13.327250473950565,
                52.512205326778705
              ],
              [
                13.327258332767013,
                52.51220583070111
              ],
              [
                13.327256129166203,
                52.51221862289173
              ],
              [
                13.327254685427022,
                52.51222700398215
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1629',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327142356717227,
                52.51217850327817
              ],
              [
                13.32719128896441,
                52.51218164090827
              ],
              [
                13.327195597375823,
                52.51215662996986
              ],
              [
                13.327213390921255,
                52.51215777092626
              ],
              [
                13.327215153797217,
                52.512147537173654
              ],
              [
                13.327086892000542,
                52.51213931277964
              ],
              [
                13.327082332837868,
                52.51216577938119
              ],
              [
                13.327158326095418,
                52.51217065221575
              ],
              [
                13.327160932417764,
                52.512155522141875
              ],
              [
                13.327162637632423,
                52.51215563148353
              ],
              [
                13.327159856542035,
                52.512171776110456
              ],
              [
                13.327143694072866,
                52.51217073974175
              ],
              [
                13.327142356717227,
                52.51217850327817
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1638',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327223243206504,
                52.51218368987581
              ],
              [
                13.327215310250672,
                52.51218318119942
              ],
              [
                13.327203151327895,
                52.512182401545886
              ],
              [
                13.327192994179144,
                52.51218175024992
              ],
              [
                13.32719439232379,
                52.512173633825476
              ],
              [
                13.327204994311188,
                52.51217431364534
              ],
              [
                13.32720772981011,
                52.51215843368442
              ],
              [
                13.327214773088578,
                52.51215888531302
              ],
              [
                13.327212181963224,
                52.512173927164866
              ],
              [
                13.32721625965078,
                52.51217418863405
              ],
              [
                13.327218850776141,
                52.51215914678218
              ],
              [
                13.327227376850225,
                52.512159693490446
              ],
              [
                13.327224816119264,
                52.51217455889832
              ],
              [
                13.327223243206504,
                52.51218368987581
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1643',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327243657920203,
                52.512182233815636
              ],
              [
                13.327243186806156,
                52.5121849686978
              ],
              [
                13.327240072935536,
                52.51218476903044
              ],
              [
                13.327227914012315,
                52.51218398937688
              ],
              [
                13.327224948421316,
                52.51218379921748
              ],
              [
                13.327226346566027,
                52.512175682793035
              ],
              [
                13.327231907049176,
                52.51217603934191
              ],
              [
                13.327234642548229,
                52.512160159381
              ],
              [
                13.32724732045001,
                52.51216097231245
              ],
              [
                13.327244706528683,
                52.51217614649732
              ],
              [
                13.327243657920203,
                52.512182233815636
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '164A',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327248051975907,
                52.512158031137226
              ],
              [
                13.327247717637274,
                52.51215997202136
              ],
              [
                13.327214132318986,
                52.51215781846613
              ],
              [
                13.327215895194954,
                52.51214758471349
              ],
              [
                13.327249480513347,
                52.51214973826873
              ],
              [
                13.327249267752487,
                52.512150973376805
              ],
              [
                13.327248051975907,
                52.512158031137226
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '1675',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32733471542012,
                52.512208008010745
              ],
              [
                13.32733456344769,
                52.51220889023078
              ],
              [
                13.327334426672502,
                52.51220968422882
              ],
              [
                13.327327754091746,
                52.51220925637016
              ],
              [
                13.3273112950595,
                52.51220820098546
              ],
              [
                13.327231817215623,
                52.512203104713436
              ],
              [
                13.327215506465224,
                52.51220205883673
              ],
              [
                13.327175026149996,
                52.51219946316091
              ],
              [
                13.327158567121414,
                52.51219840777625
              ],
              [
                13.32712090412055,
                52.51219599275189
              ],
              [
                13.327104445093257,
                52.51219493736725
              ],
              [
                13.327098217353308,
                52.51219453803252
              ],
              [
                13.32709836932569,
                52.51219365581246
              ],
              [
                13.32708146546039,
                52.51219257190389
              ],
              [
                13.327081617432755,
                52.51219168968389
              ],
              [
                13.32708313715605,
                52.5121828674834
              ],
              [
                13.327084261750894,
                52.51217633905505
              ],
              [
                13.327101165616277,
                52.512177422963596
              ],
              [
                13.327101256799628,
                52.51217689363156
              ],
              [
                13.32712216421266,
                52.512178234255295
              ],
              [
                13.327134323134,
                52.51217901390882
              ],
              [
                13.327168205007418,
                52.512181186479964
              ],
              [
                13.327180067370497,
                52.51218194711756
              ],
              [
                13.32720297655977,
                52.51218341609892
              ],
              [
                13.32721513548255,
                52.51218419575246
              ],
              [
                13.32722773924419,
                52.51218500392993
              ],
              [
                13.32723989816741,
                52.51218578358347
              ],
              [
                13.327249091499734,
                52.512186373077625
              ],
              [
                13.32726214010068,
                52.51218720977901
              ],
              [
                13.32733746612004,
                52.5121920398279
              ],
              [
                13.327337344542185,
                52.51219274560393
              ],
              [
                13.327336235144084,
                52.512199185810275
              ],
              [
                13.32733471542012,
                52.512208008010745
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '168F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327318601874097,
                52.51217187596427
              ],
              [
                13.327352261337287,
                52.512174034273556
              ],
              [
                13.327350939179015,
                52.51218170958799
              ],
              [
                13.327347528748753,
                52.51218149090466
              ],
              [
                13.327345796264815,
                52.51219154821322
              ],
              [
                13.327262314868813,
                52.512186195225944
              ],
              [
                13.32724911798832,
                52.51218534901661
              ],
              [
                13.327249589102367,
                52.512182614134446
              ],
              [
                13.327250637710865,
                52.51217652681611
              ],
              [
                13.327253426400148,
                52.51216033807818
              ],
              [
                13.327248459035046,
                52.51216001956119
              ],
              [
                13.327248793373675,
                52.51215807867708
              ],
              [
                13.327250009150255,
                52.512151020916654
              ],
              [
                13.327250518256578,
                52.512148065479465
              ],
              [
                13.327321914866157,
                52.51215264356715
              ],
              [
                13.327318601874097,
                52.51217187596427
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16A2',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327354130594733,
                52.512163182966916
              ],
              [
                13.327352641267737,
                52.51217182872342
              ],
              [
                13.327352337323383,
                52.51217359316354
              ],
              [
                13.327319419258034,
                52.51217148239411
              ],
              [
                13.327322656264009,
                52.512152691107005
              ],
              [
                13.327355574329536,
                52.51215480187644
              ],
              [
                13.327354130594733,
                52.512163182966916
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16B9',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327339317614735,
                52.51225352572839
              ],
              [
                13.327252574069519,
                52.51224796356583
              ],
              [
                13.327256168222588,
                52.51222709906187
              ],
              [
                13.327257611961763,
                52.51221871797144
              ],
              [
                13.327259815562583,
                52.51220592578082
              ],
              [
                13.32731112029119,
                52.51220921553851
              ],
              [
                13.32732757932344,
                52.51221027092321
              ],
              [
                13.327340627925864,
                52.51221110762459
              ],
              [
                13.327340118818134,
                52.51221406306173
              ],
              [
                13.327346050001129,
                52.51221444338056
              ],
              [
                13.327339317614735,
                52.51225352572839
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '16D9',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327413767675454,
                52.51224603509238
              ],
              [
                13.327413767675454,
                52.51224603509238
              ],
              [
                13.327413767675454,
                52.51224603509238
              ],
              [
                13.327347338422339,
                52.51224177552165
              ],
              [
                13.327347338422339,
                52.51224177552165
              ],
              [
                13.327347338422339,
                52.51224177552165
              ],
              [
                13.327352801835428,
                52.51221005971113
              ],
              [
                13.327335749684288,
                52.51220896629454
              ],
              [
                13.327335901656713,
                52.5122080840745
              ],
              [
                13.327337421380673,
                52.51219926187403
              ],
              [
                13.327338530778775,
                52.5121928216677
              ],
              [
                13.327355582930004,
                52.51219391508429
              ],
              [
                13.327356639137527,
                52.51218778365495
              ],
              [
                13.327353673545968,
                52.51218759349555
              ],
              [
                13.327354433407299,
                52.51218318239529
              ],
              [
                13.327357398998858,
                52.5121833725547
              ],
              [
                13.327358759150268,
                52.512175476685286
              ],
              [
                13.327384856356586,
                52.51217715008808
              ],
              [
                13.327425188404673,
                52.512179736256066
              ],
              [
                13.327413767675454,
                52.51224603509238
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2169',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326774491901368,
                52.5118284559698
              ],
              [
                13.32676025707393,
                52.51182754320474
              ],
              [
                13.326745132570098,
                52.51182657339187
              ],
              [
                13.326671289409123,
                52.5118218384232
              ],
              [
                13.326673401789824,
                52.511809575563824
              ],
              [
                13.326747541509963,
                52.51181432954841
              ],
              [
                13.326762369454956,
                52.51181528034535
              ],
              [
                13.326776604282445,
                52.5118161931104
              ],
              [
                13.326774491901368,
                52.5118284559698
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '216B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327409338169097,
                52.511814842891354
              ],
              [
                13.327280186656894,
                52.511806561448914
              ],
              [
                13.327287982692322,
                52.511761303557435
              ],
              [
                13.327417134206234,
                52.511769584999904
              ],
              [
                13.327409338169097,
                52.511814842891354
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '216C',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327397731131045,
                52.51184915283841
              ],
              [
                13.32732581553417,
                52.51184454147265
              ],
              [
                13.327309949619995,
                52.511843524119776
              ],
              [
                13.327303425318943,
                52.51184310576908
              ],
              [
                13.32730875945892,
                52.51181213984355
              ],
              [
                13.327403065271877,
                52.5118181869129
              ],
              [
                13.327397731131045,
                52.51184915283841
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '216D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327301007690457,
                52.51184669699786
              ],
              [
                13.327299381611823,
                52.51185613675291
              ],
              [
                13.327299168853829,
                52.51185737186102
              ],
              [
                13.327271737134371,
                52.51185561288649
              ],
              [
                13.327274837321095,
                52.5118376155965
              ],
              [
                13.327266681945208,
                52.51183709265812
              ],
              [
                13.327267472188462,
                52.51183250511361
              ],
              [
                13.327268748734905,
                52.51182509446476
              ],
              [
                13.327272752283074,
                52.51182535117996
              ],
              [
                13.327275396556493,
                52.511810000550184
              ],
              [
                13.327306980104087,
                52.51181202574791
              ],
              [
                13.327301007690457,
                52.51184669699786
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '216E',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327395254020349,
                52.511863533025995
              ],
              [
                13.327323338423763,
                52.511858921660235
              ],
              [
                13.327307472509656,
                52.51185790430738
              ],
              [
                13.327303320681724,
                52.51185763808419
              ],
              [
                13.327303533439729,
                52.51185640297607
              ],
              [
                13.327305159518378,
                52.51184696322103
              ],
              [
                13.327305372276276,
                52.511845728112874
              ],
              [
                13.327309524104225,
                52.51184599433607
              ],
              [
                13.327325390018384,
                52.51184701168889
              ],
              [
                13.327397305615218,
                52.511851623054675
              ],
              [
                13.327395254020349,
                52.511863533025995
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '216F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32731904939252,
                52.511916891935094
              ],
              [
                13.32725944100736,
                52.51191306973097
              ],
              [
                13.327261492607201,
                52.5119011597598
              ],
              [
                13.32726193744588,
                52.51190118828371
              ],
              [
                13.327276765401963,
                52.51190213908077
              ],
              [
                13.327321100992565,
                52.51190498196392
              ],
              [
                13.32731904939252,
                52.511916891935094
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2170',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32732128335696,
                52.511903923299805
              ],
              [
                13.327276947766338,
                52.511901080416635
              ],
              [
                13.327262119810253,
                52.511900129619605
              ],
              [
                13.327261674971579,
                52.51190010109568
              ],
              [
                13.327262146079509,
                52.51189736621341
              ],
              [
                13.327257994251857,
                52.51189709999025
              ],
              [
                13.327260030653186,
                52.51188527824103
              ],
              [
                13.327261459172865,
                52.511876985372126
              ],
              [
                13.327263632345378,
                52.51186436962478
              ],
              [
                13.327267784173063,
                52.51186463584796
              ],
              [
                13.327268255280234,
                52.51186190096566
              ],
              [
                13.32730636312843,
                52.51186434451409
              ],
              [
                13.32732222904251,
                52.51186536186692
              ],
              [
                13.327327863666282,
                52.51186572316981
              ],
              [
                13.32732128335696,
                52.511903923299805
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2171',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.32725825129848,
                52.51188516414539
              ],
              [
                13.327256214897163,
                52.51189698589458
              ],
              [
                13.327252507908216,
                52.51189674819534
              ],
              [
                13.327252355937926,
                52.51189763041543
              ],
              [
                13.327239307337022,
                52.51189679371405
              ],
              [
                13.327239459307307,
                52.51189591149395
              ],
              [
                13.327233972963816,
                52.51189555969906
              ],
              [
                13.327234580844907,
                52.511892030818686
              ],
              [
                13.327236009365066,
                52.51188373794981
              ],
              [
                13.32723961105711,
                52.51186282933357
              ],
              [
                13.32726185299066,
                52.51186425552914
              ],
              [
                13.327259679818162,
                52.51187687127649
              ],
              [
                13.32725825129848,
                52.51188516414539
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '2172',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326784665222112,
                52.51188253698953
              ],
              [
                13.326725056883417,
                52.511878714785894
              ],
              [
                13.326727108481386,
                52.51186680481464
              ],
              [
                13.326771444036444,
                52.51186964769746
              ],
              [
                13.326786271981916,
                52.511870598494376
              ],
              [
                13.326786716820287,
                52.51187062701828
              ],
              [
                13.326784665222112,
                52.51188253698953
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5B6B',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326674510941869,
                52.51180494436786
              ],
              [
                13.326748342674124,
                52.51180967860366
              ],
              [
                13.326763170619124,
                52.51181062940059
              ],
              [
                13.326770738557373,
                52.511811114671644
              ],
              [
                13.326775407922586,
                52.511784007898974
              ],
              [
                13.32680770604063,
                52.51178607891758
              ],
              [
                13.326812552788747,
                52.51175980692213
              ],
              [
                13.326813403817411,
                52.51175486648951
              ],
              [
                13.326817476563578,
                52.511731223171765
              ],
              [
                13.326688399171251,
                52.51172294647601
              ],
              [
                13.326674510941869,
                52.51180494436786
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5B7D',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326820310254206,
                52.51183139393234
              ],
              [
                13.327255111624925,
                52.51185927423982
              ],
              [
                13.327259416246779,
                52.51183662676757
              ],
              [
                13.3272589733546,
                52.51182179175448
              ],
              [
                13.327271428837674,
                52.51182259042401
              ],
              [
                13.327278206683383,
                52.51178324340734
              ],
              [
                13.327268716791393,
                52.511782634897244
              ],
              [
                13.327270282076867,
                52.511773548030135
              ],
              [
                13.327265833690022,
                52.51177326279102
              ],
              [
                13.327266575087828,
                52.51177331033086
              ],
              [
                13.327239588208258,
                52.51177157988024
              ],
              [
                13.326830633317053,
                52.511745356899006
              ],
              [
                13.326828824881728,
                52.511755855318334
              ],
              [
                13.326827973853037,
                52.51176079575095
              ],
              [
                13.326815861870298,
                52.51183110869325
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5B8F',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.326792993184053,
                52.51183419132831
              ],
              [
                13.326793403502872,
                52.51183180933404
              ],
              [
                13.326778573216158,
                52.511830858387
              ],
              [
                13.326759892346157,
                52.511829660532975
              ],
              [
                13.32674476784233,
                52.511828690720115
              ],
              [
                13.326733795163296,
                52.51182798713038
              ],
              [
                13.326727310042722,
                52.51186574738148
              ],
              [
                13.326771626400665,
                52.51186858903331
              ],
              [
                13.326786454346138,
                52.511869539830265
              ],
              [
                13.326787355095052,
                52.511866921693894
              ],
              [
                13.32679758637763,
                52.51186757774376
              ],
              [
                13.326810486690663,
                52.511868404937104
              ],
              [
                13.327110971457262,
                52.51188767260512
              ],
              [
                13.32711343268475,
                52.51187338464395
              ],
              [
                13.327114945477241,
                52.51186460254966
              ],
              [
                13.327116539037819,
                52.51185493771829
              ],
              [
                13.326792993184053,
                52.51183419132831
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5BA4',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327112383740971,
                52.51188776316348
              ],
              [
                13.327232193609179,
                52.51189544560339
              ],
              [
                13.327237831702455,
                52.511862715237925
              ],
              [
                13.327118021833101,
                52.51185503279798
              ],
              [
                13.327112383740971,
                52.51188776316348
              ]
            ]
          }
        },
        {
          'type': 'Feature',
          'properties': {
            'Layer': 'SYM_FL_100_',
            'SubClasses': null,
            'ExtendedEntity': null,
            'Linetype': null,
            'EntityHandle': '5BB6',
            'Text': null
          },
          'geometry': {
            'type': 'LineString',
            'coordinates': [
              [
                13.327233972963816,
                52.51189555969906
              ],
              [
                13.327256214897163,
                52.51189698589458
              ],
              [
                13.32726185299066,
                52.51186425552914
              ],
              [
                13.32723961105711,
                52.51186282933357
              ],
              [
                13.327233972963816,
                52.51189555969906
              ]
            ]
          }
        }
      ]
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCFloor',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'message',
    'uid': '4706ef24-b631-5c20-91d1-3c627decca5a',
    'image': 'icon ion-android-hand stapps-color-red-dark',
    'name': 'Lösung für das Problem des Zurücksetzens der StApps-App gefunden',
    'message': 'Wie bereits berichtet, klagten User über das Löschen ihres Stundenplans beim Update von Version 0.8.0 auf 0.8.1. Wir haben eine Lösung für das Problem gefunden und testen diese ausführlich bis zum Ende dieser Woche. Wenn alles glatt verläuft, dann kommt am Wochenende die fehlerbereinige Version 0.8.2 heraus.\n\n*(25.Okt 2016)*',
    'audiences': [
      'students'
    ],
    'datePublished': '2018-08-01',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCMessage',
}
,
{
  'errorNames': [
    'enum'
  ],
  'instance': {
    'type': 'invalid-value-in-schema',
    'uid': 'cdb7059c-a1a2-5229-821d-434c345e2917',
    'image': 'icon ion-android-hand stapps-color-red-dark',
    'name': 'Lösung für das Problem des Zurücksetzens der StApps-App gefunden',
    'message': 'Wie bereits berichtet, klagten User über das Löschen ihres Stundenplans beim Update von Version 0.8.0 auf 0.8.1. Wir haben eine Lösung für das Problem gefunden und testen diese ausführlich bis zum Ende dieser Woche. Wenn alles glatt verläuft, dann kommt am Wochenende die fehlerbereinige Version 0.8.2 heraus.\n\n*(25.Okt 2016)*',
    'audiences': [
      'students'
    ],
    'datePublished': '2018-08-01',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'foo',
      'type': 'remote'
    }
  },
  'schema': 'SCMessage',
}
,
{
  'errorNames': [
    'additionalProperties'
  ],
  'instance': {
    'type': 'message',
    'invalid-non-existing-key-in-schema': 1,
    'uid': '4706ef24-b631-5c20-91d1-3c627decca5a',
    'image': 'icon ion-android-hand stapps-color-red-dark',
    'name': 'Lösung für das Problem des Zurücksetzens der StApps-App gefunden',
    'message': 'Wie bereits berichtet, klagten User über das Löschen ihres Stundenplans beim Update von Version 0.8.0 auf 0.8.1. Wir haben eine Lösung für das Problem gefunden und testen diese ausführlich bis zum Ende dieser Woche. Wenn alles glatt verläuft, dann kommt am Wochenende die fehlerbereinige Version 0.8.2 heraus.\n\n*(25.Okt 2016)*',
    'audiences': [
      'students'
    ],
    'datePublished': '2018-08-01',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCMessage',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'organization',
    'uid': '20e48393-0d2b-5bdc-9d92-5e0dc1e2860e',
    'name': 'Technische Universität Berlin',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCOrganization',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'person',
    'uid': '97044080-fdf3-5ec0-8734-c412ac2dde03',
    'givenName': 'Michael',
    'familyName': 'Joswig',
    'gender': 'male',
    'honorificPrefix': 'Prof. Dr.',
    'name': 'Michael Joswig',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCPerson',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'person',
    'uid': 'eb516021-3b37-5358-baef-345a0e10da5b',
    'givenName': 'Michael',
    'familyName': 'Gradzielski',
    'gender': 'male',
    'honorificPrefix': 'Prof. Dr.',
    'name': 'Michael Gradzielski',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCPerson',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'person',
    'familyName': 'Mustermann',
    'givenName': 'Erika',
    'honorificPrefix': "Univ.-Prof'in Dr.",
    'gender': 'female',
    'jobTitles': [
      'Direktor/in, Akademie für Bildungsforschung und Lehrerbildung',
      'Professorinnen und Professoren, Politikwissenschaft mit dem Schwerpunkt Internationale Institutionen und Friedensprozesse',
      'Vizepräsidentinnen und Vizepräsidenten, Leitung der Universität (Präsidium)',
      'Senat',
      'Leitung, Projektkoordination - Abteilung Lehre und Qualitätssicherung'
    ],
    'name': "Univ.-Prof'in Dr. Erika Mustermann",
    'uid': 'be34a419-e9e8-5de0-b998-dd1b19e7f451',
    'workLocations': [
      {
        'url': 'http://www.fb03.uni-frankfurt.de/1234567',
        'email': 'mustermann@soz.uni-frankfurt.de',
        'faxNumber': '',
        'telephone': '069/123-36232; -1324325',
        'hoursAvailable': 'siehe Homepage',
        'areaServed': {
          'type': 'room',
          'categories': [
            'education'
          ],
          'uid': '39c1a574-04ef-5157-9c6f-e271d93eb273',
          'name': '3.G 121',
          'alternateNames': [
            'Dienstzimmer'
          ],
          'geo': {
            'point': {
              'coordinates': [
                8.66919,
                50.12834
              ],
              'type': 'Point'
            }
          },
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          }
        }
      },
      {
        'url': 'http://www2.uni-frankfurt.de/12345/vizepraesidenten',
        'email': '',
        'faxNumber': '',
        'telephone': '069/123-1235',
        'hoursAvailable': 'siehe Homepage',
        'areaServed': {
          'type': 'room',
          'categories': [
            'education'
          ],
          'uid': '5e54aefd-078b-5007-bca1-53dc60f79d37',
          'name': '4.P 22',
          'alternateNames': [
            'Dienstzimmer'
          ],
          'geo': {
            'point': {
              'coordinates': [
                8.66898,
                50.12772
              ],
              'type': 'Point'
            }
          },
          'origin': {
            'indexed': '2018-09-11T12:30:00Z',
            'name': 'Dummy',
            'type': 'remote'
          }
        }
      }
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCPerson',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': 'f5fe4d13-d56a-5770-b16e-78782841bf02',
    'name': 'Validierer (UB)',
    'type': 'point of interest',
    'categories': [
      'validator'
    ],
    'description': 'EG Eingangsbereich',
    'geo': {
      'point': {
        'type': 'Point',
        'coordinates': [
          8.653079867363,
          50.120368286434
        ]
      }
    },
    'inPlace': {
      'name': 'UB',
      'address': {
        'addressCountry': 'Deutschland',
        'addressLocality': 'Frankfurt am Main',
        'addressRegion': 'Hessen',
        'postalCode': '60325',
        'streetAddress': 'Bockenheimer Landstr. 134-138'
      },
      'geo': {
        'point': {
          'coordinates': [
            8.65302,
            50.12036
          ],
          'type': 'Point'
        },
        'polygon': {
          'coordinates': [
            [
              [
                8.6524924635887,
                50.120309814205
              ],
              [
                8.6525192856789,
                50.120423319054
              ],
              [
                8.6526641249657,
                50.120426758591
              ],
              [
                8.6526963114738,
                50.120547142219
              ],
              [
                8.6526480317116,
                50.120554021274
              ],
              [
                8.6527070403099,
                50.120839501198
              ],
              [
                8.65344196558,
                50.120777590034
              ],
              [
                8.6533936858177,
                50.120498988804
              ],
              [
                8.6533454060554,
                50.120498988804
              ],
              [
                8.6533239483833,
                50.120375165515
              ],
              [
                8.6534956097603,
                50.12035796781
              ],
              [
                8.6534580588341,
                50.120241023257
              ],
              [
                8.6524924635887,
                50.120309814205
              ]
            ]
          ],
          'type': 'Polygon'
        }
      },
      'categories': [
        'library'
      ],
      'type': 'building',
      'uid': '65596790-a217-5d70-888e-16aa17bfda0a',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCPointOfInterest',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '5a5ca30a-1494-5707-9692-ff902e104c12',
    'name': 'Drucker 1 (IG)',
    'type': 'point of interest',
    'categories': [
      'printer'
    ],
    'description': 'Raum 124 (Q1) (Bibliothek BzG, EG), 1x KM Farb-Drucker',
    'geo': {
      'point': {
        'type': 'Point',
        'coordinates': [
          8.6657989025116,
          50.125455096926
        ]
      }
    },
    'inPlace': {
      'name': 'IG-Farben-Haus',
      'address': {
        'addressCountry': 'Deutschland',
        'addressLocality': 'Frankfurt am Main',
        'addressRegion': 'Hessen',
        'postalCode': '60323',
        'streetAddress': 'Norbert-Wollheim-Platz 1'
      },
      'geo': {
        'point': {
          'coordinates': [
            8.66754,
            50.12539
          ],
          'type': 'Point'
        },
        'polygon': {
          'coordinates': [
            [
              [
                8.6657291650772,
                50.125584925616
              ],
              [
                8.6659651994705,
                50.125568589575
              ],
              [
                8.6659370362759,
                50.125409527832
              ],
              [
                8.6663648486137,
                50.125404369064
              ],
              [
                8.6663661897182,
                50.125483470114
              ],
              [
                8.6665780842304,
                50.125483470114
              ],
              [
                8.6665780842304,
                50.125404369064
              ],
              [
                8.6670058965683,
                50.125416406189
              ],
              [
                8.6669830977917,
                50.125504964942
              ],
              [
                8.6671829223633,
                50.125527319552
              ],
              [
                8.6671949923038,
                50.125484329907
              ],
              [
                8.6672767996788,
                50.125493787632
              ],
              [
                8.6672674119473,
                50.125550533945
              ],
              [
                8.6672835052013,
                50.125590084365
              ],
              [
                8.6673210561275,
                50.125612438936
              ],
              [
                8.6673800647259,
                50.125631354334
              ],
              [
                8.6674457788467,
                50.12562705538
              ],
              [
                8.667494058609,
                50.125610719354
              ],
              [
                8.6675289273262,
                50.125578047284
              ],
              [
                8.6675503849983,
                50.12552645976
              ],
              [
                8.6676281690598,
                50.125540216438
              ],
              [
                8.6676093935966,
                50.125587504991
              ],
              [
                8.6677998304367,
                50.125621036845
              ],
              [
                8.6678387224674,
                50.125534197892
              ],
              [
                8.6682437360287,
                50.125637372868
              ],
              [
                8.6681927740574,
                50.125707015851
              ],
              [
                8.6683765053749,
                50.125763761911
              ],
              [
                8.6684314906597,
                50.125691539641
              ],
              [
                8.6688230931759,
                50.125829105775
              ],
              [
                8.6686930060387,
                50.125961512804
              ],
              [
                8.6688874661922,
                50.126038893366
              ],
              [
                8.6690349876881,
                50.125889290834
              ],
              [
                8.6690711975098,
                50.125852320021
              ],
              [
                8.6692562699318,
                50.125660587207
              ],
              [
                8.6690685153007,
                50.125582346242
              ],
              [
                8.6689116060734,
                50.125738828044
              ],
              [
                8.6685039103031,
                50.125591803948
              ],
              [
                8.6686179041862,
                50.125439620635
              ],
              [
                8.6684314906597,
                50.125383733986
              ],
              [
                8.6683201789856,
                50.125530758722
              ],
              [
                8.6678843200207,
                50.125420705161
              ],
              [
                8.6679527163506,
                50.125253904751
              ],
              [
                8.6677622795105,
                50.125222092235
              ],
              [
                8.6677542328835,
                50.125234989203
              ],
              [
                8.6677099764347,
                50.125231550012
              ],
              [
                8.6677341163158,
                50.125143850553
              ],
              [
                8.6674189567566,
                50.125101720364
              ],
              [
                8.6673907935619,
                50.125191139497
              ],
              [
                8.6672754585743,
                50.125179102316
              ],
              [
                8.6672808229923,
                50.125161906337
              ],
              [
                8.6670783162117,
                50.125139551556
              ],
              [
                8.667034059763,
                50.125310651347
              ],
              [
                8.6665807664394,
                50.125294315213
              ],
              [
                8.6665767431259,
                50.125107738964
              ],
              [
                8.6663635075092,
                50.125106019364
              ],
              [
                8.6663635075092,
                50.125293455416
              ],
              [
                8.6659182608128,
                50.125301193586
              ],
              [
                8.6658847332001,
                50.125124934962
              ],
              [
                8.6656500399113,
                50.125142130954
              ],
              [
                8.6657291650772,
                50.125584925616
              ]
            ]
          ],
          'type': 'Polygon'
        }
      },
      'type': 'building',
      'categories': [
        'education'
      ],
      'uid': 'a825451c-cbc4-544a-9d96-9de0b635fdbd',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCPointOfInterest',
}
,
{
  'errorNames': [],
  'instance': {
    'geo': {
      'point': {
        'type': 'Point',
        'coordinates': [
          13.32615,
          52.51345
        ]
      }
    },
    'type': 'room',
    'categories': [
      'cafe'
    ],
    'uid': 'b7206fb5-bd77-5572-928f-16aa70910f64',
    'alternateNames': [
      'MA Mathe Cafeteria'
    ],
    'name': 'Mathe Cafeteria',
    'address': {
      'addressCountry': 'Germany',
      'addressLocality': 'Berlin',
      'addressRegion': 'Berlin',
      'postalCode': '10623',
      'streetAddress': 'Straße des 17. Juni 136'
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCRoom',
}
,
{
  'errorNames': [],
  'instance': {
    'geo': {
      'point': {
        'type': 'Point',
        'coordinates': [
          13.3306966,
          52.5104675
        ]
      }
    },
    'type': 'room',
    'categories': [
      'library'
    ],
    'uid': '6e5abbff-d995-507b-982b-e0d094da6606',
    'alternateNames': [
      'BIB'
    ],
    'name': 'Universitätsbibliothek',
    'floor': '0',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCRoom',
}
,
{
  'errorNames': [],
  'instance': {
    'geo': {
      'point': {
        'type': 'Point',
        'coordinates': [
          13.3262843,
          52.5135435
        ]
      }
    },
    'description': 'loses Mobiliar im Foyer',
    'type': 'room',
    'categories': [
      'learn'
    ],
    'uid': 'd33fa478-7e5d-5197-9f0e-091f7f8105df',
    'name': 'MA Foyer',
    'inPlace': {
      'geo': {
        'point': {
          'type': 'Point',
          'coordinates': [
            13.32577,
            52.51398
          ]
        },
        'polygon': {
          'type': 'Polygon',
          'coordinates': [
            [
              [
                13.3259988,
                52.5141108
              ],
              [
                13.3259718,
                52.5143107
              ],
              [
                13.3262958,
                52.5143236
              ],
              [
                13.3263291,
                52.5143052
              ],
              [
                13.3263688,
                52.5140098
              ],
              [
                13.3264324,
                52.5139643
              ],
              [
                13.3264849,
                52.5139415
              ],
              [
                13.3265148,
                52.5139004
              ],
              [
                13.3265336,
                52.5138571
              ],
              [
                13.3265411,
                52.5137933
              ],
              [
                13.3265336,
                52.5137546
              ],
              [
                13.3264961,
                52.5137044
              ],
              [
                13.3264399,
                52.5136725
              ],
              [
                13.3263875,
                52.5136497
              ],
              [
                13.3263351,
                52.5136429
              ],
              [
                13.3263613,
                52.5134286
              ],
              [
                13.3262564,
                52.5133603
              ],
              [
                13.3260767,
                52.5133671
              ],
              [
                13.3259418,
                52.5134286
              ],
              [
                13.3258744,
                52.5135061
              ],
              [
                13.3258444,
                52.5135677
              ],
              [
                13.3261366,
                52.5135836
              ],
              [
                13.3261066,
                52.513807
              ],
              [
                13.3260579,
                52.5138047
              ],
              [
                13.3260317,
                52.5139096
              ],
              [
                13.3254137,
                52.5138708
              ],
              [
                13.3254287,
                52.5137819
              ],
              [
                13.3250879,
                52.513766
              ],
              [
                13.3250018,
                52.5142697
              ],
              [
                13.3253613,
                52.5142902
              ],
              [
                13.3253838,
                52.5140747
              ],
              [
                13.3259988,
                52.5141108
              ]
            ]
          ]
        }
      },
      'type': 'building',
      'categories': [
        'education'
      ],
      'name': 'Mathematikgebäude',
      'alternateNames': [
        'MA'
      ],
      'uid': 'edfaba58-254f-5da0-82d6-3b46a76c48ce',
      'address': {
        'addressCountry': 'Germany',
        'addressLocality': 'Berlin',
        'addressRegion': 'Berlin',
        'postalCode': '10623',
        'streetAddress': 'Straße des 17. Juni 136'
      },
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'floor': '0',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCRoom',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': 'ea9db087-240b-5b10-a65b-9684d88a3e4e',
    'name': 'Poolraum (HoF)',
    'type': 'room',
    'categories': [
      'computer',
      'learn'
    ],
    'description': '1. OG, Raum 1.29, 18 Plätze, rollstuhlgerecht, Montag bis Freitag von 8:00 bis 20:00 Uhr',
    'geo': {
      'point': {
        'type': 'Point',
        'coordinates': [
          8.6654716730118,
          50.127288142239
        ]
      }
    },
    'inPlace': {
      'name': 'HoF',
      'address': {
        'addressCountry': 'Deutschland',
        'addressLocality': 'Frankfurt am Main',
        'addressRegion': 'Hessen',
        'postalCode': '60323',
        'streetAddress': 'Theodor-W.-Adorno-Platz 3'
      },
      'geo': {
        'point': {
          'coordinates': [
            8.66521,
            50.12715
          ],
          'type': 'Point'
        },
        'polygon': {
          'coordinates': [
            [
              [
                8.6646019667387,
                50.127282983673
              ],
              [
                8.6655273288488,
                50.127413667164
              ],
              [
                8.6656533926725,
                50.127055146471
              ],
              [
                8.6647307127714,
                50.126922742467
              ],
              [
                8.6646019667387,
                50.127282983673
              ]
            ]
          ],
          'type': 'Polygon'
        }
      },
      'categories': [
        'education'
      ],
      'type': 'building',
      'uid': '583b4bd4-d7b7-5736-b2df-87e05c41d97a',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCRoom',
}
,
{
  'errorNames': [],
  'instance': {
    'query': '*',
    'filter': {
      'arguments': {
        'filters': [
          {
            'arguments': {
              'field': 'type',
              'value': 'dish'
            },
            'type': 'value'
          },
          {
            'arguments': {
              'field': 'categories',
              'value': 'main dish'
            },
            'type': 'value'
          },
          {
            'arguments': {
              'fromField': 'availabilityStarts',
              'toField': 'availabilityEnds',
              'time': '2018-01-15T04:13:00+00:00'
            },
            'type': 'availability'
          }
        ],
        'operation': 'and'
      },
      'type': 'boolean'
    }
  },
  'schema': 'SCSearchRequest',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '622b950b-a29c-593b-9059-aece622228a0',
    'type': 'semester',
    'name': 'Wintersemester 2017/2018',
    'acronym': 'WS 2017/18',
    'alternateNames': [
      'WiSe 2017/18'
    ],
    'startDate': '2017-10-01',
    'endDate': '2018-03-31',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCSemester',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '7cebbc3e-0a21-5371-ab0d-f7ba12f53dbd',
    'type': 'semester',
    'name': 'Sommersemester 2018',
    'acronym': 'SS 2018',
    'alternateNames': [
      'SoSe 2018'
    ],
    'startDate': '2018-04-01',
    'endDate': '2018-09-30',
    'eventsStartDate': '2018-04-09',
    'eventsEndDate': '2018-07-13',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCSemester',
}
,
{
  'errorNames': [],
  'instance': {
    'categories': [
      'privacy'
    ],
    'description': 'This is a Description',
    'input': {
      'defaultValue': 'student',
      'inputType': 'singleChoice',
      'values': [
        'student',
        'employee',
        'guest'
      ]
    },
    'name': 'group',
    'order': 0,
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    },
    'translations': {
      'de': {
        'description': 'Dies ist eine Beschreibung'
      }
    },
    'type': 'setting',
    'uid': 'c4ff2b08-be18-528e-9b09-cb8c1d18487b'
  },
  'schema': 'SCSetting',
}
,
{
  'errorNames': [],
  'instance': {
    'categories': [
      'privacy'
    ],
    'description': 'This is a Description',
    'input': {
      'defaultValue': [],
      'inputType': 'multipleChoice',
      'values': [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8
      ]
    },
    'name': 'numbers',
    'order': 1,
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    },
    'translations': {
      'de': {
        'description': 'Dies ist eine Beschreibung',
        'name': 'Nummern'
      },
      'en': {
        'description': 'This is a Description',
        'name': 'Numbers'
      }
    },
    'type': 'setting',
    'uid': '9f0c362e-0b41-532f-9e8b-a0ac373fbede'
  },
  'schema': 'SCSetting',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'ticket',
    'name': 'Ticket',
    'uid': '34fc6cd9-5bd1-5779-a75d-a25d01ad4dae',
    'currentTicketNumber': '250',
    'approxWaitingTime': 'PT43S',
    'inPlace': {
      'geo': {
        'point': {
          'type': 'Point',
          'coordinates': [
            13.3255622,
            52.5118668
          ]
        }
      },
      'type': 'room',
      'categories': [
        'office'
      ],
      'openingHours': 'Mo-Fr 09:30-12:30; Tu 13:00-16:00; We off',
      'uid': '7257a1d7-47ac-4acc-a8cc-3f9ac6442e5d',
      'alternateNames': [
        'H 0010'
      ],
      'name': 'Prüfungsamt - Team 2',
      'floor': '0',
      'origin': {
        'indexed': '2018-09-11T12:30:00Z',
        'name': 'Dummy',
        'type': 'remote'
      }
    },
    'serviceType': {
      'name': 'Prüfungsamt'
    },
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCTicket',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '4f1c1810-1a9f-52cb-bfe9-65dcab5e889a',
    'type': 'tour',
    'name': 'Stundenplan erstellen',
    'description': 'Veranstaltung suchen und zum Stundenplan hinzufügen',
    'steps': [
      {
        'type': 'location',
        'location': '#/b-tu/main'
      },
      {
        'type': 'tooltip',
        'element': 'ion-nav-bar div[nav-bar="active"] div.buttons-left',
        'text': 'Öffne das Menü.',
        'resolved': {
          'menu': 'open-left'
        }
      },
      {
        'type': 'tooltip',
        'element': '#stapps-main-menu-main-b-tu-search',
        'text': 'Öffne die Suche.',
        'resolved': {
          'location': {
            'is': '#/b-tu/search'
          }
        },
        'position': 'bottom'
      },
      {
        'type': 'tooltip',
        'element': 'ion-header-bar label',
        'text': "Such' nach einer Veranstaltung indem du beispielsweise den Titel einer Veranstaltung eingibst."
      },
      {
        'type': 'tooltip',
        'element': 'ion-nav-bar div[nav-bar="active"] div.buttons-right',
        'text': 'Öffne die Filter.',
        'resolved': {
          'menu': 'open-right'
        }
      },
      {
        'type': 'tooltip',
        'element': '.stapps-context-menu-selectable-lists',
        'text': 'Filtere um eine Veranstaltung zu finden...',
        'position': 'top'
      },
      {
        'type': 'menu',
        'side': 'right',
        'action': 'close'
      },
      {
        'type': 'tooltip',
        'element': '.stapps-search-results .stapps-data-list-remote-items',
        'text': 'Klicke auf eine Veranstaltung...',
        'resolved': {
          'location': {
            'match': '#/b-tu/data/detail/Event'
          }
        },
        'position': 'top'
      },
      {
        'type': 'tooltip',
        'element': 'ion-view[nav-view="active"] stapps-event-date-in-course stapps-event-date-toggle',
        'text': 'Füge die Veranstaltung zu deinem Stundenplan hinzu.'
      },
      {
        'type': 'tooltip',
        'element': 'ion-nav-bar div[nav-bar="active"] div.buttons-left',
        'text': 'Öffne das Menü erneut.',
        'resolved': {
          'menu': 'open-left'
        }
      },
      {
        'type': 'tooltip',
        'element': '#stapps-main-menu-personal-b-tu-events',
        'text': 'Öffne deinen Stundenplan.',
        'resolved': {
          'location': {
            'is': '#/b-tu/events'
          }
        },
        'position': 'right'
      },
      {
        'type': 'tooltip',
        'element': '.stapps-timetable-clocks',
        'text': 'Dies ist dein Stundenplan. Die Veranstaltung wird dir nun hier angezeigt.',
        'position': 'right'
      }
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCTour',
}
,
{
  'errorNames': [],
  'instance': {
    'uid': '665d834d-594a-5d72-be94-ff2892a6003c',
    'type': 'tour',
    'name': 'Favorisierte Essensorte',
    'description': 'Essensorte favorisieren, um ihre Speisepläne als Widget auf der Startseite zu sehen',
    'init': "injector.get('stappsHomeChosenWidgets').remove('stappsEatAndDrinkWidgets.dishesInFavoritedPlaces');",
    'steps': [
      {
        'type': 'location',
        'location': '#/b-tu/main'
      },
      {
        'type': 'tooltip',
        'element': 'ion-nav-bar div[nav-bar="active"] div.buttons-right .ion-ios-unlocked-outline',
        'text': 'Wechsle in den Bearbeitungsmodus.',
        'resolved': {
          'event': 'stappsMenu.toggleEditMode'
        },
        'canFail': true,
        'tries': 2
      },
      {
        'type': 'tooltip',
        'element': [
          '#stapps-home-add-widgets',
          '#stapps-home-personalize'
        ],
        'text': 'Öffne die Widget-Auswahl.',
        'resolved': {
          'element': 'ion-modal-view.active'
        }
      },
      {
        'type': 'tooltip',
        'element': '#stapps-home-widgets-add-stapps-eat-and-drink-widgets-dishes-in-favorited-places',
        'text': 'Füge das Widget "Gerichte in favorisierten Orten" zu deiner Startseite hinzu.',
        'resolved': {
          'event': 'stappsHomeChosenWidgets.addedWidget'
        }
      },
      {
        'type': 'tooltip',
        'element': 'ion-nav-bar div[nav-bar="active"] div.buttons-right .ion-ios-locked',
        'text': 'Speichere deine Änderungen.',
        'resolved': {
          'event': 'stappsMenu.toggleEditMode'
        },
        'canFail': true,
        'tries': 1
      },
      {
        'type': 'tooltip',
        'element': '#stapps-home-widgets-stapps-eat-and-drink-widgets-dishes-in-favorited-places',
        'text': 'Das ist das Widget, dass dir die Speisepläne deiner favorisierten Essensorte anzeigt. Klicke auf "Essensorte", um zur Übersicht der Essensorte zu gelangen.',
        'resolved': {
          'location': {
            'is': '#/b-tu/places?types=FoodEstablishment'
          }
        }
      },
      {
        'type': 'tooltip',
        'element': '#b-tu-places-types-food-establishment-place-m-a-mathe-cafeteria h2',
        'text': 'Wähle die "Mathe Cafeteria" aus, um sie zu favorisieren.',
        'resolved': {
          'location': {
            'is': '#/b-tu/map?place=MA%20Mathe%20Cafeteria'
          }
        }
      },
      {
        'type': 'tooltip',
        'element': 'ion-nav-bar div[nav-bar="active"] div.buttons-right stapps-favorite',
        'text': 'Favorisiere die Mathe-Cafeteria, über den Favoritenstern. Sobald du sie favorisiert hast, wird er gelb.',
        'resolved': {
          'event': 'stappsFavorites.addedFavorite'
        }
      },
      {
        'type': 'tooltip',
        'element': 'ion-nav-bar div[nav-bar="active"] div.buttons-left',
        'text': 'Öffne das Menü, um zur Startseite zurückzukehren.',
        'resolved': {
          'menu': 'open-left'
        }
      },
      {
        'type': 'tooltip',
        'element': 'ion-side-menu.menu-left header',
        'text': 'Klicke auf das TU-Logo oder "StApps", um zur Startseite zurückzukehren.',
        'resolved': {
          'location': {
            'is': '#/b-tu/main'
          }
        }
      },
      {
        'type': 'tooltip',
        'element': '#stapps-home-widgets-stapps-eat-and-drink-widgets-dishes-in-favorited-places',
        'text': 'Das Widget zeigt dir nun mindestens den Speiseplan der Mathe Cafeteria an.'
      },
      {
        'type': 'tooltip',
        'element': 'ion-nav-bar div[nav-bar="active"] div.buttons-left',
        'text': 'Öffne das Menü ein letztes Mal.',
        'resolved': {
          'menu': 'open-left'
        }
      },
      {
        'type': 'tooltip',
        'element': '#stapps-main-menu-personal-b-tu-favorites',
        'text': 'Öffne deine Favoriten.',
        'resolved': {
          'location': {
            'is': '#/b-tu/favorites'
          }
        }
      },
      {
        'type': 'tooltip',
        'element': '#b-tu-favorites-favorite-favorite-m-a-mathe-cafeteria',
        'text': 'Hier siehst du nun auch die Mathe Cafeteria in der Liste deiner Favoriten.'
      }
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCTour',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'video',
    'uid': 'e274cc82-f51c-566b-b8da-85763ff375e8',
    'url': 'https://vimeo.com/1084537',
    'name': 'Big Buck Bunny',
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCVideo',
}
,
{
  'errorNames': [],
  'instance': {
    'type': 'video',
    'uid': '2def52c8-f901-5b30-96fc-ba570a038508',
    'url': 'https://vimeo.com/1084537',
    'name': 'Big Buck Bunny',
    'sources': [
      {
        'url': 'https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4',
        'mimeType': 'video/mp4',
        'height': 240,
        'width': 320
      },
      {
        'url': 'https://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_1080p_stereo.ogg',
        'mimeType': 'video/ogg',
        'height': 1080,
        'width': 1920
      },
      {
        'url': 'https://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_480p_stereo.ogg',
        'mimeType': 'video/ogg',
        'height': 480,
        'width': 854
      }
    ],
    'duration': 'PT9M57S',
    'thumbnails': [
      'https://peach.blender.org/wp-content/uploads/bbb-splash.png?x11217'
    ],
    'actors': [
      {
        'type': 'person',
        'uid': '540862f3-ea30-5b8f-8678-56b4dc217642',
        'name': 'Big Buck Bunny',
        'givenName': 'Big Buck',
        'familyName': 'Bunny',
        'origin': {
          'indexed': '2018-09-11T12:30:00Z',
          'name': 'Dummy',
          'type': 'remote'
        }
      }
    ],
    'origin': {
      'indexed': '2018-09-11T12:30:00Z',
      'name': 'Dummy',
      'type': 'remote'
    }
  },
  'schema': 'SCVideo',
}];

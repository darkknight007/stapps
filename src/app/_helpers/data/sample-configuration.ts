/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCBackendAggregationConfiguration, SCThingType} from '@openstapps/core';

// provides sample aggregations to be used in tests or backendless development
export const sampleAggregations: SCBackendAggregationConfiguration[] = [
  {
    fieldName: 'categories',
    onlyOnTypes: [
      SCThingType.AcademicEvent,
      SCThingType.Article,
      SCThingType.Building,
      SCThingType.Catalog,
      SCThingType.Dish,
      SCThingType.PointOfInterest,
      SCThingType.Room,
    ],
  },
  {
    fieldName: 'inPlace.name',
    onlyOnTypes: [
      SCThingType.DateSeries,
      SCThingType.Dish,
      SCThingType.Floor,
      SCThingType.Organization,
      SCThingType.PointOfInterest,
      SCThingType.Room,
      SCThingType.Ticket,
    ],
  },
  {
    fieldName: 'academicTerms.acronym',
    onlyOnTypes: [
      SCThingType.AcademicEvent,
      SCThingType.SportCourse,
    ],
  },
  {
    fieldName: 'academicTerm.acronym',
    onlyOnTypes: [
      SCThingType.Catalog,
    ],
  },
  {
    fieldName: 'majors',
    onlyOnTypes: [
      SCThingType.AcademicEvent,
    ],
  },
  {
    fieldName: 'keywords',
    onlyOnTypes: [
      SCThingType.Article,
      SCThingType.Book,
      SCThingType.Message,
      SCThingType.Video,
    ],
  },
  {
    fieldName: 'type',
  },
];

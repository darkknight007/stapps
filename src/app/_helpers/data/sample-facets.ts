/*
 * Copyright (C) 2019, 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCFacet, SCThingType} from '@openstapps/core';

export const facetsMock: SCFacet[] = [
  {
    'buckets': [
      {
        'count': 60,
        'key': 'academic event',
      },
      {
        'count': 160,
        'key': 'message',
      },
      {
        'count': 151,
        'key': 'date series',
      },
      {
        'count': 106,
        'key': 'dish',
      },
      {
        'count': 20,
        'key': 'building',
      },
      {
        'count': 20,
        'key': 'semester',
      },
    ],
    'field': 'type',
  },
  {
    'buckets': [
      {
        'count': 12,
        'key': 'Max Mustermann',
      },
      {
        'count': 2,
        'key': 'Foo Bar',
      },
    ],
    'field': 'performers',
    'onlyOnType': SCThingType.AcademicEvent,
  },
  {
    'buckets': [
      {
        'count': 5,
        'key': 'colloquium',
      },
      {
        'count': 15,
        'key': 'course',
      },
    ],
    'field': 'categories',
    'onlyOnType': SCThingType.AcademicEvent,
  },
  {
    'buckets': [
      {
        'count': 5,
        'key': 'unipedia',
      }],
    'field': 'categories',
    'onlyOnType': SCThingType.Article,
  },
  {
    'buckets': [
      {
        'count': 5,
        'key': 'employees',
      },
      {
        'count': 15,
        'key': 'students',
      }],
    'field': 'audiences',
    'onlyOnType': SCThingType.Message,
  },
  {
    'buckets': [
      {
        'count': 5,
        'key': 'main dish',
      },
      {
        'count': 15,
        'key': 'salad',
      }],
    'field': 'categories',
    'onlyOnType': SCThingType.Dish,
  },
];

/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {
  SCAcademicEvent, SCArticle, SCBook, SCBuilding, SCCatalog,
  SCDateSeries, SCDish, SCFavorite, SCMessage, SCPerson, SCRoom, SCSearchFilter,
  SCThing, SCThingOriginType, SCThingType, SCToDo, SCToDoPriority,
} from '@openstapps/core';
import {Observable, of} from 'rxjs';
import {checkFilter} from '../fakesearch/filters';
import {sampleResources} from './resources/test-resources';

// tslint:disable:no-magic-numbers
const sampleMessages: SCMessage[] = [
  {
    audiences: ['students'],
    messageBody: 'Foo Message Text',
    name: 'Foo Message',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Message,
    uid: 'message-123',
  },
  {
    audiences: ['employees'],
    messageBody: 'Bar Message Text',
    name: 'Bar Message',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Message,
    uid: 'message-456',
  },
];

const sampleDishes: SCDish[] = [
  {
    categories: ['main dish', 'salad'],
    name: 'Foo Dish',
    // offers: [
    //   {
    //     'availability': 'in stock',
    //     'availabilityStarts': '2017-01-30T00:00:00.000Z',
    //     'availabilityEnds': '2017-01-30T23:59:59.999Z',
    //     'prices': {
    //       'default': 4.85,
    //       'student': 2.85,
    //       'employee': 3.85,
    //       'guest': 4.85,
    //     },
    // ],
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Dish,
    uid: 'dish-123',
  },
  {
    categories: ['side dish', 'salad'],
    name: 'Bar Dish',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Dish,
    uid: 'dish-456',
  },
];

const sampleBuildings: SCBuilding[] = [
  {
    categories: ['education'],
    geo: {
      point: {type: 'Point', coordinates: [12, 12]},
    },
    name: 'Foo Building',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Building,
    uid: 'building-123',
  },
];

const sampleRooms: SCRoom[] = [
  {
    categories: ['library'],
    geo: {
      point: {type: 'Point', coordinates: [12, 12]},
    },
    name: 'Foo Room',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Room,
    uid: 'room-123',
  },
];

const sampleArticles: SCArticle[] = [
  {
    articleBody: 'Foo Text',
    categories: ['unipedia'],
    name: 'Foo Article',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Article,
    uid: 'article-123',
  },
  {
    articleBody: 'Bar Text',
    categories: ['unipedia'],
    name: 'Bar Article',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Article,
    uid: 'article-456',
  },
];

const samplePersons: SCPerson[] = [
  {
    familyName: 'Person',
    givenName: 'Foo',
    name: 'Foo Person',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Person,
    uid: 'person-123',
  },
  {
    familyName: 'Person',
    givenName: 'Bar',
    name: 'Bar Person',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Person,
    uid: 'person-456',
  },
];

const sampleBooks: SCBook[] = [
  {
    authors: samplePersons,
    isbn: '123456',
    name: 'Foo Book',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Book,
    uid: 'book-123',
  },
  {
    authors: [],
    isbn: '123456',
    name: 'Bar Book',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Book,
    uid: 'book-234',
  },
];

const sampleCatalogs: SCCatalog[] = [
  {
    categories: ['university events'],
    level: 1,
    name: 'Foo Catalog',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Catalog,
    uid: 'catalog-123',
  },
  {
    categories: ['university events'],
    level: 1,
    name: 'Bar Catalog',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.Catalog,
    uid: 'catalog-456',
  },
];

const sampleTodos: SCToDo[] = [
  {
    categories: ['foo category'],
    done: false,
    name: 'Foo Todo',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    priority: SCToDoPriority.LOW,
    type: SCThingType.ToDo,
    uid: 'todo-123',
  },
  {
    categories: ['bar category'],
    done: true,
    name: 'Bar Todo',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    priority: SCToDoPriority.HIGH,
    type: SCThingType.ToDo,
    uid: 'todo-456',
  },
];

const sampleFavorites: SCFavorite[] = [
  {
    data: sampleBuildings[0],
    name: 'Foo Favorite',
    origin: {
      created: 'SOME-DATE',
      type: SCThingOriginType.User,
    },
    type: SCThingType.Favorite,
    uid: 'favorite-123',
  },
  {
    data: samplePersons[1],
    name: 'Bar Favorite',
    origin: {
      created: 'SOME-DATE',
      type: SCThingOriginType.User,
    },
    type: SCThingType.Favorite,
    uid: 'favorite-456',
  },
];

const sampleAcademicEvents: SCAcademicEvent[] = [
  {
    categories: ['course'],
    majors: ['Major One', 'Major Two'],
    name: 'Foo Academic Event',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    performers: samplePersons,
    type: SCThingType.AcademicEvent,
    uid: 'academic-event-123',
  },
  {
    categories: ['practicum'],
    majors: ['Major Two', 'Major Three'],
    name: 'Bar Academic Event',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    performers: samplePersons,
    type: SCThingType.AcademicEvent,
    uid: 'academic-event-456',
  },
];

const sampleDateSeries: SCDateSeries[] = [
  {
    dates: ['2019-03-01T17:00:00+00:00', '2019-03-08T17:00:00+00:00'],
    duration: 'PT2H',
    event: sampleAcademicEvents[0],
    frequency: 'once',
    name: 'Foo Date Event - Date Series',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.DateSeries,
    uid: 'date-series-123',
  },
  {
    dates: ['2019-03-03T10:00:00+00:00', '2019-03-11T10:00:00+00:00'],
    duration: 'PT2H',
    event: sampleAcademicEvents[1],
    frequency: 'weekly',
    name: 'Bar Date Event - Date Series',
    origin: {
      indexed: 'SOME-DATE',
      name: 'some name',
      type: SCThingOriginType.Remote,
    },
    type: SCThingType.DateSeries,
    uid: 'date-series-456',
  },
];

export const sampleThingsMap: { [key in SCThingType | string]: SCThing[] } = {
  'academic event': sampleAcademicEvents,
  article: sampleArticles,
  book: sampleBooks,
  building: sampleBuildings,
  catalog: sampleCatalogs,
  'course of studies': [],
  'date series': sampleDateSeries,
  diff: [],
  dish: sampleDishes,
  favorite: sampleFavorites,
  floor: [],
  message: sampleMessages,
  organization: [],
  person: samplePersons,
  'point of interest': [],
  room: sampleRooms,
  semester: [],
  setting: [],
  'sport course': [],
  ticket: [],
  todo: sampleTodos,
  tour: [],
  video: [],
};

/**
 * TODO
 */
@Injectable()
export class SampleThings {
  /**
   * TODO
   */
  http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  /**
   * TODO
   */
  // tslint:disable-next-line:prefer-function-over-method no-any
  getSampleThing(uid: string): Observable<any[]> {
    // tslint:disable-next-line:no-any
    const sampleThings: any[] = [];
    for (const resource of sampleResources) {
      if (resource.instance.uid as SCThingType === uid) {
        sampleThings.push(resource.instance);

        return of(sampleThings);
      }
    }

    return of(sampleThings);
  }

  /**
   * TODO
   */
  // tslint:disable-next-line:prefer-function-over-method no-any
  getSampleThings(filter?: SCSearchFilter): Observable<any[]> {
    // tslint:disable-next-line:no-any
    const sampleThings: any[] = [];
    for (const resource of sampleResources) {
      // tslint:disable-next-line:max-line-length
      // if ([SCThingType.Video].includes(resource.instance.type as SCThingType)) {
      if (typeof filter === 'undefined' || checkFilter(resource.instance as SCThing, filter)) {
        sampleThings.push(resource.instance);
      }
      // }
    }

    return of(sampleThings);
  }
}

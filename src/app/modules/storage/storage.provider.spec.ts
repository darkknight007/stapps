/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {TestBed} from '@angular/core/testing';
import {Storage} from '@ionic/storage';
import {StorageModule} from './storage.module';
import {StorageProvider} from './storage.provider';

describe('StorageProvider', () => {
  let storageProvider: StorageProvider;
  let storage: Storage;

  const sampleEntries: Map<string, any> = new Map([
    ['foo', 'Bar'],
    ['bar', {foo: 'BarFoo'} as any],
    ['foo.bar', 123],
  ]);

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [StorageModule],
      providers: [
        StorageProvider,
        // { provide: Storage, useClass: StorageMock }
      ],
    });
    storageProvider = TestBed.get(StorageProvider);
    storage = TestBed.get(Storage);
    await storage.clear();
  });

  it('should call ready method of storage on init', () => {
    spyOn(storage, 'ready');
    storageProvider.init();
    expect(storage.ready).toHaveBeenCalled();
  });

  it('should call set method of storage to put a value', () => {
    spyOn(storage, 'set');
    storageProvider.put('some-uid', {some: 'thing'});
    expect(storage.set).toHaveBeenCalledWith('some-uid', {some: 'thing'});
  });

  it('should call get method of storage to get a value', () => {
    spyOn(storage, 'get');
    storageProvider.get<any>('some-uid');
    expect(storage.get).toHaveBeenCalledWith('some-uid');
  });

  it('should properly put and get a value', async () => {
    await storageProvider.init();
    await storageProvider.put('some-uid', {some: 'thing'});
    const result: Map<string, object> = await storageProvider.get<Map<string, object>>('some-uid');
    expect(result).toEqual({some: 'thing'});
  });

  it('should throw an error when value is null', async () => {
    let error: Error = new Error();
    spyOn(storage, 'get').and.returnValue(Promise.resolve(null));
    try {
      await storageProvider.get('something-else');
    } catch (err) {
      error = err;
    }
    expect(error).toEqual(new Error('Value not found.'));
  });

  it('should put multiple values into the storage', async () => {
    spyOn(storageProvider, 'put');
    await storageProvider.putMultiple(sampleEntries);
    expect((storageProvider.put as jasmine.Spy).calls.count()).toEqual(3);
    expect(storageProvider.put).toHaveBeenCalledWith('foo', 'Bar');
    expect(storageProvider.put).toHaveBeenCalledWith('bar', {foo: 'BarFoo'});
  });

  it('should get multiple values from the storage', async () => {
    spyOn(storageProvider, 'get').and.callThrough();
    await storageProvider.putMultiple(sampleEntries);
    const entries = await storageProvider.getMultiple(['foo', 'bar']);
    expect((storageProvider.get as jasmine.Spy).calls.count()).toEqual(2);
    expect(storageProvider.get).toHaveBeenCalledWith('foo');
    expect(storageProvider.get).toHaveBeenCalledWith('bar');
    expect(Array.from(entries.values())).toEqual(['Bar', {foo: 'BarFoo'}]);
    expect(Array.from(entries.keys())).toEqual(['foo', 'bar']);
  });

  it('should get all values from the storage', async () => {
    spyOn(storageProvider, 'get').and.callThrough();
    await storageProvider.putMultiple(sampleEntries);
    const entries = await storageProvider.getAll();
    expect(Array.from(entries.values()).map((item) => (item.foo) ? item.foo : item).sort())
      .toEqual(Array.from(sampleEntries.values()).map((item) => (item.foo) ? item.foo : item).sort());
    expect(Array.from(entries.keys()).sort()).toEqual(['bar', 'foo', 'foo.bar']);
  });

  it('should delete one or more entries from the storage', async () => {
    spyOn(storage, 'remove').and.callThrough();
    await storageProvider.putMultiple(sampleEntries);
    let entries = await storageProvider.getAll();
    expect(Array.from(entries.values()).length).toBe(3);
    await storageProvider.delete('bar');

    expect(storage.remove).toHaveBeenCalled();
    entries = await storageProvider.getAll();
    expect(Array.from(entries.values())).toEqual(['Bar', 123]);

    await storageProvider.delete('foo', 'foo.bar');
    expect(await storageProvider.length()).toBe(0);
  });

  it('should delete all entries in the storage', async () => {
    spyOn(storage, 'clear').and.callThrough();
    await storageProvider.putMultiple(sampleEntries);
    let entries = await storageProvider.getAll();
    expect(Array.from(entries.values()).length).toBe(3);
    await storageProvider.deleteAll();

    entries = await storageProvider.getAll();
    expect(storage.clear).toHaveBeenCalled();
    expect(Array.from(entries.values()).length).toBe(0);
  });

  it('should provide number of entries', async () => {
    spyOn(storage, 'length').and.callThrough();

    expect(await storageProvider.length()).toBe(0);
    expect(storage.length).toHaveBeenCalled();

    await storageProvider.putMultiple(sampleEntries);
    expect(await storageProvider.length()).toBe(3);
  });

  it('should provide information if storage is empty', async () => {
    spyOn(storage, 'length').and.callThrough();

    expect(await storageProvider.isEmpty()).toBeTruthy();
    expect(storage.length).toHaveBeenCalled();

    await storageProvider.putMultiple(sampleEntries);
    expect(await storageProvider.isEmpty()).toBeFalsy();
  });

  it('should provide information if storage contains a specific entry (key)', async () => {
    spyOn(storage, 'keys').and.returnValue(Promise.resolve(Array.from(sampleEntries.keys())));
    expect(await storageProvider.has('foo')).toBeTruthy();
    expect(await storageProvider.has('something-else')).toBeFalsy();
  });

  it('should allow search by regex', async () => {
    await storageProvider.putMultiple(sampleEntries);
    const found: Map<string, any> = await storageProvider.search<any>(/bar/);
    expect(Array.from(found.keys()).sort()).toEqual(['bar', 'foo.bar']);
    expect(Array.from(found.values())).toEqual([{foo: 'BarFoo'}, 123]);
  });

  it('should allow search by string', async () => {
    await storageProvider.putMultiple(sampleEntries);
    const found: Map<string, any> = await storageProvider.search<any>('foo.ba');
    expect(Array.from(found.keys())).toEqual(['foo.bar']);
    expect(Array.from(found.values())).toEqual([123]);
  });
});

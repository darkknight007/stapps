/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {AppError} from './../../_helpers/errors';

/**
 * Error that is thrown when fetching from backend fails
 */
export class ConfigFetchError extends AppError {
  constructor() {
    super('ConfigFetchError', 'App configuration could not be fetched!');
  }
}

/**
 * Error that is thrown when the ConfigProvider could be initialised
 */
export class ConfigInitError extends AppError {
  constructor() {
    super('ConfigInitError', 'App configuration could not be initialised!');
  }
}

/**
 * Error that is thrown when the requested config value is not available
 */
export class ConfigValueNotAvailable extends AppError {
  constructor(valueKey: string) {
    super('ConfigValueNotAvailable', `No attribute "${valueKey}" in config available!`);
  }
}

/**
 * Error that is thrown when no saved config is available
 */
export class SavedConfigNotAvailable extends AppError {
  constructor() {
    super('SavedConfigNotAvailable', 'No saved app configuration available.');
  }
}

/**
 * Error that is thrown when the SCVersion of the saved config is not compatible with the app
 */
export class WrongConfigVersionInStorage extends AppError {
  constructor(correctVersion: string, savedVersion: string) {
    super('WrongConfigVersionInStorage', `The saved configs backend version ${savedVersion} ` +
      `does not equal the configured backend version ${correctVersion} of the app.`);
  }
}

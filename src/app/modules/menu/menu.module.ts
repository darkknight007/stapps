/*
 * Copyright (C) 2018, 2019, 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {SettingsModule} from '../settings/settings.module';
import {ContextMenuComponent} from './context/context-menu.component';
import {ContextMenuService} from './context/context-menu.service';
import {NavigationComponent} from './navigation/navigation.component';

/**
 * Menu module
 */
@NgModule({
  declarations: [
    NavigationComponent,
    ContextMenuComponent,
  ],
  exports: [
    NavigationComponent,
    ContextMenuComponent,
  ],
  imports: [
    FormsModule,
    IonicModule.forRoot(),
    TranslateModule.forChild(),
    CommonModule,
    RouterModule,
    SettingsModule,
  ],
  providers: [
    ContextMenuService,
  ],
})
export class MenuModule {}

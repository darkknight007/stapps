/*
 * Copyright (C) 2018-2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {SCAppConfigurationMenuCategory, SCLanguage, SCThingTranslator, SCTranslations} from '@openstapps/core';
import {NGXLogger} from 'ngx-logger';
import {ConfigProvider} from '../../config/config.provider';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'stapps-navigation',
  styleUrls: ['navigation.scss'],
  templateUrl: 'navigation.html',
})
export class NavigationComponent {

  /**
   * Possible languages to be used for translation
   */
  language: keyof SCTranslations<SCLanguage> = 'en';

  /**
   * Menu entries from config module
   */
  menu: SCAppConfigurationMenuCategory[];

  /**
   * Core translator
   */
  translator: SCThingTranslator;

  constructor(private readonly configProvider: ConfigProvider,
              public translateService: TranslateService,
              private readonly logger: NGXLogger) {
    this.loadMenuEntries();
    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.language = event.lang as keyof SCTranslations<SCLanguage>;
      this.translator = new SCThingTranslator(this.language);
    });
    this.translator = new SCThingTranslator('en');
  }

  /**
   * Loads menu entris from configProvider
   */
  async loadMenuEntries() {
    try {
      this.menu = await this.configProvider.getValue('menus') as SCAppConfigurationMenuCategory[];
    } catch (error) {
      this.logger.error(`error from loading menu entries: ${error}`);
    }

  }

  // openPage(page) {
  //   this.nav.setRoot(page.component);
  // }
}

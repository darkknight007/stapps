/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCFacet, SCFacetBucket} from '@openstapps/core';

export type ContextType = FilterContext | SortContext;

/**
 * A sort context
 */
export interface SortContext {
  /**
   * Name of the context
   */
  name: 'sort';

  /**
   * Reverse option
   */
  reversed: boolean;

  /**
   * sort value
   */
  value: string;

  /**
   * Sort options
   */
  values: SortContextOption[];
}

/**
 * A sort context option
 */
export interface SortContextOption {
  /**
   * sort option is reversible
   */
  reversible: boolean;

  /**
   * sort option value
   */
  value: string;
}

/**
 * A filter context
 */
export interface FilterContext {
  /**
   * Compact view of the filter options
   */
  compact?: boolean;
  /**
   * Name of the context
   */
  name: 'filter';

  /**
   * Filter values
   */
  options: FilterFacet[];
}

export interface FilterFacet extends SCFacet {
  /**
   * FilterBuckets of a FilterFacet
   */
  buckets: FilterBucket[];
  /**
   * Compact view of the option buckets
   */
  compact?: boolean;
}

export interface FilterBucket extends SCFacetBucket {
  /**
   * Sets the Filter active
   */
  checked: boolean;
}

/*
 * Copyright (C) 2018, 2019, 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {
  SCLanguage,
  SCThingTranslator,
  SCThingType,
  SCTranslations,
} from '@openstapps/core';
import {ContextMenuService} from './context-menu.service';
import {FilterContext, SortContext, SortContextOption} from './context-type';
import {Subscription} from 'rxjs';

/**
 * The context menu
 *
 * It can be configured with sorting types and filtering on facets
 *
 * Example:<br>
 *  `<stapps-context (optionChange)="onOptionChange($event)" (settingChange)="onSettingChange($event)"
 *  [sortOption]="SortContext" [filterOption]="FilterContext"></stapps-context>`
 */
@Component({
  selector: 'stapps-context',
  templateUrl: 'context-menu.html',
})
export class ContextMenuComponent {

  /**
   * Amount of filter options shown on compact view
   */
  compactFilterOptionCount = 5;

  /**
   * Container for the filter context
   */
  filterOption: FilterContext;

  /**
   * Possible languages to be used for translation
   */
  language: keyof SCTranslations<SCLanguage>;

  /**
   * Mapping of SCThingType
   */
  scThingType = SCThingType;

  /**
   * Container for the sort context
   */
  sortOption: SortContext;

  /**
   * Core translator
   */
  translator: SCThingTranslator;

  /**
   * Array of all Subscriptions
   */
  subscriptions: Subscription[] = [];


  constructor(private translateService: TranslateService,
              private readonly contextMenuService: ContextMenuService) {
    this.language = this.translateService.currentLang as keyof SCTranslations<SCLanguage>;
    this.translator = new SCThingTranslator(this.language);

    this.subscriptions.push(this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.language = event.lang as keyof SCTranslations<SCLanguage>;
      this.translator = new SCThingTranslator(this.language);
    }));

    this.subscriptions.push(this.contextMenuService.filterContextChanged$.subscribe((filterContext) => {
      this.filterOption = filterContext;
    }));

    this.subscriptions.push(this.contextMenuService.sortOptions.subscribe((sortContext) => {
      this.sortOption = sortContext;
    }));
  }

  /**
   * Unsubscribe from Observables
   */
  ngOnDestroy() {
    for (let sub of this.subscriptions) {
      sub.unsubscribe();
    }
  }

  /**
   * Sets selected filter options and updates listener
   */
  filterChanged = () => {
    this.contextMenuService.contextFilterChanged(this.filterOption);
  }

  /**
   * Returns translated property name
   */
  getTranslatedPropertyName(property: string, onlyForType?: SCThingType): string {
    return (this.translator
      // tslint:disable-next-line:no-any
      .translatedPropertyNames(onlyForType ? onlyForType : SCThingType.AcademicEvent) as any)[property];
  }

  /**
   * Returns translated type of given SCThingType string
   */
  getTranslatedType(scThingType: string) {
    return this.translator.translatedThingType(scThingType as SCThingType);
  }

  /**
   * Resets filter options
   */
  resetFilter = (option: FilterContext) => {
    option.options.forEach((filterFacet) => filterFacet.buckets.forEach((filterBucket) => {
      filterBucket.checked = false;
    }));
    this.contextMenuService.contextFilterChanged(this.filterOption);
  }

  /**
   * Updates selected sort option and updates listener
   */
  sortChanged = (option: SortContext, value: SortContextOption) => {
    if (option.value === value.value) {
      if (value.reversible) {
        option.reversed = !option.reversed;
      }
    } else {
      option.value = value.value;
      if (value.reversible) {
        option.reversed = false;
      }
    }
    this.contextMenuService.contextSortChanged(option);
  }
}

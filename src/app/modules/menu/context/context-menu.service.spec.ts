import {TestBed} from '@angular/core/testing';

import {ContextMenuService} from './context-menu.service';
import {SCFacet} from '@openstapps/core';
import {FilterContext, SortContext} from './context-type';

describe('ContextMenuService', () => {
  let service: ContextMenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContextMenuService]
    });
    service = TestBed.get(ContextMenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update filterOptions', (done) => {
    service.filterContextChanged$.subscribe((result) => {
      expect(result).toBeDefined();
      done();
    });
    service.updateContextFilter(facetsMock);
  });

  it('should update filterQuery', (done) => {
    service.filterContextChanged$.subscribe(() => {
      service.contextFilterChanged(filterContext);
    });
    service.filterQueryChanged$.subscribe((result) => {
      expect(result).toBeDefined();
      done();
    });
    service.updateContextFilter(facetsMock);
  });

  it('should update sortOptions', (done) => {
    service.sortContextChanged$.subscribe((result) => {
      expect(result).toBeDefined();
      done();
    });
    service.setContextSort(sortContext);
  });

  it('should update sortQuery', (done) => {
    service.sortContextChanged$.subscribe(() => {
      service.contextSortChanged(sortContext);
    });
    service.sortQueryChanged$.subscribe((result) => {
      expect(result).toBeDefined();
      done();
    });
    service.setContextSort(sortContext);
  });
});


const facetsMock: SCFacet[] =
  [{
    'buckets': [
      {
        'count': 60,
        'key': 'academic event',
      },
      {
        'count': 160,
        'key': 'message',
      },
      {
        'count': 151,
        'key': 'date series',
      },
      {
        'count': 106,
        'key': 'dish',
      },
      {
        'count': 20,
        'key': 'building',
      },
      {
        'count': 20,
        'key': 'semester',
      },
    ],
    'field': 'type',
  }];

const filterContext: FilterContext = {
  name: 'filter',
  options: [
    {
      buckets: [
        {
          checked: true,
          count: 60,
          key: 'academic event'
        }, {
          checked: false,
          count: 160,
          key: 'message',
        },
        {
          checked: false,
          count: 151,
          key: 'date series'
        }, {
          checked: false,
          count: 106,
          key: 'dish'
        },
        {
          checked: false,
          count: 20,
          key: 'building'
        },
        {
          checked: false,
          count: 20,
          key: 'semester'
        }
      ],
      field: 'type'
    }]
}

const sortContext: SortContext = {
  name: 'sort',
  reversed: false,
  value: 'name',
  values: [
    {
      reversible: false,
      value: 'relevance',
    },
    {
      reversible: true,
      value: 'name',
    },
    {
      reversible: true,
      value: 'type',
    },
  ],
}

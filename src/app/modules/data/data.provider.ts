/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Injectable} from '@angular/core';
import {Client} from '@openstapps/api/lib/client';
import {SCSearchQuery, SCSearchResponse, SCThingOriginType, SCThings, SCThingType} from '@openstapps/core';
import {SCSaveableThing} from '@openstapps/core';
import {environment} from '../../../environments/environment';
import {StorageProvider} from '../storage/storage.provider';
import {StAppsWebHttpClient} from './stapps-web-http-client.provider';

export enum DataScope {
  Local = 'local',
  Remote = 'remote',
}

/**
 *  Generated class for the DataProvider provider.
 *
 * See https://angular.io/guide/dependency-injection for more info on providers
 * and Angular DI.
 */
@Injectable()
export class DataProvider {
  /**
   * TODO
   */
  private _storagePrefix = 'stapps.data';
  /**
   * Version of the app (used for the header in communication with the backend)
   */
  appVersion = environment.backend_version;
  /**
   * TODO
   */
  backendUrl = environment.backend_url;
  /**
   * TODO
   */
  client: Client;
  /**
   * TODO
   */
  storageProvider: StorageProvider;

  /**
   * TODO
   *
   * @param stAppsWebHttpClient TODO
   * @param storageProvider TODO
   */
  constructor(stAppsWebHttpClient: StAppsWebHttpClient, storageProvider: StorageProvider) {
    this.client = new Client(stAppsWebHttpClient, this.backendUrl, this.appVersion);
    this.storageProvider = storageProvider;
  }

  /**
   * Delete a data item
   *
   * @param uid Unique identifier of the saved data item
   */
  async delete(uid: string): Promise<void> {
    return this.storageProvider.delete(this.getDataKey(uid));
  }

  /**
   * Delete all the previously saved data items
   */
  async deleteAll(): Promise<void> {
    const keys = Array.from((await this.getAll()).keys());

    return this.storageProvider.delete(...keys);
  }

  /**
   * Provides a saveable thing from the local database using the provided UID
   */
  async get(uid: string, scope: DataScope.Local): Promise<SCSaveableThing<SCThings>>;
  /**
   * Provides a thing from the backend
   */
  async get(uid: string, scope: DataScope.Remote): Promise<SCThings | SCSaveableThing<SCThings>>;
  /**
   * Provides a thing from both local database and backend
   */
  async get(uid: string): Promise<Map<DataScope, SCThings | SCSaveableThing<SCThings>>>;

  /**
   * Provides a thing from the local database only, backend only or both, depending on the scope
   *
   * @param uid Unique identifier of a thing
   * @param scope From where data should be provided
   */
  async get(uid: string, scope?: DataScope):
    Promise<SCThings | SCSaveableThing<SCThings> | Map<DataScope, SCThings | SCSaveableThing<SCThings>>> {
      if (scope === DataScope.Local) {
        return this.storageProvider.get<SCSaveableThing<SCThings>>(this.getDataKey(uid));
      }
      if (scope === DataScope.Remote) {
        return this.client.getThing(uid);
      }
      const map: Map<DataScope, SCThings | SCSaveableThing<SCThings>> = new Map();
      map.set(DataScope.Local, await this.get(uid, DataScope.Local));
      map.set(DataScope.Remote, await this.get(uid, DataScope.Remote));

      return map;
    }

  /**
   * Provides all things saved in the local database
   */
  async getAll(): Promise<Map<string, SCSaveableThing<SCThings>>> {
      return this.storageProvider.search<SCSaveableThing<SCThings>>(this.storagePrefix);
  }

  /**
   * Provides key for storing data into the local database
   *
   * @param uid Unique identifier of a resource
   */
  getDataKey(uid: string): string {
    return `${this.storagePrefix}.${uid}`;
  }

  /**
   * Provides information if something with an UID is saved as a data item
   *
   * @param uid Unique identifier of the saved data item
   */
  async isSaved(uid: string): Promise<boolean> {
    return this.storageProvider.has(this.getDataKey(uid));
  }

  /**
   * Save a data item
   *
   * @param item Data item that needs to be saved
   * @param [type] Saveable type (e.g. 'favorite'); if nothing is provided then type of the thing is used
   */
  async put(item: SCThings, type?: SCThingType): Promise<SCSaveableThing<SCThings>> {
    const saveableItem: SCSaveableThing<SCThings> = {
      data: item,
      name: item.name,
      origin: {
        created: new Date().toISOString(),
        type: SCThingOriginType.User,
      },
      type: (typeof type === 'undefined') ? item.type : type,
      uid: item.uid,
    };

    // @TODO: Implementation for saving item into the backend (user's account)
    return ( this.storageProvider.put<SCSaveableThing<SCThings>>(this.getDataKey(item.uid), saveableItem));
  }

  /**
   * Searches the backend using the provided query and returns response
   *
   * @param query - query to send to the backend
   */
  async search(query: SCSearchQuery): Promise<SCSearchResponse> {
    return (this.client.search(query));
  }

  /**
   * TODO
   */
  get storagePrefix(): string {
    return this._storagePrefix;
  }

  /**
   * TODO
   */
  set storagePrefix(storagePrefix) {
    this._storagePrefix = storagePrefix;
  }
}

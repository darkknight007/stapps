/*
 * Copyright (C) 2018, 2019, 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {Network} from '@ionic-native/network/ngx';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {MarkdownModule} from 'ngx-markdown';
import {MomentModule} from 'ngx-moment';
import {MenuModule} from '../menu/menu.module';
import {StorageModule} from '../storage/storage.module';
import {DataFacetsProvider} from './data-facets.provider';
import {DataRoutingModule} from './data-routing.module';
import {DataProvider} from './data.provider';
import {DataDetailContentComponent} from './detail/data-detail-content.component';
import {DataDetailComponent} from './detail/data-detail.component';
import {AddressDetailComponent} from './elements/address-detail.component';
import {LongInlineText} from './elements/long-inline-text.component';
import {OffersDetailComponent} from './elements/offers-detail.component';
import {OffersInListComponent} from './elements/offers-in-list.component';
import {OriginDetailComponent} from './elements/origin-detail.component';
import {OriginInListComponent} from './elements/origin-in-list.component';
import {SimpleCardComponent} from './elements/simple-card.component';
import {SkeletonListItem} from './elements/skeleton-list-item.component';
import {SkeletonSegment} from './elements/skeleton-segment-button.component';
import {SkeletonSimpleCard} from './elements/skeleton-simple-card.component';
import {DataListItem} from './list/data-list-item.component';
import {DataListComponent} from './list/data-list.component';
import {FoodDataListComponent} from './list/food-data-list.component';
import {StAppsWebHttpClient} from './stapps-web-http-client.provider';
import {ArticleDetailContentComponent} from './types/article/article-detail-content.component';
import {ArticleListItem} from './types/article/article-list-item.component';
import {CatalogDetailContentComponent} from './types/catalog/catalog-detail-content.component';
import {CatalogListItem} from './types/catalog/catalog-list-item.component';
import {DateSeriesDetailContentComponent} from './types/date-series/date-series-detail-content.component';
import {DateSeriesListItem} from './types/date-series/date-series-list-item.component';
import {DishDetailContentComponent} from './types/dish/dish-detail-content.component';
import {DishListItem} from './types/dish/dish-list-item.component';
import {EventDetailContentComponent} from './types/event/event-detail-content.component';
import {EventListItemComponent} from './types/event/event-list-item.component';
import {FavoriteDetailContentComponent} from './types/favorite/favorite-detail-content.component';
import {FavoriteListItem} from './types/favorite/favorite-list-item.component';
import {MessageDetailContentComponent} from './types/message/message-detail-content.component';
import {MessageListItem} from './types/message/message-list-item.component';
import {OrganizationDetailContentComponent} from './types/organization/organization-detail-content.component';
import {OrganizationListItem} from './types/organization/organization-list-item.component';
import {PersonDetailContentComponent} from './types/person/person-detail-content.component';
import {PersonListItem} from './types/person/person-list-item.component';
import {PlaceDetailContentComponent} from './types/place/place-detail-content.component';
import {PlaceListItem} from './types/place/place-list-item.component';
import {PlaceMensaDetailComponent} from './types/place/special/mensa/place-mensa-detail.component';
import {SemesterDetailContentComponent} from './types/semester/semester-detail-content.component';
import {SemesterListItem} from './types/semester/semester-list-item.component';
import {VideoDetailContentComponent} from './types/video/video-detail-content.component';
import {VideoListItem} from './types/video/video-list-item.component';

/**
 * TODO
 */
@NgModule({
  declarations: [
    OffersDetailComponent,
    OffersInListComponent,
    AddressDetailComponent,
    ArticleDetailContentComponent,
    ArticleListItem,
    SimpleCardComponent,
    SkeletonSimpleCard,
    CatalogDetailContentComponent,
    CatalogListItem,
    DataDetailComponent,
    DataDetailContentComponent,
    FoodDataListComponent,
    DataListComponent,
    DataListItem,
    DateSeriesDetailContentComponent,
    DateSeriesListItem,
    DishDetailContentComponent,
    DishListItem,
    EventDetailContentComponent,
    EventListItemComponent,
    FavoriteDetailContentComponent,
    FavoriteListItem,
    LongInlineText,
    MessageDetailContentComponent,
    MessageListItem,
    OrganizationDetailContentComponent,
    OrganizationListItem,
    OriginDetailComponent,
    OriginInListComponent,
    PersonDetailContentComponent,
    PersonListItem,
    PlaceDetailContentComponent,
    PlaceListItem,
    PlaceMensaDetailComponent,
    SemesterDetailContentComponent,
    SemesterListItem,
    SkeletonListItem,
    SkeletonSegment,
    VideoDetailContentComponent,
    VideoListItem,
  ],
  entryComponents: [
    DataListComponent,
  ],
  imports: [
    IonicModule.forRoot(),
    CommonModule,
    FormsModule,
    DataRoutingModule,
    HttpClientModule,
    MarkdownModule.forRoot(),
    MenuModule,
    MomentModule.forRoot({
      relativeTimeThresholdOptions: {
        'm': 59,
      },
    }),
    StorageModule,
    TranslateModule.forChild(),
  ],
  providers: [
    DataProvider,
    DataFacetsProvider,
    Network,
    StAppsWebHttpClient,
  ],
})
export class DataModule {
}

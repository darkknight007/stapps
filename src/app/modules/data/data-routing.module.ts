/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DataDetailComponent} from './detail/data-detail.component';
import {DataListComponent} from './list/data-list.component';
import {FoodDataListComponent} from './list/food-data-list.component';

const dataRoutes: Routes = [
  {path: 'search', component: DataListComponent},
  {path: 'data-detail/:uid', component: DataDetailComponent},
  {path: 'canteen', component: FoodDataListComponent},
];

/**
 * TODO
 */
@NgModule({
  exports: [
    RouterModule,
  ],
  imports: [
    RouterModule.forChild(dataRoutes),
  ],
})
export class DataRoutingModule {}

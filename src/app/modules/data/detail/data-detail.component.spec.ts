/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ActivatedRoute, RouterModule} from '@angular/router';
import {IonRefresher, IonTitle} from '@ionic/angular';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {sampleThingsMap} from '../../../_helpers/data/sample-things';
import {DataRoutingModule} from '../data-routing.module';
import {DataModule} from '../data.module';
import {DataProvider} from '../data.provider';
import {DataDetailComponent} from './data-detail.component';
import {By} from '@angular/platform-browser';
import {Observable, of} from 'rxjs';

const translations: any = {data: {detail: {TITLE: 'Foo'}}};

class TranslateFakeLoader implements TranslateLoader {
  getTranslation(_lang: string): Observable<any> {
    return of(translations);
  }
}

describe('DataDetailComponent', () => {
  let comp: DataDetailComponent;
  let fixture: ComponentFixture<DataDetailComponent>;
  let detailPage: DebugElement;
  let dataProvider: DataProvider;
  let refresher: IonRefresher;
  const sampleThing = sampleThingsMap.message[0];
  let translateService: TranslateService;

  // @Component({ selector: 'stapps-data-list-item', template: '' })
  // class DataListItemComponent {
  //   @Input() item;
  // }

  const fakeActivatedRoute = {
    snapshot: {
      paramMap: {
        get: () => {
          return sampleThing.uid;
        },
      },
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([]), DataRoutingModule, DataModule,
      TranslateModule.forRoot({
        loader: {provide: TranslateLoader, useClass: TranslateFakeLoader},
      })],
      providers: [{provide: ActivatedRoute, useValue: fakeActivatedRoute}],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(async () => {
    dataProvider = TestBed.get(DataProvider);
    translateService = TestBed.get(TranslateService);
    refresher = jasmine.createSpyObj('refresher', ['complete']);
    spyOn(dataProvider, 'get' as any).and.returnValue(Promise.resolve(sampleThing));
    spyOn(DataDetailComponent.prototype, 'getItem').and.callThrough();
    fixture = await TestBed.createComponent(DataDetailComponent);
    comp = fixture.componentInstance;
    detailPage = fixture.debugElement;
    translateService.use('foo');
    fixture.detectChanges();
    await dataProvider.deleteAll();
  });

  it('should create component', () =>
    expect(comp).toBeDefined(),
  );

  it('should have apropriate title', async () => {
    const title: DebugElement | null = detailPage.query(By.directive(IonTitle));
    expect(title).not.toBe(null);
    expect(title!.nativeElement.textContent).toBe('Foo');
  });

  it('should get a data item', () => {
    comp.getItem(sampleThing.uid);
    expect(DataDetailComponent.prototype.getItem).toHaveBeenCalledWith(sampleThing.uid);
  });

  it('should get a data item when component is accessed', async () => {
    expect(DataDetailComponent.prototype.getItem).toHaveBeenCalledWith(sampleThing.uid);
  });

  it('should update the data item when refresh is called', async () => {
    await comp.refresh(refresher);
    expect(DataDetailComponent.prototype.getItem).toHaveBeenCalledWith(sampleThing.uid);
    expect(refresher.complete).toHaveBeenCalled();
  });
});

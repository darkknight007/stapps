/*
 * Copyright (C) 2018-2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {
  SCSearchQuery, SCThing,
} from '@openstapps/core';
import {DataListComponent} from './data-list.component';

/**
 * TODO
 */
@Component({
  selector: 'stapps-data-list',
  templateUrl: 'data-list.html',
})
export class FoodDataListComponent extends DataListComponent {
  /**
   * Fetches items with set query configuration
   *
   * @param append If true fetched data gets appended to existing, override otherwise (default false)
   */
  protected async fetchAndUpdateItems(append = false): Promise<void> {
    try {
      // build query search options
      const searchOptions: SCSearchQuery = {
        from: this.from,
        size: this.pageSize,
      };

      if (this.queryText && this.queryText.length > 0) {
        // add query string
        searchOptions.query = this.queryText;
      }

      if (this.sortQuery) {
        // add query sorting
        searchOptions.sort = [this.sortQuery];
      }

      searchOptions.filter = {
          arguments: {
            filters: [
              {
                arguments: {
                  field: 'categories',
                  value: 'canteen',
                },
                type: 'value',
              },
              {
              arguments: {
                field: 'categories',
                value: 'student canteen',
              },
              type: 'value',
              },
              {
                arguments: {
                  field: 'categories',
                  value: 'cafe',
                },
                type: 'value',
              },
              {
                arguments: {
                  field: 'categories',
                  value: 'restaurant',
                },
                type: 'value',
              },
            ],
            operation: 'or',
          },
          type: 'boolean',
      };

      if (this.filterQuery !== undefined) {
       searchOptions.filter = {
          arguments: {
            filters: [
              searchOptions.filter,
              this.filterQuery,
           ],
            operation: 'and',
          },
         type: 'boolean',
      };
    }

      const result = await this.dataProvider.search(searchOptions);

      this.items = (async () => {
        this.updateContextFilter(result.facets);
        let items: SCThing[];
        if(append) {
          items = (await this.items).concat(result.data);
        } else {
          items = result.data;
        }

        return items;
      })();
    } catch (err) {
      const alert: HTMLIonAlertElement = await this.alertController.create({
        buttons: ['Dismiss'],
        header: 'Error',
        subHeader: err.message,
      });

      await alert.present();
    }
  }

}

/*
 * Copyright (C) 2018-2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, OnInit} from '@angular/core';
import {AlertController} from '@ionic/angular';
import {
  SCFacet,
  SCSearchFilter,
  SCSearchQuery,
  SCSearchSort,
  SCThing,
} from '@openstapps/core';
import {NGXLogger} from 'ngx-logger';
import {Subject, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {ContextMenuService} from '../../menu/context/context-menu.service';
import {SettingsProvider} from '../../settings/settings.provider';
import {DataProvider} from '../data.provider';

/**
 * DataListComponent queries things and shows list of things and context menu
 */
@Component({
  selector: 'stapps-data-list',
  templateUrl: 'data-list.html',
  providers: [ContextMenuService],
})
export class DataListComponent implements OnInit {

  /**
   * Api query filter
   */
  filterQuery: SCSearchFilter | undefined;

  /**
   * Thing counter to start query the next page from
   */
  from = 0;

  /**
   * Container for queried things
   */
  items: Promise<SCThing[]>;

  /**
   * Page size of queries
   */
  pageSize = 30;

  /**
   * Search value from search bar
   */
  queryText: string;

  /**
   * Subject to handle search text changes
   */
  queryTextChanged: Subject<string> = new Subject<string>();

  /**
   * Time to wait for search query if search text is changing
   */
  searchQueryDueTime = 1000;

  /**
   * Api query sorting
   */
  sortQuery: SCSearchSort | undefined;

  /**
   * Array of all subscriptions to Observables
   */
  subscriptions: Subscription[] = [];

  /**
   *
   * @param alertController AlertController
   * @param dataProvider DataProvider
   * @param contextMenuService ContextMenuService
   * @param settingsProvider SettingsProvider
   * @param logger An angular logger
   */
  constructor(
    protected readonly alertController: AlertController,
    protected dataProvider: DataProvider,
    protected readonly contextMenuService: ContextMenuService,
    protected readonly settingsProvider: SettingsProvider,
    protected readonly logger: NGXLogger,
  ) {
    this.subscriptions.push(this.queryTextChanged
      .pipe(
        debounceTime(this.searchQueryDueTime),
        distinctUntilChanged())
      .subscribe((model) => {
        this.from = 0;
        this.queryText = model;
        this.fetchAndUpdateItems();
      }));

    this.subscriptions.push(this.contextMenuService.filterQueryChanged$.subscribe((query) => {
      this.filterQuery = query;
      this.from = 0;
      this.fetchAndUpdateItems();
    }));
    this.subscriptions.push(this.contextMenuService.sortQueryChanged$.subscribe((query) => {
      this.sortQuery = query;
      this.from = 0;
      this.fetchAndUpdateItems();
    }));

    this.fetchAndUpdateItems();

    /**
     * Subscribe to 'settings.changed' events
     */
    this.subscriptions.push(this.settingsProvider.settingsActionChanged$.subscribe(({type, payload}) => {
        if (type === 'stapps.settings.changed') {
          const {category, name, value} = payload!;
          this.logger.log(`received event "settings.changed" with category:
            ${category}, name: ${name}, value: ${JSON.stringify(value)}`);
        }
      },
    ));
  }

  /**
   * Fetches items with set query configuration
   *
   * @param append If true fetched data gets appended to existing, override otherwise (default false)
   */
  protected async fetchAndUpdateItems(append = false): Promise<void> {
    // build query search options
    const searchOptions: SCSearchQuery = {
      from: this.from,
      size: this.pageSize,
    };

    if (this.queryText && this.queryText.length > 0) {
      // add query string
      searchOptions.query = this.queryText;
    }

    if (this.filterQuery) {
      // add query filtering
      searchOptions.filter = this.filterQuery;
    }

    if (this.sortQuery) {
      // add query sorting
      searchOptions.sort = [this.sortQuery];
    }

    return this.dataProvider.search(searchOptions)
      .then(async (result) => {
        if (append) {
          let items = await this.items;
          // append results
          items = items.concat(result.data);
          this.items = (async () => items)();
        } else {
          // override items with results
          this.items = (async () => {
            this.updateContextFilter(result.facets);
            return result.data} )();
        }
      }, async (err) => {
        const alert: HTMLIonAlertElement = await this.alertController.create({
          buttons: ['Dismiss'],
          header: 'Error',
          subHeader: err.message,
        });

        await alert.present();
      });
  }

  /**
   * Loads next page of things
   */
  // tslint:disable-next-line:no-any
  async loadMore(event: any): Promise<void> {
    this.from += this.pageSize;
    await this.fetchAndUpdateItems(true);
    event.target.complete();
  }

  /**
   * Initialises the possible sort options in ContextMenuService
   */
  ngOnInit(): void {
    this.contextMenuService.setContextSort({
      name: 'sort',
      reversed: false,
      value: 'relevance',
      values: [
        {
          reversible: false,
          value: 'relevance',
        },
        {
          reversible: true,
          value: 'name',
        },
        {
          reversible: true,
          value: 'type',
        },
      ],
    });
  }

  /**
   * Unsubscribe from Observables
   */
  ngOnDestroy() {
    for (let sub of this.subscriptions) {
      sub.unsubscribe();
    }
  }

  /**
   * Search event of search bar
   */
  searchStringChanged(queryValue: string) {
    this.queryTextChanged.next(queryValue);
  }

  /**
   * Updates the possible filter options in ContextMenuService with facets
   */
  updateContextFilter(facets: SCFacet[]) {
    this.contextMenuService.updateContextFilter(facets);
  }
}

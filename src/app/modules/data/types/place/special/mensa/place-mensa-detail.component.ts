/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import moment, {Moment} from 'moment';

import {AfterViewInit, ChangeDetectorRef, Component, Input} from '@angular/core';
import {SCDish, SCPlace, SCThing, SCThingTranslator, SCTranslations} from '@openstapps/core';
import {PlaceMensaService} from './place-mensa-service';

/**
 * TODO
 */
@Component({
  providers: [PlaceMensaService],
  selector: 'stapps-place-mensa-detail-content',
  templateUrl: 'place-mensa.html',
})
export class PlaceMensaDetailComponent implements AfterViewInit {

  /**
   * TODO
   */
  dishes: Promise<Record<string, SCDish[]>> | null = null;

  /**
   * number of days to display mensa menus for
   */
  displayRange = 5;

  /**
   * TODO
   */
  @Input() item: SCPlace;

  /**
   * TODO
   */
  @Input() language: keyof SCTranslations<SCThing>;

  /**
   * TODO
   */
  selectedDay: string;

  /**
   * First day to display menu items for
   */
  startingDay: Moment;

  /**
   * TODO
   */
  translator: SCThingTranslator;


  constructor(private mensaService: PlaceMensaService, private changeDectectorRef: ChangeDetectorRef) {
    // TODO: translation
    this.translator = new SCThingTranslator(this.language);
    this.startingDay = moment();
    this.startingDay.hour(0);
    this.startingDay.minute(0);
    this.startingDay.millisecond(0);
  }

  /**
   * TODO
   */
  ngAfterViewInit() {
    this.dishes = this.mensaService.getAllDishes(this.item)
      .then((dishesResult) => {
        const out = this.splitDishes(dishesResult, this.item, this.startingDay, this.displayRange);
        for (const key in out) {
          if (out.hasOwnProperty(key)) {
            this.selectedDay = key;
            break;
          }
        }
        this.changeDectectorRef.detectChanges();

        return out;
      });
  }

  /**
   * Splits a list of dishes with availability start and end into multiple lists by day
   */
  // tslint:disable-next-line:prefer-function-over-method
  splitDishes(dishes: SCDish[], place: SCPlace, startingDay: Moment, displayRange: number): Record<string, SCDish[]> {
    const out: Record<string, SCDish[]> = {};

    for (let i = 0; i < displayRange; i++) {
      const nextDay: Moment = moment(startingDay)
        .add(1, 'days');
      const selectedDishes: SCDish[] = [];
      for (const dish of dishes) {
        // go through all offers
        if (dish.offers === undefined) { continue; }
        for (const offer of dish.offers) {
          if (offer.inPlace === undefined || offer.inPlace.uid !== place.uid) { continue; }
          // get availabilities
          const availabilityStarts = offer.availabilityStarts === undefined ? undefined : moment(offer.availabilityStarts);
          const availabilityEnds = offer.availabilityEnds === undefined ? undefined : moment(offer.availabilityEnds);

          if ((availabilityStarts === undefined || availabilityStarts.isBefore(nextDay))
            && (availabilityEnds === undefined || availabilityEnds.isAfter(startingDay))) {
            selectedDishes.push(dish);
            break;
          }
        }
      }
      if (selectedDishes.length) {
        out[startingDay.format('L')] = selectedDishes;
      }
      // tslint:disable-next-line:no-parameter-reassignment
      startingDay = nextDay;
    }

    return out;
  }
}

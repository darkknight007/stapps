/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Injectable} from '@angular/core';
import {SCDish, SCPlace, SCThingType} from '@openstapps/core';
import {DataProvider} from '../../../../data.provider';

/**
 * TODO
 */
@Injectable({
  providedIn: 'root',
})
export class PlaceMensaService {
  constructor(private dataProvider: DataProvider) { }

  /**
   * Fetches all dishes for this building
   */
  async getAllDishes(place: SCPlace): Promise<SCDish[]> {
    // use filter to get all dishes with the building's uid in one of the offer's inPlace field
    // TODO: make sure this actually works with ES
    const result = await this.dataProvider.search({
      filter: {
        arguments: {
          filters: [
            {
              arguments: {
                field: 'offers.inPlace.uid',
                value: place.uid,
              },
              type: 'value',
            },
            {
              arguments: {
                field: 'type',
                value: SCThingType.Dish,
              },
              type: 'value',
            },
          ],
          operation: 'and',
        },
        type: 'boolean',
      },
      size: 1000,
    });

    return result.data as SCDish[];
  }
}

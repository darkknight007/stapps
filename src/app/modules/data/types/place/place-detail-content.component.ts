/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCBuilding, SCFloor, SCPointOfInterest, SCRoom, SCThing, SCTranslations} from '@openstapps/core';
import {SCThings, SCThingTranslator} from '@openstapps/core';
import {DataProvider} from '../../data.provider';

/**
 * TODO
 */
@Component({
  providers: [ DataProvider ],
  selector: 'stapps-place-detail-content',
  templateUrl: 'place-detail-content.html',
})
export class PlaceDetailContentComponent {
  /**
   * TODO
   */
  @Input() item: SCBuilding | SCRoom | SCPointOfInterest | SCFloor;
  /**
   * TODO
   */
  @Input() language: keyof SCTranslations<SCThing>;
  /**
   * TODO
   */
  objectKeys = Object.keys;
  /**
   * TODO
   */
  translator: SCThingTranslator;

  /**
   * TODO
   */
  constructor() {
    this.translator = new SCThingTranslator(this.language);
  }

  /**
   * TODO
   *
   * @param item TODO
   */
  // tslint:disable-next-line:completed-docs prefer-function-over-method
  hasCategories(item: SCThings): item is SCThings & { categories: string[]; } {
    // tslint:disable-next-line:completed-docs
    return typeof (item as { categories: string[]; }).categories !== 'undefined';
  }

  /**
   * Helper function as 'typeof' is not accessible in HTML
   *
   * @param item TODO
   */
  isMensaThing(item: SCThings): boolean  {
    return this.hasCategories(item) &&
      ((item.categories as string[]).includes('canteen') || (item.categories as string[]).includes('cafe')
      || (item.categories as string[]).includes('student canteen') || (item.categories as string[]).includes('restaurant'));
  }
}

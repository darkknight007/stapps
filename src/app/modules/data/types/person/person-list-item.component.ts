/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCPerson} from '@openstapps/core';
import {DataListItem} from '../../list/data-list-item.component';

/**
 * TODO
 */
@Component({
  selector: 'stapps-person-list-item',
  templateUrl: 'person-list-item.html',
})

export class PersonListItem extends DataListItem {
  /**
   * TODO
   */
  @Input() item: SCPerson;

  /**
   * TODO
   */
  // tslint:disable-next-line:prefer-function-over-method
  ngOnInit() {
    // TODO: translation
  }
}

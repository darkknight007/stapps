/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input} from '@angular/core';
import {SCCatalog, SCThing, SCTranslations} from '@openstapps/core';
import {SCThingTranslator} from '@openstapps/core';

/**
 * TODO
 */
@Component({
  selector: 'stapps-catalog-detail-content',
  templateUrl: 'catalog-detail-content.html',
})
export class CatalogDetailContentComponent {
  /**
   * TODO
   */
  @Input() item: SCCatalog;
  /**
   * TODO
   */
  @Input() language: keyof SCTranslations<SCThing>;
  /**
   * TODO
   */
  objectKeys = Object.keys;
  /**
   * TODO
   */
  translator: SCThingTranslator;
  /**
   * TODO
   */
  constructor() {
    this.translator = new SCThingTranslator(this.language);
  }
}

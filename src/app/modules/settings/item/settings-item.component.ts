/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component, Input, OnInit} from '@angular/core';
import {AlertController} from '@ionic/angular';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {
  SCLanguage,
  SCLanguageCode,
  SCSetting,
  SCSettingValue,
  SCSettingValues,
  SCThingTranslator,
  SCTranslations,
} from '@openstapps/core';
import {SettingsProvider} from '../settings.provider';

/**
 * Setting item component
 */
@Component({
  selector: 'stapps-settings-item',
  templateUrl: 'settings-item.html',
})
export class SettingsItemComponent implements OnInit {

  /**
   * If set the setting will be shown as compact view
   */
  @Input() compactView = false;

  /**
   * Flag for workaround for selected 'select option' not updating translation
   */
  isVisible = true;
  /**
   * current language
   */
  language: keyof SCTranslations<SCLanguage>;
  /**
   * The setting to handle
   */
  @Input() setting: SCSetting;

  /**
   * Translated setting from SCThingTranslator
   */
  // tslint:disable-next-line:no-any
  translatedSetting: any;

  /**
   * Core translator
   */
  translator: SCThingTranslator;

  /**
   *
   * @param alertCtrl AlertController
   * @param translateService TranslateService
   * @param settingsProvider SettingProvider
   */
  constructor(private readonly alertCtrl: AlertController,
              private readonly translateService: TranslateService,
              private readonly settingsProvider: SettingsProvider) {
    this.language = translateService.currentLang as keyof SCTranslations<SCLanguage>;
    this.translator = new SCThingTranslator(this.language);

    translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.isVisible = false;
      this.language = event.lang as keyof SCTranslations<SCLanguage>;
      this.translator.language = this.language;
      // TODO: Issue #53 check workaround for selected 'select option' not updating translation
      setTimeout(() => this.isVisible = true);
    });
  }

  /**
   * Checks for user permission to use location,
   * if no permission is granted, setting is set to false and an alert is presented to the user
   */
  private async checkGeoLocationPermission() {
    const permissionGranted = await this.settingsProvider.checkGeoLocationPermission();
    if (!permissionGranted) {
      // revert setting value
      this.setting.value = false;
      await this.presentGeoLocationAlert();
    }
  }

  /**
   * Shows alert with error message on denied user permission or disabled location services
   */
  private async presentGeoLocationAlert() {
    const title = await this.translateService.get('settings.geoLocation.permission_denied_title')
      .toPromise();
    const message = await this.translateService.get('settings.geoLocation.permission_denied_message')
      .toPromise();
    await this.presentAlert(title, message);
  }

  /**
   * NgInit
   */
  ngOnInit(): void {
    this.translatedSetting = this.translator.translate(this.setting);
  }

  /**
   * Shows alert with given title and message and a 'ok' button
   *
   * @param title Title of the alert
   * @param message Message of the alert
   */
  async presentAlert(title: string, message: string) {
    const alert = await this.alertCtrl.create({
      buttons: ['OK'],
      header: title,
      message: message,
    });
    await alert.present();
  }

  /**
   * Handles value changes of the setting
   */
  async settingChanged(): Promise<void> {
    if (typeof this.setting.value !== 'undefined'
      && SettingsProvider.validateValue(this.setting, this.setting.value)) {
      // handle general settings, with special actions
      switch (this.setting.name) {
        case 'language':
          this.translateService.use(this.setting.value as SCLanguageCode);
          break;
        case 'geoLocation':
          if (!!this.setting.value) {
            await this.checkGeoLocationPermission();
          }
          break;
        default:
      }
      await this.settingsProvider
        .setSettingValue(this.setting.categories[0],
          this.setting.name,
          this.setting.value);
    } else {
      // reset setting
      this.setting.value =
        await this.settingsProvider
          .getValue(this.setting.categories[0], this.setting.name) as (SCSettingValue | SCSettingValues);
    }
  }

  /**
   * Mapping of typeOf for Html usage
   */
  // tslint:disable-next-line:prefer-function-over-method
  typeOf(val: unknown) {
    return typeof (val);
  }
}

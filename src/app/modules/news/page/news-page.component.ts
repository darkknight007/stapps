/*
 * Copyright (C) 2020-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {IonRefresher} from '@ionic/angular';
import {SCMessage} from '@openstapps/core';
import {NewsProvider} from '../news.provider';
/**
 * News page component
 */
@Component({
  selector: 'stapps-news-page',
  templateUrl: 'news-page.html',
})
export class NewsPageComponent {
  /**
   * News (messages) to show
   */
  news: SCMessage[] = [];

  constructor(private newsProvider: NewsProvider) {
  }

  /**
   * Fetch news from the backend
   */
  async fetchNews() {
    /* tslint:disable:no-magic-numbers */
    this.news = await this.newsProvider.getList(30, true);
  }

  /**
   * Initialize the local variables on component initialization
   */
  ngOnInit() {
    void this.fetchNews();
  }

  /**
   * Updates the shown list
   *
   * @param refresher Refresher component that triggers the update
   */
  async refresh(refresher: IonRefresher) {
    try {
      await this.fetchNews();
    } catch (e) {
      this.news = [];
    } finally {
      await refresher.complete();
    }
  }
}

/*
 * Copyright (C) 2020-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';
import {MomentModule} from 'ngx-moment';
import {DataModule} from '../data/data.module';
import {SettingsProvider} from '../settings/settings.provider';
import {NewsPageComponent} from './page/news-page.component';
import {SkeletonNewsItem} from './page/skeleton-news-item.component';
import {NewsItemComponent} from './page/news-item.component';

const newsRoutes: Routes = [
  {path: 'news', component: NewsPageComponent},
];

/**
 * News Module
 */
@NgModule({
  declarations: [
    NewsPageComponent,
    SkeletonNewsItem,
    NewsItemComponent,
  ],
  imports: [
    IonicModule.forRoot(),
    TranslateModule.forChild(),
    RouterModule.forChild(newsRoutes),
    CommonModule,
    MomentModule,
    DataModule,
  ],
  providers: [
    SettingsProvider,
  ],
})
export class NewsModule {}

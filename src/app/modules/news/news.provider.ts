/*
 * Copyright (C) 2020-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Injectable} from '@angular/core';
import {SCMessage} from '@openstapps/core';
import {DataProvider} from '../data/data.provider';
/**
 * Service for providing news messages
 */
@Injectable({
  providedIn: 'root',
})
export class NewsProvider {
  constructor(private dataProvider: DataProvider) {
  }

  /**
   * Get news messages
   * TODO: make dates sortable on the backend side and then adjust this method
   * @param size How many messages/news to fetch
   * @param sort If sort by date needs to be performed
   */
  async getList(size: number, sort?: boolean): Promise<SCMessage[]> {
    const result = await this.dataProvider.search({
      filter: {
        type: 'value',
        arguments: {
          field: 'type',
          value: 'message',
        },
      },
      size: size,
    });

    const news = result.data as SCMessage[];

    if (sort) {
      news.sort((a, b) => {
        if (typeof a.datePublished !== 'undefined' && typeof b.datePublished !== 'undefined') {
          return (a.datePublished > b.datePublished) ? -1 : ((a.datePublished < b.datePublished) ? 1 : 0);
        }

        return 0;
      });
    }

    return news;
  }
}

import {Component,OnInit,ViewChild} from '@angular/core';
import {CatalogService} from './catalog.service';
import {IonInfiniteScroll,PopoverController} from '@ionic/angular';
import {FilterListComponent} from './CatalogContent/filter-list/filter-list.component';
import {DisplayDetailComponent} from './CatalogContent/display-detail/display-detail.component';
import {ModalController} from '@ionic/angular';
import {HDSCatalog} from './hdscatalog-response';


@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
  providers:[ CatalogService]
})


export class CatalogComponent implements OnInit {  
  
  query:string="";      
  pageCounter:number=1;
  catalogResponse: any;
  response:any;
  totalNumberOfHits:number;
  hits:any[];
  //popoverController:PopoverController;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
 
  
  constructor(private catalogService: CatalogService, private popoverController:PopoverController, private modalController: ModalController) {
    
 
  }
  ngOnInit(){
    console.clear();

  }
  
 
  async presentModal(hit:HDSCatalog) {
    
    const modal = await this.modalController.create({
      component: DisplayDetailComponent,
      cssClass: 'fullscreen',
      componentProps: { 
        ppn:hit['@id'].value,
        name: hit.name,
        description: hit.description,
        author:hit.author,
        type:hit['@type'],
        sameAs:hit.sameAs,
        categories:hit.categories,
        bookEdition:hit.bookEdition,
        isPartOfName:hit.isPartOf?.name,
        isPartOfType:hit.isPartOf?.['@type'],
        isPartOfVolumNumber:hit.isPartOf?.volumeNumber,
        isbn:(hit.isbn)?String(hit.isbn).split(','):hit.isbn,
        issn:hit.issn,
        sourceOrganization: hit.sourceOrganization,
        publiicationType:hit.publication?.['@type'],
        publishedByType:hit.publication?.publishedBy?.['@type'],
        publishedByName:hit.publication?.publishedBy?.name,
        publishedByLocation:hit.publication?.publishedBy?.location,
        startDate:hit.publication?.startDate,

      },
      showBackdrop:true
    });
    return await modal.present();
  }

 

  
  loadData(event:any) {
    ++this.pageCounter;
    console.log(this.pageCounter);
    setTimeout(() => {
      console.log('Done');
      event.target.complete();
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      const params= this.getRequestParam(this.query,this.pageCounter);
      console.log('parameters'+params);
      //this.getCatalog();
      this.catalogService.getCatalog(params).subscribe(
        response => {
          const {hits,totalNumberOfHits} = response.response;
          for(let i=0; i<hits.length;i++){
            this.hits.push(hits[i]);
          }
          
          this.totalNumberOfHits = totalNumberOfHits;
          console.log(hits);
        },
        error => {
          console.log(error);
        }
      );
      
      //let data=this.catalogResponse;
      if (this.hits.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  // toggleInfiniteScroll() {
   // this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  //}

  

  getRequestParam(query:string,page?:number,pageSize?:number){

    let params:any={};
    if(query){
      // to do check if small letter
      //(query.includes('heb'))?query.replace('heb','HEB'):query;
      // digits then x need regex
     // (query.includes('x'))?query.replace('x','X'):query;

      params['query']=query;
    } else{
      params['query']='Rabie';
    }
    if(page){
      params['page']=page;
    }
    if(pageSize){
      params['pageSize']=pageSize;
    }
    return params;

  }
  getCatalog():void{
    //set counter value to initial value 1
    if(this.pageCounter==0){
    this.pageCounter=1;
                           }
    console.log("counter init"+this.pageCounter);
    const params= this.getRequestParam(this.query);
    console.log('parameters'+params);
    this.catalogService.getCatalog(params).subscribe(
      response => {
        const {hits,totalNumberOfHits} = response.response;
        this.hits = hits;
        this.totalNumberOfHits = totalNumberOfHits;
        console.log(hits);
      },
      error => {
        console.log(error);
      }
    );
    // to do increase 
    this.pageCounter+=1;
  }
  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: FilterListComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
    

}

export interface HDSCatalogResponse {
     /**
    * Response Array of SCBook type or Object
    */
   'hits': HDSCatalog[];
   /**
     * Number of hits coming from HDS
     */
     'totalNumberOfHits': number;
 }
 /**
  *  Enum Type of HDS
  */
 export enum HDSType{
   Article = 'Article',
   Book = 'Book',
   Periodical = 'Periodical',
 }
 /**
  * HDSCatalog
  */
 export interface HDSCatalog{
   '@id': {
     /**
      * The type of the resource
      */
     '@type': string;
     /**
      * name PPN
      */
     'name': string;
     /**
      * value PPN example "value":  "HEB198305427" 
      */
     'value': string;
   };
   /**
    * HDS URL example https://hds2test.hebis.de/ubffm/Record/HEB359140017
    */
   'sameAs'?: string;
   /**
    * Periodical , Book, Article 
    */
   '@type'?: HDSType; // TODO: We should complete the list.... (or use enum type)
   /**
    * The category of the resource example ebook
    */
   'categories'?: string;
   /**
    * name
    */
   'name'?: string;
   /**
    * author
    */
   'author'?: string;
   /**
    * description
    */
   'description'?: string;
     /**
    * isPartOf need to be url
    */
   'isPartOf'?: {
    '@type'?: string;
    'name'?: string;
    'volumeNumber'?: string;
}
     /**
    * Book edition not in periodical
    */
   'bookEdition'?:string;
    /**
    * ISBN
    */
   'isbn'?: string[];
      /**
    * issn not isbn
    */
   'issn'?:string;
   /**
    * sourceOrganization in all three but not provided
    */
   'sourceOrganization'?:string;
   /**
    * Publication
    */
   'publication'?: {
     /**
      * Type
      */
     '@type'?: string;
     /**
      * Published
      */
     'publishedBy'?: {
       /**
        * Organization
        */
       '@type'?: string;
       /**
        * location
        */
       'location'?: string[];
       /**
        * name
        */
       'name'?: string;
     };
     /**
      * Start Date
      */
     'startDate'?: string;
   };
 }
 

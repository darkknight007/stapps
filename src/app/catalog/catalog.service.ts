import {Injectable} from '@angular/core';
import $ from 'jquery';
import {Observable} from 'rxjs';
import {HDSCatalogRequest} from './hdscatalog-request';
import { ajaxPost } from 'rxjs/internal/observable/dom/AjaxObservable';


const catalogUrl='/catalogContent/';

@Injectable({
  providedIn: 'root'
})
export class CatalogService{
   
  
 
 getCatalog(params:HDSCatalogRequest):Observable<any>{

   // request parameters 
  const settings = {
    'url': catalogUrl,
    'method': 'POST',
    'timeout': 0,
    'headers': {
      'Content-Type': 'application/json'
    },
    'data': JSON.stringify({'query':params.query,'page':params.page}),
    'async':false,
    'success':function(){      
      // URl table to load data onclick
     const currentUrl= $(location).attr('href')+' #ionList';
    $('#searchBtn').click(function(){
      // Load content
      $('#ionList').load(currentUrl);
      // Load total number of content
      $('#totalNumber').load(currentUrl);
    });      
    
  
   
    }
  }; 
  
  /* $.ajax(settings).done(function (response) {
  
  catalogData=response;
     

  }); */
  const catalogDataResponse=ajaxPost(settings.url,settings.data,settings.headers);

  return catalogDataResponse;
                                                                                                                        
  }
}

export interface HDSCatalogRequest {

        /**
         * A boolean optional variable `moreResult` is added to retrieve more results in one request to HDS.
         * Here it is not normally used with the variable page number `page`, 
         * because it starts with a counter from the first page and continue to a limit const `requestLimit`, 
         * which is the last page number, which can be set manually as default(4) 
         * and that determine the number of requests in one response to APP (stapps).
         */
          moreResult?: boolean;
        /**
         * page variable can be without value
         * in the default request can the page number sent `page` to load a specified page.
         */
          page?: number;
        /**
         * search word from HDS
        */     
          query: string;
    }
    


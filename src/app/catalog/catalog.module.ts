import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import { CatalogComponent } from './catalog.component';
import {FilterListComponent} from './CatalogContent/filter-list/filter-list.component';
import {IonicModule} from '@ionic/angular';





@NgModule({
  declarations: [CatalogComponent, FilterListComponent],
  imports: [
    BrowserModule,
    CommonModule,
    IonicModule
  ],
  entryComponents:[]
})
export class CatalogModule { }

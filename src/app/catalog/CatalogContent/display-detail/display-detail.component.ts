import {Component, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {DaiaService} from './daia.service';
import {DaiaResponse} from './daia';


@Component({
  selector: 'app-display-detail',
  templateUrl: './display-detail.component.html',
  styleUrls: ['./display-detail.component.scss'],
  providers:[DaiaService]
})

export class DisplayDetailComponent implements OnInit {
  ppn:string; 
  name:string;
  description:string;
  author:string;
  type:string;
  sameAs:string;
  categories:string;
  bookEdition:string;
  isPartOfName:string;
  isPartOfType:string;
  isPartOfVolumNumber:string;
  isbn:string[];
  issn:string;
  sourceOrganization: string;
  publiicationType:string;
  publishedByType:string;
  publishedByName:string;
  publishedByLocation:string;
  startDate:string;
  daiaResponse:DaiaResponse;
  isAvailable:boolean;
  service:DaiaService;
  loan:string;
  presentation:string;
  university:string;
  interval:any;
  daiServiceCalled:boolean;
 
  
  

  constructor(private modalController: ModalController, private daiaService:DaiaService) {
    
   }

  ngOnInit() {
    this.daiServiceCalled=false;
    console.log('Daia service call init '+this.daiServiceCalled);
    let params={'ppn':this.ppn}
    this.getDaiaInfo(params);
     
    console.log(`${this.name} ${this.publishedByName}`)
  }
  closeModal() { this.modalController.dismiss(); }
  
  
 getDaiaInfo(params?:{'university'?:string,'ppn':string}){
    console.log(this.ppn);
     this.daiaService.getDaia(params).subscribe(
      response => {
            this.daiaResponse=response;
        console.log(this.daiaResponse);
        this.presentation=this.daiaResponse.document[0].item[0].available[0].service;  
          // to test setting available change status
        if(this.presentation=='presentation'){
          this.isAvailable=true;             
       }else{
         this.isAvailable=false;
       } 
       this.daiServiceCalled=true;
        console.log(this.isAvailable);        
        console.log(this.daiaResponse.document[0].item[0].available[0].service);
        console.log('Daia service called '+this.daiServiceCalled);    
        
      },
      error => {
        console.log(error);
      }
    );
       
    
  }
/* async setUniversity(value:any){
    this.university=value;
    //this.isAvailable=false; 
    const params={'university':this.university,'ppn':this.ppn}
    
    this.getDaiaInfo(params);
    this.setAvailable(false);
    
  } */
}

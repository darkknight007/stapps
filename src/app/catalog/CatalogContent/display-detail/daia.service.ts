import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

const daiaUrl='https://daia.hebis.de/DAIA2/UB_';
let university:string;
// const daiaUrl='./assets/frankfurtDaiaTestData.json';
@Injectable({
  providedIn: 'root'
})

export class DaiaService {

  constructor(private http: HttpClient) { }

  getDaia(params?:{'university'?:string,'ppn':string}):Observable<any>{
     console.log(params);
    //params=JSON.stringify(params);
   
    switch (params?.university) {
      case 'frankfurt':
        university='Frankfurt';
        break;
      case 'kassel':
        university='Kassel';
          break;
      case 'giessen':
        university='Giessen';
        break;
      case 'marburg':
        university='Marburg';
          break;  
      default:
        university='Frankfurt';
        break;
    }
    console.log(daiaUrl+university+'?'+'id='+params?.ppn);
    // console.log(daiaUrl+params?.ppn);
    // const response=this.http.get(daiaUrl+params?.ppn);
    const response=this.http.get(daiaUrl+university+'?'+'id='+params?.ppn);
    
  return response;
    
  }
}

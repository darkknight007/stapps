import { TestBed } from '@angular/core/testing';

import { DaiaService } from './daia.service';

describe('DaiaService', () => {
  let service: DaiaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DaiaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

export interface DaiaResponse {
    /**
   * Response Array of Document type or Object
   */
  'document': [{
    'item': [
      {
        'label': string;
        'department': {
          'id': string;
          'href': string;
          'content': string;
        },
        'available': [
          {
            'delay'?: string;
            'service': DaiaService;
            'href'?: string;
          }
        ],
        'unavailable': [
          {  
            'delay'?: string;
            'service': DaiaService;
            'href'?: string;
          }
        ],
        'debugInfo': string;
        'id': string;
      }
    ],
    'id': string;
    'href':string;
  
  }];
  'institution': {
    'id': string;
    'href': string;
    'content': string;
  };
  'timestamp': string;

  
}
/**
 *  Enum Type of Daia Service
 */
export enum DaiaService{
  Presentation = 'presentation',
  Loan = 'loan',
  Interloan = 'interloan',
  Remote='remote',
  Openaccess='openaccess'
}


import { Component, OnInit } from '@angular/core';
import $ from 'jquery';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.scss'],
})
export class FilterListComponent implements OnInit {

  constructor() { }

  ngOnInit() {}
  sortItemContent(){
    console.log("Sorting");
    let itemList=$('#ionList');
    // to do each item change it to html part like html1 html2 then append them in order
    let html= itemList.html();
    // let txt= itemList.text();
    let itemArr=$.makeArray(itemList.children('.itemContent'));
    console.log(itemArr);
    itemArr.sort( (a,b)=>{
     let textA= +$(a).find('.startDate').text();
     console.log('text'+textA);
     let textB= +$(b).find('.startDate').text();
     console.log('text'+textB);
     if(textA< textB)
       return 1;
     if(textA> textB) return -1;
     return 0;
    });
    console.log("sorted"+itemArr);
     // URl table to load data onclick
     const currentUrl= $(location).attr('href')+' #ionList';
      // Load content
      $('#ionList').load(currentUrl);
     
     //itemList.empty();
     console.log('empty');
    itemList.append(html);
     $.each(itemArr,()=>{

    
      console.log("append: "+ itemArr);
     });
    
  }
}
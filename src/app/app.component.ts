/*
 * Copyright (C) 2018-2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Component} from '@angular/core';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Platform} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {NGXLogger} from 'ngx-logger';
import {ConfigProvider} from './modules/config/config.provider';
import {SettingsProvider} from './modules/settings/settings.provider';

import {initLogger} from './_helpers/ts-logger';
/**
 * TODO
 */
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent {
  /**
   * TODO
   */
  pages: Array<{
    /**
     * TODO
     */
    component: unknown;
    /**
     * TODO
     */
    title: string;
  }>;

  /**
   *
   * @param platform TODO
   * @param statusBar TODO
   * @param splashScreen TODO
   * @param translateService TODO
   * @param settingsProvider TODO
   * @param configProvider TODO
   * @param logger An angular logger
   */
  constructor(private readonly platform: Platform,
              private readonly statusBar: StatusBar,
              private readonly splashScreen: SplashScreen,
              private readonly translateService: TranslateService,
              private readonly settingsProvider: SettingsProvider,
              private readonly configProvider: ConfigProvider,
              private readonly logger: NGXLogger) {
    initLogger(logger);
    this.initializeApp();
      // clear console
      console.clear();                                                                                                  
    // this language will be used as a fallback when a translation isn't found in the current language
    translateService.setDefaultLang('en');
  }

  /**
   * TODO
   */
  initializeApp() {
    this.platform.ready()
      .then(async () => {
        // Okay, so the platform is ready and our plugins are available.
        // Here you can do any higher level native things you might need.
        this.statusBar.styleDefault();
        this.splashScreen.hide();

        // initialise the configProvider
        try {
          await this.configProvider.init();
        } catch (error) {
          if (typeof error.name !== 'undefined') {
            if (error.name === 'ConfigInitError') {
              // @TODO: Issue #43 handle initialisation error and inform user
            }
          }
          this.logger.error(error);
        }

        // set order of categories in settings
        this.settingsProvider.setCategoriesOrder([
          'profile',
          'privacy',
          'credentials',
          'others',
        ]);

        try {
          // set language from settings
          const languageCode = (await this.settingsProvider.getValue('profile', 'language')) as string;
          this.translateService.use(languageCode);
        } catch (error) {
          this.logger.warn(error);
        }
      });
  }
}
